#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

cd /opt/MonsterFrontend/lib
./node_modules/grunt-cli/bin/grunt -v --force
