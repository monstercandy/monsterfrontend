fs = require('fs');
var sourceMap = require('source-map');
var args = process.argv.slice(2);
if(args.length != 3) {
  console.error("Usage: node source-map.js path/to/mapfile.map line column");
  process.reallyExit();
}
var smc = new sourceMap.SourceMapConsumer(fs.readFileSync(args[0],"utf8"));
console.log(smc.originalPositionFor({line: parseInt(args[1]), column: parseInt(args[2])}));
