<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>Tapi via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

    <a href mc-sref="mc-webhosting-php-selector" mc-sref-params="{server:'s1',whId:'12345'}">PHP version</a>  
    <a href mc-sref="mc-webhosting-security" mc-sref-params="{server:'s1',whId:'12345'}">security settings</a>  
    <a href mc-sref="mc-webhosting-restore" mc-sref-params="{server:'s1',whId:'12347'}">restore webhosting backup</a>  
    <a href mc-sref="mc-webhosting-list" >list of webhosting packages</a>  

    <div mc-view>
    </div>

    <ul>
      <li mc-szar-link-web server="s1" wh-id="12345"></li>
    </ul>

    <table>
    <tr mc-webhosting-renamer-row server="s1" wh-id="12345" extra-call="http://monster/webhosting-rename.php" ></tr>
    <tr mc-webhosting-script-watcher-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-quota-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-mailpolicy-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-php-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-dbms-databases-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-dbms-sizes-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-ftp-accounts-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-email-accounts-row server="s1" wh-id="12345" ></tr>
    <tr mc-webhosting-email-aliases-row server="s1" wh-id="12345" ></tr>
    </table>


  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
