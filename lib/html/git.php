<!DOCTYPE html>
<html lang="hu" ng-app="app">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div >

     <a href mc-sref="mc-git-ssh-settings" mc-sref-params="{server:'s1',whId:'12345'}">git settings</a>  

     <a href mc-sref="mc-git-main" mc-sref-params="{server:'s1',whId:'12345'}">git main</a>  

    <div mc-view>

    </div>

     
  </div>

  <?include("js.mc/loader.php");?>


    
</body>
</html>
