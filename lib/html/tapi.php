<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>Tapi via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

      <table>
      <tr mc-tapi-dns-zone-on-off-row create-zone-allowed="1" domain="femforgacs.hu"></tr>
      </table>
      <ul>
      <li mc-tapi-dns-edit-link  domain="femforgacs.hu" link="/index.php?op=da-ng-dnsadmin&domain=1234&uidi=0"></li>
      <li mc-tapi-dns-edit-link2 template="kyle.foobar.hu" domain="femforgacs.hu" external-registrar="false" display="fémforgács.hu" domain="femforgacs.hu" domain-id="1234"></li>
      </ul>

    <div mc-view></div>

    <a href mc-sref="mc-dns-admin" mc-sref-params="{'external-registrar':'false','display':'fémforgács.hu','domain':'femforgacs.hu','domainId':'1234'}">dns-admin</a>


  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
