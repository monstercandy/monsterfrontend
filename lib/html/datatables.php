<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>Tapi via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>  
  <div class="container">
  
  <select class="form-control"><option>xxx</option></select>

  <table datatable="" class="table table-striped table-hover  row-border">
    <thead>
    <tr>
        <th>ID</th>
        <th lng>First name</th>
        <th lng>Last name</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>

    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>

    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>
	
    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>

    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>

    <tr>
        <td>1</td>
        <td>Foo</td>
        <td>Bar</td>
    </tr>
    <tr>
        <td>123</td>
        <td>Someone</td>
        <td>Youknow</td>
    </tr>
    <tr>
        <td>987</td>
        <td>Iamout</td>
        <td>Ofinspiration</td>
    </tr>
	
    </tbody>
  </table>

  
  
  <div  ng-controller="x">
  <button ng-click="add()">Add</button>
  <table datatable="ng"  width="100%" class="table table-striped table-hover  row-border">
    <thead>
    <tr>
        <th>ID</th>
        <th lng>First name</th>
        <th lng>Last name</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <tr ng-repeat="person in persons" >
        <td>{{::person.id}}</td>
        <td>{{::person.first}}</td>
        <td>{{::person.last}}</td>
          <td>
            <button class="btn btn-danger" confirm ng-click="delete(rule)" lng>Delete</button>    
          </td>

    </tr>

	
    </tbody>
  </table>

<!--
  <div class="table-responsive">
  </div>
-->

  </div>

  </div>

  
  <?include("js.mc/loader.php");?>

  
  <script>
  app.controller("x", function($scope, $timeout){
	  $scope.persons = [];
	  
      $scope.add = function(){
		  $scope.persons.push(
			 {id: 1, first: "First", last: "Last"}
		  );
		  
	  }
	  
	  $timeout($scope.add, 5000);
	  
  });
  </script>

    
</body>
</html>
