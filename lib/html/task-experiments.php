<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  
  <div ng-controller="test">
	      <h4>Default</h4>
		  
		  <textarea show-tail class="form-control"  rows="20">{{text}}</textarea>
		  
		  <button ng-click="read1()">xhr</button>
		  <button ng-click="read2()">$http</button>

          <button ng-click="localTask()">$task (local php)</button>

		  <button ng-click="task(1)">$task 1</button>
		  <button ng-click="task(2)">$task 2</button>
		  <button ng-click="task(3)">$task 3</button>

  </div>

  <?include("js.mc/loader.php");?>
  
  <script>
  
  app.controller("test", ["$scope","$http","$timeout", "$task", function($scope, $http, $timeout, $task){
    $scope.text = ""
    $scope.task = function(command_id){
        $task({title:"title hello "+command_id, noUriTransform: true, "url":"http://localhost:18791/commands/"+command_id}) //

          .then(function(d){
          	  console.log("just closed", d)
          })
    }

    $scope.localTask = function(command_id){
        $task({title:"local", "url":"/chunked.php", noUriTransform: true, taskMaxNumberOfBytesToReadBeforeRewrap: 50})
          .then(function(d){
          	  console.log("just closed", d)
          })
    }

    $scope.read1 = function(){
 	    var xhr = new XMLHttpRequest()
		xhr.open("GET", "/chunked.php", true)
		xhr.onprogress = function () {
		  // console.log("PROGRESS:", xhr.responseText)
		  addLine(xhr.responseText)
		}
		xhr.send()
	    addLine("sent")
	}
	$scope.read2 = function(){
	    $http({
		  url: "/chunked.php", 
		  method: "GET",
		  eventHandlers: {
		     progress: function(evt) {
			   // console.log("PROGRESS", evt)
			   addLine(evt.target.responseText)
			   
			   // evt.target.abort()
			 }
		  }
		})
		.then(function(d){
		  addLine("finished")
		})
		.catch(function(d){
		  addLine("exception")
		})
	    addLine("sent")
	}
	
	function addLine(msg) {
	    $timeout(function(){
	       $scope.text = msg+"\n"
		}, 0)
	}
}])

  
  </script>

    
</body>
</html>
