<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

     <a href mc-sref="mc-email-domain-list-and-add" mc-sref-params="{server:'s1',whId:'12345'}">email domains</a>  
     <a href mc-sref="mc-email-account-list-and-add" mc-sref-params="{server:'s1',whId:'12345',domain:'xn--femforgacs.hu',display:'fémforgács.hu'}">email accounts</a>  
     <a href mc-sref="mc-email-alias-list-and-add" mc-sref-params="{server:'s1',whId:'12345',domain:'xn--femforgacs.hu',display:'fémforgács.hu'}">email aliases</a>  
     <a href mc-sref="mc-email-bcc-list-and-add" mc-sref-params="{server:'s1',whId:'12345',domain:'xn--femforgacs.hu',display:'fémforgács.hu'}">email bccs</a>  
     <a href mc-sref="mc-email-summary" mc-sref-params="{server:'s1',whId:'12345'}">email summary</a>  

    <div mc-view>

    </div>

            <table>
            <tr mc-email-status-row server="s1"  domain="femforgacs.hu"></tr>
            <tr mc-email-status-row server="s1"  domain="xn--femforgacs.hu"></tr>
            <tr mc-email-status-row server="s1"  domain="non-active.hu"></tr>
            <tr mc-email-status-row server="s1"  domain="non-existent.hu"></tr>
            <tr mc-email-status-row server="s1"  domain="non-existent-xxxxx.hu"></tr>
            <tr mc-email-greylist-status-row server="s1"  domain="femforgacs.hu"></tr>
            </table>




  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
