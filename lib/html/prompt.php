<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  
  <div ng-controller="test">
	      <h4>Default</h4>

		  <button ng-click="ask()">ask</button>
  </div>

  <?include("js.mc/loader.php");?>
  
  <script>
  
  app.controller("test", ["$scope","$prompt", function($scope, $prompt){
    $scope.text = ""

    $scope.ask = function(){
    	$prompt({title: "question", message: "foobar"})
    	  .then(function(d){
    	  	  console.log(d)
    	  	  
    	  })
	}
}])

  
  </script>

    
</body>
</html>
