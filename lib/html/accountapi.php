<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>
  <!-- test account: radimre83@gmail.com:qymyv8x4 -->


    <a href mc-sref="mc-loginlog-and-settings">loginlog</a>

    <a href mc-sref="mc-user-settings">user settings</a>

    <a href mc-sref="mc-edit-account-show-granted-access">grant access</a>
     

     <div mc-view>

         <xxx>
        <div mc-if-role="SUPERUSER">
        	This block is visible only if there is any logged in accounts with superuser permission

        </div>
            <a href="/foobar.php" mc-switch-role="SUPERUSER">switch user to the first one with SUPERUSER role</a>
        </xxx>

           <table>

           <tr>
           	<td>t_awstats_allowed</td>
           	<td><mc-webhosting-template-flag template="WEB10000" param="t_awstats_allowed"></mc-webhosting-template-flag></td>
           </tr>

           <tr>
           	<td>t_storage_max_quota_hard_mb</td>
           	<td><mc-webhosting-template-data template="WEB10000" param="t_storage_max_quota_hard_mb"></mc-webhosting-template-data> MB</td>
           </tr>
           
           </table>

          <mc-login-or-in-progress ></mc-login-or-in-progress> <!-- legacy-integration="true" legacy-integration-uid="1" -->
          <mc-login-or-logout></mc-login-or-logout>
      	  <a href mc-link-logout-all>Logout everywhere</a>

      	  <mc-registration-or-show-account uf-tos="true" redirect-after="index3.php!"></mc-registration-or-show-account>

      	  <mc-forgotten-password-send></mc-forgotten-password-send>


      	  <mc-logged-in-account-list></mc-logged-in-account-list>

     </div>



  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
