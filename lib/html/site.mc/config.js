
window.mcConfiguration = {
       /*
       "indexPageName": "accountapi.php",
       "redirect_after_login": "accountapi.php",
       "forgotten_password_send_uri": "/index.php?op=forgotten-password-send",
       */

       "pureNg": true,

       "certPurchaseServer": "s1",

       "clone_record_support": false,

       "dev_mode": true,

       "customer_service_url": "https://something.hu/",

       "service_primary_domain": "monstermedia.hu",
       "email_webmail_url": "https://some.webmail.url",
       "docker_webapp_examples_url": "https://foo.bar/",


       "default_country_code": "36",
       "api_uri_prefix": "/api",
       "alert_timeout_ms": 10000,
       "callin_code_support": true,
       "registration_extra_email_warning": true,

       "force_email_hostname":"anything.else.tld",

       "registrationTelOptional": true,
       "itemsPerPage": 1,

       "serverUniqueDkim": true,

       "template_directory": "/template.mc",

       "sms_notification_support": true,

       "domain_dns_records_reset_template": "monstermedia.hu",
       "nameservers": "a.ns.monstermedia.hu, b.ns.monstermedia.hu",
       "changeNsHref": "/index.php?op=da-index",

       "session_lost_uri": "/",
       "terms_of_use_links": {"terms_of_use": "/index.php?op=aszf", "domain_auction_rules": "/index.php?op=aukcio-szabalyzat"},

       "callback_request_redirect": "/index.php?op=ng&state=mc-callback-request-received",

}


window.mcRedirectHttpRequests = {
  // these two redirects are applied so the site can be tested via file:// scheme
   "/languages.mc/lng-hu.json": "languages.mc/lng-hu.json?"+(new Date()),
   "/languages.mc/lng-en.json": "languages.mc/lng-en.json?"+(new Date()),
}

var dbmsScoreboard = [{dm_dbms_instance_name:"mysql", Id:15, User: "root", Host: "localhost", db:"some_db", Command: "SELECT * FROM nowhere", Time: 123, State: "Waiting for INSERT", Info: null, Progress: 0.00}];

var dockerImage1 = { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh',"com.monster-cloud.mc-params": {debug:true}, foo2: 'bar2' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-07-14--1' ],
    PrimaryTag: 'alpine-3.7-dropbear:2018-07-14--1',
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 };
var dockerContainer = {
                        "Command": "/bin/sh",
                        "Id": "new_container_id4",
                        "Image": "alpine-3.7-dropbear:2018-06-28--1",
                        "Labels": {
                            'com.monster-cloud.webstore': "23001",
                            'com.monster-cloud.user': "53001",
                            'com.monster-cloud.hostPort': "50001",
                            'com.monster-cloud.mc-app': 'ssh',
                        },
                        "Names": [
                          "/adoring_leavitt"
                        ],
                        "PrimaryName": "adoring_leavitt",
                        "State": "running",
                        "Status": "Up 31 seconds",
                        "c_user_id": "53001",
                        "c_webhosting": "23001",
                        "webshellSecret": "10001-123123123123123123123",
                        "shutdownAt": 1331209044,
                        "compatibleImages": [
                          "alpine-3.7-dropbear:2018-06-28--1",
                          "alpine-3.7-dropbear:2018-06-28--2",
                        ]
                      };

var mailqResponse = [
{ mail_id: '05E77A1205D',
    starred: false,
    size: 7265,
    ts: 'Tue May 23 18:16:42',
    sender: 'MAILER-DAEMON',
    status: '(connect to mta.ninjandninj.com[185.140.110.249]:25: Connection refused)',
    recipient: 'bwyJqzws3n3zg3hy@mta.ninjandninj.com' },
  { mail_id: '01ADF259C184',
    starred: false,
    size: 7373,
    ts: 'Thu May 25 01:57:35',
    sender: 'MAILER-DAEMON',
    status: '(connect to mta.whtuilamen.com[185.140.110.249]:25: Connection refused)',
    recipient: 'kgkeqeey-bjwkaepzf-ndrrxh-bhyrye-mkrekskyn@mta.whtuilamen.com' },
  { mail_id: '0B8EF259C1BD',
    starred: false,
    size: 7254,
    ts: 'Thu May 25 03:29:37',
    sender: 'MAILER-DAEMON',
    status: '(connect to mta.msmkhzp.com[185.140.110.249]:25: Connection refused)',
    recipient: 'MMTRPZBW.RGJ.WH.WE@mta.msmkhzp.com' },
  { mail_id: '8B164258D909',
    starred: true,
    size: 14161,
    ts: 'Wed May 24 13:20:24',
    sender: 'MAILER-DAEMON',
    status: '(connect to mta.leiskadesign.com[185.140.110.249]:25: Connection refused)',
    recipient: '4729218.786840-66320-94547473@mta.leiskadesign.com' },
                ];

var smartOutput = `
smartctl 5.41 2011-06-09 r3365 [x86_64-linux-3.2.0-4-amd64] (local build)
Copyright (C) 2002-11 by Bruce Allen, http://smartmontools.sourceforge.net

=== START OF INFORMATION SECTION ===
Device Model:     Crucial_CT256MX100SSD1
Serial Number:    14470DD1FD9E
LU WWN Device Id: 5 00a075 10dd1fd9e
Firmware Version: MU02
User Capacity:    256,060,514,304 bytes [256 GB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   8
ATA Standard is:  ATA-8-ACS revision 6
Local Time is:    Sat Apr  7 15:16:40 2018 CEST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x80) Offline data collection activity
                                        was never started.
                                        Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0) The previous self-test routine completed
                                        without error or no self-test has ever 
                                        been run.
Total time to complete Offline 
data collection:                ( 1190) seconds.
Offline data collection
capabilities:                    (0x7b) SMART execute Offline immediate.
                                        Auto Offline data collection on/off support.
                                        Suspend Offline collection upon new
                                        command.
                                        Offline surface scan supported.
                                        Self-test supported.
                                        Conveyance Self-test supported.
                                        Selective Self-test supported.
SMART capabilities:            (0x0003) Saves SMART data before entering
                                        power-saving mode.
                                        Supports SMART auto save timer.
Error logging capability:        (0x01) Error logging supported.
                                        General Purpose Logging supported.
Short self-test routine 
recommended polling time:        (   2) minutes.
Extended self-test routine
recommended polling time:        (   3) minutes.
Conveyance self-test routine
recommended polling time:        (   3) minutes.
SCT capabilities:              (0x0035) SCT Status supported.
                                        SCT Feature Control supported.
                                        SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x002f   100   100   000    Pre-fail  Always       -       0
  5 Reallocated_Sector_Ct   0x0033   100   100   000    Pre-fail  Always       -       0
  9 Power_On_Hours          0x0032   100   100   000    Old_age   Always       -       26685
 12 Power_Cycle_Count       0x0032   100   100   000    Old_age   Always       -       21
171 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
172 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
173 Unknown_Attribute       0x0032   058   058   000    Old_age   Always       -       1276
174 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       9
180 Unused_Rsvd_Blk_Cnt_Tot 0x0033   000   000   000    Pre-fail  Always       -       2159
183 Runtime_Bad_Block       0x0032   100   100   000    Old_age   Always       -       0
184 End-to-End_Error        0x0032   100   100   000    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       0
194 Temperature_Celsius     0x0022   066   042   000    Old_age   Always       -       34 (Min/Max 24/58)
196 Reallocated_Event_Count 0x0032   100   100   000    Old_age   Always       -       0
197 Current_Pending_Sector  0x0032   100   100   000    Old_age   Always       -       0
198 Offline_Uncorrectable   0x0030   100   100   000    Old_age   Offline      -       0
199 UDMA_CRC_Error_Count    0x0032   100   100   000    Old_age   Always       -       0
202 Data_Address_Mark_Errs  0x0031   058   058   000    Pre-fail  Offline      -       42
206 Flying_Height           0x000e   100   100   000    Old_age   Always       -       0
210 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
246 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       24638298584
247 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       996878216
248 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       20532076703

SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Vendor (0xff)       Completed without error       00%      8459         -
# 2  Vendor (0xff)       Completed without error       00%      8446         -
# 3  Vendor (0xff)       Completed without error       00%      8362         -
# 4  Vendor (0xff)       Completed without error       00%      8290         -
# 5  Vendor (0xff)       Completed without error       00%      8217         -
# 6  Vendor (0xff)       Completed without error       00%      8134         -
# 7  Vendor (0xff)       Completed without error       00%      8074         -
# 8  Vendor (0xff)       Completed without error       00%      7974         -
# 9  Vendor (0xff)       Completed without error       00%      7890         -
#10  Vendor (0xff)       Completed without error       00%      7799         -
#11  Vendor (0xff)       Completed without error       00%      7740         -
#12  Vendor (0xff)       Completed without error       00%      7665         -
#13  Vendor (0xff)       Completed without error       00%      7601         -
#14  Vendor (0xff)       Completed without error       00%      7526         -
#15  Vendor (0xff)       Completed without error       00%      7457         -
#16  Vendor (0xff)       Completed without error       00%      7403         -
#17  Vendor (0xff)       Completed without error       00%      7331         -
#18  Vendor (0xff)       Completed without error       00%      7265         -
#19  Vendor (0xff)       Completed without error       00%      7190         -
#20  Vendor (0xff)       Completed without error       00%      7072         -
#21  Vendor (0xff)       Completed without error       00%      7060         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.
                `;

var dbmsSlowLogResponse = {events:[
            {
               ds_id:1,
               ds_user_id: "3123",
               ds_webhosting: 12345,
               ds_dbms: "mysql",
               ds_database_name: "dbname1",
               ds_db_username: "dbuser1",
               ds_query: "SELECT SLEEP(10);",
               ds_time: 10,
               ds_locktime: 0,
               ds_rows_sent: 1,
               ds_rows_examined: 0,
               ds_timestamp: "2017-10-08T10:10:10Z"
            }

        ]};
var accessLogResponse = { events:
   [ { al_id: 1,
       al_timestamp: "2017-10-14T10:10:10Z",
       al_webhosting: 12345,
       al_vhost: 'femforgacs.hu',
       al_stat_user: 0,
       al_stat_system: 0,
       al_stat_hits: 2,
       al_stat_mempeak: 4608,
       al_stat_servtime: 42.372
     },
     { al_id: 2,
       al_timestamp: "2017-10-14T10:10:10Z",
       al_webhosting: 12345,
       al_vhost: 'emeljfelemlek.hu',
       al_stat_user: 104.56,
       al_stat_system: 11.62,
       al_stat_hits: 2,
       al_stat_mempeak: 26112,
       al_stat_servtime: 344.288
     } ] };

var slowLogResult = {events:[ { sl_id: 1,
          sl_timestamp: "2017-10-14T12:12:12Z",
    sl_user_id: "3123",
    sl_webhosting: 12345,
    sl_appserver: 'php/7.0',
    sl_path: '/bm.monstermedia.hu/pages/x.php',
    sl_dump: '[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/x.php:2\n[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/line2.php:2\n',
     } ]
        };

var clueBringerSessionsResponse = [
   {"Instance":"6688.592fc2f7.d1ac6.0", "QueueID":"D9CC427745D7","Timestamp":1496302327,"ClientAddress":"31.46.207.119","ClientName":"1F2ECF77.catv.pool.telekom.hu","ClientReverseName":"1F2ECF77.catv.pool.telekom.hu","Protocol":"ESMTP","EncryptionProtocol":"TLSv1.2","EncryptionCipher":"ECDHE-RSA-AES256-GCM-SHA384","EncryptionKeySize":256,"SASLMethod":"LOGIN","SASLSender":"","SASLUsername":"toth.zsuzsanna@iroda-raktar.hu","Helo":"ZSUZSAPC","Sender":"toth.zsuzsanna@iroda-raktar.hu","Size":3,"RecipientData":"/<info@iroda-raktar.hu>#0=1;30=6;","Recipients":["info@iroda-raktar.hu"]}
];
var cluebringerQuotasResponse  = [
  {"TrackKey":"SASLUsername:tech@monstermedia.hu","LastUpdate":1496415497,"LastUpdateHuman":"2012-03-08T12:17:24.000Z","Counter":1.6180,"Type":"MessageCount","CounterLimit":2000,"Comment":"Napi max 2000 emailt küldhetnek ezek a juzerek"}
];

var captchaResponse = {
    recaptcha: {sitekey: '6LdH0T4UAAAAAHjb8X5CuOZA5nZcnCWFV_MdNHpl'},
/*
               html:`<svg xmlns="http://www.w3.org/2000/svg"
width="150" height="50"><path fill="#222" d="M14.23 11.64L17.94 11.64L25.24 31.94L25.24 11.64L27.98 11.64L27.98 36.85L24.59 36.85L17.12 16.11L17.12 36.85L14.23 36.85L14.23 11.64Z"
transform="matrix(1 0 -0.1 1 -1 -1)"/><path fill="#444" d="M34.25 36.85L34.25 34.18L36.31 34.18L36.31 14.31L34.25 14.31L34.25 11.64L40.77 11.64Q45.06 11.64 47.37 14.63Q49.63 17.56 49.63 23.84Q49.63 30.34 47.54 33.38Q45.17 36.85 40.95 36.85L34.25 36.85M39.19 14.31L39.19 34.18L40.84 34.18Q46.68 34.18 46.68 23.84Q46.68 18.71 45.01 16.39Q43.53 14.31 40.63 14.31L39.19 14.31Z"
transform="matrix(1 0 -0.076 1 1 -2)"/><path fill="#333" d="M59.54 31.01Q59.98 34.93 63.25 34.93Q68.10 34.93 68.05 24.61Q66.03 27.58 62.97 27.58Q59.26 27.58 57.42 24.12Q56.38 22.10 56.38 19.45Q56.38 16.07 58.21 13.59Q60.18 10.92 63.25 10.92Q70.67 10.92 70.67 23.45Q70.67 37.57 63.29 37.57Q59.91 37.57 57.96 34.65Q56.96 33.14 56.59 31.01L59.54 31.01M63.38 13.40Q59.12 13.40 59.12 19.38Q59.12 21.59 59.88 23.05Q61.00 25.21 63.38 25.21Q64.89 25.21 66.07 23.88Q67.58 22.15 67.58 19.38Q67.58 16.60 66.38 14.98Q65.24 13.40 63.38 13.40Z"
transform="matrix(1.04 0 -0.165 1.04 -1 2)"/><path fill="#333" d="M86.12 14.31L86.12 34.18L89.72 34.18L89.72 36.85L79.42 36.85L79.42 34.18L83.02 34.18L83.02 14.31L79.42 14.31L79.42 11.64L89.72 11.64L89.72 14.31L86.12 14.31Z"
transform="matrix(1.04 0 -0.105 1.04 -2 2)"/><path fill="#222" d="M104.13 11.64L107.30 11.64L107.30 14.88L104.13 14.88L104.13 11.64M104.31 19.74L107.12 19.74L107.12 36.85L104.31 36.85L104.31 19.74Z"
transform="matrix(0.96 0 -0.225 0.96 2 -3)"/><path fill="#333" d="M120.60 11.64L133.31 11.64L133.31 14.17L123.34 33.96L133.70 33.96L133.70 36.85L120.12 36.85L120.12 34.04L130.00 14.45L120.60 14.45L120.60 11.64Z"
transform="matrix(1.01 0 -0.25 1.01 -2 1)"/><path d="M11 24 C56 27,61 23,126 19"
stroke="#999" fill="transparent"/><path d="M7 24 C58 40,67 34,141 34"
stroke="#555" fill="transparent"/><path d="M17 35 C71 40,75 14,139 40"
stroke="#666" fill="transparent"/><filter id="n" x="0" y="0"><feTurbulence baseFrequency=".7,.07" seed="2"/><feColorMatrix type="luminanceToAlpha"/></filter><rect width="150" height="50" filter="url(#n)" opacity="0.2"/></svg>`
*/
};

var emptyTapiResponse = {"records":[]};

var femforgacsTapiResponse = {"records":[{"type":"SOA\/NS\/A","ttl":"3600","ip":"","nameserver":"a.ns.monstermedia.hu","hash":"24684e5c1cd7d62532befb1bfdc02106b002a06f21f860bf2db8c1d2a8c6650c"
},{"type":"SOA\/NS\/A","ttl":"3600","ip":"","nameserver":"b.ns.monstermedia.hu","hash":"bd225bb6b5b26bbf453ab266a2676d9d2c5ea9239b9998b04b6d5f8a0f9574da"
},{"type":"A\/PTR","ttl":"3600","ip":"88.151.102.104","hash":"aae6f56746090a1769e322f1dddb9e3f1b6b35c6d2839474825b4ba1b94d2692"
},{"type":"A","ttl":"3600","host":"www","ip":"88.151.102.104","hash":"da3f99866510703866078b0c6dd522e6c2ee41fc5e9892a71e0ca9b56d45e1b3"
},{"type":"MX","ttl":"3600","priority":0,"mailserver":"monstermedia.hu","hash":"6b9dc7df28de00a6cdd41d1302403eff3e46b87aad1ec7c6012711d7feb9e211"
},{"type":"A","ttl":"3600","host":"dump","ip":"88.151.102.104","hash":"e65b924fbe2eef6dbc5a862f4203cff227830e318b542c88f8ea18bd61df52c5"
},{"type":"CNAME","ttl":"3600","host":"webmail","cname":"webmail.monstermedia.hu","hash":"3c21bd248a9d76a3e70ac010f6063762748d7bf3e126422b89421a0c74a2dc44"
},{"type":"TXT","ttl":"3600","text":"1234657890123465789012346578901234657890","hash":"8fd6e80a91e5edc5bccd9b7dbf8434d1bc7c707a740d8cc023834cf55ee985fe"
}]};

var vtsResponseMin2 = { count:1, 
  sum: {tl_hits: 123, tl_bytes_in: 10, tl_bytes_out: 20, tl_bytes_sum: 30}, 
  rows: [
{ tl_webhosting_id: 22001,
    tl_description: 'something.hu',
    tl_bytes_in: 14255,
    tl_bytes_out: 184929320,
    tl_bytes_sum: 184943575,
    tl_hits: 46,
    tl_timestamp: '2017-09-22T22:30:56.878Z' }
    ] };
var vtsResponseFull2 = { count: 2, 
  sum: {tl_hits: 246, tl_bytes_in: 20, tl_bytes_out: 40, tl_bytes_sum: 60},
  rows: [ vtsResponseMin2.rows[0],
{ tl_webhosting_id: 22001,
    tl_description: 'somethingelse.hu',
    tl_bytes_in: 34255,
    tl_bytes_out: 84929320,
    tl_bytes_sum: 84943575,
    tl_hits: 5,
    tl_timestamp: '2017-09-22T22:30:56.878Z' }
    ] };

var su_ftp_accounts = [
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username1", fa_subdir:"/subdir"},
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username2", fa_subdir:"/"},
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username3", fa_subdir:"/subdir"},
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username4", fa_subdir:"/"},
                ]
var ftp_accounts = [
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username1", fa_subdir:"/subdir"},
                  {fa_last_login_ip: '123.123.123.123', fa_last_login_ts: '2017-01-01 10:10:10', fa_user_id: 41234, fa_webhosting: 12345, fa_username: "username2", fa_subdir:"/"},
                ]

var python_apps = [{"webhosting":12345,"python_app_name":"foobar"}]
var python_foobar_app = {'python_app_name': "foobar", "python": "python3", module: "something", chdir: "/wsgi-root/mysite"}

var dbbackupFilemanList = [
          {"mtime":"2017-12-26T01:00:25.980Z","name":"mm_monstermedia-20171226020004.sql.gz","size":159464168,"perm":"-rw-r--r--"},
          {"mtime":"2017-12-30T01:00:24.341Z","name":"mm_monstermedia-20171230020003.sql.gz","size":159581864,"perm":"-rw-r--r--"},
          {"mtime":"2017-12-27T01:00:25.929Z","name":"mm_monstermedia-20171227020003.sql.gz","size":159414818,"perm":"-rw-r--r--"},
          {"mtime":"2017-12-28T01:00:25.712Z","name":"mm_monstermedia-20171228020004.sql.gz","size":159454050,"perm":"-rw-r--r--"},
          {"mtime":"2017-12-29T01:00:27.730Z","name":"mm_monstermedia-20171229020006.sql.gz","size":159517845,"perm":"-rw-r--r--"}
        ];

var certContactsResponse = {
                    contacts: [
                        { co_id: 1,
                               co_user_id: '14851',
                               co_deleted: 0,
                               co_EmailAddress: 'email@email.hu',
                               co_JobTitle: 'Job Title',
                               co_FirstName: 'First',
                               co_LastName: 'Last',
                               co_OrganizationName: 'Organization',
                               co_Address1: 'Cím 1',
                               co_Address2: 'Cím 2',
                               co_City: 'City',
                               co_StateProvince: 'State',
                               co_PostalCode: '1234',
                               co_Country: 'HU',
                               co_Phone: '+36.302679426'
                             }

                    ]
                }

var webhostingWithTemplate = {
                   wh_id: 12345,
                   wh_user_id: 1,
                   wh_is_expired: false,
                   wh_max_execution_second: 30,
                   wh_php_fpm_conf_extra_lines: "line1\nline2\n",
                   wh_name: "some name",
                   wh_template: "WEB10000",
                   wh_template_override: "{\"t_storage_max_quota_hard_mb\":20000}",
                   wh_php_version: "5.6",
                   wh_tally_sum_storage_mb: 550.00,
                   wh_tally_db_storage_mb: 50.00,
                   wh_tally_db_storage: {"mysqldb1":50.00},
                   wh_tally_mail_storage_mb: 150.00,
                   wh_tally_mail_storage: {"email1@email.hu":100.00, "email2@email.hu": 50.00},
                   wh_tally_web_storage_mb: 350.00,
                   wh_tally_web_storage: {"somedir1":200.00, "somedir2": 100.00},
                   template: {
                      t_awstats_allowed: 1,
                      t_domains_max_number_attachable: 10,
                      t_storage_max_quota_soft_mb: 10000,
                      t_db_max_number_of_databases: 5,
                      t_db_max_databases_size_mb: 250,
                      t_ftp_max_number_of_accounts: 10,
                      t_x509_certificate_letsencrypt_allowed: 1,
                      t_webapp_email_quota_policy: "noemail",
                      t_email_max_number_of_accounts: 10,
                      t_email_max_number_of_aliases: 11,
                      t_allowed_dynamic_languages: ["ssh"],
                   },
                }

var webhostingtemplate = { t_name: 'WEB10000',
                t_storage_max_quota_hard_mb: 10001,
                t_storage_max_quota_soft_mb: 10002,
                t_domains_max_number_attachable: 10,
                t_email_max_number_of_accounts: 101,
                t_email_max_number_of_aliases: 102,
                t_ftp_max_number_of_accounts: 103,
                t_awstats_allowed: 1,
                t_cron_max_entries: 103,
                t_allowed_dynamic_languages: ["php",".net"],
                t_php_max_number_of_emails_in_window: 104,
                t_db_max_number_of_databases: 11,
                t_db_max_databases_size_mb: 101,
                t_x509_certificate_external_allowed: 1,
                t_x509_certificate_letsencrypt_allowed: 1 }

var mockedLoginlogResponse =  {
  "count": {"count": 16},
  "search": {events:[
     {"l_timestamp":"2016-07-23 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.1","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-24 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.2","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-25 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.3","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-26 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.4","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-23 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.1","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-24 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.2","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-25 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.3","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-26 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.4","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-23 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.1","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-24 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.2","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-25 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.3","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-26 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.4","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-23 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.1","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-24 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.2","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
     {"l_timestamp":"2016-07-25 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.3","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":true},
     {"l_timestamp":"2016-07-26 10:10:10","l_username":"abc@asdf.hu","l_ip":"123.123.123.4","l_subsystem":"accountapi","l_agent":"Firefox","l_grants":" ACCOUNT_OWNER ","l_successful":false},
  ]}
}

var mockedEventlogResponse =  {
  "count": {"count": 4},
  "search": {events:[
     {"e_timestamp":"2016-07-23 10:10:10","e_username":"abc@asdf.hu","e_ip":"123.123.123.1","e_subsystem":"accountapi","e_event_type":"auth-credential","e_agent":"Firefox","e_other":" ACCOUNT_OWNER "},
     {"e_timestamp":"2016-07-24 10:10:10","e_username":"abc@asdf.hu","e_ip":"123.123.123.2","e_subsystem":"accountapi","e_event_type":"exception","e_agent":"Firefox","e_other":"sqlite error"},
     {"e_timestamp":"2016-07-25 10:10:10","e_username":"abc@asdf.hu","e_ip":"123.123.123.3","e_subsystem":"accountapi","e_event_type":"auth-token","e_agent":"Firefox","e_other":" ACCOUNT_OWNER "},
     {"e_timestamp":"2016-07-26 10:10:10","e_username":"abc@asdf.hu","e_ip":"123.123.123.4","e_subsystem":"accountapi","e_event_type":"exception","e_agent":"Firefox","e_other":"very error very"},
  ]}
}

var primary_git_branch_info = { b_id: 1,
    b_git_id: '1',
    b_branch: 'dev',
    b_pull_single_branch_only: 0,
    b_blue_green: 1,
    b_repo_path1: '/foo/dev/blue/',
    b_repo_path2: '/foo/dev/green/',
    b_docroot_subdir: '/somebody/',
    b_current_repo_index: 2,
    b_domain_docroot_changes: [
      {
      "domain": "domain2.hu",
      "host": "www.domain2.hu",
      "raw": "domain2.hu/www.domain2.hu",
      }
     ],
 }

var primary_git_info = { g_id: "1",
  g_user_id: '23456',
  g_webhosting: 12345,
  g_branches: 1,
  g_remote_origin: 'https://github.com/irsl/dfwfw.git',
  g_name: 'My awesome website',
  webhook_url: "https://some.primary.domain/api/s/s123/git/triggers/12345/abcdef01242345234234",
    }

var domainsResponse = [
                   {"d_id": 4, "d_domain_name": "femforgacs.hu", "d_domain_canonical_name": "femforgacs.hu"},
                   {"d_id": 3, "d_domain_name": "zsid.hu", "d_domain_canonical_name": "zsid.hu"},
                   {"d_id": 2, "d_domain_name": "foobár.hu", "d_domain_canonical_name": "xn--foobr-xfw.hu"},
                   {"d_id": 1, "d_domain_name": "foobar.hu", "d_domain_canonical_name": "foobar.hu"},
                ]
var credentialsResponse = [
                  {"c_username": "main",    "c_uuid": "uuid 123", "c_server": "foo", "c_grants": ["ACCOUNT_OWNER"], "is_primary":true,  "can_be_deleted": false},
                  {"c_username": "dnsonly", "c_uuid": "uuid 124", "c_server": "foo", "c_grants": ["ACCOUNT_DNS"],   "is_primary":false, "can_be_deleted": true },
                ]

var dbms_type_mysql  = {dm_dbms_instance_name:"mysql","dm_dbms_type":"mysql","dm_connection_string":'{"user":"username","password":"password","host":"127.0.0.1"}'}

var databases_list = [{db_user_id: "3123", db_webhosting:12345,db_database_name:"something",dm_dbms_instance_name: "mysql", db_tally_mb: 123.12, dm_dbms_type: "mysql"}]
var dbusers_list = [{u_webhosting: 12345, u_locked: 1, u_hostname: "",u_username:"something", db_database_name: "something", dm_dbms_instance_name: "mysql"}, {u_webhosting:12345,u_username:"something_else", u_locked: 0, u_hostname: "",db_database_name: "something", dm_dbms_instance_name: "mysql"}]

var aliasesResponse = {
                   "aliases": [
                          {
                            "el_id": 1,
                            "el_user_id":"51001",
                            "el_webhosting": 52001,
                            "el_alias": "somealias1@femforgacs.hu",
                            "el_destination": "whatever@whatever.com",
                            "el_verified": 1,
                          },
                          {
                            "el_id": 2,
                            "el_user_id":"51001",
                            "el_webhosting": 52001,
                            "el_alias": "somealias2@femforgacs.hu",
                            "el_destination": "insider@somealias.hu",
                            "el_verified": 1,
                          },
                          {
                            "el_id": 3,
                            "el_user_id":"51001",
                            "el_webhosting": 52001,
                            "el_alias": "somealias3@femforgacs.hu",
                            "el_destination": "whatever@whatever.com",
                            "el_verified": 0,
                            "verification_expired": false,
                          }

                   ],
                   "canBeAdded": true,
                }
var femforgacsEmailDomainResponse = {
                      "do_active": 1,
                      "do_domain_name": "femforgacs.hu",
                      "do_user_id": "51001",
                      "do_webhosting": 52001,
                    }
var emailDomainsResponse = {
                    "domains": [
                    {
                      "do_active": 0,
                      "do_domain_name": "somewhat1.hu",
                      "do_user_id": "51001",
                      "do_webhosting": 52001,
                    },
                    {
                      "do_active": 1,
                      "do_domain_name": "somewhat2.hu",
                      "do_user_id": "51001",
                      "do_webhosting": 52001,
                    },
                    femforgacsEmailDomainResponse,
                    {
                      "do_active": 1,
                      "do_domain_name": "xn--femforgacs.hu",
                      "do_user_id": "51001",
                      "do_webhosting": 52001,
                    }
                  ],
                    "canBeAdded": true,
                }
var emailAccountsResponse = {
                   "accounts": [
                          {
                            "ea_id": 1,
                            "ea_user_id":"51001",
                            "ea_webhosting": 52001,
                            "ea_email": "someaccount1@femforgacs.hu",
                            "ea_autoexpunge_days": 5,
                            "ea_quota_bytes": 50*1024*1024,
                            "tally_bytes": 47.1*1024*1024,
                            "tally_messages": 123,
                            "Policy":"high",
                          },
                          {
                            "ea_id": 2,
                            "ea_user_id":"51001",
                            "ea_webhosting": 52001,
                            "ea_email": "someaccount2@femforgacs.hu",
                            "ea_autoexpunge_days": 0,
                            "ea_quota_bytes": 1000*1024*1024,
                            "tally_bytes": 3.1*1024*1024,
                            "tally_messages": 12,
                          },
                          {
                            "ea_id": 3,
                            "ea_user_id":"51001",
                            "ea_webhosting": 52001,
                            "ea_email": "someaccount3@femforgacs.hu",
                            "ea_autoexpunge_days": 0,
                            "ea_quota_bytes": 100*1024*1024,
                            "tally_bytes": 4.8*1024*1024,
                            "tally_messages": 1231,
                          },
                          {
                            "ea_id": 4,
                            "ea_user_id":"51001",
                            "ea_webhosting": 12345,
                            "ea_email": "mailer@12345.s1.svc",
                            "ea_autoexpunge_days": 0,
                            "ea_quota_bytes": 100*1024*1024,
                            "tally_bytes": 4.8*1024*1024,
                            "tally_messages": 1231,
                          },
                   ],
                   "canBeAdded": true,
                }

var webhostingDomainsResponse = [ { wd_domain_canonical_name: 'femforgacs.hu',
    wd_domain_name: 'whatever1.tld',
    wd_server: 's1',
    wd_webhosting: 12345 },
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 's1',
    wd_webhosting: 0 } ];

var bccsResponse = {
                   "bccs": [
                          {
                            "bc_id": 1,
                            "bc_user_id":"51001",
                            "bc_webhosting": 52001,
                            "bc_alias": "somebcc1@femforgacs.hu",
                            "bc_destination": "whatever@whatever.com",
                            "bc_verified": 1,
                          },
                          {
                            "bc_id": 2,
                            "bc_user_id":"51001",
                            "bc_webhosting": 52001,
                            "bc_alias": "somebcc2@femforgacs.hu",
                            "bc_destination": "insider@somealias.hu",
                            "bc_verified": 1,
                          },
                          {
                            "bc_id": 3,
                            "bc_user_id":"51001",
                            "bc_webhosting": 52001,
                            "bc_alias": "somebcc3@femforgacs.hu",
                            "bc_destination": "whatever@whatever.com",
                            "bc_verified": 0,
                            "verification_expired": false,
                          }

                   ],
                   "canBeAdded": true,
                }
var csrinfo = {
              "Subject": {
                 "C": "HU",
                 "CN": "www.monstermedia.hu",
                 "L": "Budapest",
                 "O": "Noname Domain Kft.",
                 "OU": "Monster Media",
                 "ST": "Hungary",
                 "text": "C=HU, ST=Hungary, L=Budapest, O=Noname Domain Kft., OU=Monster Media, CN=www.monstermedia.hu",
               },
              "Modulus": "00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:15:29",
            }
var certinfo = {
             "CERTIFICATE": "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n",
             "Modulus": "00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:15:29",
              Subject:
   { text: 'OU=GT46628488, OU=See www.rapidssl.com/resources/cps (c)15, OU=Domain Control Validated - RapidSSL(R), CN=www.monstermedia.hu',
     OU:
      [ 'GT46628488',
        'See www.rapidssl.com/resources/cps (c)15',
        'Domain Control Validated - RapidSSL(R)' ],
     CN: 'www.monstermedia.hu' },
  Issuer:
   { text: 'C=US, O=GeoTrust Inc., CN=RapidSSL SHA256 CA - G3',
     C: 'US',
     O: 'GeoTrust Inc.',
     CN: 'RapidSSL SHA256 CA - G3' },
    "Not After": {
          "iso": "2017-05-18T21:37:09Z",
          "text": "May 18 21:37:09 2017 GMT",
    },
    "Not Before": {
          "iso": "2015-05-16T05:28:23Z",
         "text": "May 16 05:28:23 2015 GMT",
    },
  'X509v3 Subject Alternative Name':
   { text: 'DNS:www.monstermedia.hu, DNS:monstermedia.hu',
     DNS: [ 'www.monstermedia.hu', 'monstermedia.hu' ] } }

 var cert1 = { c_id: "123123", c_user_id: '14851',
  c_webhosting: 16387,
  c_issuer_organization: "Let's Encrypt",
  c_issuer_common_name: 'Foobar',
  c_subject_common_name: 'www.monstermedia.hu',
  c_subject_organization_unit: 'GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)',
  c_begin: '2017-05-16T05:28:23Z',
  c_end: '2017-06-18T21:37:09Z',
  c_private_key: 1,
  c_certificate: 1,
  c_comment: "komment",
  c_settled: 0,
  c_intermediate_chain: 1,
  c_certificate_signing_request: 0,
  c_alias: 'someAlias',
  c_product: 'LetsEncrypt',
  c_deleted: 0,
  closeToExpire: true,
  isExpired: true

   }

 var cert2 = { c_id: "123124", c_user_id: '14851',
  c_webhosting: 0,
  c_issuer_organization: 'GeoTrust Inc.',
  c_issuer_common_name: 'RapidSSL SHA256 CA - G3',
  c_subject_common_name: 'www.monstermedia.hu',
  c_subject_organization_unit: 'GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)',
  c_begin: '2015-05-16T05:28:23Z',
  c_end: '2017-05-18T21:37:09Z',
  c_private_key: 1,
  c_certificate: 1,
  c_settled: 1,
  c_intermediate_chain: 1,
  c_certificate_signing_request: 0,
  c_alias: '',
  c_product: '',
  c_deleted: 0,
  closeToExpire: true,
  isExpired: true
   }

 var purchase_info = {
                      "pc_id": "abcd1233",
                      "pc_activation": "activation-id",
                      "pc_vendor": "Let's Encrypt",
                      "pc_product": "Let's Encrypt",
                      "pc_reseller": "letsencrypt",
                      "pc_reseller_certificate_id": "234",
                      "pc_certificate": "123123",
                      "pc_subject_common_name": "something.hu",
                      "pc_product_parameters": {Years:2},
                      "pc_comment": "lucifer over london",
                      "pc_begin": null,
                      "pc_end": null,
                      "pc_status": "pending",
                      "pc_user_id": "14851",
                      "pc_misc": {ssl_szamlazasiinfo: 123},

                      canBeRenewed: true,
                      isExpired: false,

                      actions: [
                         "vendor1-activate",
                         "vendor1-reissue",
                         "vendor1-revoke",

                         "vendor2-revoke",

                         "vendor2-manual",

                         "vendor2-resend-approver",

                         "vendor2-activate-general",
                         "vendor2-reissue-general",

                         "vendor2-activate-local",
                         "vendor2-reissue-local",
                      ]
                }

 var full_cert = { c_id: "123123", c_user_id: '14851',
 data:{
                certificate: "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n",
                intermediates: [ '-----BEGIN CERTIFICATE-----\ncert1\n-----END CERTIFICATE-----\n',
     '-----BEGIN CERTIFICATE-----\ncert2\n-----END CERTIFICATE-----\n' ],
  },
  c_webhosting: 16387,
  c_issuer_organization: "Let's Encrypt",
  c_issuer_common_name: 'Foobar',
  c_subject_common_name: 'www.monstermedia.hu',
  c_subject_organization_unit: 'GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)',
  c_begin: '2017-05-16T05:28:23Z',
  c_end: '2017-06-18T21:37:09Z',
  c_comment: "This is something",
  c_private_key: 1,
  c_certificate: 1,
  c_settled: 0,
  c_intermediate_chain: 1,
  c_certificate_signing_request: 0,
  c_alias: '',
  c_product: 'LetsEncrypt',
  c_deleted: 0,
  closeToExpire: true,
  isExpired: true
   }




// abcd1234
 var femforgacsDocrootAnswer = { "certificate_id": "123123", "awstats": true, "awstats_allowed": true, "entries":[{"host":"www.femforgacs.hu", "docroot": "/femforgacs.hu/pages"},{"host":"femforgacs.hu", "docroot": "/femforgacs.hu/pages"}]}
window.mcMockedHttpResponses = {

	"POST": {
        "/index.php?op=ajax-login": "ok",
        "/index.php?op=ajax-logout": "ok",

        "/s/s1/su/editor/accountapi/list": ["account.json", "foo.json"],
        "/s/s1/su/editor/accountapi/config/defaults": {"default-key": "default-value"},
        "/s/s1/su/editor/accountapi/config/file": {"configfile-option-key": "configfile-option-value"},
        "/s/s1/su/email/postfix/config/list": ["01-additional.cf"],
        "/s/s1/su/email/postfix/config/fetch": {data: "some data\nsome other line"},
        "/s/s1/su/email/postfix/config/delete": "ok",
        "/s/s1/su/email/postfix/config/save": "ok",

        "/s/s1/docker/webhosting/12345/ssh/authz": "ok",
        "/s/s1/docker/webhosting/12345/ssh/target": "ok",
        "/s/s1/su/docker/root/images/latest/rebuild": {"php-5.6":"1.2.3"},
        "/s/s1/su/docker/root/container/new_container_id4/config": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/logs": "some line1\nsome line2\n",
        "/s/s1/su/docker/root/container/new_container_id4/logs": "some log line1\nsome log line2\n",
        "/s/s1/su/docker/root/container/new_container_id4/stats": {"stat": "istics"},
        "/s/s1/docker/webhosting/12345/container/php_container_5/attach-to-vhost": "ok",
        "/s/s1/docker/webhosting/12345/container/php_container_5/set-as-default-web": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/set-as-default-web": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/stop": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/kill": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/restart": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/start": "ok",
        "/s/s1/docker/webhosting/12345/container/new_container_id4/upgrade": "ok",

        "/s/s1/su/docker/root/containers/rebuild": {id:"docker-rebuild"},
        "/s/s1/su/docker/root/containers/upgrade": {id:"docker-upgrade"},
        "/s/s1/su/docker/root/images/cleanup": {id:"docker-cleanup"},
        "/s/s1/su/docker/root/local/cleanup/do": "ok",
        "/s/s1/su/docker/root/local/cleanup/list": {
          abandonedConfigEntries: [{whId: 1234, containerId: "123123123"}], 
          abandonedVolumes: [{name:"11004-123123123"}], 
          abandonedContainers: [{containerId:"container1"}], 
          oldImages: [{PrimaryTag:"image1:2018-12-12--1"}]
        },
        "/s/s1/su/docker/root/local/hostsfile/rebuild": "ok",
        "/s/s1/su/docker/root/container/new_container_id4/exec": {stdout: "foobar1\nfoobar2\n", stderr: "stderr1\nstderr2\n"},
        "/s/s1/docker/webhosting/12345/container/new_container_id4/exec/fast": {stdout: "foobar1\nfoobar2\n", stderr: "stderr1\nstderr2\n"},
        "/s/s1/su/docker/root/images/pull": {id: "docker-pull"},
        "/s/s1/docker/webhosting/12345/containers/list": [
               dockerContainer,
               {
                        "Command": "/usr/local/php/bin/php",
                        "Id": "php_container_5",
                        "Image": "stretch-php-7.2:2018-06-28--1",
                        "Labels": {
                            'com.monster-cloud.webstore': "23001",
                            'com.monster-cloud.user': "53001",
                            'com.monster-cloud.mc-app': 'php',
                            'com.monster-cloud.mc-webapp': 'php-fpm',
                        },
                        "Names": [
                          "/foobar_leavitt"
                        ],
                        "PrimaryName": "foobar_leavitt",
                        "State": "running",
                        "Status": "Up 3 minutes",
                        "c_user_id": "53001",
                        "c_webhosting": "23001",
                        "compatibleImages": [
                          "stretch-php-7.2:2018-06-28--1",
                          "stretch-php-7.2:2018-06-28--2",
                        ]
                      },
        ],
        "/s/s1/docker/webhosting/12345/containers/new": {
            "hostPort": 50001,
            "ssh": {
              stdout: "host keys"
            },
            "container": "new_container_id5"
        },
        "/s/s1/docker/webhosting/12345/images/available": {
               images: [
                 dockerImage1, 
                 { Containers: -1,
                      Created: 1529237078,
                      Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
                      Labels: { "com.monster-cloud.mc-app": 'webshell', foo2: 'bar2' },
                      ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      RepoDigests: null,
                      RepoTags: [ 'alpine-3.7-shellinabox:2018-07-14--1' ],
                      PrimaryTag: 'alpine-3.7-shellinabox:2018-07-14--1',
                      SharedSize: -1,
                      Size: 4147781,
                      VirtualSize: 4147781 },
                 { Containers: -1,
                      Created: 1529237078,
                      Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
                      Labels: { "com.monster-cloud.mc-app": 'php', foo2: 'bar2' },
                      ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      RepoDigests: null,
                      RepoTags: [ 'stretch-php-7.2:2018-07-14--1' ],
                      PrimaryTag: 'stretch-php-7.2:2018-07-14--1',
                      SharedSize: -1,
                      Size: 4147781,
                      VirtualSize: 4147781 },
                 { Containers: -1,
                      Created: 1529237078,
                      Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
                      Labels: { "com.monster-cloud.mc-app": 'nodejs', foo2: 'bar2' },
                      ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      RepoDigests: null,
                      RepoTags: [ 'node-alpine-node:2018-08-01--3' ],
                      PrimaryTag: 'node-alpine-node:2018-08-01--3',
                      SharedSize: -1,
                      Size: 4147781,
                      VirtualSize: 4147781 },
                 { Containers: -1,
                      Created: 1529237078,
                      Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
                      Labels: { "com.monster-cloud.mc-app": 'python', foo2: 'bar2' },
                      ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      RepoDigests: null,
                      RepoTags: [ 'alpine-3.7-python:2018-08-01--3' ],
                      PrimaryTag: 'alpine-3.7-python:2018-08-01--3',
                      SharedSize: -1,
                      Size: 4147781,
                      VirtualSize: 4147781 },
               ],
               canCreate: true,
        },

        "/s/s1/su/docker/root/containers/list": [
               dockerContainer     
        ],
        "/s/s1/su/docker/root/images/list": [
               dockerImage1
        ],

        "/s/s1/ftp/accounts/12345/username1/ssh-authz/": "ok",
        "/s/s1/ftp/accounts/12345/username1/totp/status":
          {"fa_totp_active": false, "fa_totp_secret": "EQYVE5ZPNFUD46DRH52EGQDZN5EDUVCMER2C6N2HKB3T4MR6H45A", "fa_totp_url": "otpauth://totp/MonsterMedia?secret=EQYVE5ZPNFUD46DRH52EGQDZN5EDUVCMER2C6N2HKB3T4MR6H45A"},
        "/s/s1/ftp/accounts/12345/username1/totp/activate": "ok",
        "/s/s1/ftp/accounts/12345/username1/totp/inactivate": "ok",

        "/account": "ok",
        "/account/totp/activate": "ok",
        "/account/totp/inactivate": "ok",
        "/account/totp/status": {"u_totp_active": false, "u_totp_secret": "EQYVE5ZPNFUD46DRH52EGQDZN5EDUVCMER2C6N2HKB3T4MR6H45A", "u_totp_url": "otpauth://totp/MonsterMedia?secret=EQYVE5ZPNFUD46DRH52EGQDZN5EDUVCMER2C6N2HKB3T4MR6H45A"},
		    "/account/tel/+36-30-1234567/set_primary": "ok",
        "/account/tel/+36-30-1234567/send": "ok",
        "/account/tel/+36-30-1234567/verify": "ok",
        "/account/email/foo3@bar.hu/categories": "ok",
        "/account/email/foo3@bar.hu/set_primary": "ok",
        "/account/email/foo2@bar.hu/send/confirmation": "ok",

        "/account/account_grant": "ok",
        "/account/callback_request": "ok",
        "/account/loginlog/count": mockedLoginlogResponse.count,
        "/account/loginlog/search": mockedLoginlogResponse.search,
        "/account/config/webhostingdomains/txtrecords/ex": {required: true, a_record: "123.123.123.123"},
        "/account/webhostingdomains/txt-record-code": {
           account_id: "1234",
           bareDomain: "maindomain.hu",
           server: "s1",
           code: "sdfhisufhisudfhs",
           webhosting_id: 12311,
        },
        "/su/accountapi/account/2/totp/force-off": "ok",
        "/su/accountapi/webhostingdomains/by-wh/": webhostingDomainsResponse,
        "/su/accountapi/sms/search": [ { s_id: 1,
    s_provider: '',
    s_country_code: '36',
    s_area_code: '30',
    s_phone_no: '7654321',
    s_recipient: '+36-30-7654321',
    s_message: 'hello world',
    s_status: 'failed',
    s_timestamp: '2017-02-01T12:12:12Z'},
  { s_id: 2,
    s_provider: 'seeme',
    s_country_code: '36',
    s_area_code: '30',
    s_phone_no: '7654321',
    s_recipient: '+36-30-7654321',
    s_message: 'hello world',
    s_status: 'sent',
    s_timestamp: '2017-02-01T12:12:12Z'
  } ],

        "/su/accountapi/sms/count": {count: 2},

        "/su/accountapi/tels/set-escalate": "ok",
        "/su/accountapi/tels/search": [ {
    t_user_id: '14851',
    t_country_code: '36',
    t_area_code: '30',
    t_phone_no: '7654321',
    t_human: '+36-30-7654321',
    t_confirmed: 1,
    t_confirmed_at: '2017-10-04T17:20:01.517Z',
    t_primary: 1,
    t_category: 'WORK',
    created_at: '2017-10-04 17:20:00',
    updated_at: '2017-10-04 17:20:00',
    t_escalate: 0 },
    {
    t_user_id: '14851',
    t_country_code: '36',
    t_area_code: '30',
    t_phone_no: '7654321',
    t_human: '+36-30-7654321',
    t_confirmed: 1,
    t_confirmed_at: '2017-10-04T17:20:01.517Z',
    t_primary: 1,
    t_category: 'WORK',
    created_at: '2017-10-04 17:20:00',
    updated_at: '2017-10-04 17:20:00',
    t_escalate: 1 }
     ],

    "/su/accountapi/emails/set-escalate": "ok",
     "/su/accountapi/emails/search": [ { e_user_id: '14851',
    e_email: 'emailtest2@email.ee',
    e_email_stripped: 'emailtest2@emailee',
    e_confirmed: 1,
    e_confirmed_at: '2017-10-04T17:21:08.173Z',
    e_primary: 1,
    e_categories: [],
    e_token_generated_at: '2017-10-04T17:21:07.982Z',
    created_at: '2017-10-04 17:21:07',
    updated_at: '2017-10-04 17:21:07',
    e_escalate: 0 },
  { e_user_id: '14851',
    e_email: 'emailtest-added@email.ee',
    e_email_stripped: 'emailtest-added@emailee',
    e_confirmed: 1,
    e_confirmed_at: '2017-10-04T17:21:08.995Z',
    e_primary: 1,
    e_categories: [],
    e_token_generated_at: null,
    created_at: '2017-10-04 17:21:08',
    updated_at: '2017-10-04 17:21:08',
    e_escalate: 0 },
  { e_user_id: '14851',
    e_email: 'surely.not@primary.email',
    e_email_stripped: 'surelynot@primary.email',
    e_confirmed: 1,
    e_confirmed_at: '2017-10-04T17:21:09.159Z',
    e_primary: 0,
    e_categories: [],
    e_token_generated_at: null,
    created_at: '2017-10-04 17:21:09',
    updated_at: '2017-10-04 17:21:09',
    e_escalate: 0 } ],

        "/account/email/radimre83@gmail.com/verify": "ok",

        "/s/s1/email/pub/test": "ok",
        "/s/s1/email/pub/test2": {ea_autoexpunge_days: 100, ea_spam_tag2_level: 4},
        "/s/s1/email/pub/salearn": {id:"salearn"},
        "/s/s1/email/pub/change-password": "ok",
        "/s/s1/email/pub/change": "ok",
        "/s/s1/email/pub/cleanup": {"id": "email-cleanup"},
        "/s/s1/email/pub/whiteblacklist/get": [{"wb_action":"W","wb_senderemail":"someone@somewhere.tld"}],
        "/s/s1/email/pub/whiteblacklist/put": "ok",
        "/s/s1/email/pub/whiteblacklist/del": "ok",

        "/s/s1/su/mailer/sendmail": "ok",
        "/s/s1/su/wie/backup/": "ok",
        "/s/s1/su/wie/backup/12345": "ok",

        "/s/s2/su/wie/feed/": "ok",
        "/s/s1/su/wie/feed/16384": "ok",
        "/s/s2/su/wie/feed/20001": "ok",
        "/s/s2/su/wie/feed/22345": "ok",
        "/s/s1/su/wie/feed/10001": "ok",

        "/s/s2/su/wie/restore/": "ok",
        "/s/s1/su/wie/restore/12345": "ok",
        "/s/s1/su/wie/restore/16384": "ok",
        "/s/s2/su/wie/restore/20001": "ok",
        "/s/s2/su/wie/restore/22345": "ok",

        "/s/s1/su/email/bccs/bcc/3": "ok",
        "/s/s1/su/email/aliases/alias/3": "ok",
        "/s/s1/email/domains/domain/xxx.femforgacs.hu": "ok",
        "/s/s1/email/domains/domain/femforgacs.hu/greylisting": "ok",
        "/s/s1/su/email/accounts/account/1": "ok",
        "/s/s1/su/email/accounts/account/1/tallies/recalculate": {id:"recalculate"},
        "/s/s1/email/cluebringer/policies/backup": {"some":"json"},
        "/s/s1/email/cluebringer/policies/restore": "ok",
        "/s/s1/email/cluebringer/policies/merge": "ok",
        "/s/s1/email/cluebringer/cleanup": {"id": "cluebringer-cleanup"},
        "/s/s1/su/email/cluebringer/policies/outbound/SASL/quotas": "ok",
        "/s/s1/su/email/cluebringer/policies/outbound/SASL/flush": "ok",
        "/s/s1/su/email/postfix/flush":{"id":"postfix-flush"},
        "/s/s1/su/email/amavis/stats/mails/last": [
          {"s":"sender@sender.tld","r":"recipient@email.tld","subj":"subject","c":123,"dsn":"dsn","level":3.3,"size":21221,"pb":"W","age":100},
        ],
        "/s/s1/su/email/amavis/stats/mails/clean": [
          {"domain":"domain1.tld", "cnt":101, "bspam_level":3.1},
        ],
        "/s/s1/su/email/amavis/stats/mails/spammy": [
          {"domain":"domain2.tld", "cnt":102, "bspam_level":3.2},
        ],
        "/s/s1/su/email/amavis/stats/domains": [
          {"domain":"domain3.tld", "cnt":103, "bspam_level":3.3},
        ],

        "/pub/account/forgotten/send": "ok",
        "/pub/account/registration": {"u_id": "1234"},
        "/pub/account/authentication": {
          "token": "token_by_credentials",
          "expires_at": "2016-12-12 12:30:00",
          "u_name": "Username 1", "u_id": 1,
          "u_primary_email":"info@monstermedia.hu",
          "s_grants":["SUPERUSER","ACCOUNT_OWNER"] // "SUPERUSER","SERVER_OWNER",
        },

        "/s/s1/iptables/whextra/12345/firewallrules/status": "ok",

        "/s/s1/webhosting/12345": "ok",
        "/s/s1/webhosting/12345/szar/jump/web": {url:"http://monster/jump/szar/web"},
        "/s/s1/webhosting/12345/szar/jump/mail": {url:"http://monster/jump/szar/mail"},
        "/s/s1/webhosting/12347/szar/jump/web": {url:"http://monster/jump2/szar/web"},

        "/logout": "ok",


        "/s/s1/su/cert/purchases/all/poll": {id:"poll_id"},
        "/s/s1/su/cert/purchases/all/letsencrypt/renew": {id:"letsencrypt-renew"},
        "/s/s1/su/cert/config/contacts/default": "ok",
        "/s/s1/su/cert/config/namecheap/credentials":"ok",
        "/s/s1/su/cert/config/resellers/letsencrypt/vendors/products": {products:["Let's Encrypt"]},
        "/s/s1/su/cert/config/resellers/namecheap/vendors/products": {products:["PositiveSSL", "PositiveSSL Wildcard"]},

        "/s/s1/cert/purchases/my/item/abcd1233/revoke": "ok",
        "/s/s1/cert/purchases/my/item/abcd1233/dispatch/query_validation": {
            email_validation: ["3db85a8e21b54bab848eb8f01d5d78c5.protect@whoisguard.com","postmaster@domain.com","sslwebmaster@domain.com","ssladministrator@domain.com","mis@domain.com"],
        },
        "/s/s1/cert/purchases/my/item/abcd1233/dispatch/get_approver_emails": {emails:["3db85a8e21b54bab848eb8f01d5d78c5.protect@whoisguard.com","postmaster@domain.com","sslwebmaster@domain.com","ssladministrator@domain.com","mis@domain.com"]},
        "/s/s1/cert/purchases/my/item/abcd1233/dispatch/resend_approver_email": "ok",

        "/s/s1/cert/purchases/my/item/abcd1233/reissue": "ok",
        "/s/s1/cert/purchases/my/item/abcd1233/activate/task":{id:"activation"},
        "/s/s1/cert/purchases/my/item/abcd1233/reissue/task":{id:"reissue"},

        "/s/s1/su/cert/certificates/all/search": {
           canBeAdded: false,
           certificates: [cert1, cert2]
        },
        "/s/s1/cert/info/certificate-signing-request": csrinfo,
        "/s/s1/cert/info/private-key": {"Private-Key": "(2048 bit)"},
        "/s/s1/cert/info/certificates": [ certinfo, certinfo ],


        "/s/s1/su/cert/certificates/all/item/123123/raw": "ok",
        "/s/s1/su/cert/certificates/all/item/123123/alias": "ok",
        "/s/s1/su/cert/certificates/all/item/123123/enable": "ok",
        "/s/s1/su/cert/certificates/all/item/123123/detach": "ok",
        "/s/s1/su/cert/certificates/all/item/123123/disable": "ok",
        "/s/s1/cert/certificates/my/item/123123/disable": "ok",

        "/s/s1/email/webhostings/list": { domains:
   { '20001':
      [ { do_user_ids: [ '10001' ],
          do_webhosting: 20001,
          do_domain_name: 'somealias.hu',
          do_active: 1,
          created_at: '2017-05-06 09:54:40',
          updated_at: '2017-05-06 09:54:40' },
        { do_user_ids: [ '10001' ],
          do_webhosting: 20001,
          do_domain_name: 'somebcc.hu',
          do_active: 1,
          created_at: '2017-05-06 09:54:46',
          updated_at: '2017-05-06 09:54:46' } ],
     '20005':
      [ { do_user_ids: [ '10001' ],
          do_webhosting: 20005,
          do_domain_name: 'somewebhosting.hu',
          do_active: 1,
          created_at: '2017-05-06 09:54:49',
          updated_at: '2017-05-06 09:54:49' } ] },
  accounts:
   { '20001':
      { 'somebcc.hu':
         [ { ea_id: 5,
             ea_user_id: '10001',
             ea_webhosting: 20001,
             ea_email: 'some2@somebcc.hu',
             ea_quota_bytes: 10485760,
             ea_autoexpunge_days: 0,
             created_at: '2017-05-06 09:54:46',
             updated_at: '2017-05-06 09:54:46',
             tally_bytes: 9485760,
             tally_messages: null,
             do_domain_name: 'somebcc.hu' },
           { ea_id: 6,
             ea_user_id: '10001',
             ea_webhosting: 20001,
             ea_email: 'some3@somebcc.hu',
             ea_quota_bytes: 10485760,
             ea_autoexpunge_days: 0,
             created_at: '2017-05-06 09:54:46',
             updated_at: '2017-05-06 09:54:46',
             tally_bytes: null,
             tally_messages: null,
             do_domain_name: 'somebcc.hu' } ] },
     '20005':
      { 'somewebhosting.hu':
         [ { ea_id: 7,
             ea_user_id: '10001',
             ea_webhosting: 20005,
             ea_email: 'anything@somewebhosting.hu',
             ea_quota_bytes: 10485760,
             ea_autoexpunge_days: 0,
             created_at: '2017-05-06 09:54:50',
             updated_at: '2017-05-06 09:54:50',
             tally_bytes: null,
             tally_messages: null,
             do_domain_name: 'somewebhosting.hu' } ] } },
  bccs: {
    '20001':
      { 'somebcc.hu':
         [ { bc_id: 5,
             bc_user_id: '10001',
             bc_webhosting: 20001,
             bc_alias: 'some2@somebcc.hu',
             bc_destination: "somebody@somewhere.hu" }
         ]
      },
  },
  aliases: {
    '20001':
      { 'somealias.hu':
         [ { el_id: 5,
             el_user_id: '10001',
             el_webhosting: 20001,
             el_alias: 'some2@somealias.hu',
             el_destination: "somebody@somewhere.hu" }
         ]
      },
  } },

        "/s/s1/cert/certificates/my/item/123123/copy": {c_id: "123123"},
        "/s/s1/cert/certificates/by-webhosting/16387/item/123123/copy": {c_id: "123123"},

        "/s/s1/su/cert/purchases/all/item/abcd1233/poll": "ok",
        "/s/s1/su/cert/purchases/all/item/abcd1233/raw": "ok",

        "/s/s1/email/domains/domain/non-active.hu": "ok",
        "/s/s1/email/domains/domain/femforgacs.hu": "ok",
        "/s/s1/email/domains/domain/xn--femforgacs.hu": "ok",
        "/s/s1/su/email/accounts/report/tallies": "ok",
        "/s/s1/su/email/accounts/cleanup/junk": {id:"email-cleanup-junk"},
        "/s/s1/su/email/accounts/cleanup/autoexpunge": {id:"email-autoexpunge"},
        "/s/s1/email/webhostings/12345/domains/domain/somewhat1.hu": "ok",
        "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1/cleanup": {"id":"email-cleanup"},
        "/s/s1/email/webhostings/12345/domains/domain/femforgacs.hu/account/1": "ok",
        "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1": "ok",
        "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bcc/3/verify": "ok",
        "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/alias/3/verify": "ok",
        "/s/s1/email/tasks/salearn/input": "ok",


        "/s/[this]/wizard/webhosting/installatron/query": {installatron_server: "installatronserver"},
        "/s/[this]/wizard/webhosting/installatron/kickoff": {url:"http://monster/installatron/index.php?s=sdsdfsdf"},
        "/s/installatronserver/su/installatron/guixfer": {url:"http://monster/installatron/index.php?s=superusersession"},

        "/s/s1/webhosting/12345/tallywarning": "ok",
        "/s/s1/su/webhosting/php/fpm/config": "global ok",
        "/s/s1/su/webhosting/12345/php/fpm/config": "single ok",
        "/s/s1/su/webhosting/php/fpm/rehash": "ok, all servers rehashed",
        "/s/s1/su/webhosting/php/fpm/rehash/5.6": "ok version 5.6 rehashed",

        "/s/s1/wizard/webhosting/expired": "ok",
        "/s/s1/wizard/webhosting/php": "ok",
        "/tapi/domain/foobar.hu": {"hash":["somehash"]},
        "/tapi/domain/femforgacs.hu": {"hash": ["1234"]}, // "template": "nonamedomain.hu"...
        "/tapi/domain/maindomain.hu": {"hash": ["1234"]}, // "template": "nonamedomain.hu"...

        "/su/accountapi/config/sms/seeme/credentials":"ok",
        "/su/accountapi/account/1/domains": "ok",
        "/su/accountapi/account/2/credential/dnsonly/password": "ok",
        "/su/accountapi/account/2/credential/main/password": "ok",
        "/su/accountapi/account/2/credential/main": "ok",
        "/su/accountapi/accounts/lookup/id": {
           "14851":"Cert user",
           "41234": "Ftp demo user",
           "23456": "Git Demo user",
           "123456": "Webhosting demo user",
           "3123": "Dbms demo user",
           "51001":"email user",
           "50001":"iptables user",
           "52001":"docroot user",
           "53001":"Docker user",
           "61001":"cron demo user"
        },
        "/su/accountapi/authentication/switch_user": {"token":"token_by_switch","expires_at":"2016-12-12 12:30:00","u_name":"Username 2","u_id":2,"isLoggedIn":true},
        "/su/accountapi/accounts/count": {"count": 3},
        "/su/accountapi/accounts/search": {"accounts":[{"created_at":"2016-07-01 10:10:10","u_id": 1, "u_deleted": 1, "u_name": "username1", "u_primary_email": "email@email1.ee"},{"u_id": 2, "u_name": "username2", "u_primary_email": "email@email2.ee","u_comment":"problematic customer"},{"u_id": 3, "u_name": "username3", "u_primary_email": "email@email3.ee"}]},
        "/su/accountapi/accounts/by-role/SERVER_OWNER": {"accounts":[{"u_id": 11, "u_name": "serverowner1"},{"u_id": 12, "u_name": "serverowner2"}]},
        "/su/accountapi/account/3": "ok",
        "/su/accountapi/account/2/force_password": "ok",
        "/su/accountapi/loginlog/count": mockedLoginlogResponse.count,
        "/su/accountapi/loginlog/search": mockedLoginlogResponse.search,
        "/su/accountapi/webhostingtemplate/WEB10000": "ok",

        "/su/accountapi/server/therapy": "ok",
        "/su/accountapi/server/s2": "ok",

        "/s/s1/su/eventapi/events/count": mockedEventlogResponse.count,
        "/s/s1/su/eventapi/events/search": mockedEventlogResponse.search,

        "/s/s1/webhosting/12345/python/foobar/restart": "ok",
        "/s/s1/webhosting/12345/python/foobar/requirements": {"id":"pythonreq"},

        "/s/s1/iptables/whextra/12345/firewallrules/logging":{id:"firewallrules_logging"},

        "/s/s1/su/webhosting/lookup/id": {
           "16387": {name:"Certificate store", user: "14581"},
           "12345": {name:"This is a demo webhosting", user: "123456"},
           "52001":{name:"Webhosting for e-mails", user: "51001"},
           "53001":{name:"Iptables", user: "50001"},
           "22001":{name:"Docroot", user: "52001"},
           "23001":{name:"Docker", user: "53001"},
        },

        "/s/s1/su/webhosting/12345/walk": {id:"walk-id"},
        "/s/s1/su/webhosting/12345/walk-and-store": {id:"walk-and-store-id"},

        "/s/s1/su/webhosting/12345": "ok",

        "/s/s1/webhosting/12345/szar/checkpoints/web": ["20170401010604", "20170331010606", "20170210020803"],
        "/s/s1/webhosting/12347/szar/checkpoints/web": ["20170401010604", "20170331010606", "20170210020803"],
        "/s/s1/su/webhosting/mc/szar/checkpoints": ["s1-20170401010604", "s1-20170331010606", "s1-20170210020803"],
        "/s/s2/su/webhosting/mc/szar/checkpoints": ["s2-20170401010604", "s2-20170331010606", "s2-20170210020803"],

        "/s/s1/su/webhosting/wie/szar/restore/12345": {id:"szar-backup-wie-webstore"},
        "/s/s1/su/webhosting/wie/szar/restore/": {id:"szar-backup-wie-complete"},

        "/s/s1/su/webhosting/12345/szar/backup/web": {id:"szar-backup-website"},
        "/s/s1/su/webhosting/12345/szar/backup/mail": {id:"szar-backup-website"},
        "/s/s1/su/webhosting/szar/backup/web": {id:"szar-backup-all"},
        "/s/s1/su/webhosting/szar/backup/mail": {id:"szar-backup-all"},
        "/s/s1/webhosting/12345/szar/restore/web": {id:"szar-restore-web"},
        "/s/s1/webhosting/12347/szar/restore/web": {id:"szar-restore-web"},
        "/s/s1/su/webhosting/12345/szar/restore/web": {id:"szar-restore-web"},
        "/s/s1/su/webhosting/12345/szar/restore/mail": {id:"szar-restore-mail"},


        "/s/s1/su/webhosting/accesslog/all/process": "ok",
        "/s/s1/webhosting/accesslog/by-webhosting/12345/count": {count:2},
        "/s/s1/webhosting/accesslog/by-webhosting/12345/search": accessLogResponse,
        "/s/s1/su/webhosting/accesslog/all/count": {count:2},
        "/s/s1/su/webhosting/accesslog/all/search": accessLogResponse,
        "/s/s1/dbms/slowlog/by-webhosting/12345/search": dbmsSlowLogResponse,
        "/s/s1/su/dbms/slowlog/all/count": {count:1},
        "/s/s1/su/dbms/slowlog/all/search": dbmsSlowLogResponse,
        "/s/s1/webhosting/slowlog/by-webhosting/12345/count": {count:1},
        "/s/s1/webhosting/slowlog/by-webhosting/12345/search": slowLogResult,
        "/s/s1/su/webhosting/slowlog/all/count": {count:1},
        "/s/s1/su/webhosting/slowlog/all/search": slowLogResult,

        "/s/s1/su/dbms/databases/12345/something/repair": {id:"dbms-repair-perdb"},
        "/s/s1/su/dbms/dbms/mysql/repair":{id:"dbms-repair-full"},
        "/s/s1/su/dbms/databases/dbbackup": {id: "dbbackup-full"},
        "/s/s1/su/dbms/databases/12345/dbbackup": {id: "dbbackup-wh"},
        "/s/s1/dbms/tasks/database-import/input": "ok\n",

        "/s/s1/dbms/databases/12345/supported": ["mysql"],
        "/s/s1/dbms/databases/12345/something/repair": {id:"dbms-repair-perdb"},
        "/s/s1/dbms/databases/12345/something/export": {id:"database-export"},
        "/s/s1/dbms/databases/12345/something/import": {id:"database-import"},
        "/s/s1/su/dbms/databases/12345/mm_monstermedia/export": {id:"database-export"},
        "/s/s1/su/dbms/databases/12345/mm_monstermedia/import": {id:"database-import"},
        "/s/s2/su/dbms/databases/12345/mm_monstermedia/import": {id:"database-import"},
        "/s/s1/su/dbms/databases/12345/something/user/something/lock":"ok",
        "/s/s1/su/dbms/databases/12345/something/user/something/unlock":"ok",
        "/s/s1/su/dbms/dbms/mysql": "ok",
        "/s/s1/su/dbms/dbms/mysql/test": "ok",
        "/s/s1/su/dbms/stats/12345": "ok",
        "/s/s1/su/dbms/stats/get-and-process":"ok",

        "/s/s1/docrootapi/docroots/webapps/12345/": "ok",
        "/s/s1/docrootapi/docroots/webapps/12345/somewebapp1.hu": "ok",
        "/s/s1/su/docrootapi/apache/12345/femforgacs.hu/hooks": "ok",
        "/s/s1/su/docrootapi/nginx/12345/femforgacs.hu/hooks": "ok",

        "/s/s1/docrootapi/vts/12345/search2": vtsResponseMin2,
        "/s/s1/docrootapi/vts/22001/search2": vtsResponseMin2,
        "/s/s1/su/docrootapi/vts/search2": vtsResponseFull2,
        "/s/s1/su/docrootapi/vts/compress": "ok",
        "/s/s1/su/docrootapi/vts/process": "ok",
        "/s/s1/su/docrootapi/vts/vts-url": {url:"https://something.somewhere.hu/nginx-vts"},
        "/s/s1/su/docrootapi/vts/mrtg-url": {url:"https://something.somewhere.hu/mrtg"},

        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/httpsonly": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/hstsheaders": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/anticlickjackingheaders": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/skipnginxstaticfiles": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/nginxgzip": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/forceredirect": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/djangostatic": "ok",
        "/s/s1/docrootapi/docroots/12345/femforgacs.hu/dontblockxmlrpc": "ok",
        "/s/s1/docrootapi/docroots/16387/certificate.hu/certificate": "ok",
        "/s/s1/docrootapi/docroots/16387/www.monstermedia.hu/certificate": "ok",
        "/s/s1/su/docrootapi/docroots/16387/certificate.hu/certificate": "ok",
        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/timeout": "ok",
        "/s/s1/su/docrootapi/awstats/process": {id:"awstats-process-1"},
        "/s/s1/su/docrootapi/awstats/build": {id:"awstats-build-1"},
        "/s/s1/su/docrootapi/awstats/process-and-build": {id:"awstats-process-and-build-1"},
        "/s/s1/su/docrootapi/awstats/12345/femforgacs.hu/process": {id:"awstats-process-1"},
        "/s/s1/su/docrootapi/awstats/12345/femforgacs.hu/build": {id:"awstats-build-1"},
        "/s/s1/docrootapi/awstats/12345/femforgacs.hu": "ok",

        "/s/s1/su/docrootapi/apache/rehash": {"code":0, "stdout":"response from apache webserver", "stderr":"some warning"},
        "/s/s1/su/docrootapi/nginx/rehash": {"code":0, "stdout":"response from nginx webserver", "stderr":"some warning"},

        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/postdata": "ok, set",
        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/expiredroot": "ok, expired root has changed",
        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/suspend": "ok, suspended",
        "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/primary": "ok, set as primary",

        "/s/s1/su/docrootapi/cleanup/maintane": "ok, maintaned",
        "/s/s1/su/docrootapi/cleanup/webhostings/rebuild": "ok, every docroot is rebuilt",
        "/s/s1/su/docrootapi/cleanup/redirects/rebuild": "ok, every http redirect is rebuilt",
        "/s/s1/su/docrootapi/cleanup/frames/rebuild": "ok, every frame redirect is rebuilt",
        "/s/s1/su/docrootapi/cleanup/webhosting/12345/rebuild": "ok, webhosting rebuilt",

        "/s/s1/ftp/sites/12345/acls": "ok",
        "/s/s1/ftp/sites/12345/admin-login": "ok",
        "/s/s1/ftp/sites/12345/public": "ok",

        "/s/s1/git/12345/id/1/branch/dev/log": {id: "1234gitlog"},
        "/s/s1/git/12345/id/1/branch/dev/pull": {id: "1234pull"},
        "/s/s1/git/12345/id/1/branch/dev/clone": {id: "1234clone"},
        "/s/s1/git/12345/id/1/branch/dev/checkout": {id: "1234checkout"},
        "/s/s1/git/12345/id/1/branch/dev/clean": {id: "1234clean"},
        "/s/s1/git/12345/id/1/branch/dev/revert": {id: "1234revert"},

        "/s/s1/git/12345/ssh": "ok",

        "/s/s1/git/12345/id/1/branch/dev/setdocroot": "ok",
        "/s/s1/git/12345/id/1/": "ok",
        "/s/s1/git/12345/id/1/branch/dev/": "ok",

        "/s/s1/su/fileman/tests/longread-upload": {"id": "upload"},
        "/s/s1/su/fileman/tests/longread-simple": {"id": "remove"},
        "/s/s1/su/fileman/tests/download": {"id": "download"},
        "/s/s1/su/fileman/tests/upload": {"id":"upload"},

        "/s/s1/su/fileman/12345/cleanup": {"id": "cleanup"},
        "/s/s2/su/fileman/12345/cleanup": {"id": "cleanup"},
        "/s/s2/su/fileman/20001/cleanup": {"id": "cleanup"},
        "/s/s1/su/fileman/16384/cleanup": {"id": "cleanup"},
        "/s/s1/su/fileman/12345/download": {"id": "download"},
        "/s/s2/su/fileman/12345/download": {"id": "download"},

        "/s/s1/fileman/12345/fetch": {"data": "hello there"},

        "/s/s2/fileman/20001/untgz/transfer": {"id": "transfer"},
        "/s/s1/fileman/12345/untar/transfer": {"id": "transfer"},
        "/s/s2/fileman/12345/untgz/transfer": {"id": "transfer"},
        "/s/s1/fileman/16384/untgz/transfer": {"id": "transfer"},
        "/s/s1/fileman/12345/move": {"id": "move"},
        "/s/s1/fileman/12345/copy": {"id": "copy"},
        "/s/s1/fileman/12345/mkdir": {"id": "mkdir"},
        "/s/s1/fileman/12345/remove": {"id": "remove"},
        "/s/s1/fileman/12347/remove": {"id": "remove"},
        "/s/s1/fileman/12345/download": {"id": "download"},
        "/s/s1/fileman/12345/tar": {id:"tar"},
        "/s/s1/fileman/12345/tgz": {id:"tar"},
        "/s/s1/fileman/12345/zip": {id:"zip"},
        "/s/s1/su/fileman/12345/tgz-all":{id:"tar"},
        "/s/s1/fileman/tasks/untar": "Untar file task\n::: mc task status: successful :::\n",
        "/s/s1/fileman/12345/untar": {id:"untar"},
        "/s/s1/fileman/tasks/unzip": "Unzip file task\n::: mc task status: successful :::\n",
        "/s/s1/fileman/12345/unzip": {id:"unzip"},
        "/s/s1/fileman/tasks/upload/input": "ok",
        "/s/s1/fileman/12345/upload": {id:"upload"},
        "/s/s1/fileman/12345/toreadwrite": {id: "toreadwrite"},
        "/s/s1/fileman/12345/toreadonly": {id: "toreadonly"},
        "/s/s2/su/fileman/22345/toreadonly": {id: "toreadonly"},
        "/s/s2/su/fileman/20001/toreadonly": {id: "toreadonly"},

        // this is used in the wie restoration examples
        "/s/s1/su/fileman/12345/list": dbbackupFilemanList,
        "/s/s2/su/fileman/12345/list": dbbackupFilemanList,

        "/s/s1/fileman/12345/list": [ { name: 'hello.txt',
    perm: '-rw-rw-rw-',
    mtime: '2017-02-12T14:32:39.622Z',
    size: 5 },
    { 
    name: 'f3ad8867d9ffa9cc59cab3ffbb782871-php-fpm.slow.log',
    perm: '-rw-rw-rw-',
    mtime: '2017-02-12T14:32:39.622Z',
    size: 123412 
    },
  { name: 'subdir',
    perm: 'drw-rw-rw-',
    mtime: '2017-02-12T14:32:59.039Z',
    size: 0 },
  { name: 'world.txt',
    perm: '-rw-rw-rw-',
    mtime: '2017-02-12T14:32:47.389Z',
    size: 5 } ],

        "/s/s1/fileman/12347/list": [ { name: 'hello12347.txt',
    perm: '-rw-rw-rw-',
    mtime: '2017-02-12T14:32:39.622Z',
    size: 5 },
  { name: 'subdir',
    perm: 'drw-rw-rw-',
    mtime: '2017-02-12T14:32:59.039Z',
    size: 0 },
  { name: 'world.txt',
    perm: '-rw-rw-rw-',
    mtime: '2017-02-12T14:32:47.389Z',
    size: 5 } ],


        "/su/tapi/domain/femforgacs.hu/lock": "ok",
        "/su/tapi/domain/femforgacs.hu/unlock": "ok",
        "/su/tapi/build": "ok",

        "/su/sync/tapi": "ok",
        "/s/s1/su/sync/tapi": "ok",
	},
	"GET": {
     "/s/s1/su/docker/root/container/new_container_id4/config": {config: {"foo":"bar"}},
     "/s/s1/su/docker/root/container/new_container_id4/inspect": dockerContainer,

     "/s/s1/docker/webhosting/12345/ssh": {"authz": "ssh-rsa sdfsdf foo@bar\n", "targetContainer": "new_container_id4"},
     "/s/s1/docker/webhosting/12345/container/new_container_id4/info": dockerContainer,
     "/s/s1/su/docker/root/image/sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543/inspect": dockerImage1,
        "/s/s1/su/docker/root/images/list/upgradable": [ { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Names: [ '/faporing_leavitt' ],
    PrimaryName: "faporing_leavitt",
    Labels: { 'com.monster-cloud.mc-app': 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781,
    PrimaryTag: 'alpine-3.7-dropbear:2018-06-28--1' ,
    upgradeableTo: "alpine-3.7-dropbear:2018-06-28--2"
    } ],
    "/s/s1/su/docker/root/containers/list/upgradable": [ { Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
    Names: [ '/adoring_leavitt' ],
    PrimaryName: "adoring_leavitt",
    Image: 'alpine-3.7-dropbear:2018-06-28--1',
    ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    Command: '/bin/sh',
    Created: 1529237902,
    Ports: [],
    Labels: {},
    State: 'running',
    Status: 'Up 31 seconds',
    HostConfig: { NetworkMode: 'default' },
    NetworkSettings: { Networks: [ { foo: 'bar' } ] },
    Mounts: [],
    "c_user_id": "53001",
    "c_webhosting": "23001",
    upgradeableTo: 'alpine-3.7-dropbear:2018-06-28--2' } ],
     "/pub/captcha/v2": captchaResponse,
     "/account/captcha/v2": captchaResponse,
                "/pub/account/callback_request": {"supported": true},
                "/pub/account/loginlog/subsystems": ["accountapi", "ftp", "email"],
                "/pub/captcha/expected": {"login": false, "forgotten": true, "registration": true},
                "/pub/account/country_codes": {"BD":"880","BE":"32","BF":"226","BG":"359","BA":"387","BB":"+1-246","WF":"681","BL":"590","BM":"+1-441","BN":"673","BO":"591","BH":"973","BI":"257","BJ":"229","BT":"975","JM":"+1-876","BW":"267","WS":"685","BQ":"599","BR":"55","BS":"+1-242","JE":"+44-1534","BY":"375","BZ":"501","RU":"7","RW":"250","RS":"381","TL":"670","RE":"262","TM":"993","TJ":"992","RO":"40","TK":"690","GW":"245","GU":"+1-671","GT":"502","GR":"30","GQ":"240","GP":"590","JP":"81","GY":"592","GG":"+44-1481","GF":"594","GE":"995","GD":"+1-473","GB":"44","GA":"241","SV":"503","GN":"224","GM":"220","GL":"299","GI":"350","GH":"233","OM":"968","TN":"216","JO":"962","HR":"385","HT":"509","HU":"36","HK":"852","HN":"504","HM":" ","VE":"58","PR":"+1-787 and 1-939","PS":"970","PW":"680","PT":"351","SJ":"47","PY":"595","IQ":"964","PA":"507","PF":"689","PG":"675","PE":"51","PK":"92","PH":"63","PN":"870","PL":"48","PM":"508","ZM":"260","EH":"212","EE":"372","EG":"20","ZA":"27","EC":"593","IT":"39","VN":"84","SB":"677","ET":"251","SO":"252","ZW":"263","SA":"966","ES":"34","ER":"291","ME":"382","MD":"373","MG":"261","MF":"590","MA":"212","MC":"377","UZ":"998","MM":"95","ML":"223","MO":"853","MN":"976","MH":"692","MK":"389","MU":"230","MT":"356","MW":"265","MV":"960","MQ":"596","MP":"+1-670","MS":"+1-664","MR":"222","IM":"+44-1624","UG":"256","TZ":"255","MY":"60","MX":"52","IL":"972","FR":"33","IO":"246","SH":"290","FI":"358","FJ":"679","FK":"500","FM":"691","FO":"298","NI":"505","NL":"31","NO":"47","NA":"264","VU":"678","NC":"687","NE":"227","NF":"672","NG":"234","NZ":"64","NP":"977","NR":"674","NU":"683","CK":"682","CI":"225","CH":"41","CO":"57","CN":"86","CM":"237","CL":"56","CC":"61","CA":"1","CG":"242","CF":"236","CD":"243","CZ":"420","CY":"357","CX":"61","CR":"506","CW":"599","CV":"238","CU":"53","SZ":"268","SY":"963","SX":"599","KG":"996","KE":"254","SS":"211","SR":"597","KI":"686","KH":"855","KN":"+1-869","KM":"269","ST":"239","SK":"421","KR":"82","SI":"386","KP":"850","KW":"965","SN":"221","SM":"378","SL":"232","SC":"248","KZ":"7","KY":"+1-345","SG":"65","SE":"46","SD":"249","DO":"+1-809 and 1-829","DM":"+1-767","DJ":"253","DK":"45","VG":"+1-284","DE":"49","YE":"967","DZ":"213","US":"1","UY":"598","YT":"262","UM":"1","LB":"961","LC":"+1-758","LA":"856","TV":"688","TW":"886","TT":"+1-868","TR":"90","LK":"94","LI":"423","LV":"371","TO":"676","LT":"370","LU":"352","LR":"231","LS":"266","TH":"66","TG":"228","TD":"235","TC":"+1-649","LY":"218","VA":"379","VC":"+1-784","AE":"971","AD":"376","AG":"+1-268","AF":"93","AI":"+1-264","VI":"+1-340","IS":"354","IR":"98","AM":"374","AL":"355","AO":"244","AS":"+1-684","AR":"54","AU":"61","AT":"43","AW":"297","IN":"91","AX":"+358-18","AZ":"994","IE":"353","ID":"62","UA":"380","QA":"974","MZ":"258"},
                "/pub/account/email_categories": ["TECH", "PROMOTION", "BILLING", "OTHER"],
                "/account": {
                  "u_id":"100",
                  "u_name":"u_name",
                  "u_primary_email":"email100@email.ee",
                  "u_primary_email_stripped":"email100@emailee",
                  "u_primary_tel":"+36-30-1234567",
                  "u_primary_tel_confirmed":0,
                  "u_primary_email_confirmed":0,
                  "u_comment":"",
                  "u_deleted":0,
                  "u_grant_access_for":"0",
                  "created_at":"2016-08-04 11:16:08",
                  "updated_at":"2016-08-04 11:16:08",
                  "u_email_login_fail":1,
                  "u_email_login_ok_subsystems": ["ftp"],
                },
                "/account/tels": [
                   {"t_user_id":"100","t_country_code":"36","t_area_code":"30","t_phone_no":"1234567","t_human":"+36-30-1234567","t_confirmed":0,"t_category": "HOME","t_primary": 0},
                   {"t_user_id":"100","t_country_code":"36","t_area_code":"30","t_phone_no":"7654321","t_human":"+36-30-7654321","t_confirmed":1,"t_category": "MOBILE","t_primary": 1},
                ],
                "/account/emails": [
                   {"e_user_id":"100","e_email":"foo1@bar.hu","e_confirmed":1,"e_categories": ["TECH","BILLING","PROMOTION","OTHER"],"e_primary": 1},
                   {"e_user_id":"100","e_email":"foo2@bar.hu","e_confirmed":0,"e_categories": ["PROMOTION","OTHER"],"e_primary": 0},
                   {"e_user_id":"100","e_email":"foo3@bar.hu","e_confirmed":1,"e_categories": ["OTHER"],"e_primary": 0},
                ],
                "/account/credentials": credentialsResponse,

                "/account/webhostingdomains": webhostingDomainsResponse,
                "/su/accountapi/webhostingdomains/": webhostingDomainsResponse,
                "/su/accountapi/config/sms/providers": ["seeme"],
                "/su/accountapi/config/sms/seeme/credentials": {configured:true},
                "/su/accountapi/account/2": {
                        'u_email_login_ok': 0,
                        'u_name': 'Monster Media',
                        'u_language': 'hu',
                        'u_sms_notification': 1,
                        'created_at': '2010-03-23 17:55:00',
                        'u_primary_tel_confirmed': 0,
                        'u_primary_tel': '+36-30-2679426',
                        'u_deleted': 0,
                        'u_id': '2',
                        'u_email_login_fail': 1,
                        'u_grant_access_for': '0',
                        'u_primary_email_stripped': 'info@monstermediahu',
                        'u_callin': '',
                        'updated_at': '2016-09-25 09:18:28',
                        'u_primary_email': 'info@monstermedia.hu',
                        'u_primary_email_confirmed': 1,
                        'u_comment': ''
                },
                "/su/accountapi/account/2/credentials": credentialsResponse,
                "/account/credentials/supported": [ "ACCOUNT_DNS" ],

                "/account/account_grants/for": [ // for you
                   {"u_id":21, "u_name":"Your customer #1","u_primary_email":"super1@1234.hu"},
                   {"u_id":22, "u_name":"Your customer #2","u_primary_email":"super2@1234.hu"},
                ],
                "/account/account_grants/to": [// to yours
                   {"u_id":123,"u_name":"Your referer","u_primary_email":"supervisor@1234.hu"},
                ],

                "/account/domains": domainsResponse,

                "/my/services/webhostings/details": [
                    {w_server_name:"s1",w_webhosting_id:12345},
                    {w_server_name:"s1",w_webhosting_id:16384}
                ],
                "/my/services/webhostings/details": [
                    {w_server_name:"s1",w_webhosting_id:12345,wh:{wh_id:12345, wh_name: "This works with fileman"}},
                    {w_server_name:"s1",w_webhosting_id:16384,wh:{wh_id:16384, wh_name: "Name of this amazing webstore"}}
                ],

                "/s/s1/su/webhosting/system/bin/df": {
                   output: `
Filesystem     1M-blocks   Used Available Use% Mounted on
rootfs              1895    834      1062  44% /
devtmpfs            8003      0      8003   0% /dev
tmpfs               1602      1      1602   1% /run
/dev/md0            1895    834      1062  44% /
/dev/md2            9518   6924      2595  73% /usr
tmpfs                  5      0         5   0% /run/lock
tmpfs               3967      0      3967   0% /run/shm
/dev/md1             179     20       151  12% /boot
/dev/md4            4753   3902       852  83% /home
/dev/md3             948     72       877   8% /tmp
/dev/md5            7614   6095      1520  81% /var
/dev/md0            1895    834      1062  44% /etc/tinydns/root
/dev/md126        445059 303338    141721  69% /web
/dev/loop0          2038   1347       692  67% /lxc
`
                },

                "/s/s1/su/webhosting/system/proc/partitions": {
                   output: `
major minor  #blocks  name

   8        0  250059096 sda
   8       48  732574584 sdd
   8       49    1951744 sdd1
   8       50     195584 sdd2
   8       51          1 sdd3
   8       53    1951744 sdd5
   8       54    9764864 sdd6
   8       55     975872 sdd7
   8       56    4881408 sdd8
   8       57    7811072 sdd9
   8       58  705036120 sdd10
   8       64  976762584 sde
   8       65    1951744 sde1
   8       66     195584 sde2
   8       67          1 sde3
   8       69    1952768 sde5
   8       70    9765888 sde6
   8       71     976896 sde7
   8       72    4882432 sde8
   8       73    7812096 sde9
   8       74  949217280 sde10
   8       32  250059096 sdc
   8       16  241696768 sdb
   9      127  741814848 md127
   9        0    1950656 md0
   9        4    4877248 md4
   9        2    9756544 md2
   9        1     195392 md1
   9        5    7806912 md5
   9        3     975296 md3
   7        0    2097152 loop0
   7        1     132040 loop1
   7        2      62456 loop2
   9      126  455962488 md126
`,
                   disks: ["sda", "sdb"]
                },

                "/s/s1/su/webhosting/system/proc/mdstat": {
                   output: `
Personalities : [raid0] [raid1] 
md126 : active raid1 md127[3] sde10[2](W)
      455962488 blocks super 1.2 [2/2] [UU]
      
md3 : active raid1 sde7[2] sdd7[3]
      975296 blocks super 1.2 [2/2] [UU]
      
md5 : active raid1 sde9[2] sdd9[3]
      7806912 blocks super 1.2 [2/2] [UU]
      
md1 : active raid1 sde2[2] sdd2[3]
      195392 blocks super 1.2 [2/2] [UU]
      
md2 : active raid1 sde6[2] sdd6[3]
      9756544 blocks super 1.2 [2/2] [UU]
      
md4 : active raid1 sde8[2] sdd8[3]
      4877248 blocks super 1.2 [2/2] [UU]
      
md0 : active raid1 sde1[2] sdd1[3]
      1950656 blocks super 1.2 [2/2] [UU]
      
md127 : active raid0 sda[1] sdc[0] sdb[2]
      741814848 blocks super 1.2 64k chunks
      
unused devices: <none>
`
                },


                "/s/s1/su/webhosting/system/bin/smartctl/sda": {
                   output: smartOutput
                },
                "/s/s1/su/webhosting/system/bin/smartctl/sdb": {
                   output: smartOutput
                },
                "/s/s1/su/webhosting/system/proc/swaps": {
                   output: `
Filename                                Type            Size    Used    Priority
/dev/sdd5                               partition       1951740 205752  -1
/dev/sde5                               partition       1952764 0       -2
`
                },

                "/s/s2/webhosting/12345/domains": [
                   "wietest.hu"
                ],
                "/s/s2/webhosting/22345/domains": [
                   "wietest.hu"
                ],
                "/s/s2/webhosting/20001/domains": [
                   "wietest.hu",
                   "123.foo.svc"
                ],

                "/s/s1/webhosting/12345/domains": [
                   "femforgacs.hu"
                ],

                "/s/s1/su/timemachine/list": [1523200241, 1523200300, 1523200341],
                "/s/s1/su/timemachine/fetch/1523200341": {"dbms": {"mysql3": dbmsScoreboard}},
                "/s/s1/su/timemachine/fetch/1523200300": {"dbms": {"mysql2": dbmsScoreboard}},
                "/s/s1/su/timemachine/fetch/1523200241": {"dbms": {"mysql1": dbmsScoreboard}},


                "/s/s1/su/wie/webstores/": [12345],
                "/s/s1/su/wie/config/subsystems/": ["webhosting", "git", "ftp"],
                "/s/s2/su/wie/config/subsystems/": ["webhosting", "git", "ftp"],
                "/s/s1/su/wie/fetch/": {12345:{webhosting: {"some":"setting"}, git: {"some": "setting"}}},
                "/s/s1/su/wie/fetch/12345": {webhosting: {"some":"setting"}, git: {"some": "setting"}},

                "/s/s1/su/iptables/whextra/": [
                  {
                    webhosting_id: 53001, fw_dst_ip: "123.123.123.123", fw_dst_port: 8080, fw_action: "ACCEPT",
                  }
                ],
                "/s/s1/su/iptables/bans/": {
                    "HTTP": {
                      "123.123.123.123":{ added: 1505049759, ban_duration: 3600},
                    },
                },
                "/s/s1/su/cert/config/resellers": {resellers:["letsencrypt","namecheap"]},
                "/s/s1/su/cert/config/resellers/letsencrypt/vendors": {vendors:["Let's Encrypt"]},
                "/s/s1/su/cert/config/resellers/namecheap/vendors": {vendors:["Comodo"]},
                "/s/s1/su/cert/config/contacts/default": {co_id:1},
                "/s/s1/su/cert/contacts/all/": certContactsResponse,
                "/s/s1/su/cert/config/namecheap/credentials":{
                   ApiUser: "user1",
                   UserName: "user2"
                },
                "/s/s1/su/cert/config/balances": {namecheap:{AvailableBalance: 29.86,Currency:'USD',_full:{account:{     ApiUser: "user1",
     UserName: "user2"
  }, balance:{ Currency: 'USD',
  AvailableBalance: '29.86',
  AccountBalance: '29.86',
  EarnedAmount: '0.00',
  WithdrawableAmount: '0.00',
  FundsRequiredForAutoRenew: '0.00' }}}},
                "/s/s1/cert/purchases/my/": {
                   purchases: [purchase_info]
                },
                "/s/s1/cert/purchases/my/item/abcd1233/": purchase_info,
                "/s/s1/cert/config/countries": {"HU":"Hungary"},
                "/s/s1/cert/contacts/my/": certContactsResponse,
                "/s/s1/cert/purchases/my/item/abcd1233/activation": {
                         a_id: 1,
                         a_user_id: 14851,
                         a_purchase: "abcd1233",
                         a_payload: {ApproverEmail: "some@foobar.hu"},
                         a_result: {"todo":"foobar"},
                         created_at: "2017-01-01 01:01:01"
                },
                "/s/s1/su/cert/purchases/all/item/abcd1233/activations": {
                    activations:[
                      {
                         a_id: 1,
                         a_user_id: 14851,
                         a_purchase: "abcd1233",
                         a_status: "failed",
                         a_payload: {domain: "foobar.hu"},
                         created_at: "2017-01-01 01:01:01"
                      }
                    ]
                },

                "/s/s1/su/docker/root/webstores/config": {
                    "foo": "bar",
                },

                "/s/s1/docker/tasks/docker-rebuild": "Rebuilding Docker containers\n::: mc task status: successful :::\n",
                "/s/s1/docker/tasks/docker-upgrade": "Upgrading Docker containers\n::: mc task status: successful :::\n",
                "/s/s1/docker/tasks/docker-cleanup": "Cleaning old Docker images\n::: mc task status: successful :::\n",
                "/s/s1/docker/tasks/docker-pull": "Pulling image from Docker registry\n::: mc task status: successful :::\n",
                "/s/s1/cert/tasks/activation": "Activation output\n::: mc task status: successful :::\n",
                "/s/s1/cert/tasks/reissue": "Reissue output\n::: mc task status: successful :::\n",

                "/s/s1/cert/tasks/letsencrypt": "Enrolling Let's Encrypt certificate\n::: mc task status: successful :::\n",
                "/s/s1/cert/tasks/letsencrypt-renew": "Renewing Let's Encrypt certificates\n::: mc task status: successful :::\n",
                "/s/s1/cert/tasks/poll_id": "Polling certificates\n::: mc task status: successful :::\n",
                "/s/s1/su/cert/certificates/by-webhosting/16387/item/123123": full_cert,
                "/s/s1/cert/certificates/by-webhosting/16387/item/123123": full_cert,
                "/s/s1/cert/certificates/by-webhosting/12345/item/123123": full_cert,
                "/s/s1/cert/certificates/my/item/123123": full_cert,

                "/s/s1/tasks/webstore-removal": "Removal of webstore\n::: mc task status: successful :::\n",
                "/tasks/webstore-removal-with-bindings": "Removal of webstore along with bindings\n::: mc task status: successful :::\n",


                "/s/s1/cert/certificates/my/": {
                  canBeAdded: true,
                  certificates: [
                     cert1,
                     cert2,
                  ]
                },

                "/s/s1/cert/certificates/by-webhosting/12345/": {
                  canBeAdded: true,
                  certificates: [
                     cert1,
                  ]
                },

                "/s/s1/su/email/dkim/": ["femforgacs.hu", "eurofarm1.monstermedia.hu"],
                "/s/s1/email/dkim/femforgacs.hu": {"host_prefix":"test._domainkey","dkim_key_active":false,"selector":"test","dns_dkim_record_present":false,"full_bind":"test._domainkey.test.monstermedia.hu. 3600 TXT (\n  \"v=DKIM1; p=\"\n  \"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSN\"\n  \"efNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08W\"\n  \"Gu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux3\"\n  \"5WnpGdOphYr7v6D90QIDAQAB\"\n\n)","full_hostname":"test._domainkey.test.monstermedia.hu","raw_txt_value":"v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSNefNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08WGu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux35WnpGdOphYr7v6D90QIDAQAB"},
                "/wizard/dkim/s1/femforgacs.hu": {"host_prefix":"test._domainkey","dkim_key_active":false,"selector":"test","dns_dkim_record_present":false,"full_bind":"test._domainkey.test.monstermedia.hu. 3600 TXT (\n  \"v=DKIM1; p=\"\n  \"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSN\"\n  \"efNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08W\"\n  \"Gu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux3\"\n  \"5WnpGdOphYr7v6D90QIDAQAB\"\n\n)","full_hostname":"test._domainkey.test.monstermedia.hu","raw_txt_value":"v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSNefNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08WGu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux35WnpGdOphYr7v6D90QIDAQAB"},

                "/s/s1/su/email/postfix/mailq-auto/": [{mc_id: 1, mc_email:"test@test.tt", mc_type: "recipient"}],
                "/s/s1/su/email/cluebringer/policies/outbound/SASL/members": [
                  "foo1@bar.hu",
                  "foo2@bar.hu",
                ],
                 "/s/s1/su/email/cluebringer/policies/outbound/SASL/quotas": [
                   {Interval:"daily",Limit:100,Message:"szaz"},
                   {Interval:"hourly",Limit:10,Message:"tiz"}
                 ],
                "/s/s1/su/email/cluebringer/policies/outbound/": [
                   {Name: "SASL", DefaultPolicy: true, "Description":"Default policy"},
                   {Name: "high2000", DefaultPolicy: false, "Description":"These users can send 2000 emails per day"}
                ],
                "/s/s1/su/email/webhostings/12345/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/aliases": {
                   aliases: [],
                   canBeAdded: true,
                },
                "/s/s1/email/webhostings/12345/domains/domain/12345.s1.svc/account/4/cluebringer/quotas": cluebringerQuotasResponse,
                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1/cluebringer/quotas": cluebringerQuotasResponse,

                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1/cluebringer/sessions": clueBringerSessionsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/12345.s1.svc/account/4/cluebringer/sessions": clueBringerSessionsResponse,
                "/s/s1/su/email/cluebringer/groups": [
                    {Name:"internal_ips"}, {Name:"internal_domains"}, {Name:"greylist_domains"},
                ],
                "/s/s1/su/email/cluebringer/groups/internal_ips": [
                  {Member: "@domain.hu"}, {Member: "$some@email.address"}
                ],
                "/s/s1/su/email/postfix/mailq/8B164258D909": {id:"postfix-fetch-email"},
                "/s/s1/email/postfix/mailq/by/domain/somealias.hu": mailqResponse,
                "/s/s1/email/postfix/mailq/by/domain/femforgacs.hu": mailqResponse,
                "/s/s1/email/postfix/mailq/by/webhosting/12345": mailqResponse,
                "/s/s1/su/email/postfix/mailq": mailqResponse,
                "/s/s1/su/email/amavis/msgs/quarantine/id": {id: "download-task-id"},
                "/s/s1/su/email/amavis/msgs/quarantine": [
                  {"mail_id":"id","time_iso":"2017-05-23 11:33:37","client_addr":"89.135.46.202","size":12312,"spam_level":25.1,"from_addr":"some@email.tld","subject":"get viagra"}
                ],
                "/s/s1/su/email/bccs": bccsResponse,
                "/s/s1/su/email/domains": emailDomainsResponse,
                "/s/s1/email/webhostings/12345/domains": emailDomainsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/femforgacs.hu/account/1":{
                            "ea_id": 1,
                            "ea_email": "someaccount1@femforgacs.hu",
                            "ea_autoexpunge_days": 5,
                            "ea_quota_bytes": 50*1024*1024,
                            "ea_spam_tag2_level": 3.5,
                            "tally_bytes": 47.1*1024*1024,
                            "tally_messages": 123,
                          },
                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1":{
                            "ea_id": 1,
                            "ea_email": "someaccount1@femforgacs.hu",
                            "ea_autoexpunge_days": 5,
                            "ea_quota_bytes": 50*1024*1024,
                            "tally_bytes": 47.1*1024*1024,
                            "tally_messages": 123,
                          },
                "/s/s1/su/email/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/femforgacs.hu/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/12345.s1.svc/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/somebcc.hu/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/somewebhosting.hu/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/accounts": emailAccountsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/somebcc.hu/bccs": bccsResponse,
                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bccs": bccsResponse,
                "/s/s1/su/email/aliases": aliasesResponse,
                "/s/s1/email/webhostings/12345/domains/domain/somealias.hu/aliases": aliasesResponse,
                "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/aliases": aliasesResponse,

                "/su/tapi/domain/wietest.hu": {"records":[
                   {"type":"SOA\/NS\/A","ttl":"3600","ip":"","nameserver":"a.ns.monstermedia.hu","hash":"24684e5c1cd7d62532befb1bfdc02106b002a06f21f860bf2db8c1d2a8c6650c"},
                   {"type":"CNAME","ttl":"3600","cname":"monstermedia.hu","host":"www","hash":"somehashforwie"},
                   {"type":"MX","ttl":"3600","mailserver":"monstermedia.hu","hash":"somehashforwie"},
                   {"type":"CLONE","ttl":"3600","parentZone":"monstermedia.hu","parentHost":"s1","hash":"somehashforwie"}
                ]},
                "/su/tapi/domain/monstermedia.hu": {"records":[
                   {"type":"A","ip":"123.123.123.1","host": "s1","hash":"24684e5c1cd7d62532befb1bfdc02106b002a06f21f860bf2db8c1d2a8c6650c"},
                   {"type":"A","ip":"123.123.123.2","host": "s2","hash":"24684e5c1cd7d62532befb1bfdc02106b002a06f21f860bf2db8c1d2a8c6650c"},
                ]},

                "/tapi/domain/foobar.hu": {"records":[{"type":"SOA\/NS\/A","ttl":"3600","ip":"","nameserver":"a.ns.monstermedia.hu","hash":"24684e5c1cd7d62532befb1bfdc02106b002a06f21f860bf2db8c1d2a8c6650c"}]},
                "/tapi/domain/femforgacs.hu": femforgacsTapiResponse,
                "/su/tapi/monstermedia.hu": {"records":[
                  {"type": "A", "host": "s1", "ip": "1.1.1.1"},
                  {"type": "A", "host": "s2", "ip": "2.2.2.2"}
                ]},
                "/su/tapi/domain/femforgacs.hu": femforgacsTapiResponse,
                "/su/tapi/domain/d2.hu": emptyTapiResponse,
                "/su/tapi/domain/d3.hu": emptyTapiResponse,
                "/su/tapi/domain/d4.hu": emptyTapiResponse,
                "/su/tapi/domain/d5.hu": emptyTapiResponse,
                "/su/tapi/domain/d6.hu": emptyTapiResponse,
                "/su/tapi/domain/d7.hu": emptyTapiResponse,
                "/su/tapi/domain/d8.hu": emptyTapiResponse,
                "/su/tapi/domain/d9.hu": emptyTapiResponse,
                "/su/tapi/domain/d10.hu": emptyTapiResponse,
                "/su/tapi/domain/d11.hu": emptyTapiResponse,
                "/su/tapi/template/t1.hu": emptyTapiResponse,
                "/su/tapi/template/t2.hu": emptyTapiResponse,

                "/account/webhostings": [{w_server_name: "s1", w_webhosting_id: 12345}],
                "/s/s1/webhosting/12345": webhostingWithTemplate,
                "/s/s1/su/webhosting/12345": webhostingWithTemplate,
                "/s/s1/webhosting/supported/php": [{"version":"5.3","deprecated": true}, {"version":"5.4","deprecated": true}, {"version":"5.5","deprecated": true}, {"version":"5.6"}, {"version":"7.0"}, ],
                "/s/s1/su/webhosting/12345/php/fpm/config": {
                   "/usr/local/php70": "line1\nline2\n"
                },


                "/s/s1/wizard/webhosting/installatron": {installatron_server:"installatronserver"},

                "/s/s1/docrootapi/docroots/webapps/12345": {
                    default: {type:"host-legacy"}, 
                    vhosts: {
                        "somewebapp1.hu": {
                            type: "default"
                        },
                        "somewebapp2.hu": {
                            type: "default"
                        },
                        "somewebapp3.hu": {
                            type: "default"
                        },
                        "somewebapp4.hu": {
                            type: "default"
                        },
                        "somewebapp5.hu": {
                            type: "default"
                        },
                        "somewebapp6.hu": {
                            type: "default"
                        },
                        "somewebapp7.hu": {
                            type: "default"
                        }
                    }
                },

                "/s/s1/docrootapi/docroots/12345/": ["example.hu"],

                "/s/s1/su/docrootapi/docroots/certificates/16387": { '123123': [ 'certificate.hu' ] },
                "/s/s1/docrootapi/docroots/certificates/16387": { '123123': [ 'certificate.hu' ] },
                "/s/s1/docrootapi/docroots/certificates/12345": { '123123': [ 'certificate.hu' ] },

                "/s/s1/docrootapi/docroots/hostentries/12345": [
                             {raw: "domain1.hu/www.domain1.hu", domain: "domain1.hu", host: "www.domain1.hu"},
                             {raw: "domain2.hu/www.domain2.hu", domain: "domain2.hu", host: "www.domain2.hu"},
                ],
                "/s/s1/docrootapi/docroots/12345/example.hu": femforgacsDocrootAnswer,
                "/s/s1/docrootapi/docroots/12345/femforgacs.hu": femforgacsDocrootAnswer,
                "/s/s1/docrootapi/redirects/femforgacs.hu/": [{"host":"www.femforgacs.hu", "redirect": "http://index.hu"},{"host":"femforgacs.hu", "redirect": "http://origo.hu"}],
                "/s/s1/docrootapi/frames/femforgacs.hu/": [{"host":"www.femforgacs.hu", "redirect": "http://index.hu"},{"host":"femforgacs.hu", "redirect": "http://origo.hu"}],

                "/s/s1/docrootapi/auths/12345/": [{"username":"user1"},{"username":"user2"}],

                "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": ["/dir1/", "/dir2/"],

                "/s/s1/su/docrootapi/awstats/12345/femforgacs.hu/raw":"awstats config",
                "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/raw": "some raw docroot config\ndata\nreally",
                "/s/s1/su/docrootapi/apache/12345/femforgacs.hu/raw": "some raw apache config\ndata\nreally",
                "/s/s1/su/docrootapi/nginx/12345/femforgacs.hu/raw": "some raw nginx config\ndata\nreally",

                "/s/s1/su/docrootapi/apache/12345/femforgacs.hu/hooks": {"append":"This\nwill\nbe\nappended"},
                "/s/s1/su/docrootapi/nginx/12345/femforgacs.hu/hooks": {"base":"This\nwill\nbe\nthe base"},

                "/s/s1/docrootapi/docroots/distinct/12345": {"/docroot1/pages/":["domain3.hu"], "/docroot2/pages/": ["domain1.hu", "domain2.hu"]},

                "/s/s1/ftp/accounts/12345/username1/ssh-authz/": { authz: "---- BEGIN SSH PUBLIC KEY ----\nsdffsdf\n---- END SSH PUBLIC KEY ----\n"},


                "/s/s1/ftp/sites/12345/": { wh_webhosting: 12345,
  wh_user_id: '123',
  wh_quota_b: 10485760,
  wh_public_login: 0,
  wh_last_admin_login_ts: '2017-03-08T08:22:21+01:00',
  wh_last_admin_login_ip: '4.3.2.1',
  wh_last_admin_login_enabled: 1,
  wh_ip_acl_effective: 0,
  wh_ip_acl_whitelist: [ '1.1.1.1', '2.2.2.2', '3.3.3.3' ],
  created_at: '2017-03-08 07:22:18',
  updated_at: '2017-03-08 07:22:18',
  t_tally_b: 838860.8,
  t_tally_updated: 0,
  wh_last_admin_login_allowed_until: '2017-03-09T08:22:21+01:00' },

                "/s/s1/su/ftp/scoreboard": {
  "server": {
    "server_type": "standalone",
    "pid": 23187,
    "started_ms": 1489350751000
  },
  "connections": [
    {
      "pid": 4960,
      "connected_since_ms": 1489381151000,
      "remote_name": "112.85.42.123",
      "remote_address": "112.85.42.123",
      "local_address": "::ffff:88.151.102.104",
      "local_port": 22,
      "authenticating": true,
      "protocol": "ftp",
      "idling": true
    },
    {
      "pid": 24015,
      "connected_since_ms": 1489387932000,
      "remote_name": "178.48.246.208",
      "remote_address": "178.48.246.208",
      "local_address": "::ffff:88.151.102.104",
      "local_port": 21,
      "user": "ng.monstermedia.hu",
      "protocol": "ftp",
      "location": "/",
      "command": "STOR",
      "command_args": "proftpd-1.3.5d.tar.gz",
      "uploading": true,
      "transfer_duration_ms": 2285000
    }
  ]
},
                "/s/s1/su/ftp/accounts": su_ftp_accounts,

                "/s/s1/ftp/accounts/12345/": ftp_accounts,

                "/s/s1/su/webhosting/": [{
                   wh_id: 12345,
                   wh_user_id: "123456",
                   wh_name: "some name",
                   wh_template: "WEB10000",
                   wh_tally_sum_storage_mb: 123.01,
                }],
                "/s/s2/su/webhosting/": [{
                   wh_id: 22345,
                   wh_user_id: "123456",
                   wh_name: "some name 2",
                   wh_template: "WEB10000",
                   wh_tally_sum_storage_mb: 223.01,
                }],

                "/su/servers/": ["s1","s2","tapi","webhosting","therapy"],
                "/su/servers/database": ["s1","cluster1"],
                "/su/servers/email": ["s1","email"],
                "/su/servers/tapi": ["s1", "s2", "tapi"],
                "/su/servers/accountapi": ["therapy", "s2"],
                "/su/servers/webhosting": ["s1", "s2", "webhosting"],
                "/su/servers/installatron": ["installatronserver"],
                "/su/servers/docker": ["s1","s2"],

                // this is routed to accountapi and requires additional permissions
                "/su/accountapi/config/credentials/permissions": ["SUPERUSER", "ADMIN_TAPI", "ADMIN_ACCOUNTAPI", "ACCOUNT_OWNER"],
                "/su/accountapi/servers": [
                  {"s_name": "therapy", "s_roles": ["tapi","webhosting"], "s_users":[123456,22345,23456]},
                  {"s_name": "s2", "s_roles": ["tapi"]
                }],

                "/ping": "pong",

                "/s/s1/su/fileman/cron/": {
                  entries: {
                     "id1": {
                        user_id: 61001,
                        wh_id: 12345,
                        cronExpression: "*/5 * * * *",
                        type:"url",
                        data:"http://index.hu/",
                     }
                  }
                },
               "/s/s1/fileman/12345/cron/": {
                  canBeAdded: true,
                  entries: {
                     "id1": {
                        user_id: 61001,
                        wh_id: 12345,
                        cronExpression: "*/5 * * * *",
                        type:"url",
                        data:"http://index.hu/",
                     }
                  }
               },
               "/s/s1/su/fileman/tests/tasks":["toreadwrite"],
               "/s/s1/fileman/tasks/upload/output": "Upload file task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/download": "<?php\necho 'line1';\necho 'line2';\necho 'line3';\n?>",
               "/s/s1/fileman/tasks/cleanup": "Cleaning up files of the selected webstore\n::: mc task status: successful :::\n",
               "/s/s2/fileman/tasks/cleanup": "Cleaning up files of the selected webstore\n::: mc task status: successful :::\n",
               "/s/s1/tasks/transfer": "Transferring files between stores\n::: mc task status: successful :::\n",
               "/s/s2/tasks/transfer": "Transferring files between stores\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/move": "Move task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/copy": "Copy task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/mkdir": "Mkdir task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/12345/tasks":["toreadwrite"],
               "/s/s1/fileman/tasks/zip": "Creating zip archive\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/remove": "Remove files task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/toreadwrite": "Output from the to read write task\n::: mc task status: successful :::\n",
               "/s/s1/fileman/tasks/toreadonly": "Output from the to read only task (server1)\n::: mc task status: successful :::\n",
               "/s/s2/fileman/tasks/toreadonly": "Output from the to read only task (server2)\n::: mc task status: successful :::\n",
               "/s/s1/fileman/12345/isreadonly": true,
               "/s/s1/fileman/12347/isreadonly": false,
               "/s/s1/su/fileman/12345/isreadonly": false,

               "/s/s1/email/tasks/cluebringer-cleanup": "Output from cluebringer cleanup\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/recalculate": "Recalculating tally\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/postfix-fetch-email": "Content of the e-mail itself\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/postfix-flush": "Output from postfix flush\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/postfix-delete": "Output from postfix queue delete\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/email-autoexpunge": "Output from email autoexpunge\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/email-cleanup-junk": "Output from email junk remove\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/email-account-delete": "Output from email account remove\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/email-domain-delete": "Output from email domain remove\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/email-cleanup": "Output from email cleanup\n::: mc task status: successful :::\n",
               "/s/s1/email/tasks/salearn/output": "Output of spamassssin\n::: mc task status: successful :::\n",

               "/s/s1/dbms/databases/12345/tasks": ["dbbackup-full"],
               "/s/s1/dbms/tasks/database-import": "Output from mysql import process; uploading via tasks output (fileman download or dbms export)\n::: mc task status: successful :::\n",
               "/s/s2/dbms/tasks/database-import": "Output from mysql import process; uploading via tasks output (fileman download or dbms export)\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/dbms-repair-perdb": "Output from a single database repair process\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/dbms-repair-full": "Output from full database repair process\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/dbbackup-full": "Output from dbbackup all databases\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/dbbackup-wh": "Output from dbbackup of this particular webhosting only\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/database_delete_id": "Output from delete database\n::: mc task status: successful :::\n",
               "/s/s1/dbms/tasks/database-import/output": "Output from mysqldump process (uploading from file)\n::: mc task status: successful :::\n",

                "/s/s1/su/dbms/databases/12345/something/users": dbusers_list,
                "/s/s1/dbms/databases/12345/something/users": dbusers_list,
                "/s/s1/dbms/databases/12345": databases_list,

                // this is used in the wie restoration examples
                "/s/s1/su/dbms/databases/12345": [{"db_user_id":"16384","dm_dbms_type":"mysql","db_webhosting":16387,"dm_dbms_instance_name":"mysql","db_database_name":"mm_monstermedia","db_tally_mb":1411.57,"created_at":"2017-02-11 12:32:27","updated_at":"2017-02-11 12:32:27","db_locked":0}],

                "/s/s1/su/dbms/databases/": databases_list,
                "/s/s1/su/dbms/dbms/mysql":dbms_type_mysql,
                "/s/s1/su/dbms/config/supported/dbmstypes": ["mysql","postgres"],
                "/s/s1/su/dbms/dbms/": [dbms_type_mysql],
                "/s/s1/su/dbms/stats/": [{ws_user_id:3123,ws_webhosting:12345,ws_locked: 1, ws_database_count:3, ws_tally_mb: 2301.12}],
                "/s/s1/su/dbms/stats/live": {"mysql":{"db1": 234.12}},
                "/s/s1/su/dbms/stats/scoreboard": dbmsScoreboard,

                "/s/s1/su/dbms/databases/stats": [{db_webhosting:12345,c:3,s:234.21}],


                "/s/s1/webhosting/supported/python": ["python3"],
                "/s/s1/su/webhosting/python/": python_apps,
                "/s/s1/webhosting/12345/python/": python_apps,
                "/s/s1/su/webhosting/12345/python/foobar": python_foobar_app,
                "/s/s1/webhosting/12345/python/foobar": python_foobar_app,

                "/s/s1/iptables/whextra/12345/firewallrules/": {
                  active: true,
                  rules:[
                    {fw_id:1,fw_dst_ip:"123.123.123.123", fw_dst_port: 80, fw_action:"ACCEPT"},
                    {fw_id:2,fw_dst_ip:"123.123.123.124", fw_dst_port: 80, fw_action:"ACCEPT"}
                  ]
                },
                "/s/s1/webhosting/tasks/pythonreq": "This is the pip log output\n::: mc task status: successful :::\n",
                "/s/s1/iptables/tasks/firewallrules_logging": "This is a realtime firewall log entry\n::: mc task status: successful :::\n",
                "/s/s1/webhosting/tasks/szar-backup-all": "Szar backup all stuffs\n::: mc task status: successful :::\n",

                "/s/s1/webhosting/tasks/szar-backup-wie-webstore": "Restore wie config from szar backup for the selected webstore\n::: mc task status: successful :::\n",
                "/s/s1/webhosting/tasks/szar-backup-wie-complete": "Complete wie restore from szar backup\n::: mc task status: successful :::\n",

                "/s/s1/webhosting/tasks/szar-backup-website": "Per-webstorage szar backup\n::: mc task status: successful :::\n",
                "/s/s1/webhosting/tasks/szar-restore-web": "Szar restore web stuffs\n::: mc task status: successful :::\n",
                "/s/s1/webhosting/tasks/szar-restore-mail": "Szar restore mail stuffs\n::: mc task status: successful :::\n",

                "/s/s1/webhosting/tasks/walk-id": "This is the walk log output\n::: mc task status: successful :::\n",
                "/s/s1/webhosting/tasks/walk-and-store-id": "This is the walk and store log output\n::: mc task status: successful :::\n",

                "/s/s1/su/git/": [primary_git_info,primary_git_info,primary_git_info],
                "/s/s1/docrootapi/tasks/awstats-process-and-build-1": "Awstats is processing log files and then building pages...\n::: mc task status: successful :::\n",
                "/s/s1/docrootapi/tasks/awstats-process-1": "Awstats is processing log files...\n::: mc task status: successful :::\n",
                "/s/s1/docrootapi/tasks/awstats-build-1": "Awstats is building static html pages...\n::: mc task status: successful :::\n",
                "/s/s1/git/12345/tasks": ["1234gitlog","1234clone","1234initialcloning"],
                "/s/s1/git/tasks/1234gitlog": "This is the git log output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234clone": "This is the git clone output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234revert": "This is the git revert output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234pull": "This is the git pull output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234clean": "This is the git clean output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234checkout": "This is the git checkout output\n::: mc task status: successful :::\n",
                "/s/s1/git/tasks/1234initialcloning": "The initial cloning is a boring task\n::: mc task status: successful :::\n",

                "/s/s1/git/12345/id/1/branch/dev/": primary_git_branch_info,
                "/s/s1/git/12345/ssh_deployment_key": "ssh-dss AAAAB3NzaC1kc3MAAACBAJ5j5qdPjqYA7XoQH1zuuz1Fo8D+15QquG4jvqoAVW5nJKzj8yzCNh8NgK9es1B4My74EZmAFl/4RCZdQuc8AOYNsFsrN6tFf8lsDgk50YBEQgxwZZVJoGLC/rucZo55gpM+5oJtQjaGDlILjYbtiWFrpTdUv5DRwb5Az/dfzcYrAAAAFQCG1N+gzUv2iMhXZte7Mg79iaE5JwAAAIBcTJw220TRgSXDCOVHEcwctYqcNvFQ5pjYDT/sOztXbqU7WaQ+Q4MhQ+AO/+Fj9WTmUtoW9YQMxQhNtqjX9cOdaQaU3vvnuGZ5AXyyE72Toefv6cQEAt5jK4vsnn+tbVLEcIlTzWHAiDnyHWkrbEzfI2kA1iJ34YX1nrYzojrzKQAAAIAy77UtcaOP+s8XLPZEKlYbhH9Sb3RGwTYgI7whHqIuekXK0iGHlJscdZg6awRtaRQg6r0oKQ0l0nrvEzCl7mhAlF9JwWCMsfOWrqnXfHLNLUkqNRx33oK8/6rI+owIopv4TUGKkxHmh+tLf1vs4ZAkHvFw2Ur2dzqcgMc6diP5aw== root@monstermedia",
                "/s/s1/git/12345/info": {"t_git_max_repos": 10, "current_repositories": 5, "insert_allowed": true, ssh: {StrictHostKeyChecking: true}},
                "/s/s1/git/12345/": [
                primary_git_info,
{ g_id: "2",
  g_user_id: '23456',
  g_webhosting: 12345,
  g_branches: 2,
  g_remote_origin: 'https://github.com/irsl/dfwfw.git',
  g_name: 'My even more awesome website',
  webhook_url: "https://some.primary.domain/api/s/s123/git/triggers/12345/abcdef01242345234234",
    },
                ],
                "/s/s1/git/12345/id/1/": primary_git_info,
                "/s/s1/git/12345/id/1/branches/": [primary_git_branch_info],

                "/s/s1/su/webhosting/supported/storages": ["/web/w3/"],
                "/s/s2/su/webhosting/supported/storages": ["/storage/d1/"],
                "/s/tapi/su/webhosting/supported/storages": ["/web/w3/"],

        "/s/s1/su/eventapi/config/subsystems": ["accountapi", "tapi"],
        "/s/s1/su/eventapi/config/eventtypes": ["auth-credential", "auth-token", "exception"],

                "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu": femforgacsDocrootAnswer,


                "/pub/account/webhostingtemplate/WEB10000": webhostingtemplate,


        "/su/accountapi/config/servers/roles": ["accountapi","tapi","webhosting"],

        "/su/accountapi/account/2/domains": domainsResponse,
        "/su/accountapi/account/2/webhostings": [{"w_server_name": "s1", "w_webhosting_id": 12345}],
        "/su/tapi/domains": {"domains":["femforgacs.hu","d2.hu","d3.hu","d4.hu","d5.hu","d6.hu","d7.hu","d8.hu","d9.hu","d10.hu","d11.hu"]},
        "/su/tapi/templates": {"domains":["t1.hu","t2.hu"]},
        "/su/tapi/export/tgz": {data: "abc"},
        "/s/s1/su/tapi/export/json": { 
           'good.hu': { mtime: 1, records: [{'type':'A','ip':'1.1.1.1'}] }, 
           'mtime.hu': { mtime: 1, records: [] },
           'something-else.hu': { mtime: 1, records:
               [ {'type':'A','ip':'2.1.1.1'}, { type: 'SOA/NS/A',
                   ttl: 3600,
                   nameserver: 'a.ns.nonamedomain.hu' },
                 { type: 'SOA/NS/A',
                   ttl: 3600,
                   nameserver: 'b.ns.nonamedomain.hu' },
                 { type: 'A', ttl: 3600, ip: '123.123.123.123' },
                 { type: 'CNAME',
                   ttl: 3600,
                   host: 'www',
                   cname: 'nonamedomain.hu' } ]},
           'test.hu': { mtime: 1, records: [ 
            { type: 'A', ttl: 3600, ip: '123.123.123.123' },
            { type: 'CNAME', ttl: 3600, host: 'www', cname: 'index.hu' } ] 
          } },

        "/s/s2/su/tapi/export/json": { 
          'good.hu': { mtime: 1, records: [{'type':'A','ip':'1.1.1.1'}] },
          'mtime.hu': { mtime: 2, records: [] },
          'something-else.hu': { mtime: 1, records: 
   [ {'type':'A','ip':'2.1.1.1'},  {type: 'SOA/NS/A',
       ttl: 3600,
       nameserver: 'a.ns.nonamedomain.hu' },
     { type: 'SOA/NS/A',
       ttl: 3600,
       nameserver: 'b.ns.nonamedomain.hu' },
     { type: 'A', ttl: 3600, ip: '123.123.123.122' },
     { type: 'CNAME',
       ttl: 3600,
       host: 'www',
       cname: 'nonamedomain.hu' } ] },
    },

        "/s/tapi/su/tapi/export/json": {  'something-else.hu': { mtime: 1, records: [{'type':'A','ip':'2.1.1.1'}] },
        'mtime.hu': { mtime: 3, records: [] },
        'good.hu': { mtime:1, records: [{'type':'A','ip':'1.1.1.1'}] }, 
  'test.hu': { mtime:1, records: 
   [ { type: 'A', ttl: 3600, ip: '123.123.123.124' },
     { type: 'CNAME', ttl: 3600, host: 'www', cname: 'index.hu' } ] } },


        "/su/accountapi/webhostingtemplate/WEB10000": webhostingtemplate,
        "/accountapi/webhostingtemplates": [ webhostingtemplate, { t_name: 'WEB2000' } ],
        "/su/accountapi/webhostingtemplates": [ webhostingtemplate, { t_name: 'WEB2000' } ],
        "/accountapi/config/webhostingtemplates/dynamiclanguages": [ "php", ".net", "python", "nodejs" ],
        "/su/accountapi/config/webhostingtemplates/dynamiclanguages": [ "php", ".net", "python", "nodejs" ],


        "/s/s1/su/cert/purchases/all/item/abcd1233/": purchase_info,
        "/s/s1/su/cert/purchases/all/" : {
            purchases: [purchase_info]
        },

        },
    "PUT": {
              "/account/tels": "ok",
              "/account/emails": "ok",
              "/account/credentials": "ok",

              "/account/webhostingdomains/": {domain: "foobar.hu", bareDomain: "foobar.hu"},

              "/su/accountapi/account/1/domains": "ok",
              "/su/accountapi/account/1/credentials": "ok",
              "/su/accountapi/account/1/webhostingdomains": "ok",
              "/su/accountapi/account/1/webhostings": "ok",
              "/su/accountapi/account/2/domains": "ok",
              "/su/accountapi/account/2/credentials": "ok",
              "/su/accountapi/account/2/webhostingdomains": "ok",
              "/su/accountapi/account/2/webhostings": "ok",

              "/su/accountapi/sms": "ok",

              "/wizard/dkim/s1/femforgacs.hu": {"host_prefix":"test._domainkey","dkim_key_active":true,"selector":"test","dns_dkim_record_present":false,"full_bind":"test._domainkey.test.monstermedia.hu. 3600 TXT (\n  \"v=DKIM1; p=\"\n  \"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSN\"\n  \"efNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08W\"\n  \"Gu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux3\"\n  \"5WnpGdOphYr7v6D90QIDAQAB\"\n\n)","full_hostname":"test._domainkey.test.monstermedia.hu","raw_txt_value":"v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSNefNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08WGu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux35WnpGdOphYr7v6D90QIDAQAB", "dnsSuccess": false},
              "/s/s1/email/dkim/femforgacs.hu": {"host_prefix":"test._domainkey","dkim_key_active":true,"selector":"test","dns_dkim_record_present":false,"full_bind":"test._domainkey.test.monstermedia.hu. 3600 TXT (\n  \"v=DKIM1; p=\"\n  \"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSN\"\n  \"efNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08W\"\n  \"Gu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux3\"\n  \"5WnpGdOphYr7v6D90QIDAQAB\"\n\n)","full_hostname":"test._domainkey.test.monstermedia.hu","raw_txt_value":"v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDI1Kux2W8lsGhXEnWxw5ryuiSNefNVFmBgMWOlyNSY2ag6fFohPfnakT9WxtW2dHRxWWdOsft4T8N6BKcpgf9l/08WGu1iVDFv0GhwZGTkpR8xZvDhnIeNZ8BUTVN0jLJRKOyrFx8vIjzi/gqB34gC1Ux35WnpGdOphYr7v6D90QIDAQAB"},

              "/s/s1/email/webhostings/12345/domains/": "ok",
              "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/accounts": "ok",
              "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/aliases": "ok",
              "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bccs": "ok",

              "/tapi/domain/foobar.hu": {"hash":["somehash"]},
              "/su/tapi/domain/femforgacs.hu": {"hash":["somehash"]},
              "/tapi/domain/femforgacs.hu": {"hash":["f49238c2277a4c1d56e9ad27ee8525b519e1be43db9b5826df637d67e7c6b760"]}, // {"ttl":"","host":"xxx","type":"TXT","text":"werwer"}
              "/su/tapi/domain/wietest.hu": {"hash":["somenewhashforwie"]},

              "/s/s1/su/email/domains": "ok",
              "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu/entries/": "ok",
              "/s/s1/docrootapi/docroots/12345/xxx.femforgacs.hu/entries/": "ok",
              "/s/s1/docrootapi/docroots/12345/foobar.hu/entries/": "ok",
              "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/": "ok",
              "/s/s1/docrootapi/redirects/femforgacs.hu/": "ok",
              "/s/s1/docrootapi/frames/femforgacs.hu/": "ok",
              "/s/s1/docrootapi/auths/12345/": "ok",

              "/s/s1/su/email/postfix/mailq-auto/": "ok",
              "/s/s1/su/email/cluebringer/policies/outbound/SASL/members": "ok",
              "/s/s1/su/email/cluebringer/policies/outbound/": "ok",
              "/s/s1/su/email/cluebringer/groups/internal_ips": "ok",

              "/su/accountapi/servers": "ok",

              "/s/s1/ftp/sites/12345/acls": "ok",
              "/s/s1/ftp/accounts/12345/":"ok",

              "/s/s1/su/dbms/dbms": "ok",

              "/s/s1/email/domains": "ok",

              "/s/s1/fileman/12345/cron/":"ok",

              "/s/s1/su/cert/purchases/all/": "ok",
              "/s/s1/cert/purchases/my/item/abcd1233/activations":"ok",
              "/s/s1/cert/contacts/my/": "ok",
              "/s/s1/su/cert/purchases/all/item/abcd1233/activations": "ok",
              "/s/s1/cert/certificates/by-webhosting/12345/": "ok",
              "/s/s1/cert/certificates/my/": "ok",

              "/s/s1/su/dbms/databases/12345/something/users": "ok",
              "/s/s1/dbms/databases/12345/something/users": "ok",

              "/s/s1/dbms/databases/12345": "ok",
              "/s/s1/dbms/databases/12345/foobar/users": "ok",

              "/s/s1/git/12345/id/1/branches/": {id: "1234initialcloning"},
              "/s/s1/git/12345/": "ok",

              "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": "ok",

              "/s/s1/iptables/whextra/12345/firewallrules/":"ok",

              "/s/s1/webhosting/12345/python/": "ok",

              "/su/tapi/templates":"ok",
              "/su/tapi/domains":"ok",

              "/s/s1/wizard/webhosting": {wh_id:10001},
              "/s/s2/wizard/webhosting": {wh_id:20001},

              "/s/s1/cert/certificates/my/item/123123/intermediates": "ok",

              "/su/accountapi/webhostingtemplates": "ok",

              "/s/s1/su/iptables/bans/HTTP":"ok",

              "/su/accountapi/webhostingdomains/mass": "ok",

              "/s/s1/cert/certificates/by-webhosting/12345/item/123123/private-key": "ok",
              "/s/s1/cert/certificates/by-webhosting/12345/item/123123/intermediates": "ok",
              "/s/s1/cert/purchases/my/letsencrypt": {id:"letsencrypt"},
              "/cert/purchases/my/letsencrypt": {id:"letsencrypt"},

              "/su/accountapi/webhostingdomains/": "ok",

    },
	"DELETE": {
		"/account/tel/+36-30-1234567": "ok",
    "/account/email/foo2@bar.hu": "ok",
    "/account/credential/dnsonly": "ok",
    "/account/account_grant": "ok",

    "/s/s1/su/docker/root/image/sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543": [{"untagged":"123"}],

    "/su/accountapi/account/2/credential/dnsonly": "ok",

    "/wizard/webhostingdomains/": "ok",

    "/s/s1/su/cert/contacts/all/item/1/":"ok",
    "/s/s1/su/cert/config/namecheap/credentials":"ok",
    "/s/s1/su/cert/certificates/all/item/123123/": "ok",
    "/s/s1/su/cert/purchases/all/item/abcd1233/": "ok",

    "/tapi/domain/femforgacs.hu": "ok",
    "/tapi/domain/femforgacs.hu/8fd6e80a91e5edc5bccd9b7dbf8434d1bc7c707a740d8cc023834cf55ee985fe": {"deleted":1},  // {"limit":1}
    "/su/tapi/domain/wietest.hu/somehashforwie": {"deleted":1},
    "/su/tapi/domain/femforgacs.hu/aae6f56746090a1769e322f1dddb9e3f1b6b35c6d2839474825b4ba1b94d2692": {"deleted":1},
    "/su/tapi/domain/femforgacs.hu/da3f99866510703866078b0c6dd522e6c2ee41fc5e9892a71e0ca9b56d45e1b3": {"deleted":1},
    "/su/tapi/domain/femforgacs.hu/e65b924fbe2eef6dbc5a862f4203cff227830e318b542c88f8ea18bd61df52c5": {"deleted":1},

    "/s/s1/su/tapi/domain/something-else.hu": "ok",    
    "/s/s2/su/tapi/domain/something-else.hu": "ok",
    "/s/tapi/su/tapi/domain/something-else.hu": "ok",
    "/su/tapi/domain/femforgacs.hu": "ok",

    "/s/s1/ftp/accounts/12345/username1":"ok",
    "/s/s1/ftp/accounts/12345/username2":"ok",

    "/s/s1/su/docrootapi/docroots/12345/femforgacs.hu": "ok",
    "/s/s1/su/docrootapi/cleanup/webhostings/12345": "ok",
    "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu": "ok",
    "/s/s1/docrootapi/docroots/12345/femforgacs.hu": "ok",
    "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/www.femforgacs.hu":"ok",
    "/s/s1/docrootapi/docroots/12345/femforgacs.hu/entries/femforgacs.hu":"ok",
    "/s/s1/docrootapi/redirects/femforgacs.hu/www.femforgacs.hu":"ok",
    "/s/s1/docrootapi/redirects/femforgacs.hu/femforgacs.hu":"ok",
    "/s/s1/docrootapi/frames/femforgacs.hu/www.femforgacs.hu":"ok",
    "/s/s1/docrootapi/frames/femforgacs.hu/femforgacs.hu":"ok",
    "/s/s1/docrootapi/auths/12345/user1": "ok",
    "/s/s1/docrootapi/auths/12345/user2": "ok",

    "/s/s1/wizard/webhosting": {"id": "webstore-removal"},
    "/wizard/webhosting": {"id": "webstore-removal-with-bindings"},

    "/s/s1/iptables/whextra/12345/firewallrules/rule":"ok",
    "/s/s1/webhosting/12345/python/foobar": {deleted:1},
    "/s/s1/su/webhosting/python/foobar": {deleted:1},

    "/s/s1/git/12345/id/1/branch/dev/": "ok",
    "/s/s1/git/12345/id/2": "ok",

    "/s/s1/ftp/sites/12345/acls": "ok",

    "/s/s1/docrootapi/certificates/123123": "ok",
    "/s/s1/docrootapi/protected-dirs/12345/femforgacs.hu/": "ok",

    "/s/s1/docrootapi/awstats/12345/femforgacs.hu": "ok",

    "/s/s1/dbms/databases/12345/something/user/something": "ok",
    "/s/s1/dbms/databases/12345/something/user/something_else": "ok",
    "/s/s1/su/dbms/databases/12345/something/user/something": "ok",
    "/s/s1/su/dbms/databases/12345/something/user/something_else": "ok",

    "/s/s1/su/dbms/databases/12345/something": {"id":"database_delete_id"},
    "/s/s1/dbms/databases/12345/something": {"id":"database_delete_id"},
    "/s/s1/dbms/databases/12345": {"id":"database_delete_id"},
    "/s/s1/su/dbms/databases/12345": {"id":"database_delete_id"},
    "/s/s1/su/dbms/dbms/mysql": "ok",

    "/s/s1/su/webhosting/12345": "ok, deleted",

    "/s/s1/su/docrootapi/certificates/123123": "ok",

    "/su/accountapi/account/2/domain/xn--foobr-xfw.hu": "ok",
    "/su/accountapi/account/2/webhosting/s1/12345": "ok",
    "/su/accountapi/account/2": "ok",


    "/su/accountapi/config/sms/seeme/credentials": "ok",
    "/su/accountapi/server/therapy": "ok",

    "/su/accountapi/webhostingtemplate/WEB10000": "ok",

    "/s/s1/su/iptables/bans/HTTP": "ok",
    "/s/s1/su/iptables/bans/HTTP/123.123.123.123": "ok",
    "/s/s1/su/iptables/whextra/53001/firewallrules/rule": "ok",

    "/s/s1/cert/contacts/my/item/1": "ok",

    "/s/s1/su/fileman/cron/id1": "ok",
    "/s/s1/fileman/12345/cron/id1": "ok",

    "/wizard/dkim/s1/femforgacs.hu": {"dnsSuccess":false},
    "/s/s1/email/dkim/femforgacs.hu": "ok",
    "/s/s1/su/email/postfix/mailq-auto/1": "ok",
    "/s/s1/su/email/postfix/mailq/by/email": {"id":"postfix-delete"},
    "/s/s1/su/email/postfix/mailq/by/recipient": {"id":"postfix-delete"},
    "/s/s1/su/email/postfix/mailq/by/sender": {"id":"postfix-delete"},
    "/s/s1/su/email/postfix/mailq/8B164258D909": {"id":"postfix-delete"},
    "/s/s1/su/email/postfix/mailq/ALL":{"id":"postfix-delete"},
    "/s/s1/su/email/amavis/msgs/quarantine/id": "ok",
    "/s/s1/su/email/domains/domain/somewhat1.hu": {id: "email-domain-delete"},
    "/s/s1/su/email/accounts/account/1": {id: "email-account-delete"},
    "/s/s1/su/email/accounts/account/2": {id: "email-account-delete"},
    "/s/s1/su/email/accounts/account/3": {id: "email-account-delete"},
    "/s/s1/su/email/aliases/alias/1": "ok",
    "/s/s1/su/email/aliases/alias/2": "ok",
    "/s/s1/su/email/aliases/alias/3": "ok",
    "/s/s1/su/email/bccs/bcc/1": "ok",
    "/s/s1/su/email/bccs/bcc/2": "ok",
    "/s/s1/su/email/bccs/bcc/3": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bcc/1": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bcc/2": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/bcc/3": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/account/1": {id:"email-account-delete"},
    "/s/s1/email/webhostings/12345/domains/domain/somewhat1.hu": {id:"email-domain-delete"},
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/alias/1": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/alias/2": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/xn--femforgacs.hu/alias/3": "ok",
    "/s/s1/email/webhostings/12345/domains/domain/femforgacs.hu": {id:"email-domain-delete"},

    "/s/s1/su/email/cluebringer/policies/outbound/SASL/members": "ok",
    "/s/s1/su/email/cluebringer/groups/internal_ips": "ok",
    "/s/s1/su/email/cluebringer/policies/outbound/high2000": "ok",

    "/su/accountapi/webhostingdomains/": "ok",

	},
  "SEARCH": {
    "/tapi/domain/somebcc.hu": true,
    "/tapi/domain/femforgacs.hu": true,
    "/tapi/domain/example.hu": true,
    "/tapi/domain/foobar.hu": true,
    "/tapi/domain/zsid.hu": true,
    "/s/s1/su/docrootapi/docroots": [{"wh":"12345","domain":"femforgacs.hu"},{"wh":"12345","domain":"example.hu"}],
    "/s/s1/email/domains/domain/xxx.femforgacs.hu": [],

    "/s/s1/email/domains/domain/femforgacs.hu": femforgacsEmailDomainResponse,
    "/s/s1/email/domains/domain/somealias.hu": false,
    "/s/s1/email/domains/domain/somebcc.hu": false,
    "/s/s1/email/domains/domain/somewebhosting.hu": false,
    "/s/s1/email/domains/domain/xn--femforgacs.hu": false,
    "/s/s1/email/domains/domain/non-existent.hu": false,
    "/s/s1/email/domains/domain/non-active.hu": {
                      "do_active": 0,
                      "do_domain_name": "non-active.hu",
                      "do_user_id": "51001",
                      "do_webhosting": 52001,
                    },
    "/su/servers/": [{s_name:"s1", s_roles:['accountapi','tapi','webhosting','database','mysql','geoip','installatron','email', 'docker']},
                     {s_name:"s2", s_roles:[]},
                     {s_name:"tapi", s_roles:["tapi"]},
                     {s_name:"webhosting", s_roles:["webhosting"]},
                     {s_name:"therapy",s_roles:["email"]}],


    "/s/s1/ftp/accounts/12345/": {accounts:ftp_accounts, current:2, maximum:3, can_be_added: true},

  }
}
