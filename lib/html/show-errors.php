<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div>
  
     <form name="dataForm" novalidate ng-controller="test">
	   <div>{{ chg }}</div>
       <fieldset ng-disabled="isInProgress">
            <div class="form-group" show-errors ng-if="!forgotten">
	          <label class="control-label colon" lng>Old password</label>
			  <input type="password" class="form-control" name="old_password"  ng-model="chg.old_password" id="old_password" required lng-placeholder="Old password" >
	          <p class="help-block"  ng-if="dataForm.old_password.$error.required" lng>Please enter the old password</p>
			</div>

			<div class="form-group" show-errors>
	          <label class="control-label colon" lng>New password</label>
			  <input type="password" class="form-control" name="new_password"  ng-model="chg.new_password" id="new_password" required lng-placeholder="New password" >
	          <p class="help-block"  ng-if="dataForm.new_password.$error.required" lng>Please enter the new password</p>
			</div>

			<div class="form-group" >
	          <label class="control-label colon" lng>New password (optional)</label>
			  <input type="password" class="form-control" name="new_password"  ng-model="chg.new_password" id="new_password" lng-placeholder="New password" >
			</div>

		      <div class="form-group" show-errors>
		        <label class="control-label colon" lng>Repeat the new password</label>
		        <input type="password" class="form-control" name="c_password_repeat" required ng-model="tmp.c_password_repeat" same-as="chg.new_password" lng-placeholder="Repeat the new password" >
		        <p class="help-block"  ng-if="dataForm.c_password_repeat.$error.required" lng>Please repeat the password</p>
		        <p class="help-block"  ng-if="dataForm.c_password_repeat.$error['same-as']" lng>The passwords you entered don't match</p>
		      </div>

	        <div class="text-center">
				<button  class="btn btn-success" ng-click="save()" lng>Change password</button>
	        </div>
        </fieldset>
     </form>
  
     
  </div>

  <?include("js.mc/loader.php");?>

  <script>
  app.controller("test", showErrorsController({
        fireFunctionName: "save",
		additionalInitializationCallback: function($scope){
		  $scope.chg = {}
		  console.log("inited")
		},
        fireCallback: function($scope, MonsterCloud){
		   console.log("firing")

        }

      })
  )
</script>
    
</body>
</html>
