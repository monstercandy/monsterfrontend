<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>Tapi via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div mc-view>
  Original content
  <a href="/test-mc-route.php?op=ng&state=mc-email-public&server=s1&email=somewhat@something.hu">classic link</a>
  <a href mc-sref="mc-email-public" mc-sref-params="{server:'s1',email:'somewhat@something.hu'}">same as sref</a>

  </div>

  <a mc-sref="newState1" mc-sref-params="{'a1':'b1'}">new state without href</a>
  <a href mc-sref="newState1" mc-sref-params="{'a1':'b1'}">new state 1</a>
  <button mc-sref="newState2" mc-sref-params="{'a2':'b2'}">new state 2</button>
  <a href="http://index.hu" target="_blank">classic outside link</a>

  <a href mc-sref="mc-dns-admin" mc-sref-params="{'domainId':2345,'domain':'femforgacs.hu','display':'fémforgács.hu','external-registrar':true}">femforgacs dns admin</a>

  <div class="sidebar-nav margintop">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-collapse collapse sidebar-navbar-collapse" id="mainmenu">
          <ul class="nav navbar-nav ">

    <li class=""><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Beállítások <b class="caret"></b></a>
                  <ul class="dropdown-menu">
              <li><a href="" mc-sref="mc-user-settings">Felhasználói fiók, jelszó</a></li>
              <li><a href="" mc-sref="mc-edit-account-show-granted-access">További hozzáférések</a></li>
                 </ul>
                </li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>


<div>
<mc-menu-breadcrumb></mc-menu-breadcrumb>
  </div>
  
  <?include("js.mc/loader.php");?>  
  <script>

	app.config(['mcRoutesConfigProvider',  function(mcRoutesConfigProvider) {

		mcRoutesConfigProvider.registerStateTemplate("newState1",{title:"Tinydns admin controls",template:"<mc-tapi-admin-controls></mc-tapi-admin-controls>"})
		mcRoutesConfigProvider.registerStateTemplate("newState2",{title:"Python admin controls",keywords:["minyon"],template:'<mc-superuser-servers-list kind="webhosting"  control="mc-superuser-python-list"></mc-superuser-servers-list>'})

	}]);

  </script>
    
</body>
</html>
