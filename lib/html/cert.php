<?php
function gen_link($op){
  return "/index.php?op=$op";
}
?>
<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

     <a mc-sref="mc-cert-purchases" mc-sref-params="{server:'s1'}" >purchases</a>  

     <a mc-sref="mc-cert-purchases" >purchases (without server restriction)</a>  

     <a href mc-sref="mc-cert-purchases" mc-sref-params="{server:'s1',checkboxMode:true,noHead:true}" >purchases (checkbox mode)</a>  

     <a href mc-sref="mc-cert-contact-list-and-add" mc-sref-params="{server:'s1'}">contacts on s1</a>  

     <a href mc-sref="mc-cert-list-and-add" mc-sref-params="{server:'s1'}">certificates by server</a>  

     <a href mc-sref="mc-cert-list-and-add" mc-sref-params="{server:'s1',whId:'12345'}">certificates by webhosting</a>  

     <a href mc-sref="mc-cert-activation-generic" mc-sref-params="{server:'s1',cId:'sdf'}" >activation</a>  

     <div mc-view></div>


     <button id="renew">Renew test</button>

  </div>

  
  <?include("js.mc/loader.php");?>


<script>
$(document).ready(function(){
   $("#renew").click(function(){
      var pcIds = []
      $('.cert[pc-id]').each(function(){
          if(!this.checked) return
          pcIds.push($(this).attr("pc-id"))
          //alert($(this).attr("pc-subject-common-name"))
      })
      if(!pcIds.length) return
      window.location.href="<?=gen_link('admin-hosszabbit-ssl-ng')?>&pcIds="+(pcIds.join(","))
   })
})
</script>
    
</body>
</html>
