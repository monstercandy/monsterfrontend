<?
$postdata = file_get_contents("php://input");
if($postdata){
   echo sha1($postdata)."\n";
   echo "::: mc task status: successful :::\n";
   exit();
}
?>
<!DOCTYPE html>
<html lang="hu" ng-app="app">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-controller="test">
    <input type="file" fileread="content">
	<button class="btn btn-default" ng-click="upload()">Upload</button>
	<br><br><br>
	
      <button class="btn btn-default" mc-files mc-files-consumer="one(file,data)">Browse files</button>

  </div>


  <?include("js.mc/loader.php");?>
  
  <script>
  

  app.controller("test", ["$scope","$task",function($scope,$task){

        $scope.one = function(file, data) {
          console.log("oneByOne called", file, data)
        }

      $scope.upload = function(){
	    	if(!$scope.content) {
	    		alert("select a file first")
	    		return
	    	}

			// var url = "/file-upload-test.php"
            //var url = "http://88.151.102.104:52123/tasks/foo"
			// var url = "http://webftp.monstermedia.hu:52123/tasks/foo"
      var url = "http://monster:52123/tasks/foo"
      return $task({title:"hello world", "url":url, fileInput: $scope.content, doubleRequestMode: true, noUriTransform: true})
	
		}
}])
  
  </script>
    
</body>
</html>
