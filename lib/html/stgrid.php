<!DOCTYPE html>
<html lang="hu" ng-app="app">
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>
  <!-- test account: radimre83@gmail.com:qymyv8x4 -->
  
  <cus-msg-text msg="x" item="foo1"><h1>Foobar</h1></cus-msg-text>
  <cus-msg-text msg="y" item="foo2"></cus-msg-text>

  <br><br><br>


  <div ng-controller="ctrlPipe">
		<table mc-grid st-pipe="callServer">
   <thead >
   <th st-sort="firstName">head xxx 1</th>
   <th st-sort="lastName" lng>head xxx 2</th>
   </thead>
    <tbody>
<tr ng-repeat="row in displayedCollection">    
				<td>{{row.firstName}}</td>
				<td>{{row.lastName}}</td>
				<td>{{row.birthDate}}</td>
				<td>outer: {{outer}} :outer</td>
				<td>
				<button type="button" ng-click="removeItem(row)" class="btn btn-sm btn-danger">
					<i class="glyphicon glyphicon-remove-circle"></i>
				</button>
				</td>
</tr>
      </tbody>

	 </table>
	</div>

	 <br/><br/>

  <div ng-controller="ctrl">

		<table mc-grid="stGridItems" >
   <thead >
   <th st-sort="firstName">head xxx 1</th>
   <th st-sort="lastName" lng>head xxx 2</th>
   </thead>
    <tbody>
<tr ng-repeat="row in displayedCollection">    
				<td>{{row.firstName}}</td>
				<td>{{row.lastName}}</td>
				<td>{{row.birthDate}}</td>
				<td>outer: {{outer}} :outer</td>
				<td>
				<button type="button" ng-click="removeItem(row)" class="btn btn-sm btn-danger">
					<i class="glyphicon glyphicon-remove-circle"></i>
				</button>
				</td>
</tr>
      </tbody>

	 </table>

	 <br/><br/>
	
	
			
			<table gridx2>
			<tbody>
<tr ng-repeat="row in items">    
				<td>{{row.firstName}}</td>
				<td>{{row.lastName}}</td>
				<td>{{row.birthDate}}</td>
				<td>{{row.balance}}</td>
				<td>
				<button type="button" ng-click="removeItem(row)" class="btn btn-sm btn-danger">
					<i class="glyphicon glyphicon-remove-circle"></i>
				</button>
				</td>
</tr>
</tbody>

	 </table>
	 
			<table gridx1>
   <thead>
   <th>head1</th>
   <th>head2</th>
   </thead>
    <tbody>
<tr ng-repeat="row in items">    
				<td>{{row.firstName}}</td>
				<td>{{row.lastName}}</td>
				<td>{{row.birthDate}}</td>
				<td>{{row.balance}}</td>
				<td>
				<button type="button" ng-click="removeItem(row)" class="btn btn-sm btn-danger">
					<i class="glyphicon glyphicon-remove-circle"></i>
				</button>
				</td>
</tr>
      </tbody>

	 </table>
	 
  </div>

  
  <!--
     <foo>
	    <div lng>Server</div>
	 </foo>
-->

  </div>

  
  <?include("js.mc/loader.php");?>
  
  <script>
  
app.directive('cusMsgText', function($compile) {
  return {
    restrict: 'E',
    scope: {
      msg: '@',
      item: '@'
    },
    link: function(scope, element, attrs) {
      templates = {
        x: '<div>template x {{item}}</div>',
        y: '<div>template y {{item}}</div>'
      };

      console.error("!!!", element.contents());

      var html = templates[scope.msg];
      element.replaceWith($compile(html)(scope));
    }
  };
})



 app.controller("ctrlPipe", ["$scope","$timeout",function (scope, $timeout) {

      var i = 0;
      scope.callServer = function(tableState){
        console.log("callserver", tableState)
        scope.displayedCollection = null;
        return $timeout(function(){

           scope.displayedCollection = [
              {firstName: "first"+i, lastName: "last"+i}
           ];
           i++;
           tableState.pagination.totalItemCount = 2;
        }, 2000);
      }

   }])

   app.controller('ctrl', ["$q","$scope","$timeout",function ($q, $scope, $timeout) {

   	  $scope.outer = "outer var";

		   return $timeout(function(){
				  $scope.stGridItems = [];				  
				  for(var i = 0; i< 98; i++) {
					  $scope.stGridItems.push({firstName: "first"+i, lastName: "last"+i});
				  }
			   
			   console.log("shit!", $scope.stGridItems);
		   }, 2000);			   
	   
   }])
   

   app.directive("gridx2", function() {
  return {
    restrict: 'A',
    replace: true,
    transclude: {
		 tbody: "tbody",
	},
    template: '<table class="table table-striped"><thead><th>header</th></thead><tbody ng-transclude="tbody"></tbody><tfoot><tr><th>footer</th></tr></tfoot></table>',

  };
   });
   
   app.directive("gridx1", function() {
  return {
    restrict: 'A',
    replace: true,
    transclude: true,
    template: '<div class="table-responsive"><table class="table table-striped"><thead><th>Col1</th><th>Col2</th><th>Col3</th></thead><tbody></tbody><tfoot><tr><th>footer</th></tr></tfoot></table></div>',
    link: function(scope, elem, attrs, controller, transcludeFn) {
      var item = transcludeFn(scope, function(clone) {
        return clone.children();
      });
	  var tplBody = elem.find("tbody");
	  tplBody.replaceWith(item);
	}
  };
   });
   
   app.directive("foo", function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {},
    template: '<mc-login-or-logout></mc-login-or-logout> (header) <div ng-transclude></div> (footer) <mc-login-or-logout></mc-login-or-logout>'
  };
   });
   
  </script>


    
</body>
</html>
