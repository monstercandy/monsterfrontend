(function(app){

  var templateDirectory = "email-superuser"


  var emailDelCategories = ["email", "recipient", "sender"]

  app.service("mcEmailDownload", ["$window","mcConfig",function($window,mcConfig){

         return function(server, prom){

                return prom.then(function(h){
                       $window.location.href = mcConfig.config.api_uri_prefix + "/s/"+server+"/email/tasks/"+h.id
                })
         }
  }])

  app.service("mcEmailUrlSu", function(){
    var re = function(server, suffix) {
       return "/s/"+server+"/su/email/"+(suffix||"")
    }
    return re
  })

app.directive("mcPostfixConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor url='/s/{{server}}/su/email/postfix/config'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Postfix config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})
app.directive("mcEmailConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/email'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Email microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})
app.directive("mcMailerConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/mailer'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Mailer microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  Array(
     {n:'mcSuperuserAmavisLastMails',   u: "amavis/stats/mails/last",  f: ["time_iso","s","r","subj","c","dsn","level","size","pb"], d: "Last emails went through Amavis"},
     {n:'mcSuperuserAmavisCleanMails',  u: "amavis/stats/mails/clean", d: "Last clean emails went through Amavis"},
     {n:'mcSuperuserAmavisSpammyMails', u: "amavis/stats/mails/spammy", d: "Last spams received by Amavis"},
     {n:'mcSuperuserAmavisDomains',     u: "amavis/stats/domains", d: "Amavis domain statistics"}
  ).forEach(function(x){

        app.directive(x.n, ["templateDir", function(templateDir){
            return {
              restrict: "E",
              templateUrl: templateDir(templateDirectory,"amavis-stats"),
              controller: ["$scope", "MonsterCloud","mcEmailUrlSu",
                function($scope, MonsterCloud,mcEmailUrlSu){

                     $scope.name = x.n
                     $scope.fields = x.f || ["domain", "cnt", "bspam_level"]

                     var url = mcEmailUrlSu($scope.server, x.u)
                     return MonsterCloud.post(url)
                       .then(function(data){
                            $scope.data = data
                       })
                }]
          }
        }], {
           title: x.d,
           paramsRequired:{ "server": "@" },
           menuKey: templateDirectory,
           keywords: ["amavis"],
        });


  })

  app.directive("mcSuperuserAmavisQuarantine", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"amavis-quarantine"),
        controller: ["$scope", "MonsterCloud", "mcEmailUrlSu","mcEmailDownload","pagerFactorySingleShot",
          function($scope, MonsterCloud, mcEmailUrlSu, mcEmailDownload, pagerFactorySingleShot){

               $scope.lookup = {}
               $scope.fields = ["time_iso","mail_id","from_addr","subject","client_addr","size","spam_level"]

               $scope.remove = function(msg){
                  return MonsterCloud.delete(url+"/"+msg.mail_id)
                    .then(function(){
                       return $scope.refetch();
                    })
               }
               $scope.download = function(msg){
                  return mcEmailDownload($scope.server, MonsterCloud.get(url+"/"+msg.mail_id))
               }

               var url = mcEmailUrlSu($scope.server, "amavis/msgs/quarantine")

               $scope.refetch = function(){
                 return MonsterCloud.get(url)
                   .then(function(re){
                      $scope.quarantined = re;
                   })

               }


               return $scope.refetch();


          }]
    }
  }], {
     title: "Manage Amavis quarantine",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["amavis"],
  });



  app.directive("mcSuperuserCluebringerGroups", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-groups"),
        controller: ["$scope", "MonsterCloud", "mcEmailUrlSu","$element","$compile",
          function($scope, MonsterCloud, mcEmailUrlSu, $element, $compile){

               $scope.list = function(group){
                  var t  = "<mc-superuser-cluebringer-group-members server=\""+$scope.server+"\" group=\""+group.Name+"\" ></mc-superuser-cluebringer-group-members>"
                  console.log(t)
                  var el = $compile( t )( $scope );
                  var sl = $element.find("#groupMod")
                  // console.log("sub element", sl)
                  sl.html( el );
               }

               var url = mcEmailUrlSu($scope.server, "cluebringer/groups")
               return MonsterCloud.get(url)
                 .then(function(data){
                      $scope.lookup = data
                 })
          }]
    }
  }], {
     title: "E-mail policy groups",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["cluebringer", "policyd"],
  });

  app.service("cluebringerQuotasProcessor", function(){
      return function(input) {
            var Quotas = []
            Array("hourly","daily").forEach(function(x){
               if((!input[x])||(typeof input[x].Limit == "undefined")) return
               Quotas.push({Limit:input[x].Limit,Interval:x,Message:input[x].Message})
            })
            return Quotas
      }
  })

  app.service("mcSuperuserCluebringerPolicies", ["mcEmailUrlSu", "MonsterCloud", function(mcEmailUrlSu, MonsterCloud){
      var re = function(server) {
          var url = re.getUrlBase(server);
          return MonsterCloud.get(url)
      }
      re.getUrlBase = function(server){
          return mcEmailUrlSu(server, "cluebringer/policies/outbound/")  
      }

      return re;
  }])

  app.directive("mcSuperuserCluebringerOutboundPolicies", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-outbound-policies"),
        controller:  showErrorsController({
          inject: ["mcSuperuserCluebringerPolicies","$element","$compile","cluebringerQuotasProcessor", '$alertSuccess'],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcSuperuserCluebringerPolicies, $element,$compile,cluebringerQuotasProcessor, $alertSuccess){

               $scope.url = mcSuperuserCluebringerPolicies.getUrlBase($scope.server)

               $scope.file = {};

               function showControl(t){
                  console.log(t)
                  var el = $compile( t )( $scope );
                  var sl = $element.find("#policyMod")
                  // console.log("sub element", sl)
                  sl.html( el );
               }

               function buildAndShowControl(policy,name){
                  var t  = "<"+name+" server=\""+$scope.server+"\" policy-name=\""+policy.Name+"\" ></"+name+">"
                  showControl(t)
               }

               $scope.backupPolicies = function(){
                  return MonsterCloud.post("/s/"+$scope.server+"/email/cluebringer/policies/backup")
                    .then(function(d) {
                        $scope.response = "";
                        var blob = new Blob([JSON.stringify(d)], {type: "text/json;charset=utf-8"});
                        var ts = new Date().toISOString().split('T')[0];
                        saveAs(blob, $scope.server+"-cluebringer-policies-backup-"+ts+".json");
                        operationInProgress = false;
                    })
               }
               function getRestorePayload(){
                  var decodedString = String.fromCharCode.apply(null, new Uint8Array($scope.file.content));
                  console.log("restoring", $scope.file, decodedString);
                  var n = JSON.parse(decodedString);
                  return n;      
               }
               function doRestore(mode){
                  return MonsterCloud.post("/s/"+$scope.server+"/email/cluebringer/policies/"+mode, getRestorePayload())
                    .then(function(){
                        return $alertSuccess();
                    })                
                    .then(function(){
                        return $scope.refetch();
                    })
               }
               $scope.mergePolicies = function(){
                  return doRestore("merge");
               }
               $scope.restorePolicies = function(){
                  return doRestore("restore");
               }

               $scope.members = function(policy){
                  buildAndShowControl(policy,"mc-superuser-cluebringer-outbound-policy-members")
               }

               $scope.quotas = function(policy){
                  buildAndShowControl(policy,"mc-superuser-cluebringer-outbound-policy-quotas")
               }

               $scope.flush = function(policy){
                  return MonsterCloud.post($scope.url+policy.Name+"/flush")
                    .then(function(x){
                        return $alertSuccess()
                    })
               }


               $scope.remove= function(policy){
                   return MonsterCloud.delete( $scope.url+policy.Name, {})
                     .then(function(){
                        return $scope.refetch();
                     })

               }

               $scope.refetch = function(){
                 return mcSuperuserCluebringerPolicies($scope.server)
                   .then(function(data){
                        $scope.lookup = data
                   })

               }

               $scope.refetch()
          },
          fireCallback: function($scope, MonsterCloud, $q, mcSuperuserCluebringerPolicies,$element,$compile,cluebringerQuotasProcessor){

            var d = {Name:$scope.data.Name, Description:$scope.data.Description}
            d.Quotas = cluebringerQuotasProcessor($scope.data)

            return MonsterCloud.put($scope.url, d)
               .then(function(){
                  $scope.data = {}
                  return $scope.refetch()
               })


          }
       })
    }
  }], {
     title: "E-mail outbound policies",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["cluebringer", "policyd"],
  });

  app.directive("mcSuperuserCluebringerOutboundPolicyQuotas", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-outbound-policy-quotas"),
        scope: { "server": "@", "policyName":"@" },
        controller:  showErrorsController({
          inject: ["mcEmailUrlSu","cluebringerQuotasProcessor","$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu, cluebringerQuotasProcessor){
             $scope.url = mcEmailUrlSu($scope.server, "cluebringer/policies/outbound/"+$scope.policyName+"/quotas")
             $scope.data = {}
             return MonsterCloud.get($scope.url)
               .then(function(d){
                  d.forEach(function(row){
                     $scope.data[row.Interval] = {}
                     $scope.data[row.Interval].Limit = row.Limit
                     $scope.data[row.Interval].Message = row.Message
                  })
               })

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu, cluebringerQuotasProcessor, $alertSuccess){

            var d = cluebringerQuotasProcessor($scope.data)

            return MonsterCloud.post($scope.url, d)
               .then(function(){
                return $alertSuccess()
               })


          }
       })
    }
  }]);


  app.directive("mcSuperuserCluebringerOutboundPolicyMembers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-outbound-policy-members"),
        scope: { "server": "@", policyName:"@" },
        controller:  showErrorsController({
          inject: ["mcEmailUrlSu"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu){

             $scope.url = mcEmailUrlSu($scope.server, "cluebringer/policies/outbound/"+$scope.policyName+"/members")


               $scope.remove= function(row){
                   return MonsterCloud.delete( $scope.url, {ea_email: row})
                     .then(function(){
                        return $scope.refetch();
                     })

               }

               $scope.refetch = function (){

                      return MonsterCloud.get($scope.url)
                        .then(function(data){
                           $scope.lookup = data;
                           $scope.policyMembers = data;
                        });

               }


               $scope.refetch()

          },
          fireCallback: function($scope, MonsterCloud, $q){

            return MonsterCloud.put($scope.url, $scope.data)
               .then(function(){
                  $scope.data = {}
                  return $scope.refetch()
               })


          }

        })

    }
  }]);


  app.directive("mcSuperuserCluebringerGroupMembers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-group-members"),
        scope: { "server": "@", group:"@" },
        controller:  showErrorsController({
          inject: ["mcEmailUrlSu"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu){

               $scope.url = mcEmailUrlSu($scope.server, "cluebringer/groups/"+$scope.group)



               $scope.remove= function(row){
                   return MonsterCloud.delete( $scope.url, {Member: row.Member})
                     .then(function(){
                        return  $scope.refetch()
                     })

               }

               $scope.refetch = function (){

                  return MonsterCloud.get($scope.url)
                    .then(function(data){
                       $scope.lookup = data
                       $scope.groupMembers = data;
                    })
               }


               $scope.refetch()

          },
          fireCallback: function($scope, MonsterCloud, $q){

            return MonsterCloud.put($scope.url, $scope.data)
               .then(function(){
                  $scope.data = {}
                  return $scope.refetch()
               })


          }

        })

    }
  }]);


  app.directive("mcSuperuserPostfixMailq", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"postfix-mailq"),
        controller: showErrorsController({
          inject: ["mcEmailTask","mcEmailUrlSu","mcAutoCloseParams"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailTask, mcEmailUrlSu, mcAutoCloseParams){

               $scope.lookup = {}
               $scope.fields = ["ts","mail_id","sender","recipient","size"]

               $scope.remove = function(msg){
                  return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_FROM_QUEUE", MonsterCloud.delete($scope.url+"/"+msg.mail_id), mcAutoCloseParams())
                    .then(function(){
                        return $scope.refetch();
                    })
               }
               $scope.view = function(msg){
                   var prom = MonsterCloud.get($scope.url+"/"+msg.mail_id)
                   return mcEmailTask($scope.server, "EMAIL_TASK_FETCH_MAILQ", prom)
               }

               $scope.delCategories = emailDelCategories
               $scope.del = {category:"email"}

               $scope.url = mcEmailUrlSu($scope.server, "postfix/mailq")
               $scope.refetch = function(){
                 return MonsterCloud.get($scope.url)
                   .then(function(data){
                        $scope.lookup.result = data
                   })
               }

               return $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailTask){
             return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_FROM_QUEUE", MonsterCloud.delete($scope.url+"/by/"+$scope.del.category, {emails: [$scope.del.email]}))
          }
        })

    }
  }], {
     title: "E-mail queue",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix", "mailq"],
  });

	app.directive('mcSuperuserEmailActions', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email-actions'),
	      controller: ["$scope", "MonsterCloud","mcEmailUrlSu","mcEmailTask","mcAutoCloseParams",'$alertSuccess',
	        function($scope, MonsterCloud,mcEmailUrlSu,mcEmailTask,mcAutoCloseParams,$alertSuccess){

               function showSuccess(prom){
                  return prom.then(function(){
                    return $alertSuccess()
                  })
               }

               $scope.reportTallies = function(){
               	   return showSuccess(MonsterCloud.post(mcEmailUrlSu($scope.server, "accounts/report/tallies")))
               }

               $scope.amavisCleanup = function(){
                   return showSuccess(MonsterCloud.post(mcEmailUrlSu($scope.server, "amavis/cleanup")))
               }

               $scope.autoExpunge = function(){
               	   var prom = MonsterCloud.post(mcEmailUrlSu($scope.server, "accounts/cleanup/autoexpunge"))
	                 return mcEmailTask($scope.server, "EMAIL_TASK_AUTOEXPUNGE", prom)
               }

               $scope.cleanJunk = function(){
               	   var prom = MonsterCloud.post(mcEmailUrlSu($scope.server, "accounts/cleanup/junk"))
	                 return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_JUNK", prom)
               }

               $scope.postfixFlushQueue = function(){
                 var prom = MonsterCloud.post(mcEmailUrlSu($scope.server, "postfix/flush"))
                 return showSuccess(mcEmailTask($scope.server, "EMAIL_TASK_FLUSH_QUEUE", prom, mcAutoCloseParams()))
               }

               $scope.postfixEmptyQueue = function(){
                 var prom = MonsterCloud.delete(mcEmailUrlSu($scope.server, "postfix/mailq/ALL"))
                 return showSuccess(mcEmailTask($scope.server, "EMAIL_TASK_EMPTY_QUEUE", prom, mcAutoCloseParams()))
               }
	       }]
	   }
	}], {
     title: "E-mail admin actions",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
  })


  app.service("mcEmailPagerService", [function(){
      return function($scope, dataName, dataService, fuse) {

         return dataService.Lookup()
           .then(function(d){
              $scope[dataName] = d.result[dataName];
           })

      }
  }])


	app.directive('mcSuperuserEmailDomains', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email-domains'),
	      controller: ["$scope", "MonsterCloud","mcEmailDomainService","mcEmailTask", "accountLookup","webhostingLookup", "mcEmailPagerService",
	        function($scope, MonsterCloud,mcEmailDomainService,mcEmailTask,accountLookup,webhostingLookup, mcEmailPagerService){

               var domainService = mcEmailDomainService($scope.server, "su")

               domainService.onRefresh(function(r){
               	console.log("onrefresh", r)
                    accountLookup(r.domains, "do_user_id")
                    webhostingLookup(r.domains, $scope.server, null, "do_webhosting")
               })


               $scope.delete = function(domain){
               	  var d = {do_id: domain.do_domain_name}
               	  var prom = domainService.Remove(d)
	               return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_DOMAIN", prom)
	                 .then(function(){
	                    return domainService.Lookup(true)
	                 })
               }

               mcEmailPagerService($scope, "domains", domainService);
	       }]
	   }
	}], {
     title: "E-mail admin domains",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix"],
  })

	app.directive('mcSuperuserEmailAccounts', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email-accounts'),
	      controller: ["$scope", "MonsterCloud","mcEmailAccountService","mcEmailTask", "accountLookup","webhostingLookup", "mcEmailPagerService","mcEmailUrlSu",
	        function($scope, MonsterCloud,mcEmailAccountService,mcEmailTask,accountLookup,webhostingLookup, mcEmailPagerService,mcEmailUrlSu){

               var accountService = mcEmailAccountService($scope.server, "su")

               accountService.onRefresh(function(r){
               	    console.log("onrefresh", r)
                    accountLookup(r.accounts, "ea_user_id")
                    webhostingLookup(r.accounts, $scope.server, null, "ea_webhosting")
               })


               $scope.doSuspended = function(account, suspendedness) {
                 var url = mcEmailUrlSu($scope.server, "accounts/account/"+account.ea_id)
                 return MonsterCloud.post(url, {ea_suspended: suspendedness})
                   .then(function(){
                      account.ea_suspended = suspendedness;
                   })

               }

               $scope.recalculate = function(account){
                 var url = mcEmailUrlSu($scope.server, "accounts/account/"+account.ea_id+"/tallies/recalculate")
                 var prom = MonsterCloud.post(url)
                 return mcEmailTask($scope.server, "EMAIL_TASK_RECALCULATE_TALLY", prom)
                   .then(function(){
                     return accountService.Lookup(true)
                   })

               }

               $scope.delete = function(account){
	               var prom = accountService.Remove(account)
	               return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_ACCOUNT", prom)
	                 .then(function(){
	                   return accountService.Lookup(true)
	                 })
               }

               mcEmailPagerService($scope, "accounts", accountService);

	       }]
	   }
	}], {
     title: "E-mail accounts",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["dovecot", "postfix"],
  })

	app.directive('mcSuperuserEmailAliases', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email-aliases'),
	      controller: ["$scope", "MonsterCloud","mcEmailAliasService", "accountLookup","webhostingLookup", "mcEmailPagerService",
	        function($scope, MonsterCloud,mcEmailAliasService,accountLookup,webhostingLookup, mcEmailPagerService){

               var aliasService = mcEmailAliasService($scope.server, "su")

               aliasService.onRefresh(function(r){
               	console.log("onrefresh", r)
                    accountLookup(r.aliases, "el_user_id")
                    webhostingLookup(r.aliases, $scope.server, null, "el_webhosting")
               })

               $scope.delete = function(alias){
               	  aliasService.Remove(alias)
               }

               $scope.setVerified = function(alias){
                  var c = {el_id: alias.el_id, el_verified: true};
                  return aliasService.Change(c);
               }

               mcEmailPagerService($scope, "aliases", aliasService)

	       }]
	   }
	}], {
     title: "E-mail aliases",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix"],
  })

	app.directive('mcSuperuserEmailBccs', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email-bccs'),
	      controller: ["$scope", "MonsterCloud","mcEmailBccService", "accountLookup","webhostingLookup", "mcEmailPagerService",
	        function($scope, MonsterCloud,mcEmailBccService,accountLookup,webhostingLookup, mcEmailPagerService){

               var bccService = mcEmailBccService($scope.server, "su")

               bccService.onRefresh(function(r){
               	console.log("onrefresh", r)
                    accountLookup(r.bccs, "bc_user_id")
                    webhostingLookup(r.bccs, $scope.server, null, "bc_webhosting")
               })

               $scope.delete = function(bcc){
               	  bccService.Remove(bcc)
               }

               $scope.setVerified = function(bcc){
                  var c = {bc_id: bcc.bc_id, bc_verified: true};
                  return bccService.Change(c);
               }

               mcEmailPagerService($scope, "bccs", bccService)


	       }]
	   }
	}], {
     title: "E-mail BCCs",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix"],
  })

  app.directive('mcSuperuserEmailDkimKeys', ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-dkim-keys'),
        controller: ["$scope", "MonsterCloud",
          function($scope, MonsterCloud){

            return MonsterCloud.get("/s/"+$scope.server+"/su/email/dkim/")
              .then(function(r){
                  $scope.dkimKeys = r;
              })

         }]
     }
  }], {
     title: "E-mail DKIM keys",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix","amavis", "amavisd", "dkim"],
  })

	app.directive('mcSuperuserEmail', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'email'),
	      scope: { "server": "@"  },
	   }
	}])



  app.directive("mcSuperuserMailqAutoCleanup", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"mailq-auto-cleanup"),
        scope: { "server": "@", "policyName":"@" },
        controller:  showErrorsController({
          inject: ["mcEmailUrlSu","pagerFactorySingleShot"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu,pagerFactorySingleShot){
             $scope.url = mcEmailUrlSu($scope.server, "postfix/mailq-auto/")
             $scope.data = {}

             $scope.delCategories = emailDelCategories

             $scope.delete = function(row){

               return MonsterCloud.delete($scope.url+row.mc_id, {})
                 .then($scope.refetch)

             }
             $scope.refetch = function(){

               return pagerFactorySingleShot($scope, {
                  fuse: {keys: [ "mc_email", "mc_type" ]},
                  dataName: "autoRows",
                  shot: function(){

                    return MonsterCloud.get($scope.url)
                  },
               })()

             }

             return $scope.refetch()

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailUrlSu){

            return MonsterCloud.put($scope.url, $scope.data)
               .then(function(){
                  $scope.data = {}
                  return $scope.refetch()
               })


          }
       })
    }
  }], {
     title: "Mail queue automatic cleanup",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix", "mailq"],
  });


  app.directive("mcCluebringerCleanup", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"cluebringer-cleanup"),
        controller: ["MonsterCloud", "$scope", "mcEmailTask", function(MonsterCloud, $scope, mcEmailTask) {

               $scope.cleanup = function(){
                   var prom = MonsterCloud.post("/s/"+$scope.server+"/email/cluebringer/cleanup")
                   return mcEmailTask($scope.server, "TASK_CLUEBRINGER_EMAIL_CLEANUP", prom)

               }

        }],

    }
  }], {
     title: "Cluebringer cleanup",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["cluebringer"],
  });


})(app)

