(function(app){

  var templateDirectory = "git-superuser"

app.directive("mcGitConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/git'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Git microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

	app.directive('mcSuperuserGitList', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'git-list'),
	      controller: ["$scope", "MonsterCloud", "accountLookup","webhostingLookup",
	        function($scope, MonsterCloud,accountLookup,webhostingLookup){
	           $scope.gits = []

	          
	               return MonsterCloud.get("/s/"+$scope.server+"/su/git/")
	                 .then(function(data){
	                    $scope.gits = data
	                    accountLookup($scope.gits, "g_user_id")
                        webhostingLookup($scope.gits, $scope.server, null, "g_webhosting")
	                    
	                    $scope.showListing = $scope.gits.length > 0                              
	                 })

	       }]
	   }
	}], {
		title: "List of Git repositories",
		menuKey: templateDirectory,
		paramsRequired: {server:"@"},
	})

})(app)

