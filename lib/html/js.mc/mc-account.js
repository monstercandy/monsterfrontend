(function(app){

  var templateDirectory = "accountapi"

app.directive('mcCredentialAdd', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'credential-add'),
      scope: { "refetch": "&" },
      controller:  showErrorsController({
        additionalInitializationCallback: function($scope, MonsterCloud){
             $scope.supportedPermissions = []
             $scope.cred = {}
             $scope.tmp = {}
             return MonsterCloud.get("/account/credentials/supported")
               .then(function(data){
                   $scope.supportedPermissions = data
               })

        },
        fireCallback: function($scope, MonsterCloud){

             return MonsterCloud.put("/account/credentials", $scope.cred)
               .then(function(data){
                   $scope.cred = {}
                   $scope.tmp = {}
                   return $scope.refetch()
               })
        }
      })
   }
}])

app.directive("mcSb2DomainMenu", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory, "mc-sb2-domain-menu"),
    scope: {},
    controller: ["$scope", "mcRoutesService", function($scope, mcRoutesService){
        var p = mcRoutesService.getCurrentState();
        $scope.params = {server: p.stateParams.server, whId: p.stateParams.whId, domain:p.stateParams.domain, display: p.stateParams.display};
    }]
  }
}])


  app.directive("mcAvailableDomains", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'available-domains'),
      scope: { },
      controller: ["$scope", "MonsterCloud", "mcAccountWebhostingDomains","mcWebhostingFactory", function($scope, MonsterCloud, mcAccountWebhostingDomains, mcWebhostingFactory){
           return mcAccountWebhostingDomains()
             .then(function(dom){
                $scope.wds = dom;

                if($scope.$parent) {
                   $scope.$parent.mcAccountWebhostingDomainsCount = dom.length;
                }

                dom.forEach(function(d){
                   d.params = {domain: d.wd_domain_canonical_name, display:d.wd_domain_name, server: d.wd_server, whId: d.wd_webhosting};
                   if(!d.wd_webhosting) return;

                   mcWebhostingFactory(d.wd_server, d.wd_webhosting).Lookup()
                     .then(function(w){
                        d.wh_name = w.wh_name;
                     })
                })
             })
      }]
   }
}])

Array("Domains", "Webhostings", "WebhostingDomains").forEach(function(n){
  app.service("mcAccount"+n, ["MonsterCloud", "mcCachedLookupProvider", function(MonsterCloud, mcCachedLookupProvider){
     return mcCachedLookupProvider.Factory(function(){
         return MonsterCloud.get("/account/"+n.toLowerCase())
           .then(function(res){
              if(n != "Domains")
                  return res;

              res.sort(function(a,b){
                return a.d_domain_name != b.d_domain_name ? (a.d_domain_name > b.d_domain_name ? 1 : -1) : 0;
              });

              return res;

           })
     })
  }])

})


app.service("mcWebhostingTemplateInfo", ["MonsterCloud", function(MonsterCloud) {
  var cache = {}
  return function(template_name) {
    if(!cache[template_name])
        cache[template_name] = MonsterCloud.get("/pub/account/webhostingtemplate/"+template_name)

    return cache[template_name]
  }
}])

app.directive("mcWebhostingTemplateFlag",["templateDir", "mcWebhostingTemplateInfo", function(templateDir, mcWebhostingTemplateInfo){
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            return mcWebhostingTemplateInfo(attrs.template)
              .then(function(data){
                    var html = data[attrs.param] ? '<span class="glyphicon glyphicon-ok" style="color:green"></span>' : "--";
                    element.html(html);

              })
        }
    };
}])

app.directive("mcWebhostingTemplateData",["templateDir", "mcWebhostingTemplateInfo", function(templateDir, mcWebhostingTemplateInfo){
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            return mcWebhostingTemplateInfo(attrs.template)
              .then(function(data){
                    var html = data[attrs.param];
                    element.html(html);

              })
        }
    };
}])

app.service("CredentialLogic", ["MonsterCloud", function(MonsterCloud){
   return function(data){
          data.forEach(function(q){

            if(typeof q.can_be_deleted != "undefined") {
              q.primary = q.is_primary
              q.showDel = q.can_be_deleted
            } else {
              q.primary = (MonsterCloud.UserProfile.u_primary_email == q.c_username)
              q.showDel = !q.primary
            }

          })

          return data
   }
}])


app.directive("mcCallbackRequestReceived",["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'callback-request-received'),
    }
}],
       {
       title:"Callback request received",
      }
)

app.directive('mcLoginlogUser', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'loginlog-user'),
    }
}])

app.directive('mcLoginlogAndSettings', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'loginlog-and-settings'),
    }
}],
       {
       title:"Login log",
	   menuKey: templateDirectory,
       keywords: ["login", "account", "log"]
      }
)

app.directive('mcUserSettings', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'user-settings'),
    }
}],
       {
       title:"User settings",
	   menuKey: templateDirectory,
       keywords: ["change","password", "account", "email", "e-mail", "telephone", "credentials"]
      }
)

app.directive('mcTwoFactorAuthentication', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'two-factor-authentication'),
      scope: {},
      transclude: true,
      controller:  showErrorsController({

        inject: ["$alertSuccess", "$alertError"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){

            $scope.reinit = function(){
                return MonsterCloud.post("/account/totp/status", {})
                  .then(function(totp){
                          $scope.totp = totp;
                          if(!$scope.totp.u_totp_active) {
                              var qrcode = new QRCode(document.getElementById("qrcode"));
                              qrcode.makeCode($scope.totp.u_totp_url);
                          }

                  })
            }


            return $scope.reinit();
        },
        fireCallback: function($scope, MonsterCloud,$q,$alertSuccess,$alertError){

           var data = {userToken: $scope.t.userToken};

           var action = $scope.totp.u_totp_active ? "inactivate" : "activate";

           return MonsterCloud.doRequest({method:"POST", url: "/account/totp/"+action, data: data, relayException: true})
              .then(function(){
                   $scope.totp.u_totp_active = !$scope.totp.u_totp_active;
                   return $alertSuccess()
              })
              .then(function(){
                   return $scope.reinit();
              })
              .catch(function(){
                   return $alertError();
              })
        }

      })
    }
}],
       {
       title:"Two factor authentication",
     menuKey: templateDirectory,
       keywords: ["mfa", "2fa", "authy", "authenticator", "otp", "totp", "hotp"]
      })

app.service("mcLoginlogSubsystems", ["MonsterCloud", function(MonsterCloud){
   return function(){
      return MonsterCloud.get("/pub/account/loginlog/subsystems");
   }
}])

app.directive("mcLoginlog", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"loginlog"),
    "scope": { "uriPrefix": "@", "hideUser": "@", "hideGrants":"@"},
    "controller": ["$scope", "MonsterCloud", "mcLoginlogSubsystems", function($scope, MonsterCloud,  mcLoginlogSubsystems){

        // $scope.subsystems = []  // it is with one-time prefix in the template, so we should nto initialize it here!

        $scope.successfullness = ["","0","1"]

        $scope.refetch = function(tableState){

               $scope.displayedCollection = null;

               var d = queryDetails(tableState);
               return MonsterCloud.post( $scope.uriPrefix + "/count", d)
                 .then(function(data){
                    tableState.pagination.totalItemCount = data.count;

                    var p = angular.extend(d, tableState.pagination.params);
                    return MonsterCloud.post( $scope.uriPrefix + "/search", d);
                 })
                 .then(function(data){
                    $scope.displayedCollection = data.events;
                 })          
        }


        function queryDetails(tableState) {
          var d = {where: tableState.search.str};
          Array("notbefore","notafter","l_ip","l_successful", "l_subsystem").forEach(function(v){
            if(($scope[v] !== "")&&($scope[v] !== null))
               d[v] = $scope[v]
          })
          return d
        }



       return mcLoginlogSubsystems()
         .then(function(data){
             $scope.subsystems = Array("").concat(data)
         })


    }]
  }
}])

app.directive("mcEditAccountLoginlogSettings", ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'edit-account-loginlog-settings'),
      scope: {},
      transclude: true,
      controller:  showErrorsController({

        inject: ["$alertSuccess", "mcLoginlogSubsystems"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcLoginlogSubsystems){
            $scope.Account = {}
            mcLoginlogSubsystems().then(function(subsystems){
               $scope.subsystems = subsystems;
            })
            MonsterCloud.getCurrentAccount()
              .then(function(account){
                      $scope.Account = account
              })
        },
        fireCallback: function($scope, MonsterCloud,$q,$alertSuccess){

           var data = {}
           Array("u_email_login_ok_subsystems", "u_email_login_ok", "u_email_login_fail").forEach(function(q){
               data[q] = $scope.Account[q]
           })
           return MonsterCloud.post("/account", data)
              .then(function(){
                   return $alertSuccess()
              })
        }



      })
    }
}])

app.directive('mcEditAccountCredentials', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'edit-account-credentials'),
      scope: {},
      controller:  ["$scope", "MonsterCloud", "CredentialLogic", function($scope, MonsterCloud, CredentialLogic){
           $scope.credentials = []

           $scope.data_process = function(data){



                $scope.credentials = CredentialLogic(data)
           }

           $scope.del = function(selectedCredential) {
             return MonsterCloud.delete("/account/credential/"+selectedCredential.c_username)
               .then(function(){

                      $scope.credentials = $scope.credentials.filter(function(e){return e !== selectedCredential})

               })

           }

           $scope.refetch = function(){
             return MonsterCloud.get("/account/credentials")
               .then(function(data){
                   $scope.data_process(data)
               })

           }


           return $scope.refetch()

        }]
   }
}])


  app.controller('TelConfirmationController', showErrorsController({
    inject: ["$uibModalInstance", "data", "$rootScope"],
    additionalInitializationCallback: function($scope, MonsterCloud, $q, $uibModalInstance, data, $rootScope){
        $scope.data = angular.copy(data);

        $scope.baseUrl = "/account/tel/"+$scope.data.tel.t_human+"/";

        $scope.showSendSms= true;
        $scope.showValidate= false;
        $scope.tmp = {};

        $scope.reloadCaptcha = function(){
           $rootScope.$emit("onCaptchaMismatch");
        }

        $scope.ok = function () {
          $uibModalInstance.close();
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss()
        };

    },
    fireCallback: function($scope, MonsterCloud){
      if($scope.showSendSms) {
         return MonsterCloud.post($scope.baseUrl+"send", $scope.tmp)
           .then(function(){
              $scope.reloadCaptcha();
              $scope.showSendSms = false;
              $scope.showValidate = true;
           })
      }
      if($scope.showValidate) {
         return MonsterCloud.post($scope.baseUrl+"verify", $scope.tmp)
           .then($scope.ok)
      }

    }

  }));
  app.factory('$telConfirmation', ["$uibModal", 'templateDir', function ($uibModal, templateDir) {
    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir(templateDirectory,'tel-validation'),
        controller: 'TelConfirmationController',
      }

      settings = angular.extend(defaults, (settings || {}));

      if ('templateUrl' in settings && 'template' in settings) {
        delete settings.template;
      }

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings).result;
    };
  }])

app.directive('mcEditAccountTel', ["templateDir",function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'edit-account-tel'),
      scope: {},
      controller:  showErrorsController({
        inject:["$telConfirmation"],
        additionalInitializationCallback: function($scope, MonsterCloud,$q, $telConfirmation){
           $scope.tmp = {}
           $scope.setAsPrimary = false
           $scope.tels = []

           $scope.select = function(tel){
              $scope.selectedTel = tel
              $scope.tmp = angular.copy(tel)

              $scope.showDelete = (tel.t_phone_no && !tel.t_primary)
              $scope.showSetAsPrimary = ($scope.showDelete && tel.t_confirmed)
              $scope.showValidate = $scope.selectedTel && !$scope.selectedTel.t_confirmed
           }

           $scope.showValidation = function(){
              return $telConfirmation({tel: $scope.selectedTel})
                .then(function(){
                    $scope.selectedTel.t_confirmed = true;
                    $scope.select($scope.selectedTel);
                })
           }

           $scope.setPrimary = function(){
             return MonsterCloud.post("/account/tel/"+$scope.selectedTel.t_human+"/set_primary", {})
                .then(function(){

                      $scope.tels.map(function(d){d.t_primary = false})
                      $scope.selectedTel.t_primary = 1
                      $scope.select($scope.selectedTel)
                      $scope.data_process($scope.tels)


                })

           }

           $scope.data_process = function(data){
                data.forEach(function(q){
                  q.showMod = true;
                })

                $scope.tels = data
           }


           $scope.del = function(){
             return MonsterCloud.delete("/account/tel/"+$scope.selectedTel.t_human)
               .then(function(){

                      $scope.tels = $scope.tels.filter(function(e){return e !== $scope.selectedTel})

                      $scope.select({})

               })

           }

           $scope.refetch = function(){
             return MonsterCloud.get("/account/tels")
               .then(function(data){
                      $scope.data_process(data)
               })

           }


           return $scope.refetch()

        },
        fireCallback: function($scope, MonsterCloud){

             var d = {}
             Array("t_country_code","t_category","t_area_code","t_phone_no").forEach(function(c){
                d[c] = $scope.tmp[c];
             })

             return MonsterCloud.put("/account/tels", d)
               .then(function(data){
                   return $scope.refetch()
               })
        }
      })
   }
}])




app.directive('mcEditAccountEmail', ["templateDir", function(templateDir){


    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'edit-account-email'),
      scope: {},
      controller:  showErrorsController({
        inject:["$alertSuccess"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){
           $scope.tmp = {}
           $scope.setAsPrimary = false
           $scope.emails = []
           $scope.availableEmailCategories = []

           MonsterCloud.get("/pub/account/email_categories")
             .then(function(data){
                    $scope.availableEmailCategories =data

             })


            $scope.showConfirmation=function(){
                  return $alertSuccess({timeout:0,message:"We've sent you an email with further instructions how to confirm validity of your e-mail address"})
            }

           $scope.select = function(email){
              $scope.selectedEmail = email
              $scope.tmp = angular.copy(email)
              $scope.showSendConfirmation = (email.e_email && !email.e_confirmed)
              $scope.showSetAsPrimary = (email.e_email && !email.e_primary && email.e_confirmed)
              $scope.showDelete = (email.e_email && !email.e_primary)
           }

           $scope.setPrimary = function(){
             return MonsterCloud.post("/account/email/"+$scope.selectedEmail.e_email+"/set_primary", {})
                .then(function(){

                      $scope.emails.map(function(d){d.e_primary = false})
                      $scope.selectedEmail.e_primary = 1
                      $scope.select($scope.selectedEmail)


                })

           }
           $scope.sendConfirmationTo = function(email){
             return MonsterCloud.post("/account/email/"+email+"/send/confirmation", {})

           }

           $scope.sendConfirmation = function(){
             return $scope.sendConfirmationTo($scope.selectedEmail.e_email)
                .then(function(){
                   $scope.showConfirmation()
                })

           }

           $scope.modCat = function(){

             return MonsterCloud.post("/account/email/"+$scope.selectedEmail.e_email+"/categories", {e_categories:$scope.tmp.e_categories})
               .then(function(){

                      $scope.selectedEmail.e_categories = angular.copy($scope.tmp.e_categories)
               })


           }


           $scope.del = function(){
             return MonsterCloud.delete("/account/email/"+$scope.selectedEmail.e_email)
               .then(function(){

                      $scope.emails = $scope.emails.filter(function(e){return e !== $scope.selectedEmail})

                      $scope.select({})
               })

           }

           $scope.refetch = function(){
             return MonsterCloud.get("/account/emails")
               .then(function(data){
                      $scope.emails = data
               })

           }

/*
router.post("/account/:account_id/email/:email/categories", expressEmailCommon(EMAIL_NOT_CARE, function(trx,account,email, req){
*/

           return $scope.refetch()

        },
        fireCallback: function($scope, MonsterCloud){

             return MonsterCloud.put("/account/emails", $scope.tmp)
               .then(function(data){
                 return $scope.sendConfirmationTo($scope.tmp.e_email)
               })
               .then(function(data){
                   $scope.showConfirmation()

                   return $scope.refetch()
               })
        }
      })
   }
}])


app.filter('mcLoggedInAccounts', ["MonsterCloud", function(MonsterCloud) {
  return function(input) {
    input = input || []

    var out = []
    input.forEach(function(q){
        if(!q.token) return;

        q.uri = MonsterCloud.frontPageByUidi(q.u_id)
        out.push(q)
    })

    return out
  };
}])

app.controller('mcLoggedInAccountListCtrl', ["$scope", "mcLoggedInAccountsFilter", "mcSessionCookies", function($scope, mcLoggedInAccountsFilter, mcSessionCookies){
    $scope.AccountSlots = mcSessionCookies.GetTokenArray()
    // console.log("SHIT", $scope.AccountSlots);
    $scope.LoggedInAccounts = mcLoggedInAccountsFilter($scope.AccountSlots)

}])


app.directive('mcLoggedInAccountList', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'logged-in-account-list'),
      scope: {},
      controller:  "mcLoggedInAccountListCtrl"
    }
}])

app.directive('mcEditAccount',["templateDir", function(templateDir){
    return {
      restrict: "E",
      transclude: true,
      templateUrl: templateDir(templateDirectory,'edit-account'),
      scope: { },
      controller:  showErrorsController({
         inject: ["$alertSuccess"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, mcConfig){
            $scope.mcConfig = mcConfig.config
            MonsterCloud.getCurrentAccount()
              .then(function(account){
                      $scope.Account = account
              })
         },
         inject: ["mcConfig"],
         fireCallback: function($scope, MonsterCloud,$q,$alertSuccess){

           var data = {}
            Array('u_name', 'u_sms_notification', 'u_callin').forEach(function(q){
               data[q] = $scope.Account[q]
            })
           return MonsterCloud.post("/account", data)
              .then(function(){
                   return $alertSuccess()
              })
         }

      })
    }
}])
app.directive('mcShowAccount',["templateDir", function(templateDir){
    return {
      restrict: "E",
      transclude: true,
      templateUrl: templateDir(templateDirectory,'show-account'),
      scope: { "redirectAfter": "@" },
      controller: ["$scope","MonsterCloud","$window", function($scope, MonsterCloud, $window){
            $scope.Account = {}
            MonsterCloud.getCurrentAccount()
              .then(function(account){
                      $scope.Account = account
              })
            $scope.fire = function(){
                $window.location.href = $scope.redirectAfter
            }

      }]
    }
}])

app.directive('mcDomainInfo',["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'domain-info'),
      transclude: true,
      link: function(scope, tElement, attrs, control, transclude) {
              transclude(scope, function (clone) {
                  if(!clone.children.length > 1)
                     return;
                  var target = tElement.find("[transclude-next]");

                  var source = angular.element(clone[1].innerHTML);
                  console.log("mcDomainInfo: element found?:", target, clone, source)
                  source.insertAfter(target);
              });
      },

      controller: ["mcWebhostingFactory", "$scope", function(mcWebhostingFactory, $scope){

          if($scope.whId) {
            var f = mcWebhostingFactory($scope.server, $scope.whId)
            return f.Lookup()
              .then(function(result){
                $scope.wh = result;
              })

          }

      }]
    }
}], {
   title: "Domain info",
   menuKey: templateDirectory,
   paramsRequired: {server:"@", domain: "@"},
   paramsOptional: {whId: "@", display: "@", dontShowWebstore: "@"},
})

app.service("mcCountryCodes", ["mcConfig","mcCachedLookupProvider","MonsterCloud","$filter",function(mcConfig,mcCachedLookupProvider,MonsterCloud,$filter){
     var re = mcCachedLookupProvider.Factory(function(){
         return MonsterCloud.get("/pub/account/country_codes")
             .then(function(data){
                  return transformCountryCodes(data);
             })
     })

     re.initScope = function($scope){
         $scope.availableCountryCodes = [];
         $scope.defaultCountryCode = re.defaultCode();
         return re()
           .then(function(data){
              $scope.availableCountryCodes = data;
           })
     }

     re.defaultCode = function(){
        return mcConfig.config.default_country_code;
     }

     return re;

     function transformCountryCodes(rawCountryCodes){

        var re = []
        for (var i = 0, keys = Object.keys(rawCountryCodes), ii = keys.length; i < ii; i++) {
          if(!rawCountryCodes[keys[i]]) continue;
          var str = rawCountryCodes[keys[i]]+" ("+keys[i]+")"
          //console.log("val:", rawCountryCodes[keys[i]], "key:", keys[i], "str:", str)
          re.push({"id": rawCountryCodes[keys[i]], "value":str})
        }
        // console.log(re)
        return $filter("orderBy")(re, "value")
     }

}])

app.directive('mcTelEditor',["templateDir", function(templateDir) {
    var re = {
      restrict: "E",
      scope: {
         "model": "=",
         "dataForm": "=data",
         "optional": "=",
      },
      templateUrl: templateDir(templateDirectory,'tel-editor'),
      controller: ["$scope", "mcCountryCodes", function($scope, mcCountryCodes){
           $scope.availableTelephoneCategories = ["HOME", "WORK", "MOBILE", "FAX"]

           if(!$scope.model.t_country_code)
              $scope.model.t_country_code = mcCountryCodes.defaultCode();

           return mcCountryCodes.initScope($scope)
      }]
    }

    return re
}])


app.directive('mcCaptchaBlock',["templateDir", function(templateDir) {
    var re = {
      restrict: "E",
      scope: {
         "kind": "@",
         "model": "=",
         "form": "="
      },
      templateUrl: templateDir(templateDirectory,'captcha-block'),
      controller: ["$scope", "captchaService", '$rootScope', 'MonsterCloud', '$sce', '$element', '$timeout', function($scope, captchaService, $rootScope, MonsterCloud, $sce, $element, $timeout){

           var imageUrl = $scope.kind ? "/pub/captcha" : "/account/captcha";

           var grecaptchaIds = Array();


          $rootScope.$on('onCaptchaMismatch',function(event){
              console.log("catched onCaptchaMismatch event in mcCaptchaBlock!");
              $scope.model.captcha = "";
              if($scope.recaptchaMode) {
                grecaptchaIds.forEach(function(id){
                   grecaptcha.reset(id);
                })
              }
              else
                $scope.reloadCaptcha();
          });

           $scope.captcha_expected = false;
           $scope.reloadCaptcha = function(){
             console.log("reloading captcha")

             $scope.htmlMode = false;
             $scope.recaptchaMode = false;
             $scope.decachedImageUrl = "/api"+imageUrl + '?decache=' + Math.random();
             return MonsterCloud.get(imageUrl+"/v2")
               .then(function(d){


                  if(d.recaptcha) {
                    console.log("rendering recaptcha!");
                    $scope.recaptchaMode = true;

                    recaptchaAttempts = 0;
                    renderRecaptcha();

                    function renderRecaptcha(){
                       if(recaptchaAttempts > 5) return;
                       recaptchaAttempts++;
                       var recaptchaBlockDom = $element.find("recaptcha")[0];
                       if((!grecaptcha)||(!recaptchaBlockDom)) return setTimeout(renderRecaptcha, 500);
                       // console.log("XXXXXXXXXXXXXXXXXXX", recaptchaBlockDom);
                       d.recaptcha.callback = function(data){
                          console.log("recaptcha was kind to call us back", data);
                          $timeout(function(){
                             $scope.model.captcha = data;
                          }, 0)
                       }
                       grecaptchaIds.push(grecaptcha.render(recaptchaBlockDom, d.recaptcha));
                    }

                  } else if(d.html) {
                    $scope.htmlMode = true;
                    $scope.svg = $sce.trustAsHtml(d.html);
                  }

               })
           }

           if($scope.kind) {
             captchaService.then(function(data){

                   if(data[$scope.kind])
                      turnOnCaptcha();
             })
           }
           else
            turnOnCaptcha();

           function turnOnCaptcha() {
              $scope.captcha_expected = true
              $scope.reloadCaptcha()

           }



      }]
    };
    return re
  }])

app.directive('mcRegistrationOrWarning', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      transclude: true,
      templateUrl: templateDir(templateDirectory,'registration-or-warning'),
      scope: { "redirectAfter": "@" },
      controller: showErrorsController({
         relayUserProfile: true,
      })

    };
  }])
app.directive('mcRegistrationOrShowAccount', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'registration-or-show-account'),
      scope: { "shoppingCart": "@", "redirectAfter": "@", "ufTos":"@" },
      controller: showErrorsController({
         relayUserProfile: true,
         inject: ["mcConfig"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, mcConfig){

            if(mcConfig.config.callback_request_redirect) {
              return MonsterCloud.get("/pub/account/callback_request")
                .then(function(d){
                   $scope.callbackSupported = d.supported
                })
            }

         },
      })

    };
  }])

app.directive('mcRegistration', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      transclude: true,
      templateUrl: templateDir(templateDirectory,'registration'),
      scope: { "shoppingCart": "@", "redirectAfter": "@", "nextButton":"@", "callbackSupported":"@", "ufTos":"@" },
      controller:  showErrorsController({
      	 fireFunctionName: "fire",
      	 dataFormName: "userForm",
      	 isWorkingName: "isSaving",
      	 additionalInitializationCallback: function($scope, MonsterCloud, $q, lngService, mcConfig){
            console.log("registration redirect after is: ", $scope.redirectAfter)
            $scope.user = {}
            if($scope.ufTos)
              $scope.user.uf_tos = true
            $scope.tmp = {}
            $scope.mcConfig = mcConfig.config;
            $scope.registrationTelOptional = mcConfig.config.registrationTelOptional

            $scope.register = function(){
               $scope.loginPreredirectCallback = null
               $scope.actualRedirectAfter = $scope.redirectAfter
               return $scope.fire()
            }
            $scope.callback_request = function(){
               // scope will be emptied at the end of firing before callback request takes place
               // so we make a copy here before!
               var dataPayload = {
                      shopping_cart:$scope.shoppingCart,
                      message:$scope.tmp.callback_request_message
                   }

               $scope.loginPreredirectCallback = function(){
                    console.log("and now sending the callback request", dataPayload)
                   return MonsterCloud.post("/account/callback_request", dataPayload)
               }
               $scope.actualRedirectAfter = mcConfig.config.callback_request_redirect
               return $scope.fire()
            }

      	 },
         inject: ["lngService", "mcConfig"],
      	 fireCallback: function($scope, MonsterCloud, $q, lngService){

             var d = angular.copy($scope.user)
             d.u_language = lngService.language
             console.log("registration in progress!", d)
             var p= MonsterCloud.doRequest({method:"POST", url: "/pub/account/registration",data: d})
             p.then(function(response){

                 return MonsterCloud.doLogin({"username": d.e_email, "password": d.c_password}, $scope.actualRedirectAfter, $scope.loginPreredirectCallback)

             })
             return p
      	 }

      })


    };
  }])

app.directive('mcChangePassword', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'change-password'),
      controller:  showErrorsController({
        inject: ["$location", "$alertSuccess"],
        fireFunctionName: "save",
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $location){
            $scope.chg = {} // this is important because of the ng-if in the template

            if(!$scope.forgotten) return

            var d = $location.search()

            return MonsterCloud.doRequest({"url":"/pub/account/forgotten/auth","method":"POST", data: d,relayException: true})
                .then(function(data){
                    //Result looks something like this: {"token":"sPXOgkt5chIOnLVeUzsWvA==","expires_at":"2016-09-10 08:59:35","u_name":"Felhasználói fiók neve","u_id":"1"}
                    MonsterCloud.UserProfile.setAuth(data)

                    $scope.chg = {}
                })
                .catch(function(ex){

                   $scope.isInProgress = true
                   $scope.showTokenError = true

                })
        },
        fireCallback: function($scope, MonsterCloud,$q,$location,$alertSuccess){
            var uri = $scope.forgotten ? "/account/force_password" : "/account/change_password"

            return MonsterCloud.post(uri, $scope.chg)
              .then(function(){
                  return $alertSuccess({timeout:0,message:'Password changed successfully'})

              })
        }

      })
    }
}],
       {
        title:"Change password",
        menuKey: templateDirectory,
        keywords: ["account"],
        paramsOptional: {"forgotten":"@"},
      }
)

app.directive('mcEmailVerify', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'email-verify'),
      scope: { },
      controller:  showErrorsController({
        inject: ["$location", "$alertSuccess"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $location, $alertSuccess) {

            var d = $location.search()
            console.log("verifying email", arguments)

            return MonsterCloud.doRequest({"url":"/pub/account/email/verify","method":"POST", data: d,relayException: true})
                .then(function(){
                   return $alertSuccess({timeout:0,message:'E-mail verification was successfully completed!'})
                })
                .catch(function(ex){

                   $scope.showTokenError = true

                })
        },

      })
    }
}], {
        title:"E-mail verification",
})

Array(
{
  n: "mcForgottenPasswordSend",
  t: 'forgotten-password-send',
},
{
  n: "mcSb2ForgottenPasswordSend",
  t: 'sb2-forgotten-password-send',
}).forEach(function(cat){
    app.directive(cat.n, ["templateDir",function(templateDir) {
        return {
          restrict: "E",
          templateUrl: templateDir(templateDirectory,cat.t),
          scope: {}, // dedicated scope without any external parameters
          controller:  showErrorsController({
            "fireFunctionName": "send",
            "dataFormName": "forgottenPasswordSendForm",
            /*
            // this is for testing the captcha reload feature
            <button ng-click="rel()">Fooo!</button>

            inject: ["$rootScope"],
            additionalInitializationCallback: function($scope,MonsterCloud,$q, $rootScope) {
               $scope.rel = function(){
                console.log("broadcasting")
                  $rootScope.$broadcast("onCaptchaMismatch")
               }
            },
            */
            inject:["$alertSuccess"],
            additionalInitializationCallback: function($scope, MonsterCloud){
                $scope.forgotten = {}
            },
            fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){

                 return MonsterCloud.doRequest({method:"POST", url:"/pub/account/forgotten/send", data:$scope.forgotten})
                   .then(function(ex) {

                        $scope.forgotten = {}
                        return $alertSuccess({timeout:0,message: "We've just sent you an email with further instructions how to reset your forgotten password. If you can't find the email, please check your junk folder as well."})

                   })
            }
          })
        }
    }],
           {
            title:"Forgotten password",
          }
    )
  
})

app.service('mcMyGrants', ["MonsterCloud","mcCachedLookupProvider",function(MonsterCloud, mcCachedLookupProvider){
     return mcCachedLookupProvider.Factory(function(){
         return MonsterCloud.get("/account/account_grants/to")
           .then(function(rows){

               rows.push({u_id: MonsterCloud.UserProfile.u_id, u_name: MonsterCloud.UserProfile.u_name});

               return rows;
           });
     })
}])

app.directive('mcEditAccountShowGrantedAccess',["templateDir", function(templateDir) {

    var re = {
      restrict: "E",
      scope: {},
      templateUrl: templateDir(templateDirectory,'edit-account-show-granted-access'),
      controller: ["$scope", "MonsterCloud", function($scope, MonsterCloud){
          function calc_show(){
             $scope.show_granted_to_you = $scope.granted_to_you && $scope.granted_to_you.length > 0
             $scope.show_granted_to_yours = $scope.granted_to_yours && $scope.granted_to_yours.length > 0
          }
          function fetch(trailer, arrname) {
            console.log("fetching access_grants", trailer)
            return MonsterCloud.get("/account/account_grants/"+trailer)
              .then(function(d){
                  $scope[arrname] = d
                  calc_show()
              })
          }

          $scope.reload = function(){
             fetch("for", "granted_to_you")
             fetch("to", "granted_to_yours")

          }

          $scope.reset = function(){
             return MonsterCloud.delete("/account/account_grant", {})
               .then(function(){
                   $scope.granted_to_yours = []
                   calc_show()
               })
          }


          $scope.reload()

      }]
    };
    return re


}],
       {
       title:"Grant access to others",
	   menuKey: templateDirectory,
       keywords: ["account"]
      }
)

app.directive('mcEditAccountGrantAccess',["templateDir", function(templateDir) {
    var re = {
      restrict: "E",
      scope: {
         "reload": "&",
      },
      templateUrl: templateDir(templateDirectory,'edit-account-grant-access'),
      controller:  showErrorsController({
        additionalInitializationCallback: function($scope, MonsterCloud){
           $scope.x = {}
        },
        fireCallback: function($scope, MonsterCloud){

              return MonsterCloud.post("/account/account_grant", $scope.x)
                .then(function(){
                     $scope.x = {}
                     $scope.reload()
                })

        }

      })
    };
    return re
  }])


registerSwitcher("mcSwitchUser", "/account/switch_user")


})(app)
