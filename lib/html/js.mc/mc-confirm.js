/*
 * angular-confirm
 * https://github.com/Schlogen/angular-confirm
 * @version v1.2.5 - 2016-05-20
 * @license Apache
 */
(function (root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['angular'], factory);
  } else if (typeof module !== 'undefined' && typeof module.exports === 'object') {
    module.exports = factory(require('angular'));
  } else {
    return factory(root.angular);
  }
}(this, function (angular) {
angular.module('angular-confirm', ['ui.bootstrap.modal'])
  .provider("confirmConfig", [function(){
     this.def = {
		  text: "Are you sure?",
		  title: "Confirm",
		  ok: 'Ok',
		  cancel: 'Cancel'
     }

	 this.setDefault = function(def) {
	    this.def = def
	 }
	 this.$get = [function(){
	    return { def: this.def }
	 }]
  }])
  .controller('ConfirmModalController', ["$scope", "$uibModalInstance", "data", function ($scope, $uibModalInstance, data) {
    $scope.data = angular.copy(data);

    $scope.ok = function (closeMessage) {
      $uibModalInstance.close(closeMessage);
    };

    $scope.cancel = function (dismissMessage) {
      if (angular.isUndefined(dismissMessage)) {
        dismissMessage = 'cancel';
      }
      $uibModalInstance.dismiss(dismissMessage);
    };

  }])
  .value('$confirmModalDefaults', {
    template: '<div class="modal-header"><h3 class="modal-title" lng="{{::data.title}}"></h3></div>' +
    '<div class="modal-body" lng="{{::data.text}}"></div>' +
    '<div class="modal-footer">' +
    '<button class="btn btn-primary" ng-click="ok()" lng="{{::data.ok}}"></button>' +
    '<button class="btn btn-default" ng-click="cancel()" lng="{{::data.cancel}}"></button>' +
    '</div>',
    controller: 'ConfirmModalController'
  })
  .factory('$confirm', ["$uibModal", "$confirmModalDefaults", function ($uibModal, $confirmModalDefaults) {
    return function (data, settings) {
      var defaults = angular.copy($confirmModalDefaults);
      settings = angular.extend(defaults, (settings || {}));

      if ('templateUrl' in settings && 'template' in settings) {
        delete settings.template;
      }

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings).result;
    };
  }])
  .directive('confirm', ["$confirm", "$timeout", "confirmConfig", function ($confirm, $timeout, confirmConfig) {
    return {
      priority: 1,
      restrict: 'A',
      scope: {
        confirmIf: "=",
        ngClick: '&',
        confirm: '@',
        confirmSettings: "=",
        confirmTitle: '@',
        confirmOk: '@',
        confirmCancel: '@'
      },
      link: function (scope, element, attrs) {

        function onSuccess() {
          var rawEl = element[0];
          if (["checkbox", "radio"].indexOf(rawEl.type) != -1) {
            var model = element.data('$ngModelController');
            if (model) {
              model.$setViewValue(!rawEl.checked);
              model.$render();
            } else {
              rawEl.checked = !rawEl.checked;
            }
          }
          scope.ngClick();
        }

        element.unbind("click").bind("click", function ($event) {

          $event.preventDefault();

          if(attrs.disabled) {
            console.log("disabled attributes shouldnt be clicked")
            return
          }

          $timeout(function() {

            if (angular.isUndefined(scope.confirmIf) || scope.confirmIf) {
              var data = {
			     text: (scope.confirm || confirmConfig.def.text) ,
				 title: (scope.confirmTitle || confirmConfig.def.title),
				 ok: (scope.confirmOk || confirmConfig.def.ok),
				 cancel: (scope.confirmCancel || confirmConfig.def.cancel),
			  };

              $confirm(data, scope.confirmSettings || {}).then(onSuccess);
            } else {
              scope.$apply(onSuccess);
            }

          });

        });

      }
    }
  }]);
}));
