(function(angular){


    var mcRoutes = angular.module("mc-script", [])
      .service("mcLoadScript", ['$q',function($q){
         return function(name, scriptPath) {

            return $q(function(resolve, reject) {
                if(window[name]) return resolve()

                console.log("script with name ", name, "not found, fetching it from", scriptPath)

                return $.getScript(scriptPath)
                      .done(function( script, textStatus ) {
                          resolve()
                      })
                      .fail(function( jqxhr, settings, exception ) {
                          reject(exception)
                      });              
              });

         }
      }])


})(angular)
