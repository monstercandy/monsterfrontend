(function(app){

  app.controller('FileEditorModalController', ["$scope", "$uibModalInstance", "data","mcConfig","mcFilemanTask","MonsterCloud", "$timeout", 'mcAutoCloseParams',
   function ($scope, $uibModalInstance, data, mcConfig, mcFilemanTask,MonsterCloud, $timeout, mcAutoCloseParams) {

    const extensionToAceTypeMapping = {
      "php": "php",
      "js": "javascript",
      "json": "json",
      "html": "html",
      "css": "css",
      "xml": "xml",
      "md": "markdown",
      "py": "python",
      "pl": "perl",
    }

    if(!window.ace) throw new Error("Ace is not loaded")

    var editor 

    var saves = 0
    $scope.data = data

    // console.log("fooooo!!!", data)
    $scope.saveInProgress = false
    $scope.changed = false

    var detectedFileType = detectFileType(data.filename)

    data.textPromise.then(function(d){
       var fileContent = d.text
       $scope.changed = false

       console.log($uibModalInstance)
       $uibModalInstance.rendered.then(function(){
          console.log("got the element, configuring ace")

           editor = ace.edit("aceEditor");
           editor.setValue(fileContent, -1)
           var session = editor.getSession()           
           if(detectedFileType)
             session.setMode("ace/mode/"+detectedFileType);
           session.on('change', function(e) {
                if($scope.changed) return
                $timeout(function(){
                   $scope.changed = true  
                })          
           });

       })


    })

    $scope.save = function(){
       $scope.saveInProgress = true

       var upload = MonsterCloud.post(data.uploadUrl,  {path_category: data.path_category, dst_full_path: data.filename})

       var fileContent = editor.getValue()
       var prom = mcFilemanTask(
                 "FILEMAN_UPLOAD_FILE_TASK", 
                 data.server, 
                 upload,
                 mcAutoCloseParams({stringInput: fileContent})
              )

       return prom
         .then(function(x){
             saves++
             $scope.changed = false
             $scope.saveInProgress = false

         })
    }

    $scope.close = function () {
      $uibModalInstance.close(saves ? "saved" : null);
    };


    function detectFileType(filename) {
       var m = /.+\.(.+)/.exec( filename.toLowerCase() )
       console.log("detect", filename, m)
       if(!m) return

       var ext = m[1]
       return extensionToAceTypeMapping[ext]

    }



  }])
  .factory('$fileEditor', ["$uibModal", 'templateDir', function ($uibModal, templateDir) {

    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir('common','file-editor'),    
        controller: 'FileEditorModalController',
        size: "lg",
        backdrop  : 'static',
        keyboard  : false,
      }

      settings = angular.extend({}, defaults, settings);

      settings.resolve = {
        data: function () {
          return data;
        }
      };


      return $uibModal.open(settings).result;          

    };
  }])


})(app)
