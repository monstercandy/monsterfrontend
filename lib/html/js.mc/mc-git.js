(function(app){

  var templateDirectory = "git"

  app.service("mcGitTask", ["$q","$task",function($q, $task){
    return function(server, title, prom) {
        return prom.then(function(r){
            if(!r.id) return $q.when(r)

            var url = "/s/"+server+"/git/tasks/"+r.id
            return $task({title:title, "url":url})

        })
    }
  }])

  app.service("mcGitUrl", function(){
    return function(server, whId, suffix) {
       return "/s/"+server+"/git/"+whId+"/"+(suffix||"")
    }
  })

  app.service("mcGitInfo", ["mcGitUrl", "MonsterCloud", "$q", function(mcGitUrl,MonsterCloud, $q){
     return function(server,whId){
         var url = mcGitUrl(server, whId, "info")
         return MonsterCloud.get(url)
           .then(function(info){
              info.insert_not_allowed = !info.insert_allowed
              return $q.when(info)
           })
     }
  }])

  app.service("mcGitSshDeploymentKey", ["mcGitUrl", "MonsterCloud", function(mcGitUrl,MonsterCloud){
     return function(server,whId){
         var url = mcGitUrl(server, whId, "ssh_deployment_key")
         return MonsterCloud.get(url)
     }

  }])


  app.directive("mcGitInProgress", ["mcGitUrl","$compile",function(mcGitUrl,$compile){
      return {
        restrict: "E",
        scope: { "server":"@", "whId":"@" },
        link: function(scope, elm, attrs, ctrl) {
              var tasksGetUrl = mcGitUrl(scope.server, scope.whId, "tasks")
              var taskUrl = "/s/"+scope.server+"/git/tasks"
              var t  = "<mc-tasks-in-progress title='GIT_TASK_TITLE_IN_PROGRESS' task-url='"+taskUrl+"' tasks-get-url='"+tasksGetUrl+"' ></mc-tasks-in-progress>"
              var el = $compile( t )( scope );
              elm.html( el );
        }

     }

  }])

  app.directive("mcGitSshKey", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-ssh-key'),
        scope: { "server":"@", "whId":"@" },
        controller: ["mcGitSshDeploymentKey","$scope", function(mcGitSshDeploymentKey, $scope){
            mcGitSshDeploymentKey($scope.server, $scope.whId)
              .then(function(ssh){
                  $scope.ssh = ssh
              })
        }]
     }

  }])

  app.directive("mcGitListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-list-and-add'),
        scope: { "server":"@", "whId":"@" },
     }

  }])

  app.directive("mcGitBranchListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-branch-list-and-add'),
     }

  }], {
     title: "Git repository branches",
     menuKey: templateDirectory,
     paramsRequired: { "server":"@", "whId":"@", "git": "@" },
  })

  app.directive("mcGitList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-list'),
        scope: { "server":"@", "whId":"@", },
        controller: ["$scope","MonsterCloud","mcGitUrl",
          function($scope,MonsterCloud,mcGitUrl) {
            var url = mcGitUrl($scope.server,$scope.whId)

            $scope.refetch = function(){
                console.log("refetching")
                MonsterCloud.get(url)
                   .then(function(gits){
                      $scope.showListing = gits.length > 0
                      $scope.gits = gits
                   })

            }

            $scope.delete = function(git){
               MonsterCloud.delete(url+"id/"+git.g_id)
                 .then(function(){
                    return $scope.refetch();
                 })
                 .then(function(){
                    $scope.$parent.$broadcast("reinfo")
                    $scope.showListing = $scope.gits.length > 0
                 })
            }

            $scope.$on("refetch",function(event){
               return $scope.refetch()
            });


            return $scope.refetch()

        }]
     }

  }])


  app.directive("mcGitBranchList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-branch-list'),
        scope: { "server":"@", "whId":"@", "git": "@"},
        controller: ["$scope","MonsterCloud","mcGitUrl",
          function($scope,MonsterCloud,mcGitUrl) {
            var url = mcGitUrl($scope.server,$scope.whId,"id/"+$scope.git+"/")

            $scope.refetch = function(){
                console.log("refetching")
                MonsterCloud.get(url+"branches/")
                   .then(function(branches){
                      $scope.showListing = branches.length > 0
                      $scope.branches = branches
                   })

            }


            $scope.delete = function(branch){
               MonsterCloud.delete(url+"branch/"+branch.b_branch+"/")
                 .then(function(){
                    return $scope.refetch();
                 })
                 .then(function(){
                    $scope.$parent.$broadcast("reinfo")
                    $scope.showListing = $scope.branches.length > 0
                 })
            }

            $scope.$on("refetch",function(event){
               return $scope.refetch()
            });


            return $scope.refetch()

        }]
     }

  }])



  app.directive("mcGitMain", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-main'),
      }
  }], {
        title: "Git repositories",
        menuKey: templateDirectory,
        paramsRequired: { "server":"@", "whId":"@"},
        keywords: ["git"],
  })


  app.directive("mcGitSshSettings", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-ssh-settings'),
        controller:  showErrorsController({
          inject: ["mcGitUrl", "mcGitInfo", "$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcGitUrl, mcGitInfo){


               mcGitInfo($scope.server, $scope.whId)
                 .then(function(info){
                    $scope.info = info
                 })


             $scope.baseUrl = mcGitUrl($scope.server, $scope.whId, "ssh")


          },
          fireCallback: function($scope, MonsterCloud, $q, mcGitUrl, mcGitInfo, $alertSuccess){

             return MonsterCloud.post($scope.baseUrl, $scope.info.ssh)
               .then(function(){

                  return $alertSuccess()

               })


          }

        })

     }

  }], {
        title: "Git settings",
        menuKey: templateDirectory,
        paramsRequired: { "server":"@", "whId":"@"},
        keywords: ["SSH"],
  })

  app.directive("mcGitAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-add'),
        scope: { "server":"@", "whId":"@"},
        controller:  showErrorsController({
          inject: ["mcGitUrl", "mcGitInfo"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcGitUrl, mcGitInfo){

             $scope.git = {}

             $scope.reinfo = function() {
               console.log("doing reinfo")
               mcGitInfo($scope.server, $scope.whId)
                 .then(function(info){
                    $scope.info = info
                 })

             }

             $scope.baseUrl = mcGitUrl($scope.server, $scope.whId)

             $scope.$on("reinfo",function(event){
               return $scope.reinfo()
             });

             return $scope.reinfo()

          },
          fireCallback: function($scope, MonsterCloud, $q){

             return MonsterCloud.put($scope.baseUrl, $scope.git)
               .then(function(){
                  $scope.git = {}

                  return $scope.$parent.$broadcast("refetch")

               })


          }

        })

     }

  }])



  app.directive("mcGitBranchAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-branch-add'),
        scope: { "server":"@", "whId":"@", "git": "@"},
        controller:  showErrorsController({
          inject: ["mcGitUrl", "mcGitInfo","mcGitTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcGitUrl, mcGitInfo, mcGitTask){


             $scope.reinfo = function() {
               console.log("doing reinfo")
               mcGitInfo($scope.server, $scope.whId)
                 .then(function(info){
                    $scope.info = info
                 })

             }

             $scope.baseUrl = mcGitUrl($scope.server, $scope.whId, "id/"+$scope.git+"/branches/")

             $scope.$on("reinfo",function(event){
               return $scope.reinfo()
             });

             $scope.resetForm = function(){
                $scope.branch = {b_pull_single_branch_only: 1}
             }

             $scope.resetForm()

             return $scope.reinfo()

          },
          fireCallback: function($scope, MonsterCloud, $q, mcGitUrl, mcGitInfo, mcGitTask){

             var p = MonsterCloud.put($scope.baseUrl, $scope.branch)
             return mcGitTask($scope.server, "Cloning the repository", p)
                .then(function(){
                    $scope.resetForm()
                    return $scope.$parent.$broadcast("refetch")
                })

          }

        })

     }

  }])


  app.directive("mcGitEdit", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-edit'),
        scope: { "server":"@", "whId":"@", "git": "@"},
        controller:  showErrorsController({
          inject: ["mcGitUrl", "$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcGitUrl){

               console.log("fetching git info")
             $scope.baseUrl = mcGitUrl($scope.server, $scope.whId, "id/"+$scope.git+"/")

               MonsterCloud.get($scope.baseUrl)
                 .then(function(git){
                    $scope.git_info = git
                 })


          },
          fireCallback: function($scope, MonsterCloud, $q, mcGitUrl, $alertSuccess){

             return MonsterCloud.post($scope.baseUrl, $scope.git_info)
               .then(function(){

                  return $alertSuccess()

               })


          }

        })

     }

  }])




  app.directive("mcGitBranchShow", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-branch-show'),
        controller:  showErrorsController({
          inject: ["mcGitUrl","$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcGitUrl){

             $scope.baseUrl = mcGitUrl($scope.server, $scope.whId, "id/"+$scope.git+"/branch/"+$scope.branch+"/")


             $scope.reinfo = function() {
               MonsterCloud.get($scope.baseUrl)
                 .then(function(branch){
                    branch.b_domain_docroot_changes = branch.b_domain_docroot_changes.map(function(a){return a.raw})
                    $scope.branch_info = branch
                    $scope.showDocroot = true; // $scope.branch_info.b_blue_green
                 })

             }
             $scope.$on("reinfo",function(event){
               return $scope.reinfo()
             });

             $scope.reinfo()

          },
          fireCallback: function($scope, MonsterCloud, $q, mcGitUrl, $alertSuccess){

             return MonsterCloud.post($scope.baseUrl, $scope.branch_info)
               .then(function(){

                return $alertSuccess()
               })


          }

        })

     }

  }],{
     title: "Git repository branch",
     menuKey: templateDirectory,
     paramsRequired: { "server":"@", "whId":"@", "git": "@", "branch": "@"},
  })



  app.directive("mcGitBranchPathActions", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'git-branch-path-actions'),
        scope: { "server":"@", "whId":"@", "git": "@", branch: "@", path: "@", showDocroot: "=" },
        controller: ["$scope","MonsterCloud","mcGitUrl","mcGitTask", "$prompt","$alertSuccess",
          function($scope,MonsterCloud,mcGitUrl,mcGitTask, $prompt, $alertSuccess) {
            var url_base = mcGitUrl($scope.server,$scope.whId, "id/"+$scope.git+"/branch/"+$scope.branch+"/")

            $scope.do_setdocroot = function(){
               MonsterCloud.post(url_base+"setdocroot", getPostPayload())
                 .then(function(){
                     $scope.$parent.$broadcast("reinfo")
                     return $alertSuccess()
                 })
            }

            Array("log", "pull", "clone", "checkout", "clean").forEach(function(n){
                $scope["do_"+n] = function(){
                   return mcGitTask($scope.server, "GIT_ACTION_TITLE_"+n, MonsterCloud.post(url_base+n, getPostPayload()))
                }
            })


            $scope.do_revert = function(){
                return $prompt({title: "GIT_ACTION_TITLE_revert", message: "GIT_ACTION_MESSAGE_revert", placeholder: "GIT_ACTION_PLACEHOLDER_revert"})
                  .then(function(commit){
                      return mcGitTask($scope.server, "GIT_ACTION_TITLE_revert", MonsterCloud.post(url_base+"revert", angular.extend(getPostPayload(), {commit: commit})))

                  })
            }

            function getPostPayload(){
              return {path: $scope.path}
            }


        }]
     }

  }])


})(app)
