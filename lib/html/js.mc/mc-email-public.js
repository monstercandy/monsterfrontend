(function(app){

  var templateDirectory = "email-public"



  app.directive("mcEmailPublic", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public'),
        scope: { server: "@", email: "@" },
        controller:  showErrorsController({
          inject: ["mcRoutesService","showException"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcRoutesService,showException){
             $scope.account = {}

             var callbacks = []

             if(!$scope.server)
               throw new Error("Server not specified")

             if($scope.email)
                $scope.account.ea_email = $scope.email

            $scope.registerDoCallback = function(callback){
               callbacks.push(callback)
            }

             $scope.do = function(method, params) {
                callbacks.forEach(function(cb){
                  cb()
                })
                return MonsterCloud.doRequest({
                   method:"POST", 
                   url:"/s/"+$scope.server+"/email/pub/"+method, 
                   data: angular.extend({}, params, $scope.account),
                   relayException: true
                })
                .catch(function(ex){
                    showException({message: "AUTH_FAILED"});
                    throw ex;
                })
             }

             $scope.jumpBackToLogin = function(){
                mcRoutesService.renderState({stateName:"mc-email-public", stateParams:{server:$scope.server, email: $scope.email}})
             }
          },
          fireCallback: function($scope, MonsterCloud, $q, mcRoutesService){
            return $scope.do("test2")
              .then(function(data){
                  $scope.loggedIn = true

                  $scope.accountDetails = data;

                  return mcRoutesService.renderState({
                     targetView: "subControls",
                     parentScope: $scope,
                     template: "<mc-email-public-countdown></mc-email-public-countdown>"+
                               "<mc-email-public-changepw></mc-email-public-changepw>"+
                               "<mc-email-public-change></mc-email-public-change>"+
                               "<mc-email-public-cleanup></mc-email-public-cleanup>"+
                               "<mc-email-public-whiteblacklist></mc-email-public-whiteblacklist>"+
                               "<mc-email-public-salearn></mc-email-public-salearn>"
                  })

              })
          }
        })
     }

  }], {
     title: "E-mail account settings for end-users",
     // no menuKey!
     paramsRequired:  { server: "@" },
     paramsOptional:  { email: "@" },
     keywords: ["password", "quota"],
  })


  function checkEmailPublicContext($scope){
     if((!$scope.$parent)||(!$scope.$parent.$parent)||(!$scope.$parent.$parent.do)) throw new Error("Parent should be mcEmailPublic")
     Array("accountDetails", "do","server", "registerDoCallback", "jumpBackToLogin").forEach(function(x){
        $scope[x] = $scope.$parent.$parent[x]

     })

  }

  app.directive("mcEmailPublicCountdown", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-countdown'),
        scope: { "server":"@",  },
        controller:  ["$scope", "$interval", function($scope, $interval){
           checkEmailPublicContext($scope)

           $scope.registerDoCallback(function(){
              $scope.reset()
           })

           $scope.logout = function(){
              $scope.jumpBackToLogin()
           }

           $scope.reset = function(){
              $scope.counter = 300
           }

           $scope.reset()

           var prom = $interval(function(){
              $scope.counter--
              if($scope.counter <= 0) {
                $scope.logout()
                $interval.cancel(prom)
              }

           }, 1000)


        }],
     }

  }])

  app.directive("mcEmailPublicChangepw", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-changepw'),
        scope: { "server":"@",  },
        controller:  showErrorsController({
          inject: ["$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
            checkEmailPublicContext($scope)
          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
            return $scope.do("change-password", $scope.data)
              .then(function(){
                  $scope.data = {}
                  $scope.tmp = {}
                  return $alertSuccess()
              })
          }
        })
     }

  }])

  app.directive("mcEmailPublicChange", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-change'),
        scope: { "server":"@",  },
        controller:  showErrorsController({
          inject: ["$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
            checkEmailPublicContext($scope);
             $scope.showSpamTag2 = $scope.accountDetails.ea_spam_tag2_level ? true : false;
             $scope.showSpamKill = $scope.accountDetails.ea_spam_kill_level ? true : false;
          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
            var d = {ea_autoexpunge_days: $scope.accountDetails.ea_autoexpunge_days};
            console.log("shit!", $scope.accountDetails, d, $scope.showSpamTag2, $scope.showSpamKill)
            d.ea_spam_tag2_level = $scope.showSpamTag2 ? parseFloat($scope.accountDetails.ea_spam_tag2_level || 0) : 0;
            d.ea_spam_kill_level = $scope.showSpamKill ? parseFloat($scope.accountDetails.ea_spam_kill_level || 0) : 0;

            return $scope.do("change", d)
              .then(function(){
                  return $alertSuccess()
              })
          }
        })
     }

  }])
  app.directive("mcEmailPublicCleanup", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-cleanup'),
        scope: { "server":"@",  },
        controller:  showErrorsController({
          inject:["mcEmailTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
            checkEmailPublicContext($scope)
            $scope.data = {older_than_days: 180}
          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailTask){
            return mcEmailTask($scope.server, "EMAIL_TASK_CLEANUP", $scope.do("cleanup", $scope.data))
          }
        })
     }

  }])

  app.directive("mcEmailPublicWhiteblacklist", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-whiteblacklist'),
        scope: { "server":"@",  },
        controller:  showErrorsController({
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
            $scope.actions = ["W", "B"]
            checkEmailPublicContext($scope)

            $scope.remove = function(line){
              return $scope.do("whiteblacklist/del", line)
                .then(function(){
                    return $scope.refetch();
                })
            }

            $scope.refetch = function(){
              return $scope.do("whiteblacklist/get")
                .then(function(data){
                    $scope.lookup = data
                    $scope.lookupReady = true
                })

            }


            $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q){
            return $scope.do("whiteblacklist/put", $scope.data)
              .then(function(){
                  $scope.lookup.push($scope.data)
                  $scope.data = {}
              })
          }
        })
     }

  }])

  app.directive("mcEmailPublicSalearn", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-public-salearn'),
        scope: { "server":"@",  },
        controller:  showErrorsController({
          inject:["mcEmailTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
            $scope.data = {}
            checkEmailPublicContext($scope)
            $scope.spam_or_ham_options = ["spam", "ham"]
          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailTask){
            var d = angular.copy($scope.data)
            var content = d.input
            delete d.input
            return mcEmailTask(
              $scope.server,
              "EMAIL_SPAMASSASSIN_TEACH",
              $scope.do("salearn", d),
              {stringInput: content, doubleRequestMode: true}
            )
              .then(function(){
                  $scope.data = {}
              })
          }
        })
     }

  }])

})(app)
