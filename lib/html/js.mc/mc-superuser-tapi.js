(function(app){

  function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}


  var templateDirectory = "tapi-superuser"
  var apiPrefix = "/su/tapi"

  var domain_and_template = ["domain" ,"template"]

  const permissions = "SUPERUSER ADMIN_TAPI";


app.directive("mcTapiConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/tapi'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Tapi microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
    permissions: permissions,
})

// this is a global control which needs no server!
app.directive('mcTapiAdminControls', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'tapi-admin-controls'),
      controller: ["$scope", "MonsterCloud", "$q", function($scope, MonsterCloud, $q) {
          $scope.result = ""
          $scope.action = function(type) {
             $scope.result = "..."
             $scope.showCompare = false;
             MonsterCloud.post(apiPrefix+"/"+type, {})
               .then(function(d){
                  $scope.result = d
               })
          }

          $scope.removeZone = function(serverName, zoneName) {

             return MonsterCloud.delete("/s/"+serverName+"/su/tapi/domain/"+zoneName, {}).then($scope.compare)
          }

          $scope.sync = function(dstServerName, srcServerName, zoneName) {

             return MonsterCloud.post("/su/sync/tapi", {src: srcServerName, dst: dstServerName, payload: {zones: [{zone: zoneName}]}}).then($scope.compare)
          }


          $scope.compare = function() {
             $scope.result = "...";
             $scope.tapiShowCompare = false;
             $scope.tapiServers;
             $scope.tapiByServer = {};
             var tapiByZones = {};
             $scope.tapiMissingZonesCount = 0;
             $scope.tapiMissingZonesCount = 0;
             $scope.tapiZonesCount = 0;
             $scope.tapiZonesCount = 0;
             $scope.tapiRecordsCount = 0;
             $scope.tapiRecordsMismatchCount = 0;
             $scope.wtfZones = 0;
             $scope.mtimeIssues = 0;
             $scope.tapiByZonesMismatch = {};
             $scope.tapiHideGoodRecords = true;     
             return  MonsterCloud.get("/su/servers/tapi") //  $q.when(["stan","eric"])
              .then(function(tapiServers){
                  $scope.tapiServers = tapiServers;
                  // ["s1", "s2", "tapi"]

                  var ps = [];
                  tapiServers.forEach(function(server){
                     ps.push(
                        // $q.when(server=="eric" ? dnsExportEric : dnsExportStan)
                        MonsterCloud.get("/s/"+server+"/su/tapi/export/json")
                          .then(function(response){
                             $scope.tapiByServer[server] = response;
                          })
                     )
                  });

                  return $q.all(ps);

              })
              .then(function(){
                 // lets process the responses now
                 var numberOfServers = $scope.tapiServers.length;
                 $scope.tapiServers.forEach(function(server){
                    Object.keys($scope.tapiByServer[server]).forEach(function(zoneName){
                        var records = $scope.tapiByServer[server][zoneName].records;                        
                        if(!Array.isArray(records)){
                           console.error("wtf, records is not an array?!", server, zoneName);
                           $scope.wtfZones++;
                           return;
                        }
                        if(!tapiByZones[zoneName]) {                          
                           tapiByZones[zoneName] = { 
                             expectedMtime: $scope.tapiByServer[server][zoneName].mtime,
                             zonesServers: {}, 
                             zonesPresent: 0, 
                             records: {},
                           };
                        }
                        else {
                           if($scope.tapiByServer[server][zoneName].mtime != tapiByZones[zoneName].expectedMtime)
                             tapiByZones[zoneName].mtimeProblem = true;
                        }

                        tapiByZones[zoneName].zonesPresent++;
                        tapiByZones[zoneName].zonesPresenceOk = (numberOfServers == tapiByZones[zoneName].zonesPresent);

                        tapiByZones[zoneName].zonesServers[server] = true;

                        if(!tapiByZones[zoneName].records)
                           tapiByZones[zoneName].records = {};

                        // per server record helper
                        var recordKeyBaseHelper = {};
                        records.forEach(function(record){
                           var keyBase = getUniqueKeyForRecord(record);
                           if(!recordKeyBaseHelper[keyBase]) recordKeyBaseHelper[keyBase] = 0;

                           recordKeyBaseHelper[keyBase]++;
                           var key = keyBase+" #"+recordKeyBaseHelper[keyBase];

                           if(!tapiByZones[zoneName].records[key]) {
                             tapiByZones[zoneName].records[key] = {record: record, recordsPresent: 0, recordsServers: {}};
                           }

                           tapiByZones[zoneName].records[key].recordsPresent++;
                           tapiByZones[zoneName].records[key].recordsPresenceOk = (numberOfServers == tapiByZones[zoneName].records[key].recordsPresent);

                           tapiByZones[zoneName].records[key].recordsServers[server] = true;

                        })
                        

                    })                    
                 })

                 Object.keys(tapiByZones).forEach(function(zoneName){
                   tapiByZones[zoneName].recordsPresenceOk = true;
                   tapiByZones[zoneName].matchingRecordsCount = 0;
                   Object.keys(tapiByZones[zoneName].records).some(function(key){
                      var recordData = tapiByZones[zoneName].records[key];
                      if(!recordData.recordsPresenceOk) {
                        tapiByZones[zoneName].recordsPresenceOk = false;
                        return true;
                      }
                      tapiByZones[zoneName].matchingRecordsCount++;
                   })

                   if(tapiByZones[zoneName].mtimeProblem)
                     $scope.mtimeIssues++;

                   if(!tapiByZones[zoneName].zonesPresenceOk)
                     $scope.tapiMissingZonesCount++;

                   tapiByZones[zoneName].recordsCount = Object.keys(tapiByZones[zoneName].records).length;
                   $scope.tapiRecordsCount += tapiByZones[zoneName].recordsCount;
                   $scope.tapiZonesCount++;

                   if(!tapiByZones[zoneName].recordsPresenceOk)
                     $scope.tapiRecordsMismatchCount++;

                   if((!tapiByZones[zoneName].zonesPresenceOk)||
                      (tapiByZones[zoneName].mtimeProblem)||
                      (!tapiByZones[zoneName].recordsPresenceOk)) {
                      $scope.tapiByZonesMismatch[zoneName] = tapiByZones[zoneName];
                   }

                 })


                 $scope.result = "";
                 $scope.tapiShowCompare = true;


                 function getUniqueKeyForRecord(record) {
                    var keys = Object.keys(record)
                    keys.sort();
                    var re = "";
                    keys.forEach(function(keyName){
                       var value = record[keyName];
                       if(re) re += ", ";
                       re += keyName+"="+value;;
                    })
                    return re;
                 }
                 
              })
          }

          $scope.export = function(type) {
             $scope.result = "..."
             $scope.showCompare = false;
             MonsterCloud.get(apiPrefix+"/export/tgz")
               .then(function(d){
                    var rightNow = new Date();
                    var res = rightNow.toISOString().slice(0,10).replace(/-/g,"");

                    $scope.result = "";

                    var blob = new Blob([atob(d.data)], {type: "application/tar+gzip"});
                    saveAs(blob, "tapi-export-"+res+".tgz");

               })
          }

      }]
   }
}],
      {
       title:"DNS admin controls",
       menuKey: templateDirectory,       
       permissions: permissions,
       keywords: ["dns record", "export", "rebuild", "synchronization"]
      }
)

app.directive('mcTapiList', ["templateDir", function(templateDir){

            return {
              restrict: "E",
              templateUrl: templateDir(templateDirectory,'tapi-list'),
              controller: ["$scope", "MonsterCloud", '$alertSuccess',
               function($scope, MonsterCloud,  $alertSuccess){
                 $scope.kinds = domain_and_template
                 $scope.kind = "domain"

                 var listedKind
                 var allDomains = []


                 Array("lock","unlock").forEach(function(lockUnlock){
                    $scope[lockUnlock] = function(domain) {
                       return MonsterCloud.post(apiPrefix+"/"+listedKind+"/"+domain+"/"+lockUnlock,{})
                         .then(function(){
                           return $alertSuccess()
                         })

                    }

                 })

                 $scope.delete = function(domain) {
                    return MonsterCloud.delete(apiPrefix+"/"+listedKind+"/"+domain,{})
                      .then(function(){
                         $scope.refilter()
                      })
                 }


                 $scope.refilter = function() {

                    listedKind = $scope.kind;
                    return MonsterCloud.get( apiPrefix+"/"+listedKind+"s")
                      .then(function(data){
                          $scope.domains = data.domains;
                      })

                 }

                 $scope.refilter();


              }]
            }
        }],
              {
                 title:"DNS admin superuser list control",
                 menuKey: templateDirectory,
                 permissions: permissions,
                 keywords: ["dns record", "copy", "restore"]
              }
      )

app.service('mcTapiDnsRecordReplacer', ['MonsterCloud', '$q', 'mcConfig', 'showException', function(MonsterCloud, $q, mcConfig, showException){
   return function(targets, rules, options) {
      if(typeof options == "function")
        options = {appendText: options};

      var appendText = options.appendText;
      if(!appendText) appendText = function(){};
      var stats = {recordsReplaced: 0};
      return $q(function(resolve,reject){
          nextDomain();

          function nextDomain(){
             var target = targets.shift();
             if(!target) return resolve();

             appendText("Querying records of "+target.name);
             return MonsterCloud.doRequest({method:"GET", url: "/su/tapi/"+target.kind+"/"+target.name, relayException: true})
               .then(function(re){
                   appendText("Processing records of "+target.name);

                   var recordsToProcess = angular.copy(re.records);

                   return $q(function(resolve,reject){
                       nextRecord();

                       function nextRecord(){
                          var record = recordsToProcess.shift();
                          if(!record) return resolve();

                          var changes = 0;
                          rules.forEach(function(rule){
                             if(rule.dnsRecordType.indexOf(record.type) < 0) return;

                             if(record.type == "CLONE") {
                                if(record.parentZone != mcConfig.config.service_primary_domain) return;
                             }

                             if(typeof record[rule.field_name] == "undefined") return;
                             if((rule.old_value)&&(record[rule.field_name] != rule.old_value)) return;
                             if(record[rule.field_name] == rule.new_value) return;

                             changes++;
                             record[rule.field_name] = rule.new_value;
                          })

                          if(changes <= 0) return nextRecord();

                          var hash = record.hash;
                          delete record.hash;

                          appendText("Replacing record "+record.type);

                          var urlPrefix = "/su/tapi/"+target.kind+"/"+target.name;
                          return MonsterCloud.delete(urlPrefix+"/"+hash, {limit: 1})
                            .then(function(){
                               return MonsterCloud.put(urlPrefix, record);
                            })
                            .then(function(){
                               stats.recordsReplaced++;
                               return nextRecord();
                            })
                            .catch(reject);

                       }
                   })
               })
               .catch(function(ex){
                   var serviceDomain = false;
                   mcConfig.config.mailServiceDomains.some(function(s){
                      if(target.name.endsWith(s)) {
                         serviceDomain = true;
                         return true;
                      }
                   })                   

                   if((!serviceDomain)&&(!options.robust))
                     throw ex;

                   appendText("There was an exception while working on "+target.name+", ignoring");
               })               
               .then(function(){
                  return nextDomain();
               })
               .catch(reject);
          }
      })
      .then(function(){
          return stats;
      })
   }
}])

app.directive('mcTapiDnsRecordReplace', ["templateDir", function(templateDir){

            return {
              restrict: "E",
              templateUrl: templateDir(templateDirectory,'tapi-dns-record-replace'),
              controller: showErrorsController({
                  inject:['$task','mcTapiSupportedDnsRecordTypes','mcTapiDnsRecordReplacer'],
                  additionalInitializationCallback: function($scope, MonsterCloud, $q, $task, mcTapiSupportedDnsRecordTypes, mcTapiDnsRecordReplacer){
                      $scope.supportedDnsRecordTypes = mcTapiSupportedDnsRecordTypes();
                      $scope.rules = [];

                      $scope.showCompleteReplace = ($scope.server && $scope.whId);
                      $scope.completeReplace = !$scope.showCompleteReplace;
                      $scope.categories = ["domain"];
                      $scope.kind = ["domain"];
                      if(!$scope.showCompleteReplace) {
                        $scope.categories.push("template");
                      }

                      if($scope.showCompleteReplace) {
                         MonsterCloud.get("/s/"+$scope.server+"/webhosting/"+$scope.whId)
                           .then(function(wh){
                              $scope.wh = wh;
                           })                        
                      }

                      $scope.startDisabled = function(){
                        if($scope.rules.length <= 0)return true;
                        if($scope.kind.length <= 0)return true;
                        return false;
                      }

                      $scope.start = function(){

                          var state = "kickoff";
                          const title = "DNS_RECORDS_MASS_CHANGE_TASK";
                          var stuffToBeProcessed = [];
                          var recordsReplaced = 0;
                          var d = {
                             title: title,
                             abortOnly: true,
                             undeterministic: true,
                             urlFactory: function(index, appendText, lastPromiseResult) {
                                 var aData = {
                                   title: title,
                                   mcDontDisplay: true,
                                   mcTaskStatusNotNeeded: true,
                                 }
                                 if(state == "kickoff") {
                                    appendText("Started.");
                                    state = "queryTemplates";
                                    if($scope.kind.indexOf("domain") > -1) {
                                      if($scope.completeReplace) {
                                          appendText("Querying complete list of zones");
                                          aData.prom = MonsterCloud.get("/su/tapi/domains")
                                            .then(function(re){
                                                appendText("Found "+re.domains.length+" zones");
                                                re.domains.forEach(function(d){
                                                   stuffToBeProcessed.push({name:d, kind: "domain"});
                                                })
                                            })                                        
                                      } else {
                                          appendText("Querying domains that belong to selected webstore");
                                          aData.prom = MonsterCloud.get("/s/"+$scope.server+"/webhosting/"+$scope.whId+"/domains")
                                            .then(function(re){
                                                appendText("Found "+re.length+" domains");
                                                re.forEach(function(d){
                                                   stuffToBeProcessed.push({name:d, kind: "domain"});
                                                })
                                            })                                        

                                      }
                                    }
                                    else
                                      aData.prom = $q.when();
                                 }
                                 else if(state == "queryTemplates") {
                                    state = "process";
                                    if($scope.kind.indexOf("template") > -1) {
                                      appendText("Querying list of templates");
                                      aData.prom = MonsterCloud.get("/su/tapi/templates")
                                        .then(function(re){
                                            re.domains.forEach(function(d){
                                               stuffToBeProcessed.push({name:d, kind: "template"});
                                            })
                                        })
                                    }
                                    else
                                      aData.prom = $q.when();

                                 }
                                 else if(state == "process"){
                                    appendText("");
                                    var target = stuffToBeProcessed.shift();
                                    if(!target) {
                                       appendText("We are done! "+recordsReplaced+ " records have been replaced altogether.");
                                       return $q.when();
                                    }

                                    appendText("Processing "+target.name+", remaining in queue: "+stuffToBeProcessed.length);
                                    aData.prom = mcTapiDnsRecordReplacer([target], $scope.rules, {appendText:appendText, robust: $scope.robust})
                                      .then(function(stats){
                                         recordsReplaced += stats.recordsReplaced;
                                      })
                                 }
                                 return $q.when(aData);
                             }

                          }

                          console.log("triggering undeterministic task chain", d)

                          return $task(d)
                            .then(function(){
                                $scope.operationInProgress = false;
                            })

                      }

                      $scope.delete = function(index){
                        $scope.rules.splice(index, 1);
                      }

                  },
                  fireCallback: function($scope, MonsterCloud, $q, $task){
                     var x = $scope.tmp;
                     $scope.tmp = {};
                     $scope.rules.push(x);
                  }

              })
            }
        }],
              {
                 title:"Mass replace DNS records",
                 menuKey: templateDirectory,
                 permissions: permissions,
                 paramsOptional: {"server":"@", "whId": "@"},
              }
      )

app.directive('mcTapiAddZone', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'tapi-add-zone'),
      scope: { },
      controller:  showErrorsController({
        additionalInitializationCallback: function($scope, MonsterCloud){
           $scope.pl = {}
           $scope.kinds = domain_and_template
           $scope.kind = "domain"

        },
        fireCallback: function($scope, MonsterCloud){

             return MonsterCloud.put(apiPrefix + "/"+$scope.kind+"s", $scope.pl)
               .then(function(data){
                   console.log("zone has been added");
                   $scope.pl = {}
                   if($scope.$parent.refilter) {
                      console.log("refreshing DNS zone list");
                      $scope.$parent.refilter()
                   }
               })
        }
      })
   }
}])





})(app)


