(function(app){

  var templateDirectory = "iptables-superuser"

app.directive("mcIptablesConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/iptables'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Iptables microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcSuperuserIptablesBans", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'banned-ips'),
        controller:  showErrorsController({
         additionalInitializationCallback: function($scope, MonsterCloud, $q){
            $scope.categories = [];

            $scope.baseUrl = "/s/"+$scope.server+"/su/iptables/bans/";

            $scope.reset = function(){
              $scope.data = {duration: 3600};
            }

            $scope.delete = function(entry){
               return MonsterCloud.delete($scope.baseUrl+entry.category+"/"+entry.ip).then(function(x){
                  return $scope.refetch();
               })
            }

            $scope.flush=function(category){
               return MonsterCloud.delete($scope.baseUrl+category).then(function(x){
                  return $scope.refetch();
               })
            }

            $scope.refetch = function(){
                var newDate = new Date();

                return MonsterCloud.get($scope.baseUrl)
                  .then(function(d){
                      var re = [];
                      Object.keys(d).forEach(function(c){
                         $scope.categories.push(c);
                         Object.keys(d[c]).forEach(function(ip){
                            var row = d[c][ip]
                            row.ip = ip;
                            row.category = c;
                            newDate.setTime(row.added*1000);
                            row.added_human = newDate.toISOString();
                            re.push(row);
                         })
                      })

                     $scope.bans = re;
                  })

            }

            $scope.reset();
            return $scope.refetch();

         },
         fireCallback: function($scope, MonsterCloud, $q){
              var d = angular.copy($scope.data);
              var category = d.category;
              delete d.category;
              return MonsterCloud.put($scope.baseUrl+category, d)
               .then(function(){
                  $scope.reset();
               })
         }

      })

     }

  }], {
     title: "Banned IP addresses",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"},
     keywords: ["iptables", "fail2ban", "ban"],
  })





  app.directive("mcSuperuserWebstorageFirewallRules", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'firewall-rules'),
        controller: ["$scope", "MonsterCloud", "pagerFactorySingleShot", "$q", "webhostingLookup", "accountLookup",
         function($scope, MonsterCloud, pagerFactorySingleShot, $q, webhostingLookup, accountLookup){

            $scope.baseUrl = "/s/"+$scope.server+"/su/iptables/whextra/";

            $scope.delete = function(rule){
              return MonsterCloud.delete($scope.baseUrl+rule.webhosting_id+"/firewallrules/rule", rule)
                .then(function(){
                    return $scope.refetch();
                })
            }

            $scope.refetch = function(){
               return pagerFactorySingleShot($scope, {
                  dataName: "rules",
                  fuse: {castToString:["fw_dst_port","webhosting_id"], keys: [ "wh_name", "u_name", "webhosting_id", "fw_dst_ip", "fw_dst_port" ]},
                  shot: function(){

                    return MonsterCloud.get($scope.baseUrl)
                      .then(function(d){
                         webhostingLookup(d, $scope.server, null, "webhosting_id", null, "user_id")
                           .then(function(){
                             return accountLookup(d, "user_id")
                           })

                         return $q.when(d)
                      })

                  }
               })()
            }

            return $scope.refetch();
        }]
     }

  }], {
     title: "Webstorage firewall rules",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"},
     keywords: ["whextra"],
  })


})(app)

