(function(app){

  var templateDirectory = "cert-superuser"

  app.service("mcSuperuserPurchaseUrl", function(){
     return function(server, pcId){
        return "/s/"+server+"/su/cert/purchases/all"+(pcId ? "/item/"+pcId : "") + "/"

     }
  })

  app.service("mcSuperuserContactsUrl", function(){
     return function(server, coId){
        return "/s/"+server+"/su/cert/contacts/all"+(coId ? "/item/"+coId : "") + "/"

     }
  })

  app.service("mcSuperuserCertUrl", function(){
     return function(server, cId){
        return "/s/"+server+"/su/cert/certificates/all"+(cId ? "/item/"+cId : "") + "/"

     }
  })
  app.service("mcSuperuserConfigUrl", function(){
     return function(server, suffix){
        return "/s/"+server+"/su/cert/config/"+(suffix||"")

     }
  })

app.directive("mcCertConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/cert'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Certificate microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

	app.directive('mcSuperuserCertificates', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'certificates'),
	      controller: ["$scope", "MonsterCloud","accountLookup","webhostingLookup", "mcSuperuserCertUrl","$q",'$prompt',
	        function($scope,MonsterCloud,accountLookup,webhostingLookup,mcSuperuserCertUrl,$q,$prompt){

           $scope.filter = {groupOnly:true}
           $scope.status = "0"
           $scope.statusOptions = [0, 1]

           function certOperation(cert, method, suffix, data) {
              return MonsterCloud[method](mcSuperuserCertUrl($scope.server, cert.c_id)+suffix, data || {})
                .then($scope.refetch)
           }

           $scope.enable= function(cert){
              return certOperation(cert, "post", "enable")
           }
           $scope.disable= function(cert){
              return certOperation(cert, "post", "disable")
           }
           $scope.wipe= function(cert){
              return certOperation(cert, "delete", "")
           }
           $scope.detach= function(cert){
              return certOperation(cert, "post", "detach")
           }

           $scope.setAlias= function(cert){
              return $prompt({title: "CERT_CHANGE_ALIAS_TITLE", message: "CERT_CHANGE_ALIAS_MESSAGE", text: cert.c_alias, placeholder: "CERT_CHANGE_ALIAS_PLACEHOLDER"})
                .then(function(alias){
                    return certOperation(cert, "post", "alias", {alias: alias})
                })

           }

           $scope.setComment= function(cert){
            console.log("setComment",cert)
              return $prompt({title: "CERT_CHANGE_COMMENT", message: "CERT_CHANGE_COMMENT_MESSAGE", text: cert.c_comment, placeholder: "CERT_CHANGE_COMMENT_PLACEHOLDER"})
                .then(function(comment){
                    return certOperation(cert, "post", "raw", {c_comment: comment})
                })
           }


           $scope.refetch = function(){
                    return MonsterCloud.post(mcSuperuserCertUrl($scope.server)+"search", {c_deleted: $scope.status})
                        .then(function(data){
                            var certs = data.certificates
                            accountLookup(certs, "c_user_id")
                            webhostingLookup(certs, $scope.server, null, "c_webhosting")
                            $scope.certificates = certs;
                        })

           }

           $scope.refetch()


	       }]
	   }
	}], {
     title: "Certificates",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["x509","x.509"]
  })



  app.directive('mcSuperuserCertPurchases', ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'purchases'),
        controller: ["$scope", "MonsterCloud","accountLookup","mcSuperuserPurchaseUrl","$q",'$prompt','$task','mcCertTask','$timeout',
          function($scope,MonsterCloud,accountLookup,mcSuperuserPurchaseUrl,$q,$prompt,$task,mcCertTask,$timeout){

           function purchaseOperation(purchase, method, suffix, data) {
              return MonsterCloud[method](mcSuperuserPurchaseUrl($scope.server, purchase.pc_id)+suffix, data || {})
                .then(function(){
                   Object.keys(data).forEach(function(key){
                      purchase[key] = data[key]
                   })
                })
           }

           $scope.reissueLetsEncrypt = function(){
            var url = mcSuperuserPurchaseUrl($scope.server)+"letsencrypt/renew"
            var prom = MonsterCloud.post(url)
            return mcCertTask($scope.server, "LETSENCRYPT_RENEW_CERTIFICATES", prom)
           }
           $scope.pollAll = function(){

            var url = mcSuperuserPurchaseUrl($scope.server)+"poll"
            return MonsterCloud.post(url)
              .then(function(h){
                 return $task({title:"CERTIFICATE_PURCHASE_POLL_TITLE", "url":"/s/"+$scope.server+"/cert/tasks/"+h.id})
              })


           }


           $scope.remove = function(purchase){
                 return MonsterCloud.delete(mcSuperuserPurchaseUrl($scope.server, purchase.pc_id), {}).then(function(){
                    return $scope.refetch()
                 })

           }

           $scope.setChangeResellerCertificateID=function(purchase){
            console.log("setComment",purchase)
              return $prompt({title: "CERT_PURCHASE_TITLE_CHANGE_RESELLER_CERTIFICATE_ID", message: "CERT_PURCHASE_MESSAGE_CHANGE_RESELLER_CERTIFICATE_ID", text: purchase.pc_reseller_certificate_id, placeholder: "CERT_PURCHASE_PLACEHOLDER_CHANGE_RESELLER_CERTIFICATE_ID"})
                .then(function(pc_reseller_certificate_id){
                    if(pc_reseller_certificate_id=="-") pc_reseller_certificate_id = ""
                    return purchaseOperation(purchase, "post", "raw", {pc_reseller_certificate_id: pc_reseller_certificate_id})
                })
           }
           $scope.setComment= function(purchase){
              console.log("setComment",purchase)
              return $prompt({title: "CERT_PURCHASE_CHANGE_COMMENT", message: "CERT_PURCHASE_CHANGE_COMMENT_MESSAGE", text: purchase.pc_comment, placeholder: "CERT_PURCHASE_CHANGE_COMMENT_PLACEHOLDER"})
                .then(function(comment){
                    return purchaseOperation(purchase, "post", "raw", {pc_comment: comment})
                })

           }


           $scope.refetch = function(){
                    return MonsterCloud.get(mcSuperuserPurchaseUrl($scope.server))
                        .then(function(data){
                            var purchs = data.purchases
                            accountLookup(purchs, "pc_user_id")
                            $scope.purchases = purchs;
                        })

           }

           $scope.refetch()


         }]
     }
  }], {
     title: "Certificate purchases",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
     keywords: ["x509","x.509"]
  })


  app.directive('mcSuperuserCertContacts', ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'contacts'),
        controller: ["$scope", "MonsterCloud","accountLookup","mcSuperuserContactsUrl","$q",'$prompt','mcSuperuserConfigUrl',
          function($scope,MonsterCloud,accountLookup,mcSuperuserContactsUrl,$q,$prompt,mcSuperuserConfigUrl){

           $scope.defContact = 0

           var contactsUrl = mcSuperuserConfigUrl($scope.server, "contacts/default")

           $scope.delete = function(contact){
                 return MonsterCloud.delete(mcSuperuserContactsUrl($scope.server, contact.co_id), {}).then(function(){
                    return $scope.refetch()
                 })

           }

           $scope.select= function(contact){
              return MonsterCloud.post(contactsUrl, {co_id:contact.co_id})
                .then(function(){
                   $scope.defContact = contact.co_id
                })
           }


           MonsterCloud.get(contactsUrl)
                .then(function(c){
                   $scope.defContact = c.co_id
                })


           $scope.refetch = function(){

                    return MonsterCloud.get(mcSuperuserContactsUrl($scope.server))
                        .then(function(data){
                            var contacts = data.contacts
                            accountLookup(contacts, "co_user_id")
                            $scope.contacts = contacts;
                        })

           }

           $scope.refetch()


         }]
     }
  }], {
     title: "Certificate contacts",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
  })


  app.directive('mcSuperuserCertPurchaseInfo', ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'cert-purchase-info'),
        controller: ["$scope", "MonsterCloud","mcSuperuserPurchaseUrl","$q",'$alertSuccess',
          function($scope,MonsterCloud,mcSuperuserPurchaseUrl,$q,$alertSuccess){


           $scope.change = {}
           $scope.purchaseStatuses = Array("pending", "processing", "issued", "revoked", "removed", "renewed")

            var baseUrl = mcSuperuserPurchaseUrl($scope.server, $scope.pcId)
            var activationsUrl = baseUrl+"activations"
            MonsterCloud.get(baseUrl)
              .then(function(data){
                  $scope.data = data
                  $scope.change.pc_status = data.pc_status
                  $scope.fieldsToShow = Object.keys(data)

              })


           $scope.doChange = function(){
              return MonsterCloud.post(baseUrl+"raw", $scope.change)
                .then(function(){
                    return $alertSuccess()
                })
           }
           $scope.doPoll = function(){
              return MonsterCloud.post(baseUrl+"poll")
                .then(function(r){
                    $scope.showPayload = r
                })
           }

           $scope.resend = function(activation){
              return MonsterCloud.put(activationsUrl, activation.a_payload)
                .then(function(){
                   return $scope.refetch()
                })
           }

           $scope.show = function(activation){
              $scope.showPayload = activation
           }

           $scope.refetch = function(){
                    return MonsterCloud.get(activationsUrl)
                        .then(function(data){
                            $scope.activations = data.activations
                        })

           }

           $scope.refetch()


         }]
     }
  }], {
     title: "Purchased certificate info",
     paramsRequired:{ "server": "@", "pcId": "@" },
     menuKey: templateDirectory,
  })



  app.directive('mcSuperuserResellerBalances', ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'reseller-balances'),
        controller: ["$scope", "MonsterCloud","mcSuperuserConfigUrl",
          function($scope,MonsterCloud,mcSuperuserConfigUrl){

              return MonsterCloud.get(mcSuperuserConfigUrl($scope.server, "balances"))
                  .then(function(data){
                      $scope.response=data
                  })


         }]
     }
  }], {
     title: "Reseller balances",
     paramsRequired:{ "server": "@" },
     menuKey: templateDirectory,
  })



  app.directive("mcSuperuserNamecheapCredentials", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'namecheap-credentials'),
        controller:  showErrorsController({
          inject:["mcSuperuserConfigUrl",'$alertSuccess'],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcSuperuserConfigUrl,$alertSuccess){

               $scope.baseUrl = mcSuperuserConfigUrl($scope.server, "namecheap/credentials")


               $scope.delete = function(cert){

                  return MonsterCloud.delete($scope.baseUrl)
                    .then(function(){
                      $scope.creds = {}
                      $scope.thereIs= false
                      return $alertSuccess()
                    })
               }


               $scope.creds = {}

                return MonsterCloud.get($scope.baseUrl)
                  .then(function(r){
                     $scope.creds = r
                     if(r.UserName)
                       $scope.thereIs= true
                  })


          },
          fireCallback: function($scope, MonsterCloud, $q, mcSuperuserConfigUrl,$alertSuccess){

             return MonsterCloud.post($scope.baseUrl, $scope.creds)
               .then(function(){
                  delete $scope.creds.ApiKey
                  $scope.thereIs= true
                  return $alertSuccess()

               })


          }
        })
     }

  }], {
       title:"Namecheap account",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@"},
  })


  app.directive("mcSuperuserCertPurchaseAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'purchase-add'),
        controller:  showErrorsController({
          inject:["mcRoutesService", "mcSuperuserPurchaseUrl","mcSuperuserConfigUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcRoutesService, mcSuperuserPurchaseUrl, mcSuperuserConfigUrl){

              $scope.baseUrl = mcSuperuserPurchaseUrl($scope.server)
              $scope.purchase = {pc_product_parameters:{}}
              $scope.statuses = ["pending","processing","issued","removed","revoked","renewed"]

              var vendorsCache = {}
              var productsCache = {}

              $scope.onResellerChange= function() {
                 var reseller = $scope.purchase.pc_reseller
                 $scope.purchase.pc_vendor = ""
                 $scope.purchase.pc_product = ""

                 if(vendorsCache[reseller])
                   return $scope.vendors = vendorsCache[reseller]

                return MonsterCloud.get(mcSuperuserConfigUrl($scope.server)+"resellers/"+reseller+"/vendors")
                  .then(function(r){
                     $scope.vendors = vendorsCache[reseller] = r.vendors
                  })

              }
              $scope.onVendorChange= function() {
                 var reseller = $scope.purchase.pc_reseller
                 var vendor = $scope.purchase.pc_vendor
                 var key = reseller+"-"+vendor

                 $scope.purchase.pc_product = ""

                 if(productsCache[key])
                   return $scope.products = productsCache[key]

                return MonsterCloud.post(mcSuperuserConfigUrl($scope.server)+"resellers/"+reseller+"/vendors/products", {vendor: vendor})
                  .then(function(r){
                     $scope.products = productsCache[key] = r.products
                  })

              }

                return MonsterCloud.get(mcSuperuserConfigUrl($scope.server)+"resellers")
                  .then(function(r){
                        $scope.resellers = r.resellers

                  })

          },
          fireCallback: function($scope, MonsterCloud, $q, mcRoutesService){

             var d = angular.copy($scope.purchase)
             d.pc_misc = angular.fromJson(d.pc_misc)

             return MonsterCloud.put($scope.baseUrl, d)
               .then(function(){
                   return mcRoutesService.jumpToState({stateName:"mc-superuser-cert-purchases",stateParams: {server: $scope.server}})

               })


          }
        })
     }

  }], {
       title:"Add new certificate purchase",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@"},
  })



})(app)

