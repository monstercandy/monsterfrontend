/**
 * @license Pass*Field | (c) 2013 Antelle | https://github.com/antelle/passfield/blob/master/MIT-LICENSE.txt
 */

// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:

// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

/**
 * Entry point
 * Initializes PassField
 * If jQuery is present, sets jQuery plugin ($.passField)
 */
(function($, document, window) {
    "use strict";

    var PassField = window.PassField = window.PassField || {};
    PassField.Config = PassField.Config || {};

    PassField.Config.locales = {
        en: {
            lower: true,
                msg: {
                pass: "password",
                    and: "and",
                    showPass: "Show password",
                    hidePass: "Hide password",
                    genPass: "Random password",
                    passTooShort: "password is too short (min. length: {})",
                    noCharType: "password must contain {}",
                    digits: "digits",
                    letters: "letters",
                    lettersUp: "letters in UPPER case",
                    symbols: "symbols",
                    inBlackList: "password is in list of top used passwords",
                    passRequired: "password is required",
                    equalTo: "password is equal to login",
                    repeat: "password consists of repeating characters",
                    badChars: "password contains bad characters: ÔÇť{}ÔÇŁ",
                    weakWarn: "weak",
                    invalidPassWarn: "*",
                    weakTitle: "This password is weak",
                    generateMsg: "To generate a strong password, click {} button."
            }
        },
        de: {
            lower: false,
                msg: {
                pass: "Passwort",
                    and: "und",
                    showPass: "Passwort anzeigen",
                    hidePass: "Passwort verbergen",
                    genPass: "Zufallspasswort",
                    passTooShort: "Passwort ist zu kurz (Mindestl+Ąnge: {})",
                    noCharType: "Passwort muss {} enthalten",
                    digits: "Ziffern",
                    letters: "Buchstaben",
                    lettersUp: "Buchstaben in GROSSSCHRIFT",
                    symbols: "Symbole",
                    inBlackList: "Passwort steht auf der Liste der beliebtesten Passw+Ârter",
                    passRequired: "Passwort wird ben+Âtigt",
                    equalTo: "Passwort ist wie Anmeldung",
                    repeat: "Passwort besteht aus sich wiederholenden Zeichen",
                    badChars: "Passwort enth+Ąlt ung++ltige Zeichen: ÔÇť{}ÔÇŁ",
                    weakWarn: "Schwach",
                    invalidPassWarn: "*",
                    weakTitle: "Dieses Passwort ist schwach",
                    generateMsg: "Klicken Sie auf den {}-Button, um ein starkes Passwort zu generieren."
            }
        },
        fr: {
            lower: true,
                msg: {
                pass: "mot de passe",
                    and: "et",
                    showPass: "Montrer le mot de passe",
                    hidePass: "Cacher le mot de passe",
                    genPass: "Mot de passe al+ęatoire",
                    passTooShort: "le mot de passe est trop court (min. longueur: {})",
                    noCharType: "le mot de passe doit contenir des {}",
                    digits: "chiffres",
                    letters: "lettres",
                    lettersUp: "lettres en MAJUSCULES",
                    symbols: "symboles",
                    inBlackList: "le mot de passe est dans la liste des plus utilis+ęs",
                    passRequired: "le mot de passe est requis",
                    equalTo: "le mot de passe est le m+¬me que l'identifiant",
                    repeat: "le mot de passe est une r+ęp+ętition de caract+Ęres",
                    badChars: "le mot de passe contient des caract+Ęres incorrects: ÔÇť{}ÔÇŁ",
                    weakWarn: "faible",
                    invalidPassWarn: "*",
                    weakTitle: "Ce mot de passe est faible",
                    generateMsg: "Pour cr+ęer un mot de passe fort cliquez sur le bouton {}."
            }
        },
        it: {
            lower: false,
                msg: {
                pass: "password",
                    and: "e",
                    showPass: "Mostra password",
                    hidePass: "Nascondi password",
                    genPass: "Password casuale",
                    passTooShort: "la password +Ę troppo breve (lunghezza min.: {})",
                    noCharType: "la password deve contenere {}",
                    digits: "numeri",
                    letters: "lettere",
                    lettersUp: "lettere in MAIUSCOLO",
                    symbols: "simboli",
                    inBlackList: "la password +Ę nella lista delle password pi++ usate",
                    passRequired: "+Ę necessaria una password",
                    equalTo: "la password +Ę uguale al login",
                    repeat: "la password +Ę composta da caratteri che si ripetono",
                    badChars: "la password contiene caratteri non accettati: ÔÇť{}ÔÇŁ",
                    weakWarn: "debole",
                    invalidPassWarn: "*",
                    weakTitle: "Questa password +Ę debole",
                    generateMsg: "Per generare una password forte, clicca sul tasto {}."
            }
        },
        nl: {
            lower: true,
                msg: {
                pass: "wachtwoord",
                    and: "en",
                    showPass: "Toon wachtwoord",
                    hidePass: "Verberg wachtwoord",
                    genPass: "Willekeurig wachtwoord",
                    passTooShort: "Wachtwoord is te kort (min. lengte: {})",
                    noCharType: "wachtwoord moet {} bevatten",
                    digits: "cijfers",
                    letters: "letters",
                    lettersUp: "hoofdletters",
                    symbols: "symbolen",
                    inBlackList: "wachtwoord is in de lijst meest gebruikte wachtwoorden",
                    passRequired: "wachtwoord is verplicht",
                    equalTo: "wachtwoord is hetzelfde als login",
                    repeat: "wachtwoord bestaat uit herhaalde karakters",
                    badChars: "wachtwoord bevat verboden karakters: ÔÇť{}ÔÇŁ",
                    weakWarn: "zwak",
                    invalidPassWarn: "*",
                    weakTitle: "Dit wachtwoord is zwak",
                    generateMsg: "Klik {} om een sterk wachtwoord te genereren."
            }
        },
        ru: {
            lower: true,
                msg: {
                pass: "đ+đ-ĐÇđżđ+Đî",
                    and: "đŞ",
                    showPass: "đčđżđ|đ-đĚđ-ĐéĐî đ+đ-ĐÇđżđ+Đî",
                    hidePass: "đíđ|ĐÇĐőĐéĐî đ+đ-ĐÇđżđ+Đî",
                    genPass: "đíđ+ĐâĐçđ-đ+đŻĐőđ+ đ+đ-ĐÇđżđ+Đî",
                    passTooShort: "đ+đ-ĐÇđżđ+Đî Đüđ+đŞĐłđ|đżđ+ đ|đżĐÇđżĐéđ|đŞđ+ (đ+đŞđŻ. đ+đ+đŞđŻđ-: {})",
                    noCharType: "đ- đ+đ-ĐÇđżđ+đÁ đ+đżđ+đÂđŻĐő đ-ĐőĐéĐî {}",
                    digits: "ĐćđŞĐäĐÇĐő",
                    letters: "đ-Đâđ|đ-Đő",
                    lettersUp: "đ-Đâđ|đ-Đő đ- đĺđĽđáđąđŁđĽđť ĐÇđÁđ|đŞĐüĐéĐÇđÁ",
                    symbols: "ĐüđŞđ+đ-đżđ+Đő",
                    inBlackList: "ĐŹĐéđżĐé đ+đ-ĐÇđżđ+Đî Đçđ-ĐüĐéđż đŞĐüđ+đżđ+ĐîđĚĐâđÁĐéĐüĐĆ đ- đśđŻĐéđÁĐÇđŻđÁĐéđÁ",
                    passRequired: "đ+đ-ĐÇđżđ+Đî đżđ-ĐĆđĚđ-ĐéđÁđ+đÁđŻ",
                    equalTo: "đ+đ-ĐÇđżđ+Đî Đüđżđ-đ+đ-đ+đ-đÁĐé Đü đ+đżđ|đŞđŻđżđ+",
                    repeat: "đ+đ-ĐÇđżđ+Đî ĐüđżĐüĐéđżđŞĐé đŞđĚ đ+đżđ-ĐéđżĐÇĐĆĐÄĐëđŞĐůĐüĐĆ ĐüđŞđ+đ-đżđ+đżđ-",
                    badChars: "đ- đ+đ-ĐÇđżđ+đÁ đÁĐüĐéĐî đŻđÁđ+đżđ+ĐâĐüĐéđŞđ+ĐőđÁ ĐüđŞđ+đ-đżđ+Đő: +ź{}++",
                    weakWarn: "Đüđ+đ-đ-Đőđ+",
                    invalidPassWarn: "*",
                    weakTitle: "đčđ-ĐÇđżđ+Đî Đüđ+đ-đ-Đőđ+, đÁđ|đż đ+đÁđ|đ|đż đ-đĚđ+đżđ+đ-ĐéĐî",
                    generateMsg: "đžĐéđżđ-Đő Đüđ|đÁđŻđÁĐÇđŞĐÇđżđ-đ-ĐéĐî đ+đ-ĐÇđżđ+Đî, đŻđ-đÂđ+đŞĐéđÁ đ|đŻđżđ+đ|Đâ {}."
            }
        },
        ua: {
            lower: true,
                msg: {
                pass: "đ+đ-ĐÇđżđ+Đî",
                    and: "i",
                    showPass: "đčđżđ|đ-đĚđ-ĐéđŞ đ+đ-ĐÇđżđ+Đî",
                    hidePass: "đíĐůđżđ-đ-ĐéđŞ đ+đ-ĐÇđżđ+Đî",
                    genPass: "đĺđŞđ+đ-đ+đ|đżđ-đŞđ+ đ+đ-ĐÇđżđ+Đî",
                    passTooShort: "đ+đ-ĐÇđżđ+Đî Đö đĚđ-đŻđ-đ+Đéđż đ|đżĐÇđżĐéđ|đŞđ+ (đ+iđŻ. đ+đżđ-đÂđŞđŻđ-: {})",
                    noCharType: "đ+đ-ĐÇđżđ+Đî đ+đżđ-đŞđŻđÁđŻ đ+ĐľĐüĐéđŞĐéđŞ {}",
                    digits: "ĐćđŞĐäĐÇđŞ",
                    letters: "đ-Đâđ|đ-đŞ",
                    lettersUp: "đ-Đâđ|đ-đŞ Đâ đĺđĽđáđąđŁđČđ×đťđú ĐÇđÁđ|ĐľĐüĐéĐÇĐľ",
                    symbols: "cđŞđ+đ-đżđ+đŞ",
                    inBlackList: "đ+đ-ĐÇđżđ+Đî đ-Đůđżđ+đŞĐéĐî đ+đż Đüđ+đŞĐüđ|Đâ đ+đ-ĐÇđżđ+đÁđ+, Đëđż đ-đŞđ|đżĐÇđŞĐüĐéđżđ-ĐâĐÄĐéĐîĐüĐĆ đŻđ-đ+Đçđ-ĐüĐéĐľĐłđÁ",
                    passRequired: "đ+đ-ĐÇđżđ+Đî Đö đżđ-đżđ-'ĐĆđĚđ|đżđ-đŞđ+",
                    equalTo: "đ+đ-ĐÇđżđ+Đî Đéđ- đ+đżđ|ĐľđŻ đżđ+đŻđ-đ|đżđ-Đľ",
                    repeat: "đ+đ-ĐÇđżđ+Đî đ+ĐľĐüĐéđŞĐéĐî đ+đżđ-ĐéđżĐÇĐÄđ-đ-đŻĐľ ĐüđŞđ+đ-đżđ+đŞ",
                    badChars: "đ+đ-ĐÇđżđ+Đî đ+ĐľĐüĐéđŞĐéĐî đŻđÁđ+ĐÇđŞđ+ĐâĐüĐéđŞđ+Đľ ĐüđŞđ+đ-đżđ+đŞ: +ź{}++",
                    weakWarn: "Đüđ+đ-đ-đ|đŞđ+",
                    invalidPassWarn: "*",
                    weakTitle: "đŽđÁđ+ đ+đ-ĐÇđżđ+Đî Đö Đüđ+đ-đ-đ|đŞđ+",
                    generateMsg: "đęđżđ- ÔÇőÔÇőĐüĐéđ-đżĐÇđŞĐéđŞ đŻđ-đ+Đľđ+đŻđŞđ+ đ+đ-ĐÇđżđ+Đî, đŻđ-ĐéđŞĐüđŻĐľĐéĐî đ|đŻđżđ+đ|Đâ {}."
            }
        },
        es: {
            lower: true,
                msg: {
                pass: "contrase+-a",
                    and: "y",
                    showPass: "Mostrar contrase+-a",
                    hidePass: "Ocultar contrase+-a",
                    genPass: "Contrase+-a aleatoria",
                    passTooShort: "contrase+-a demasiado corta (longitud m+şn.: {})",
                    noCharType: "la contrase+-a debe contener {}",
                    digits: "d+şgitos",
                    letters: "letras",
                    lettersUp: "letras en MAY+ÜSCULAS",
                    symbols: "s+şmbolos",
                    inBlackList: "la contrase+-a est+í en la lista de las contrase+-as m+ís usadas",
                    passRequired: "se requiere contrase+-a",
                    equalTo: "la contrase+-a es igual al inicio de sesi+|n",
                    repeat: "la contrase+-a tiene caracteres repetidos",
                    badChars: "la contrase+-a contiene caracteres no permitidos: ÔÇť{}ÔÇŁ",
                    weakWarn: "d+ębil",
                    invalidPassWarn: "*",
                    weakTitle: "Esta contrase+-a es d+ębil",
                    generateMsg: "Para generar una contrase+-a segura, haga clic en el bot+|n de {}."
            }
        },
        el: {
            lower: true,
                msg: {
                pass: "¤Ç¤ü¤î¤â+-+-¤â+Ě¤é",
                    and: "+|+-++",
                    showPass: "+á¤ü+++-+++++« +|¤ë+++++|++¤Ź ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é",
                    hidePass: "+Ĺ¤Ç¤î+|¤ü¤ů¤ł+Ě +|¤ë+++++|++¤Ź ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é",
                    genPass: "+Ą¤ů¤ç+-+»++¤é +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é",
                    passTooShort: "++ +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é +Á+»+Ż+-++ ¤Ç++++¤Ź +++++|¤ü¤î¤é (+Á+++Č¤ç++¤â¤ä++ +++«+|++¤é: {})",
                    noCharType: "++ +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é ¤Ç¤ü+ş¤Ç+Á++ +Ż+- ¤Ç+Á¤ü+++ş¤ç+Á++ {}",
                    digits: "¤ł+Ě¤ć+»+-",
                    letters: "+++-¤ä+++Ż+++|+Č +|¤ü+Č+++++-¤ä+-",
                    lettersUp: "+++-¤ä+++Ż+++|+Č +|¤ü+Č+++++-¤ä+- +++Á +Ü+Ľ+Ž+Ĺ+Ť+Ĺ+Ö+Ĺ",
                    symbols: "¤â¤Ź+++-+++++-",
                    inBlackList: "++ +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é +-¤ü+»¤â+|+Á¤ä+-++ ¤â+Á +|+-¤ä+Č+++++|++ +++Ě++++¤ć+++++ş¤â¤ä+Á¤ü¤ë+Ż +|¤ë+++++|¤Ä+Ż",
                    passRequired: "+-¤Ç+-++¤ä+Á+»¤ä+-++ +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é",
                    equalTo: "++ +|¤ë+++++|¤î¤é +Á+»+Ż+-++ ¤î++++++++¤é +++Á ¤ä++ ¤î+Ż+++++- ¤ç¤ü+«¤â¤ä+Ě",
                    repeat: "++ +|¤ë+++++|¤î¤é +-¤Ç++¤ä+Á+++Á+»¤ä+-++ +-¤Ç¤î +Á¤Ç+-+Ż+-+++-+++-+-+Ż¤î+++Á+Ż++¤ů¤é ¤ç+-¤ü+-+|¤ä+«¤ü+Á¤é",
                    badChars: "++ +|¤ë+++++|¤î¤é ¤Ç+Á¤ü+++ş¤ç+Á++ +++Ě +Á¤Ç++¤ä¤ü+Á¤Ç¤ä++¤Ź¤é ¤ç+-¤ü+-+|¤ä+«¤ü+Á¤é: ÔÇť{}ÔÇŁ",
                    weakWarn: "+-++¤Ź+Ż+-++++¤é",
                    invalidPassWarn: "*",
                    weakTitle: "+Ĺ¤ů¤ä¤î¤é ++ +|¤ë+++++|¤î¤é ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é +Á+»+Ż+-++ +-++¤Ź+Ż+-++++¤é",
                    generateMsg: "+ô+++- +Ż+- +++Ě++++++¤ů¤ü+|+«¤â+Á¤ä+Á ++¤ů+Ż+-¤ä¤î +|¤ë+++++|¤î ¤Ç¤ü¤î¤â+-+-¤â+Ě¤é, +|+Č+Ż¤ä+Á +|+++++| ¤â¤ä++ +|++¤ů++¤Ç+» {}."
            }
        },
        pt: {
            lower: true,
                msg: {
                pass: "senha",
                    and: "e",
                    showPass: "Mostrar senha",
                    hidePass: "Ocultar senha",
                    genPass: "Senha aleat+|ria",
                    passTooShort: "senha muito curta (tamanho m+şnimo: {})",
                    noCharType: "Senha deve conter {}",
                    digits: "d+şgito",
                    letters: "letras",
                    lettersUp: "letras mai+|sculas",
                    symbols: "s+şmbolos",
                    inBlackList: "senha est+í na lista das senhas mais usadas",
                    passRequired: "senha +ę obrigat+|ria",
                    equalTo: "senha igual ao login",
                    repeat: "senha consiste em uma repeti+ž+úo de caracteres",
                    badChars: "senha tem caracteres inv+ílidos: ÔÇť{}ÔÇŁ",
                    weakWarn: "fraca",
                    invalidPassWarn: "*",
                    weakTitle: "Esta senha +ę fraca",
                    generateMsg: "Para gerar uma senha forte, clique no bot+úo {}."
            }
        }
    };
})(window.jQuery, document, window);
