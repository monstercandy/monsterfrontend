(function(app){

  var templateDirectory = "docrootapi"

  app.directive("mcDocrootWebhostingDocrootsAndProtectedDirs", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroots-and-protected-dirs'),
     }

  }],{
    title: "Docroot API admin controls",
    paramsRequired: { "server": "@",domain:"@",whId:"@" },
    paramsOptional: {display:"@"},
    menuKey: templateDirectory,
  })

  app.factory("mcDocrootCertificateQuery", ["MonsterCloud",function(MonsterCloud){
      return function(server, webhostingId, superuser) {
         return MonsterCloud.get("/s/"+server+(superuser?"/su":"")+"/docrootapi/docroots/certificates/"+webhostingId)
      }
  }])


  app.service("mcDocrootTask", ["$task","mcDocrootApiUrl",function($task,mcDocrootApiUrl){

         return function(title, server, prom){
              var taskUrlBase = mcDocrootApiUrl(server)+"/tasks/"

              return prom.then(function(h){
                  return $task({title:title, url:taskUrlBase+h.id})
              })
         }
  }])
  app.service("mcAwstatsTask", ["mcDocrootTask", function(mcDocrootTask){
         return function(server, prom){
              return mcDocrootTask("AWSTATS_TASK_TITLE", server, prom)
         }
  }])


  app.service("mcDocrootApiUrl", function(){
     return function(server, superuser) {
        return "/s/"+server+(superuser? "/su": "")+"/docrootapi"
     }
  })

  app.service("mcDocrootHostentriesService", ["MonsterCloud","mcDocrootApiUrl",function(MonsterCloud, mcDocrootApiUrl){
      return function(server, whId) {
          var url = mcDocrootApiUrl(server)+"/docroots/hostentries/"+whId
          return MonsterCloud.get(url)
      }

  }])
  app.directive("mcDocrootHostentries", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-hostentries'),
        scope: { "server":"@", "whId":"@", "model":"=", "form": "=" },
        controller: ["$scope","mcDocrootHostentriesService",function($scope,mcDocrootHostentriesService){
            return mcDocrootHostentriesService($scope.server, $scope.whId)
              .then(function(d){
                  $scope.hostentries = d
              })

        }]
     }

  }])

  app.directive("mcDocrootDomainsOfWebstore", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-domains-of-webstore'),
        scope: { "server":"@", "whId":"@" },
        controller: ["$scope","MonsterCloud","mcDocrootApiUrl",function($scope,MonsterCloud,mcDocrootApiUrl){
            var url = mcDocrootApiUrl($scope.server)+"/docroots/"+$scope.whId+"/"
            return MonsterCloud.get(url)
              .then(function(d){
                  $scope.docrootDomains = d
              })

        }]
     }

  }],{
    title: "List of virtual hosts attached to the webstore",
    paramsRequired: { server: "@",whId:"@" },
    menuKey: templateDirectory,
  })
  app.service("mcDocrootWebhostingDomains", ["mcDocrootApiUrl","MonsterCloud", function(mcDocrootApiUrl, MonsterCloud){
     return function(server, whId){
        return MonsterCloud.get(mcDocrootApiUrl(server)+"/docroots/"+whId+"/")
     }
  }])

  app.service("mcDocrootDistinct", ["mcDocrootApiUrl","MonsterCloud", function(mcDocrootApiUrl, MonsterCloud){
     return function(server, whId){
        return MonsterCloud.get(mcDocrootApiUrl(server)+"/docroots/distinct/"+whId)
     }
  }])
  app.directive("mcDocrootDistinctSelectbox", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-distinct-selectbox'),
        scope: { "server":"@", "whId":"@", "distinct":"=", "required":"@","form":"=" },
        controller: ["$scope","mcDocrootDistinct",function($scope,mcDocrootDistinct){
            mcDocrootDistinct($scope.server,$scope.whId)
              .then(function(d){
                  $scope.distinctDocroots = Object.keys(d)
                  $scope.showError = $scope.distinctDocroots.length <= 0
                  $scope.showSelectbox = $scope.distinctDocroots.length > 0
              })

        }]
     }

  }])
  app.directive("mcDocrootDistinctRemove", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-distinct-remove'),
        scope: { "server":"@", "whId":"@","removeUrlPrefix":"@" },
        controller: ["$scope","mcDocrootDistinct","$window",function($scope,mcDocrootDistinct,$window){
            $scope.remove = function(docroot){
               $window.location.href = $scope.removeUrlPrefix+encodeURIComponent(docroot)
            }
            mcDocrootDistinct($scope.server,$scope.whId)
              .then(function(d){
                  $scope.distinctDocroots = d
              })

        }]
     }

  }])

  app.directive("mcDocrootWebhosting", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-webhosting'),
        scope: { "server":"@", "domain": "@", "display": "@", "whId":"@", "showForce": "@" },
        controller: ["$scope",function($scope){
            $scope.showError = !$scope.whId || !$scope.server

        }]
     }

  }])

app.directive("mcAwstatsRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'awstats-row'),
      scope: { "server":"@","whId":"@","domain": "@","link":"@" },
      controller: ["$scope", "mcDocroot", function($scope, mcDocroot){
          mcDocroot.Lookup($scope.server, $scope.whId, $scope.domain)
            .then(function(result){
              $scope.lookupFinished = true
              $scope.docroot = result
            })

          $scope.awstatsOn = function(){
             mcDocroot.CreateAwstats($scope.server, $scope.whId, $scope.domain)
          }
          $scope.awstatsOff = function(){
             mcDocroot.DeleteAwstats($scope.server, $scope.whId, $scope.domain)
          }
      }]
   }

}])

Array(
   {n:"mcDocrootHttpsonlyRow", t:'httpsonly-row', k: "httpsonly"},
   {n:"mcDocrootHstsheadersRow", t:'hstsheaders-row', k: "hstsheaders"},
   {n:"mcDocrootAnticlickjackingheadersRow", t:'anticlickjackingheaders-row', k: "anticlickjackingheaders"},
   {n:"mcDocrootSkipnginxstaticfilesRow", t:'skipnginxstaticfiles-row', k: "skipnginxstaticfiles"},
   {n:"mcDocrootNginxgzipRow", t:'nginxgzip-row', k: "nginxgzip"},
   {n:"mcDocrootDjangostaticRow", t:'djangostatic-row', k: "djangostatic"},
   {n:"mcDocrootDontblockxmlrpcRow", t:'dontblockxmlrpc-row', k: "dontblockxmlrpc"},
   {n:"mcDocrootForceredirectRow", t:'forceredirect-row', k: "forceredirect"}
).forEach(function(x){
    app.directive(x.n, ["templateDir", function(templateDir){
      return {
          restrict: "A",
          templateUrl: templateDir(templateDirectory, x.t),
          scope: { "server":"@","whId":"@","domain": "@" },
          controller: ["$scope", "mcDocroot", function($scope, mcDocroot){
              var option = x.k;
              mcDocroot.Lookup($scope.server, $scope.whId, $scope.domain)
                .then(function(result){
                  $scope.lookupFinished = true
                  $scope.docroot = result
                })

              $scope.turnOn = function(){
                 mcDocroot.TurnFlag($scope.server, $scope.whId, $scope.domain, option, true);
              }
              $scope.turnOff = function(){
                 mcDocroot.TurnFlag($scope.server, $scope.whId, $scope.domain, option, false);
              }
          }]
       }

    }])

})

app.directive('mcDocrootSettings',  ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'docroot-settings'),
   }
}],
       {
       title:"Document root settings",
       menuKey: templateDirectory,
       keywords: ["docroot", "webserver", "awstats", "hsts", "https", "clickjacking"],
       paramsRequired: { "server":"@","domain": "@", "whId":"@"},
       paramsOptional: {"awstatsLink": "@", "securityOnly": "@", "hidePageHeader": "@"  },
      }
);

app.directive('mcAwstatsLink', ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'awstats-link'),
      scope: { "server":"@","whId":"@","domain": "@", link: "@", },
      controller: ["$scope", "mcDocroot", function($scope, mcDocroot){
          mcDocroot.Lookup($scope.server, $scope.whId, $scope.domain)
            .then(function(result){
              $scope.docroot = result
            })
      }]
   }

}]);


app.service("mcDocrootVtsListService", ["MonsterCloud","mcDocrootApiUrl","formatBytes", function(MonsterCloud,mcDocrootApiUrl,formatBytes){
  return function($scope, superuser, webhostingLookup, accountLookup){

           $scope.superuser = superuser;

           var serverBaseUrl = mcDocrootApiUrl($scope.server, $scope.superuser);

           var dateDiffMs =
             $scope.superuser ?
               12096e5 : // 12096e5 is a magic number which is 14 days in milliseconds.
               1*24*60*60*1000
           ;


           var fromDate = new Date(+new Date - dateDiffMs);

           var fieldsToProcess = Array("tl_bytes_in","tl_bytes_out","tl_bytes_sum");
           $scope.filterData = {from: fromDate};

           $scope.opened = Array(false, false);
           $scope.open = function(i){
              $scope.opened[i] = true;
           }


           $scope.refetch = function(tableState){

               $scope.displayedCollection = null;

                  var d = angular.extend({}, tableState.pagination.params, $scope.filterData);
                  if(tableState.search.str)
                    d.filter = tableState.search.str;

                  return MonsterCloud.post(serverBaseUrl+"/vts/"+($scope.whId?$scope.whId+"/":"")+"search2", d)
                    .then(function(re){

                       tableState.pagination.totalItemCount = re.count;

                        $scope.vtsRe = re;
                        var rows = re.rows;

                        rows.forEach(function(row){
                            fieldsToProcess.forEach(function(k){
                               row[k+"_human"] = formatBytes(row[k]);
                            });                          
                        })

                        fieldsToProcess.forEach(function(k){
                           re.sum[k+"_human"] = formatBytes(re.sum[k]);
                        });


                        if(webhostingLookup) {
                          webhostingLookup(rows, $scope.server, null, "tl_webhosting_id", null, "user_id")
                             .then(function(){
                               return accountLookup(rows, "user_id")
                             })
                        }

                        $scope.displayedCollection = rows;
                    })

           }

  }
}]);


  app.directive("mcDocrootVtsListCommon", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'vts-list-common'),
        //shared scope. this stuff is here only for the template
     }
  }])

  app.directive("mcDocrootVtsList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        template: '<mc-docroot-vts-list-common></mc-docroot-vts-list-common>',
        controller: ["$scope","mcDocrootVtsListService", function($scope,mcDocrootVtsListService){
           return mcDocrootVtsListService($scope, false);
        }]
     }
  }],
       {
       title:"VTS list",
       menuKey: templateDirectory,
       keywords: ["docroot", "nginx", "bandwidth"],
       paramsRequired: { "server":"@", "whId":"@"},
      }
  );

  app.directive("mcDocrootWebhostingDocroots", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docroot-webhosting-docroots'),
        controller:  showErrorsController({
          inject: ["mcDocrootApiUrl","mcDocroot","mcAwstatsTask","$prompt", "$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcDocrootApiUrl,mcDocroot,mcAwstatsTask, $prompt, $alertSuccess) {

             $scope.delete = function(record) {
                return MonsterCloud.delete($scope.baseUrl+record.host, {force:true})
                  .then(function(){
                      return $scope.refetch();

                  })
             }


             $scope.resetPl = function(){
                $scope.pl = {"host": $scope.domain}

             }


             $scope.refetch = function(){
               return mcDocroot.Lookup($scope.server,$scope.whId,$scope.domain, true)
                 .then(function(d){
                    $scope.all = d
                    $scope.records = d.entries
                    setShowListing($scope)
                 })

             }

             function thenShowIt(p) {
                 return p.then(function(d){
                       $scope.serverConfig =d
                       return $scope.refetch()
                  })
             }
             function thenShowSuccess(p) {
                 return thenShowIt(p)
                   .then(function(){
                      return $alertSuccess();
                   });
             }
             function getBaseDocrootUrl(){
                return mcDocrootApiUrl($scope.server, true)+"/docroots/"+$scope.whId+"/"+$scope.domain+"/"
             }

             $scope.docrootConfigShow = function() {
                thenShowIt(MonsterCloud.get(getBaseDocrootUrl()+"raw"))
             }

             $scope.awstatsProcess = function(){
                mcAwstatsTask($scope.server, MonsterCloud.post($scope.awstatsBaseUrl+"/process", {}))
             }
             $scope.awstatsBuild = function(){
                mcAwstatsTask($scope.server, MonsterCloud.post($scope.awstatsBaseUrl+"/build", {}))
             }

             $scope.awstatsConfigShow = function(){
                thenShowIt(MonsterCloud.get($scope.awstatsBaseUrl+"/raw"))
             }

             $scope.suspend = function(suspendStatus){
                thenShowIt(MonsterCloud.post(getBaseDocrootUrl()+"suspend", {"suspended":suspendStatus}), true)
             }

             $scope.expiredRoot = function(){
                thenShowIt(MonsterCloud.post(getBaseDocrootUrl()+"expiredroot", {"expired-root": true}), true)
             }

             $scope.bandwidthExceeded = function(exceeded){
                thenShowIt(MonsterCloud.post(getBaseDocrootUrl()+"bandwidthexceeded", {"bandwidth-exceeded": exceeded}), true)
                  .then(function(){
                      $scope.all['bandwidth-exceeded'] = exceeded;
                  })
             }

             $scope.setPostdata = function(postdata){
                thenShowSuccess(MonsterCloud.post(getBaseDocrootUrl()+"postdata", {"postdata": postdata}), true)
                  .then(function(){
                      $scope.all['postdata'] = postdata;
                  })
             }
             $scope.bandwidthExceededRoot = function(){
                thenShowIt(MonsterCloud.post(getBaseDocrootUrl()+"bandwidthexceededroot", {"bandwidth-exceeded-root": true}), true)
             }
             $scope.primary = function(){
                thenShowIt(MonsterCloud.post(getBaseDocrootUrl()+"primary", {"primary": true}), true)
             }
             $scope.rebuildWebhostingDocroot = function(){
                thenShowIt(MonsterCloud.post("/s/"+$scope.server+"/su/docrootapi/cleanup/webhosting/"+$scope.whId+"/rebuild"))

             }
             $scope.requestTimeout = function(){
                return $prompt({title: "DOCROOT_DYN_TIMEOUT_TITLE", message: "DOCROOT_DYN_TIMEOUT_MESSAGE", placeholder: "DOCROOT_DYN_TIMEOUT_PLACEHOLDER"})
                  .then(function(timeout){
                      return MonsterCloud.post(getBaseDocrootUrl()+"timeout", {"timeout": timeout})

                  })
                  .then(function(d){
                     $scope.serverConfig =d
                  })
             }



             $scope.awstatsBaseUrl = mcDocrootApiUrl($scope.server, true)+"/awstats/"+$scope.whId+"/"+$scope.domain
             $scope.baseUrlRoot = mcDocrootApiUrl($scope.server, $scope.showForce)+"/docroots/"+$scope.whId+"/"+$scope.domain
             $scope.baseUrl = $scope.baseUrlRoot+"/entries/"

             $scope.docrootPlaceholder = "/"+$scope.domain+"/pages"
             $scope.endsWith = !$scope.showForce ? $scope.domain : ""
             $scope.records = []

             setShowListing($scope)
             $scope.resetPl()

             return $scope.refetch()


          },
          fireCallback: function($scope, MonsterCloud, $q){
             return MonsterCloud.put($scope.baseUrl, $scope.pl)
               .then(function(){
                  $scope.resetPl()

                  return $scope.refetch()

               })


          }

        })

     }

  }],
       {
       title:"Web server document roots",
       menuKey: templateDirectory,
       keywords: ["docroot", "webserver"],
       paramsRequired: { "server":"@","domain": "@", "whId":"@"},
       paramsOptional: {"display": "@", "showForce": "@"}
      }
  )


  app.service("mcDocroot", ["MonsterCloud", '$q', 'mcDocrootApiUrl', function(MonsterCloud, $q, mcDocrootApiUrl){
      var cache = {}
      var inProgress = {}

      function getKey(server,whId,domain){
            return server+"-"+whId+"-"+domain
      }

      function baseUrl(server, whId, domain, key){
         return mcDocrootApiUrl(server)+"/"+key+"/"+whId+"/"+domain;
      }

      var re = {
         "Lookup": function(server, whId, domain, force) {
            var key= getKey(server,whId,domain)
            if((!force)&&(cache[key])) return $q.when(cache[key])

            if(!inProgress[key])
              inProgress[key] = MonsterCloud.get(baseUrl(server, whId, domain, "docroots"))
                .catch(function(ex){
                   return $q.when({entries:["-"]})
                })
                .then(function(d){
                   delete inProgress[key]
                   cache[key] = d
                   return $q.when(cache[key])
                })

            return $q.when(inProgress[key])
         },
         "TurnAwstats": function(server, whId, domain, method, newState) {
             return MonsterCloud[method](baseUrl(server, whId, domain, "awstats"))
               .then(function(d){
                  var key= getKey(server,whId,domain)
                  if(cache[key]) cache[key].awstats = newState
                  return $q.when(d)
               })

         },
         "CreateAwstats": function(server, whId, domain){
            return re.TurnAwstats(server, whId, domain, "post", true);
         },
         "DeleteAwstats": function(server, whId, domain){
            return re.TurnAwstats(server, whId, domain, "delete", false);
         },

         "TurnFlag": function(server, whId, domain, optionKey, optionState) {
            var payload = {};
            payload[optionKey] = optionState;
            return MonsterCloud.post(baseUrl(server, whId, domain, "docroots")+"/"+optionKey, payload)
              .then(function(d){
                  var cacheKey= getKey(server,whId,domain);
                  if(cache[cacheKey]) cache[cacheKey][optionKey] = optionState;
                  return $q.when(d);
              })
         },
      }

      return re;
  }])

  app.directive("mcDocrootCurrent", ["templateDir", "mcDocroot", function(templateDir, mcDocroot){
      return {
        restrict: "E",
        scope: { "server":"@","domain": "@", "whId":"@", },
        link: function (scope, element, attrs) {

              mcDocroot.Lookup(scope.server,scope.whId,scope.domain)
                .then(function(d){
                    var t = ""
                    if((d.entries)&&(d.entries[0])&&(d.entries[0].docroot))
                       t = d.entries[0].docroot
                    element.text(t);
                })

        }
      }

  }])


  Array({m:"Redirects",n:"HTTP",k:[]},{m:"Frames",n:"Frame"}).forEach(function(cat){
      app.directive("mcDocroot"+cat.m, ["templateDir", function(templateDir){
          return {
            restrict: "E",
            templateUrl: templateDir(templateDirectory,'docroot-redirects-base'),
            controller:  showErrorsController({
              inject: ["mcDocrootApiUrl"],
              additionalInitializationCallback: function($scope, MonsterCloud, $q, mcDocrootApiUrl){

                 $scope.redirectType = cat.n
                 $scope.redirectUri = cat.m.toLowerCase()

                 $scope.delete = function(record) {
                    return MonsterCloud.delete($scope.baseUrl+record.host, {})
                      .then(function(){
                          return $scope.refetch();

                      })
                 }


                 $scope.resetPl = function(){
                    $scope.pl = {"host": $scope.domain}

                 }

                 $scope.refetch = function(){
                   return MonsterCloud.get($scope.baseUrl)
                     .then(function(d){
                        $scope.records = d
                        setShowListing($scope)
                     })

                 }

                 $scope.baseUrl = mcDocrootApiUrl($scope.server)+"/"+$scope.redirectUri+"/"+$scope.domain+"/"
                 $scope.records = []

                 setShowListing($scope)
                 $scope.resetPl()

                 return $scope.refetch()


              },
              fireCallback: function($scope, MonsterCloud, $q){

                 return MonsterCloud.put($scope.baseUrl, $scope.pl)
                   .then(function(){

                      $scope.resetPl()

                      return $scope.refetch()

                   })


              }

            })

         }

      }],{
       title:"Redirect web site visitors ("+cat.n+")",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@","domain": "@"},
       paramsOptional: {"display": "@","whId":"@"}
      })
  })

  app.directive("mcDocrootWebhostingProtectedDirsAndAccounts", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'protected-dirs-and-accounts'),
      }
  }],
      {
       title:"Password protected directories and accounts",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@","domain": "@", "whId":"@"},
       paramsOptional: {"display": "@", "showWarningAboutDomains": "@"}
      }
  )

  app.directive("mcDocrootWebhostingProtectedDirAccounts", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'protected-dir-accounts'),
        controller:  showErrorsController({
          inject: ["mcDocrootApiUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcDocrootApiUrl){

             $scope.delete = function(record) {
                return MonsterCloud.delete($scope.baseUrl+record.username, {})
                  .then(function(){
                      return $scope.refetch();
                  })
             }


             $scope.resetPl = function(){
                $scope.pl = {}
                $scope.tmp = {}

             }

             $scope.refetch = function(){
               return MonsterCloud.get($scope.baseUrl)
                 .then(function(d){
                    $scope.records = d
                    setShowListing($scope)
                 })

             }

             $scope.baseUrl = mcDocrootApiUrl($scope.server)+"/auths/"+$scope.whId+"/"
             $scope.records = []

             setShowListing($scope)
             $scope.resetPl()

             return $scope.refetch()

          },
          fireCallback: function($scope, MonsterCloud, $q){

             return MonsterCloud.put($scope.baseUrl, $scope.pl)
               .then(function(){

                  $scope.resetPl()

                  return $scope.refetch()

               })


          }

        })

     }

  }], {
     title: "Accounts for password protected web directories",
     menuKey: templateDirectory,
     paramsRequired: { "server":"@","whId": "@" },
     paramsOptional: { "showWarningAboutDomains": "@" },
  })


  app.directive("mcGlobalWebhostingDocrootSelector", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'global-webhosting-docroot-selector'),
        scope: { "server":"@", "model":"=", "whId":"@" },
        controller:  ["$scope", "mcDocrootWebhostingDomains", function($scope, mcDocrootWebhostingDomains){
           $scope.model = {}

           $scope.onWebhostingSelected = function(){
              console.log("new webhosting selected!", $scope.model)
              $scope.docrootLookupFinished = false

              return mcDocrootWebhostingDomains($scope.model.server, $scope.model.webhosting_id)
                .then(function(entries){
                  console.log("HEY", entries)
                   $scope.docrootLookupFinished = true
                   $scope.docrootDomains = entries
                })

           }

           // this feature locks the webhosting (no selectbox is displayed)
           if($scope.whId) {
              $scope.model = {server:$scope.server, webhosting_id: $scope.whId}
              $scope.onWebhostingSelected()
           }

        }]

     }

  }])

  app.directive("mcDocrootWebhostingProtectedDirs", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'protected-dirs'),
        scope: { "server":"@","domain": "@", "display": "@", "whId":"@"},
        controller:  showErrorsController({
          inject: ["mcDocrootApiUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcDocrootApiUrl){

             $scope.delete = function(record) {
                return MonsterCloud.delete($scope.baseUrl, {"dir": record})
                  .then(function(){
                      return $scope.refetch();

                  })
             }


             $scope.resetPl = function(){
                $scope.pl = {}
             }

             $scope.refetch = function(){
               return MonsterCloud.get($scope.baseUrl)
                 .then(function(d){
                    $scope.records = d
                    setShowListing($scope)
                 })

             }

             $scope.baseUrl = mcDocrootApiUrl($scope.server)+"/protected-dirs/"+$scope.whId+"/"+$scope.domain+"/"
             $scope.records = []

             setShowListing($scope)
             $scope.resetPl()

             return $scope.refetch()

          },
          fireCallback: function($scope, MonsterCloud, $q){

             return MonsterCloud.put($scope.baseUrl, $scope.pl)
               .then(function(){

                  $scope.resetPl()

                  return $scope.refetch()

               })


          }

        })

     }

  }])



  function setShowListing($scope) {
               $scope.showListing = $scope.records && $scope.records.length > 0

  }


})(app)
