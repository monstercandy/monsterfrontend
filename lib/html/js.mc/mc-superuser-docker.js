(function(app){

  var templateDirectory = "docker-superuser"


app.directive("mcDockerConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/docker'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Docker microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcSuperuserDockerImages", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'images'),
        controller: showErrorsController({
          inject: ["$alertSuccess","mcDockerTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){


             $scope.pl = {};
             $scope.baseUrl = "/s/"+$scope.server+"/su/docker/root/";
            var url = $scope.baseUrl+"images/list"
            $scope.refetch = function(){
                return MonsterCloud.post(url)
                   .then(function(images){
                      $scope.images = images;
                      $scope.images.map(function(x){
                         x.createdHuman = new Date(x.Created*1000).toISOString();
                         x.name_joined = x.RepoTags.join(", ");
                      })
                   })
            }

            $scope.remove = function(imageId, force){
               return MonsterCloud.delete($scope.baseUrl+"image/"+imageId, {force: force})
                 .then(function(){
                    return $alertSuccess();
                 })
                 .then(function(){
                    return $scope.refetch();
                 })
            }

            $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcDockerTask){
             // pulling.
             var p = MonsterCloud.post($scope.baseUrl+"images/pull", {tag: $scope.pl.image});
             return mcDockerTask($scope.server, "DOCKER_TASK_PULL_IMAGE", p)
               .then(function(){
                  return $scope.refetch();
               })
          }
      })

     }

  }], {
     title: "Docker application images",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"}
  })

  app.directive("mcSuperuserDockerContainers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'containers'),
        controller: showErrorsController({
          inject: ["accountLookup","webhostingLookup","mcDockerTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, accountLookup, webhostingLookup, mcDockerTask){


             $scope.pl = {};
             $scope.baseUrl = "/s/"+$scope.server+"/su/docker/root/";
              var url = $scope.baseUrl + "containers/list"

              $scope.rebuild = function(container){
                 var payload;
                 if(container) {
                   payload = {containers: [container.Id]};
                 }

                 var p = MonsterCloud.post($scope.baseUrl+"containers/rebuild", payload);
                 return mcDockerTask($scope.server, "DOCKER_TASK_REBUILD_CONTAINERS", p)
                   .then(function(){
                      return $scope.refetch();
                   })
              }

              $scope.refetch = function(){
                  return MonsterCloud.post(url)
                     .then(function(containers){
                        $scope.containers = containers;

                        accountLookup($scope.containers, "c_user_id")
                        webhostingLookup($scope.containers, $scope.server, null, "c_webhosting")

                     })
              }

              $scope.refetch();


          },
          fireCallback: function($scope, MonsterCloud, $q){
             var command = $scope.pl.command.split(" ");
             return MonsterCloud.post($scope.baseUrl+"container/"+$scope.pl.container+"/exec", {command: command})
               .then(function(re){
                  $scope.lastCmd = re;
               })
          }
      })
     }

  }], {
     title: "Docker containers",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"}
  })


  app.directive("mcSuperuserDockerContainerInspect", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'container-inspect'),
        controller: showErrorsController({
          additionalInitializationCallback: function($scope, MonsterCloud, $q){


             $scope.pl = {};
             $scope.baseUrl = "/s/"+$scope.server+"/su/docker/root/container/"+$scope.cId+"/";
             var url = $scope.baseUrl+"inspect";
             return MonsterCloud.get(url)
               .then(function(info){
                  $scope.info = info;

                  return MonsterCloud.post($scope.baseUrl+"stats");
               })
               .then(function(stats){
                  $scope.stats = stats;

                  return MonsterCloud.post($scope.baseUrl+"logs");
               })
               .then(function(logs){
                  $scope.logs = logs;
               })


          },
          fireCallback: function($scope, MonsterCloud, $q){
             var command = $scope.pl.command.split(" ");
             return MonsterCloud.post($scope.baseUrl+"exec", {command: command})
               .then(function(re){
                  $scope.lastCmd = re;
               })
          }
      })
     }

  }], {
     title: "Docker container info",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@", "cId": "@"}
  })



  app.directive("mcSuperuserDockerContainerConfig", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'container-volume-config'),
        controller: showErrorsController({
          inject: ["$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){

              $scope.baseUrl = "/s/"+$scope.server+"/su/docker/root/container/"+$scope.cId+"/";

              return MonsterCloud.get($scope.baseUrl+"config")
               .then(function(config){
                  $scope.configStr = JSON.stringify(config, null, 2);
               });

          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
                return MonsterCloud.post($scope.baseUrl+"config", JSON.parse($scope.configStr))
                  .then($alertSuccess)
          }
      })
     }

  }], {
     title: "Docker volume config",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@", "cId": "@"}
  })


  app.directive("mcSuperuserDockerImageInspect", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'image-inspect'),
        controller: ["$scope","MonsterCloud",
          function($scope,MonsterCloud) {
             var url = "/s/"+$scope.server+"/su/docker/root/image/"+$scope.iId+"/inspect";
             return MonsterCloud.get(url)
               .then(function(info){
                  $scope.info = info;
               })
          }]        
     }

  }], {
     title: "Docker application image info",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@", "iId": "@"}
  })

  app.directive("mcDockerUpgradeContainers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docker-upgrade-containers'),
        scope: {server:"@"},
        controller: ["$scope","MonsterCloud","mcDockerTask","webhostingLookup","accountLookup","$alertSuccess",
          function($scope,MonsterCloud,mcDockerTask,webhostingLookup,accountLookup,$alertSuccess) {
            $scope.upgradeAllContainersDisabled = true;

            var baseUrl = "/s/"+$scope.server+"/su/docker/root/";

            $scope.upgradeContainers = function(container){
               var payload = {};
               if(container)
                  payload.containers = [container.Id];
               var p = MonsterCloud.post(baseUrl+"containers/upgrade", payload);
               return mcDockerTask($scope.server, "DOCKER_TASK_UPGRADE_CONTAINERS", p)
                 .then(function(){
                    return $scope.listUpgradeableContainers();      
                 })
            }

            $scope.listUpgradeableContainers = function(){
              $scope.showContainers = true;
              return MonsterCloud.get(baseUrl+"containers/list/upgradable")
                .then(function(containers){
                    $scope.upgradableContainers = containers;
                    $scope.upgradeAllContainersDisabled = ($scope.upgradableContainers.length == 0);
                    accountLookup($scope.upgradableContainers, "c_user_id")
                    webhostingLookup($scope.upgradableContainers, $scope.server, null, "c_webhosting")
                })
            }

          }]        
     }

  }]);

  app.directive("mcDockerCleanupOldImages", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docker-cleanup-old-images'),
        scope: {server:"@"},
        controller: ["$scope","MonsterCloud","mcDockerTask","webhostingLookup","accountLookup","$alertSuccess",
          function($scope,MonsterCloud,mcDockerTask,webhostingLookup,accountLookup,$alertSuccess) {

            $scope.removeAllImagesDisabled = true;

            var baseUrl = "/s/"+$scope.server+"/su/docker/root/";


            $scope.removeOldImages = function(image){
               var payload = {};
               if(image)
                  payload.images = [image.Id];
               var p = MonsterCloud.post(baseUrl+"images/cleanup", payload);
               return mcDockerTask($scope.server, "DOCKER_TASK_REMOVE_IMAGES", p)
                 .then(function(){
                    return $scope.listUpgradeableImages();      
                 })
            }


            $scope.listUpgradeableImages = function(){
              $scope.showImages = true;
              return MonsterCloud.get(baseUrl+"images/list/upgradable")
                .then(function(images){
                    $scope.upgradableImages = images;
                    $scope.removeAllImagesDisabled = ($scope.upgradableImages.length == 0);
                })
            }


          }]        
     }

  }]);

  app.directive("mcSuperuserDockerAdmin", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'docker-admin'),
        controller: ["$scope","MonsterCloud","mcDockerTask","webhostingLookup","accountLookup","$alertSuccess",
          function($scope,MonsterCloud,mcDockerTask,webhostingLookup,accountLookup,$alertSuccess) {
            $scope.cleanupDisabled = true;
            $scope.cl = {
              configEntries: true, 
              volume: true, 
              container: true,
              image: false,
            };

            var baseUrl = "/s/"+$scope.server+"/su/docker/root/";


            $scope.cleanupDo = function(){
                var cl = angular.copy($scope.cl);
                return MonsterCloud.post(baseUrl+"local/cleanup/do", cl)
                  .then(function(r){
                      $scope.cleanupResult = r;
                      $scope.cleanupDisabled = true;
                  });              
            }

            $scope.cleanupList = function(){
                return MonsterCloud.post(baseUrl+"local/cleanup/list")
                  .then(function(list){
                      $scope.cleanupResult = list;
                      $scope.cleanupDisabled = false;
                  });

            }

            $scope.rotatePhpFpmLogs = function(){

              return MonsterCloud.post(baseUrl+"local/php-fpm/rotate-logs")
                .then(function(r){
                    return $alertSuccess();
                })
            }
            

            $scope.rebuildLatestMap = function(){              
              return MonsterCloud.post(baseUrl+"images/latest/rebuild")
                .then(function(r){
                    return $alertSuccess();
                })
            }

            $scope.rebuildHostsFile = function(){
              return MonsterCloud.post(baseUrl+"local/hostsfile/rebuild")
                .then(function(r){
                    return $alertSuccess();
                })
            }

            $scope.showWebstoresConfig = function(){
               return MonsterCloud.get(baseUrl+"webstores/config")
                 .then(function(r){
                    $scope.miscResult = r;
                 })

            }

          }]        
     }

  }], {
     title: "Docker admin tools",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"}
  })

})(app)

