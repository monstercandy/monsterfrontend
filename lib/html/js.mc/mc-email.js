(function(app){

  var templateDirectory = "email"


  app.service("emailAccountFixture", ["formatBytes", function(formatBytes){
    return function(a){
         if(!a.ea_id) return
         a.ea_quota_bytes_human = formatBytes(a.ea_quota_bytes)
         a.tally_bytes_human = formatBytes(a.tally_bytes)
         if(a.ea_quota_bytes * 0.9 < a.tally_bytes)
           a.warning = true
    }

  }])

  app.service("mcEmailTask", ["$task",function($task){
    return function(server, title, prom, extraParams){
      return prom.then(function(h){
       var taskUrl = "/s/"+server+"/email/tasks/"+h.id
        return $task(angular.extend({}, {title:title, "url":taskUrl}, extraParams))
      })
    }
  }])

  app.service("mcEmailUrl", function(){
    var re = function(server, whId, domain, suffix) {
       if(whId == "su")
         return "/s/"+server+"/su/email/"+(suffix||"")
       else
         return "/s/"+server+"/email/webhostings/"+whId+"/domains/"+(domain ? ("domain/"+domain+"/"+(suffix||"")) : "")
    }

    return re
  })

  app.directive("mcEmailAliasVsBccInfo", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-alias-vs-bcc-info'),
        scope: { "domain":"@", "display":"@",  },
     }

  }])


  app.directive("mcEmailAccountEdit", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-edit'),
     }

  }], {
    title: "E-mail account settings",
    menuKey: templateDirectory,
    paramsRequired: { "server":"@", "whId":"@", "domain":"@", "eaId": "@", "eaEmail": "@" },
    paramsOptional: { "display":"@"},
  })

  app.directive("mcEmailHelp", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-help'),
        controller: ["mcConfig","$scope",function(mcConfig, $scope){
           $scope.eaEmail = $scope.email || "full.address@domain.tld";
           var m = /(.+)@/.exec($scope.eaEmail);
           $scope.username = m[1];
           m = /(.+?)\./.exec($scope.server);
           var server = m ? m[1] : $scope.server;
           $scope.mxHost = server+"."+mcConfig.config.service_primary_domain;
           $scope.webmailHost = "webmail."+$scope.mxHost;
           $scope.webmailUrl = "https://"+$scope.webmailHost;

        }]
     }

  }], {
    title: "E-mail settings help page",
    menuKey: templateDirectory,
    paramsRequired: { "server":"@" },
    paramsOptional: { "email":"@"},
  })

  app.directive("mcEmailAccountCluebringer", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-cluebringer'),
     }

  }], {
    title: "Quotas and outgoing e-mails",
    menuKey: templateDirectory,
    paramsRequired: { "server":"@", "whId":"@", "domain":"@", "eaId": "@", "eaEmail": "@" },
    paramsOptional: { "display":"@"},
  })

  Array({n:"Account", t:"accounts"},{n:"Alias",t:"aliases"},{n:"Bcc",t:"bccs"}, {n:"Domain",t:"domains"}).forEach(function(c){
      var register = {
       title:"Manage e-mail "+c.t,
       menuKey: templateDirectory,
       keywords: ["postfix","dovecot"],
       paramsRequired: { "server":"@","whId":"@"},
       paramsOptional: {}
      }
      if(c.n != "Domain") {
         register.paramsRequired.domain = "@"
         register.paramsOptional.display = "@"

      }
      app.directive("mcEmail"+c.n+"ListAndAdd", ["templateDir", function(templateDir){
          return {
            restrict: "E",
            templateUrl: templateDir(templateDirectory,'email-'+c.n.toLowerCase()+'-list-and-add'),
         }

      }], register)

  })



app.service("mcEmailStatusService", ["MonsterCloud","$q","showException", function(MonsterCloud,$q, showException){
  var cacheResult = {}
  var cacheProm = {}
  var re = function($scope){
          var urlBase = "/s/"+$scope.server+"/email/domains"
          var url = urlBase + "/domain/"+$scope.domain
          var grUrl = url +"/greylisting"

          $scope.toOn = function(){
             var d = getData()
             var p = !d.data ?
                MonsterCloud.put(urlBase, {do_domain_name: $scope.domain, do_webhosting: $scope.whId || 0}) :
                MonsterCloud.post(url, {do_active: true, do_webhosting: $scope.whId || 0})

             return p.then(function(){
                  var result = getData()
                  result.active = true
             })
          }

          $scope.toOff = function(){
             return MonsterCloud.post(url, {do_active: false})
               .then(function(){
                  var result = getData()
                  result.active = false
               })
          }

          $scope.toGreylistOn = function(){
             return grLogic(true)
          }

          $scope.toGreylistOff = function(){
             return grLogic(false)
          }


          if(cacheResult[url])
            return $q.when(cacheResult[url])

          if(!cacheProm[url]){
            cacheProm[url] = MonsterCloud.doRequest({"method":"SEARCH", "url": url, relayException: true})
              .catch(function(ex){
                 // we catch these exceptions as this control could be used by some frontend control
                 // where we dont want any exception windows to appear
                 console.error("error while checking status of email domain", ex);
                 if((ex.message == "PERMISSION_DENIED")||(!ex.message))
                   return;
                 showException(ex);
                 throw ex;
              })
              .then(function(data){
                delete cacheProm[url]
                var result = {data: data}
                cacheResult[url] = result
                result.active = data && data.do_active
                result.greylist = data && data.Greylisted
                return $q.when(result)
              })
          }

          return cacheProm[url]


          function isActive(){
            var r = getData()
            return r && r.active
          }
          function throwIfNotActive(){
             if(!isActive()) throw new Error("DOMAIN_NOT_ACTIVE")
          }
          function grLogic(newStatus){
             throwIfNotActive()
             return MonsterCloud.post(grUrl,{active:newStatus})
               .then(function(){
                  var result = cacheResult[url]
                  result.greylist = newStatus
                  if(result.data) result.data.Greylisted = newStatus
               })

          }
          function getData(){
              var result = cacheResult[url]
              return result
          }
  }
  re.assign = function($scope){
     return re($scope).then(function(l){
        $scope.lookup = l;
     })
  }
  return re
}])

app.directive("mcEmailStatusRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'email-status-row'),
      scope: { "server":"@","domain": "@", "whId":"@" },
      controller: ["$scope", "mcEmailStatusService", function($scope, mcEmailStatusService){
         return mcEmailStatusService.assign($scope)
      }]
   }

}])
app.directive("mcEmailGreylistStatusRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'email-greylist-status-row'),
      scope: { "server":"@","domain": "@", "whId":"@" },
      controller: ["$scope", "mcEmailStatusService", function($scope, mcEmailStatusService){
         return mcEmailStatusService.assign($scope)
      }]
   }

}])

app.directive("mcEmailStatusWarning", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'email-status-warning'),
      scope: { "server":"@","domain": "@", "whId":"@" },
      controller: ["$scope", "mcEmailStatusService", function($scope, mcEmailStatusService){
         return mcEmailStatusService($scope)
      }]
   }

}])

Array(
  {
    name: "alias",
    serviceName: "mcEmailAliasService",
    queryUrlSuffix: "aliases",
    idKey: "el",
  },
  {
    name: "bcc",
    serviceName: "mcEmailBccService",
    queryUrlSuffix: "bccs",
    idKey: "bc",
  },
  {
    name: "account",
    serviceName: "mcEmailAccountService",
    queryUrlSuffix: "accounts",
    idKey: "ea",
  },
  {
    name: "domain",
    serviceName: "mcEmailDomainService",
    queryUrlSuffix: "domains",
    idKey: "do",
  }
).forEach(function(cat){

  app.service(cat.serviceName, ["MonsterCloud", "mcEmailUrl", "$q", 'emailAccountFixture',"showException", 'mcRoutesService', function(MonsterCloud, mcEmailUrl, $q, emailAccountFixture, showException, mcRoutesService ){
     var maindata = {}
     var cachedProm = {}
     return function(server, whId, domain){
        domain = domain || ""
        var key = server+"-"+whId+"-"+domain
        if(!maindata[key]) maindata[key] = {}
        var data = maindata[key]

        var urlBase;
        if(whId == "su") {
          urlBase = "/s/"+server+"/su/email/"
        }
        else {
          urlBase = "/s/"+server+"/email/webhostings/"+whId+"/";
          if(domain)
            urlBase += "domains/domain/"+domain+"/";
        }

        var entitiesUrl = urlBase+cat.queryUrlSuffix

        var onRefreshCallbacks = []


        var re = {
           Lookup: function(force){
                if((!force)&&(data.result)) return $q.when(data)

                if(!cachedProm[key])
                  cachedProm[key] = MonsterCloud.doRequest({method: "GET", url:entitiesUrl, relayException: true})
                    .then(function(d){
                       delete cachedProm[key]
                       if(d.accounts)
                          d.accounts.forEach(emailAccountFixture)
                       data.result = d

                       onRefreshCallbacks.forEach(function(cb){
                         cb(data.result)
                       })

                       return $q.when(data)
                    })
                    .catch(function(ex){
                        var exMessage = (
                               (ex.data)&&(ex.data.result)&&(ex.data.result.error) ? 
                               ex.data.result.error.message : 
                               ""
                        );
                        if(
                            (cat.serviceName != "mcEmailAccountService")
                            ||
                            (exMessage != "DOMAIN_NOT_FOUND")
                          ) {
                            showException(ex);
                            throw ex;
                        }

                        delete cachedProm[key]; // repeat next time!
                        return mcRoutesService.jumpToState({
                          stateName:"mc-email-domain-list-and-add",
                          stateParams: {
                             server: server,
                             whId: whId,
                          }
                        })


                    })

                return cachedProm[key]
           },
           Remove: function(entity){
             return MonsterCloud.delete(getEntityUrl(entity[cat.idKey+"_id"]))
               .then(function(h){
                  return h.id ? $q.when(h) : re.Lookup(true)
               })

           },
           Verify: function(entity, code){
             return MonsterCloud.post(getEntityUrl(entity[cat.idKey+"_id"])+"/verify", {"token": code})
               .then(function(){
                  entity[cat.idKey+"_verified"] = true
               })
           },

           Add: function(entity, skipLookup){
             return MonsterCloud.put(entitiesUrl, entity)
               .then(function(){
                  if(skipLookup) return $q.when()

                  return re.Lookup(true)
               })
           },

           Change: function(entity, forceLookup){

             return MonsterCloud.post(getEntityUrl(entity[cat.idKey+"_id"]), entity)
               .then(function(x){

                  var p = forceLookup ? re.Lookup(true) : $q.when()

                  return p.then(function(){return $q.when(x)})
               })
           },

           Get: function(entityId){
             return MonsterCloud.get(getEntityUrl(entityId))
               .then(function(account){
                   emailAccountFixture(account)
                   return $q.when(account)
               })
           },
           Post: function(entityId, uri, payload){
             return MonsterCloud.post(getEntityUrl(entityId)+"/"+uri, payload)
           },
           onRefresh: function(callback){
              onRefreshCallbacks.push(callback)
           }

        }

        return re

        function getEntityUrl(entityId){
           var n = urlBase;

           if(whId == "su")
             n += cat.queryUrlSuffix + "/";
           else
           if(cat.serviceName=="mcEmailDomainService")            
             n += "domains/";

           n+= cat.name+"/"+entityId;
           return n;
        }
     }
  }])

})


  app.directive("mcEmailAliasesList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-aliases-list'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller: ["$scope","MonsterCloud","mcEmailAliasService", "$prompt",
          function($scope,MonsterCloud,mcEmailAliasService,$prompt) {

            var aliasService = mcEmailAliasService($scope.server, $scope.whId, $scope.domain)
            aliasService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            $scope.confirm = function(alias){
                return $prompt({title: "EMAIL_ALIAS_CONFIRM_TITLE", message: "EMAIL_ALIAS_CONFIRM_MESSAGE", placeholder: "EMAIL_ALIAS_CONFIRM_PLACEHOLDER"})
                  .then(function(code){
                      return aliasService.Verify(alias, code)
                  })
            }
            $scope.delete = function(alias){
               return aliasService.Remove(alias)
            }


        }]
     }

  }])


  app.directive("mcEmailAliasAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-alias-add'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller:  showErrorsController({
          inject: ["mcEmailAliasService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailAliasService){

            $scope.aliasService = mcEmailAliasService($scope.server, $scope.whId, $scope.domain)
            $scope.aliasService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

          },
          fireCallback: function($scope, MonsterCloud, $q){

            return $scope.aliasService.Add($scope.alias)
               .then(function(){
                  $scope.alias = {}
               })


          }

        })

     }

  }])



  app.directive("mcEmailBccsList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-bccs-list'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller: ["$scope","MonsterCloud","mcEmailBccService", "$prompt",
          function($scope,MonsterCloud,mcEmailBccService,$prompt) {

            var bccService = mcEmailBccService($scope.server, $scope.whId, $scope.domain)
            bccService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            $scope.confirm = function(alias){
                return $prompt({title: "EMAIL_ALIAS_CONFIRM_TITLE", message: "EMAIL_ALIAS_CONFIRM_MESSAGE", placeholder: "EMAIL_ALIAS_CONFIRM_PLACEHOLDER"})
                  .then(function(code){
                      return bccService.Verify(alias, code)
                  })
            }
            $scope.delete = function(alias){
               return bccService.Remove(alias)
            }


        }]
     }

  }])


  app.directive("mcEmailBccAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-bcc-add'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller:  showErrorsController({
          inject: ["mcEmailBccService","mcEmailAccountService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailBccService, mcEmailAccountService){

            $scope.directions = ["IN", "OUT", "BOTH"]

            $scope.resetForm = function(){
              $scope.bcc = {bc_direction: "BOTH"}
            }

            $scope.bccService = mcEmailBccService($scope.server, $scope.whId, $scope.domain)
            $scope.bccService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            $scope.accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)
            $scope.accountService.Lookup()
              .then(function(lookup){
                 $scope.accountLookup = lookup
              })

            $scope.resetForm()

          },
          fireCallback: function($scope, MonsterCloud, $q){

            var ps = []
            if($scope.bcc.bc_direction == "BOTH") {
              addBcc("IN")
              addBcc("OUT")
            }
            else
              addBcc()

            return $q.all(ps)
              .then(function(){
                 $scope.resetForm()
                 return $scope.bccService.Lookup(true)
              })

            function addBcc(dir){
               var c = angular.copy($scope.bcc)
               if(dir)
                 c.bc_direction = dir
               ps.push($scope.bccService.Add(c, true))
            }

          }

        })

     }

  }])


  app.directive("mcCommonEmailAccountInfo", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'common-email-account-info'),
        scope: { "server":"@", email: "@" },
        controller: ["$scope","mcServerHostname",
          function($scope,mcServerHostname) {
		    mcServerHostname.apply($scope.server, "email", $scope)

            $scope.config = mcConfig.config
          }]
     }
  }])

  app.constant('EMAIL_SERVICE_ACCOUNT_TLD', '.svc');

  app.directive("mcEmailAccountsList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-accounts-list'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller: ["$scope","MonsterCloud","mcEmailAccountService", "mcEmailTask","$element","mcAutoCloseParams", "EMAIL_SERVICE_ACCOUNT_TLD",
          function($scope,MonsterCloud,mcEmailAccountService, mcEmailTask,$element, mcAutoCloseParams, EMAIL_SERVICE_ACCOUNT_TLD) {

            $scope.serviceAccount = $scope.domain.endsWith(EMAIL_SERVICE_ACCOUNT_TLD);

            var accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)
            accountService.onRefresh(function(){
              console.log("onRefresh called")
              var sl = $element.find("#emailMod")
              sl.html("")
            })
            accountService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })
            $scope.delete = function(account){
               var prom = accountService.Remove(account)
               return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_ACCOUNT", prom, mcAutoCloseParams())
                 .then(function(){
                   return accountService.Lookup(true)
                 })
            }

        }]
     }

  }])


  app.directive("mcEmailAccountAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-add'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller:  showErrorsController({
          inject: ["mcEmailAccountService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailAccountService){

            $scope.accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)
            $scope.accountService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            $scope.resetForm = function(){
                  $scope.account = {ea_quota_mb: 250}
                  $scope.tmp = {}
            }

            $scope.resetForm()

          },
          fireCallback: function($scope, MonsterCloud, $q){

            var c = angular.copy($scope.account)

            fixQuota(c)

            return $scope.accountService.Add(c)
               .then(function(){
                  $scope.resetForm()
               })


          }

        })

     }

  }])




  app.directive("mcEmailAccountPassword", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-password'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  "eaId": "@", "eaEmail": "@" },
        controller:  showErrorsController({
          inject: ["mcEmailAccountService", "$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailAccountService){

            $scope.accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)

            $scope.resetForm = function(){
                  $scope.account = {ea_id: $scope.eaId}
                  $scope.tmp = {}
            }

            $scope.resetForm()

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailAccountService, $alertSuccess){

            return $scope.accountService.Change($scope.account)
               .then(function(){
                  $scope.resetForm()
                  return $alertSuccess()
               })


          }

        })

     }

  }])

  app.directive("mcEmailAccountParams", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-params'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  "eaId": "@", "eaEmail": "@" },
        controller:  showErrorsController({
          inject: ["mcEmailAccountService","$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailAccountService){

            $scope.accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)
            $scope.accountService.Get($scope.eaId)
              .then(function(account){
                 account.ea_quota_mb = account.ea_quota_bytes / 1024 / 1024;

                 $scope.showSpamTag2 = account.ea_spam_tag2_level ? true : false;
                 $scope.showSpamKill = account.ea_spam_kill_level ? true : false;

                 $scope.account = account

              })

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailAccountService, $alertSuccess){

            var d = {};
            Array("ea_id", "ea_email", "ea_autoexpunge_days", "ea_quota_mb").forEach(function(x){
                d[x] = $scope.account[x];
            });

            d.ea_spam_tag2_level = $scope.showSpamTag2 ? parseFloat($scope.account.ea_spam_tag2_level || 0) : 0;
            d.ea_spam_kill_level = $scope.showSpamKill ? parseFloat($scope.account.ea_spam_kill_level || 0) : 0;
            
            fixQuota(d);

            return $scope.accountService.Change(d)
               .then(function(){
                  return $alertSuccess()
               })


          }

        })

     }

  }])



  app.directive("mcEmailAccountCleanup", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-cleanup'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  "eaId": "@", "eaEmail": "@" },
        controller:  showErrorsController({
          inject: ["mcEmailAccountService", "mcEmailTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailAccountService){

            $scope.accountService = mcEmailAccountService($scope.server, $scope.whId, $scope.domain)
            $scope.accountService.Get($scope.eaId)
              .then(function(account){
                 $scope.account = account
              })

          },
          fireCallback: function($scope, MonsterCloud, $q, mcEmailAccountService, mcEmailTask){

            return mcEmailTask($scope.server, "EMAIL_TASK_CLEANUP", $scope.accountService.Post($scope.eaId, "cleanup", {older_than_days:$scope.account.ea_autoexpunge_days}))

          }

        })

     }

  }])

  app.service("mcFindEmailId", ["MonsterCloud", "$q", "mcEmailUrl", function(MonsterCloud, $q, mcEmailUrl){
     var cache = {};
     return function(email, server, whId, domain, idHint) {

        if(!cache[email]) {

          if(!idHint) {
            cache[email] = MonsterCloud.get(mcEmailUrl(server, whId, domain, "accounts"))
              .then(function(are){                 
                 var id;
                 are.accounts.some(function(account){
                    if(account.ea_email == email) {
                       id = account.ea_id;
                       return true;
                    }
                 })

                 cache[email] = $q.when(id);
                 return id;
              });

          } else {
            cache[email] = $q.when(idHint);
          }

        }

        console.log("foo!", cache)

        return cache[email];
     }
  }])


  app.directive("mcEmailAccountCluebringerSessions", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-cluebringer-sessions'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  "eaId": "@", "eaEmail": "@" },
        controller: ["$scope","MonsterCloud","mcEmailUrl","mcFindEmailId",
          function($scope,MonsterCloud,mcEmailUrl,mcFindEmailId) {

            $scope.fields = Array("Helo","Sender","Size","Recipients","Instance","QueueID","Timestamp","ClientAddress","ClientName","ClientReverseName","Protocol","EncryptionProtocol","EncryptionCipher","EncryptionKeySize","SASLMethod","SASLSender","SASLUsername");
            return mcFindEmailId($scope.eaEmail, $scope.server, $scope.whId, $scope.domain, $scope.eaId)
              .then(function(eaId){
                 var url = mcEmailUrl($scope.server, $scope.whId, $scope.domain, "account/"+eaId+"/cluebringer/sessions")

                  return MonsterCloud.get(url)
                    .then(function(d){
                       d.forEach(function(row){
                          row.Recipients = row.Recipients.join(", ")
                       })
                       $scope.sessions = d;
                    })                 

              })

        }]
     }

  }])

  app.directive("mcEmailAccountCluebringerQuotas", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-account-cluebringer-quotas'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  "eaId": "@", "eaEmail": "@" },
        controller: ["$scope","MonsterCloud","mcEmailUrl","mcFindEmailId",
          function($scope,MonsterCloud,mcEmailUrl,mcFindEmailId) {

            $scope.fields = Array("LastUpdateHuman","Counter","Type","CounterLimit","Comment")
            return mcFindEmailId($scope.eaEmail, $scope.server, $scope.whId, $scope.domain, $scope.eaId)
              .then(function(eaId){
                  var url = mcEmailUrl($scope.server, $scope.whId, $scope.domain, "account/"+eaId+"/cluebringer/quotas")

                  return MonsterCloud.get(url)

              })
              .then(function(data){
                 $scope.quotas = data
              })


        }]
     }

  }])

  app.directive("mcEmailDomainsList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-domains-list'),
        scope: {"server":"@","whId":"@"},
        controller: ["$scope","MonsterCloud","mcEmailDomainService","mcEmailTask", "$element",
          function($scope,MonsterCloud,mcEmailDomainService, mcEmailTask, $element) {

            var domainService = mcEmailDomainService($scope.server, $scope.whId)
            domainService.onRefresh(function(){
              console.log("onRefresh called")
              var sl = $element.find("#domainMod")
              sl.html("")
            })
            domainService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            function getDomain(domain){
               var d = {}
               d.do_id = domain.do_domain_name
               return d
            }
            $scope.doActivate=function(domain, newState){
               var d = getDomain(domain)
               d.do_active = newState
               return domainService.Change(d, true)
            }

            $scope.delete=function(domain){
               var prom = domainService.Remove(getDomain(domain))
               return mcEmailTask($scope.server, "EMAIL_TASK_REMOVE_DOMAIN", prom)
                 .then(function(){
                    return domainService.Lookup(true)
                 })
            }

        }]
     }

  }])


  app.directive("mcEmailDomainAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-domain-add'),
        scope: { "server":"@", "whId":"@", },
        controller:  showErrorsController({
          inject: ["mcEmailDomainService","mcAccountDomains"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcEmailDomainService,mcAccountDomains){

            $scope.domainService = mcEmailDomainService($scope.server, $scope.whId)
            $scope.domainService.Lookup()
              .then(function(lookup){
                 $scope.lookup = lookup
              })

            mcAccountDomains().then(function(d){
                $scope.domains = d
            })

            $scope.resetForm = function(){
                  $scope.domain = {do_active: true}
            }

            $scope.resetForm()

          },
          fireCallback: function($scope, MonsterCloud, $q){

            return $scope.domainService.Add($scope.domain)
               .then(function(){
                  $scope.resetForm()
               })


          }

        })

     }

  }])


  app.directive("mcEmailSummary", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'email-summary'),
        controller:  ["$scope", "MonsterCloud", "emailAccountFixture", "mcRoutesService", function($scope, MonsterCloud, emailAccountFixture, mcRoutesService){
            function addControl(name, domain){

                return mcRoutesService.jumpToState({
                  stateName:"mc-email-"+name+"-list-and-add",
                  stateParams: {
                     server: $scope.server,
                     whId: $scope.whId,
                     domain: domain.do_domain_name,
                     display: domain.do_domain_name
                  }
                })

            }

            $scope.doAccounts = function(domain){
              addControl("account", domain)
            }
            $scope.doAliases = function(domain){
              addControl("alias", domain)
            }
            $scope.doBccs = function(domain){
              addControl("bcc", domain)
            }

            return MonsterCloud.post("/s/"+$scope.server+"/email/webhostings/list",{webhosting:$scope.whId})
               .then(function(result){
                  Object.keys(result.accounts).forEach(function(whId){
                      Object.keys(result.accounts[whId]).forEach(function(domain){
                          result.accounts[whId][domain].forEach(function(ea){
                            emailAccountFixture(ea)
                          })
                      })
                  })
                  $scope.result = result
               })

        }]

     }

  }], {
       title:"E-mail summary",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@","whId":"@"},
  })

  app.directive("mcAmavisdDkim", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"amavisd-dkim"),
        controller: ["$scope", "MonsterCloud", "$q", "$alertSuccess", "$alertError", "mcConfig",
          function($scope, MonsterCloud, $q, $alertSuccess, $alertError, mcConfig){
            $scope.config = mcConfig.config;

            var mainUrl = "/wizard/dkim/"+$scope.server+"/"+$scope.domain;
            $scope.refetch = function(){
               return MonsterCloud.get(mainUrl)
                 .then(function(d){
                    $scope.dkim = d;
                 })
            }

            $scope.activate = function(){
               return MonsterCloud.put(mainUrl)
                 .then(function(details){
                     if(details.dnsSuccess) {
                        return $alertSuccess();
                     } else {
                        return $alertError({message:'The domain key has been added, but we were unable to reconfigure the DNS zone. Please take care of it manually.'})
                     }
                 })
                 .then(function(){
                    return $scope.refetch();
                 })

            }

            $scope.deactivate = function(){
               var deleteSucceeded = false;
               return MonsterCloud.delete(mainUrl)
                 .then(function(details){
                     if(details.dnsSuccess) {
                        return $alertSuccess();
                     } else {
                        return $alertError({message:'The domain key has been removed, but we were unable to reconfigure the DNS zone. Please take care of it manually.'})
                     }
                 })
                 .then(function(){
                    return $scope.refetch();
                 })
            }

            $scope.refetch();
        }]

    }
  }], {
     title: "E-mail domain keys (DKIM)",
     paramsRequired:{ "server": "@", "domain": "@", "whId": "@" },
     paramsOptional:{ "display": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix", "amavisd", "dkim", "signature", "selector"],
  });


  app.directive("mcPostfixMailq", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,"postfix-mailq"),
        controller: ["$scope", "MonsterCloud", "$q", function($scope, MonsterCloud, $q){
               $scope.fields = ["ts","mail_id","sender","recipient","size"]
               $scope.lookup = {};

               $scope.refetch = function(){
                 var url = "/s/"+$scope.server+"/email/postfix/mailq/by/";
                 if($scope.domain)
                   url += "domain/"+$scope.domain;
                 else
                 if($scope.whId)
                   url += "webhosting/"+$scope.whId;
                 
                 var p;
                 if(url)
                   p = MonsterCloud.get(url);
                 else
                   p = $q.when([]);

                 
                 return p.then(function(data){
                        $scope.lookup.result = data
                   })
               }

               return $scope.refetch();

        }]

    }
  }], {
     title: "E-mail queue",
     paramsRequired:{ "server": "@" },
     paramsOptional:{ "domain": "@", "whId": "@" },
     menuKey: templateDirectory,
     keywords: ["postfix", "mailq"],
  });

  function fixQuota(account){
            account.ea_quota_bytes = account.ea_quota_mb * 1024 * 1024
            delete account.ea_quota_mb
  }


})(app)
