(function(){
var app = angular.module("app", [
   "ui.bootstrap.showErrors",
   "angular-confirm",
   "ui.bootstrap.tpls",
   'ui.bootstrap.modal',
   'ui.bootstrap',
   "mc-sessionlib",
   "mc-lng",
   "mc-config",
   "mc-routes",
   "mc-script",
   "smart-table",
])//, []
window.app = app

function endsWithFn(baseStr, suffixStr){
   return baseStr.indexOf(suffixStr, baseStr.length - suffixStr.length) !== -1;
}

if(!String.prototype.endsWith) {
  String.prototype.endsWith = function(suffix) {
      return endsWithFn(this, suffix);
  };
}

var templateDirectory = "common"

app.service("mcAutoCloseParams", ["mcConfig", function(mcConfig){
   return function(extras){
      return angular.extend({autoClose: true, dontDisplayUntilMs: mcConfig.config.tasksDontDisplayUntilMs}, extras)
   }
}])

app.factory("mcCachedLookupProvider", ["$q", function($q){
   var promCache = {};
   var resultCache = {};
   var n = function(promiseProvider, instance){
      if(!instance) instance = "main";
      if(resultCache[instance]) return returnWithResult()
      if(!promCache[instance])
         promCache[instance] = promiseProvider()
           .then(function(data){
               promCache[instance] = null
               resultCache[instance] = data
               return returnWithResult()
           })

      return promCache[instance]

      function returnWithResult(){
          return $q.when(angular.copy(resultCache[instance]))
      }
   }
   n.Reset = function(){
      resultCache = {}
   }
   var gCounter = 0;
   n.Factory = function(callback){
     gCounter++;
     var fCounter = gCounter;
     var re = function(){
       var i = n(callback, "factory"+fCounter);
       i.Reset = n.Reset;
       return i;
     }
     re.Reset = n.Reset;
     return re;
   }
   return n
}])


app.service("formatBytes", function(){
    return function(bytes,decimals) {
       if(!bytes) return '0';
       var k = 1024,
           dm = decimals + 1 || 3,
           sizes = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
           i = Math.floor(Math.log(bytes) / Math.log(k));
       return (parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]).trim();
    }
})

  app.directive("mcUserSettingsMenu", ["templateDir", function(templateDir){
      return {
        restrict: "A",
        templateUrl: templateDir(templateDirectory,'user-settings-menu'),
     }

  }])

  app.directive("mcTasksInProgress", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'tasks-in-progress'),
        scope: { "title":"@","taskUrl":"@","tasksGetUrl":"@" },
        controller: ["$task","$scope","MonsterCloud", "$q", function($task, $scope, MonsterCloud, $q){
            $scope.open=function(task_id) {
              $task({title:$scope.title, "url":$scope.taskUrl+"/"+task_id})
            }
            MonsterCloud.get($scope.tasksGetUrl)
              .then(function(tasks){
                  $scope.tasks = tasks
                  $scope.showList = $scope.tasks.length > 0
              })
        }]
     }

  }])

app.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);

app.config(['showErrorsConfigProvider','$compileProvider','$locationProvider',  function(showErrorsConfigProvider, $compileProvider, $locationProvider) {
     showErrorsConfigProvider.showSuccess(true);
     $locationProvider.html5Mode({
      enabled:true,
      requireBase:false, // this one was needed for the location parsing for some reason
      rewriteLinks: false, // this is to keep the old A tags working
    });

	 // leave the comment in following line untouched as it will be processed by Grunt:
	 //ENABLE THIS ONE IN PRODUCTION $compileProvider.debugInfoEnabled(false);
}]);

  app.directive('validUrl', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.validUrl = function(modelValue, viewValue) {

          if(!viewValue) return true

          var re =  viewValue.match(/^https?:\/\/(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])(\/[a-zA-Z0-9_\-\.\?=%\/]*)*$/) ? true : false;

           // console.log("hey", modelValue, viewValue, re)

          return re
        };
      }
    };
  });

  app.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
  }]);

  app.directive('emailAppend', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('keyup', function (e) {
               if(e.key != "@") return;
               var f = element.val();
               if(!f.match(/@$/)) return;
               var count = (f.match(/@/g) || []).length;
               if(count != 1) return; // prevent foobar@domain.hu@ to append domain once again
               element.val(f+attrs.emailAppend);
            });
        }
    };
  }]);

  app.directive('validJson', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.validJson = function(modelValue, viewValue) {
          // console.log("hey json", modelValue, viewValue, ctrl)

          if(!viewValue) return true

          try{
            JSON.parse(viewValue);
            return true;
          }catch(ex){
            return false;
          }

        };
      }
    };
  });
  app.directive('noMatch', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.noMatch = function(modelValue, viewValue) {
          // console.log("hey", modelValue, viewValue, attrs.endsWith, ctrl)

          if(!viewValue) return true;

          var what = attrs.noMatch;
          return (viewValue.toLowerCase() != what.toLowerCase())

        };
      }
    };
  });

  app.directive('validSshKeys', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.validSshKeys = function(modelValue, viewValue) {

          if(!viewValue) return true
          var lines = viewValue.split("\n");
          var goods = 0;
          var bads = 0;
          lines.some(function(line){
             line = line.trim();
             if(!line) return;
             if(!line.match(/^ssh-[a-z0-9]+\s+[A-Za-z0-9+/]+\s+[a-zA-Z0-9\-\.@]+$/))  {
                bads++;
                return true;
             } else {
                goods++;
             }
          })

          return ((goods > 0) && (bads == 0));
        };
      }
    };
  });
  app.directive('endsWith', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.endsWith = function(modelValue, viewValue) {
          // console.log("hey", modelValue, viewValue, attrs.endsWith, ctrl)

          if(!viewValue) return true

          var sep = attrs.endsWithSeparator
          var shouldEndWith = attrs.endsWith
          if(sep) {
              if(viewValue == shouldEndWith)
                return true
              shouldEndWith = sep + shouldEndWith
          }

          return endsWithFn(viewValue, shouldEndWith);
        };
      }
    };
  });
  function regexpConstraint(name, regexp) {
    app.directive(name, function() {
        return {
          require: 'ngModel',
          link: function(scope, elm, attrs, ctrl) {
            ctrl.$validators[name] = function(modelValue, viewValue) {

              if(!viewValue) return true

              var re = viewValue.match(regexp) != null
              return re
            };
          }
        };
      } )
  }
  regexpConstraint('strictString',/^[a-zA-Z0-9]+$/);

  regexpConstraint('dbString', /^[a-z0-9_]{2,16}$/);

  app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "=",
            filename: "=",
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var file = changeEvent.target.files[0]
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                        // console.log("FOOO", typeof scope.filename, scope.filename, attributes.filename)
                        if(attributes.filename)
                          scope.filename = file.name
                    });
                }
                reader.readAsArrayBuffer(file);
            });
        }
    }
  }]);

  app.service("mcFilesService", ["$q", function($q){
     return function(rawFilesEvent) {

              var files = Array.from(rawFilesEvent)
              files.forEach(function(file){
                  var reader = new FileReader();
                  file.readAsArrayBuffer = function(){
                      return $q(function(resolve, reject) {
                        reader.onload = function (loadEvent) {
                            resolve(loadEvent.target.result)
                        }
                        reader.readAsArrayBuffer(file);
                      })
                  }
              })

              return files
     }
  }])

  app.directive("mcFiles", ["mcFilesService",function (mcFilesService) {
    return {
        restrict: "A",
        scope: {mcFilesConsumer:"&", mcFilesRaw:"&",mcFilesMultiple:"@",mcFilesParams:"@"},
        transclude:true,
        // template: "<ng-transclude class='original'></ng-transclude><div class='input-here hidden'></div>",
        replace: true,
        transclude: true,
        template: function(tElement, tAttrs) {
            var str = '<div><div ng-click="onHandleClick()" ng-transclude></div><div class="input-here hidden"></div></div>'
            // console.log(str)
            return str;
        },
        link: function (scope, element, attributes) {
           scope.onHandleClick = function(){
              console.log("its pretty much clicked")
              f.click()
           }

           var e = element.find(".input-here")
           var str = "<input class='file-input' type='file' "+(scope.mcFilesParams||"")+" >"
           e.html(str)

           var f = element.find(".file-input")
           f.bind("change", function (changeEvent) {
              var files = mcFilesService(changeEvent.target.files)

              if(attributes.mcFilesRaw)
                 scope.mcFilesRaw({files:files})

              var consumerIndex = 0
              if(attributes.mcFilesConsumer)
                doConsumer()

              function doConsumer(){
                // console.log("here",consumerIndex )
                if(consumerIndex >= files.length) return

                var file = files[consumerIndex++]
                file.readAsArrayBuffer().then(function(data){
                   var p = scope.mcFilesConsumer({file:file, data:data})
                   if((!p)||(!p.then))
                     return doConsumer()

                   p.then(function(){
                      doConsumer()
                   })
                })

              }

           });
        }

    }
  }]);



app.directive('mcSorter', ["templateDir",function (templateDir) {
   return {
     restrict: "A",
     transclude: true,
     templateUrl: templateDir(templateDirectory,"sorter"),
   }
}])

app.directive('mcSorterLink', ['$compile',function ($compile) {
   return {
     restrict: "A",
     link: function (scope, element, attrs, ctrl) {

        var p = element.parent();
        var n = p.attr("mc-sorter")
        attrs.$set('ngClick', "sort('"+n+"', "+attrs.mcSorterLink+")");
        element.removeAttr('mc-sorter-link');
        $compile(element)(scope);
     }

   }
}])
app.directive('showTail', function () {
    return function (scope, elem, attr) {
        scope.$watch(function () {
            return elem[0].value;
        },
        function (e) {
            elem[0].scrollTop = elem[0].scrollHeight;
        });
    }

});

app.directive('mcTypicalFilter', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     // shared scope!
     templateUrl: templateDir(templateDirectory,"typical-filter"),
   }
}])

app.directive('mcTypicalPagination', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     // shared scope!
     templateUrl: templateDir(templateDirectory,"typical-pagination"),
   }
}])

app.directive('mcPagination', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     scope: {
      "itemsPerPage": "=",
      "totalItems": "=",
      "ngModel": "=",
      "ngChange": "&",
     },
     templateUrl: templateDir(templateDirectory,"pagination"),
     controller: ["$scope", function($scope){
        $scope.$watch("ngModel",function(newValue, oldValue){
           if(newValue!==oldValue)
             $scope.$eval($scope.ngChange)
        })

        if(!$scope.itemsPerPage) $scope.itemsPerPage = 10
        if(!$scope.ngModel) $scope.ngModel = 1
     }]
   }

}]);

app.directive('mcDebug', ["templateDir",function (templateDir) {

   return {
     restrict: "E",
     scope: {
      "model": "=",
      "force": "@",
     },
     templateUrl: templateDir(templateDirectory,"debug"),
     controller: ["$scope", "mcConfig", function($scope, mcConfig){
        $scope.dev_mode = mcConfig.config.dev_mode || $scope.force
     }]
   }

}])


app.directive('mcKeyEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.mcKeyEnter);
                });

                event.preventDefault();
            }
        });
    };
});


app.service("captchaService", ["MonsterCloud", function(MonsterCloud) {
    return MonsterCloud.get("/pub/captcha/expected")
}])



app.controller('ExceptionModalController', ["$scope", "$uibModalInstance", "data", function ($scope, $uibModalInstance, data) {
    $scope.data = angular.copy(data);

    $scope.ok = function (closeMessage) {
      $uibModalInstance.close(closeMessage);
    };

  }])
app.factory('showException', ["$uibModal", "templateDir", function ($uibModal, templateDir) {
    return function (data, settings) {
      var defaults = {
          templateUrl: templateDir(templateDirectory,"exception"),
          controller: 'ExceptionModalController',
      }

      settings = angular.extend(defaults, (settings || {}))

      // this line is important else the data details wont show up
      data = {"message": data.message, "params": data.params }

      data.paramKeys = Object.keys(data.params || {})
      //params: { u_name: [ 'can\'t be blank' ] },

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings);
    };
  }])

app.directive("mcConfigHref", ["mcConfig", "MonsterCloud", function(mcConfig, MonsterCloud){
   return {
     link: function(scope, elem, attrs) {
	    var key = attrs.mcConfigHref
	    elem.attr("href", MonsterCloud.appendUidi(mcConfig.config[key]))
	 }
   }
}])

app.directive('type', ["$parse",'$timeout', function($parse,$timeout) {
  return {
    restrict: "A",
    require: '^?form',//
    link: function($scope, elem, attrs, ctrl) {

        if (elem.prop('nodeName') !== 'INPUT' || attrs.type !== 'password') return
        if((attrs.cleanpassword)||(attrs.cleanpassword==="")) return

        var pf = $(elem).passField({
             pattern: "ABCDefgh1234",
             showTip: false,
        });
        $(elem).on("pass:generated", function(e, pass) {

          // console.log("password generated:",pass, attrs.ngModel, ctrl)

          $timeout(function(){

             if((ctrl)&&(attrs.name)&&(ctrl[attrs.name])) {
                var field = ctrl[attrs.name]
                // console.log("we have got a form field!", field)

                field.$setViewValue(pass)
                field.$render()
              } else if(!attrs.ngModel) {
                   // else we just try to update model
                   var getter = $parse(attrs.ngModel);
                   console.log("getter:",getter)
                   getter.assign($scope, pass);
              }
          })

        });

        var name = attrs.name;
        if(name){
            var actrl = ctrl[name];
            actrl.$validators.passfield = function(modelValue, viewValue) {

              if(!viewValue) return true

              return pf.validatePass();
            };
        }


    }
  }
}]);


app.directive("sameAs", function(){
  return {
      require: 'ngModel',
      "link": function(scope, elem, attrs, ngModel) {
          ngModel.$parsers.unshift(validate);

          // Force-trigger the parsing pipeline.
          scope.$watch(attrs.sameAs, function() {
              ngModel.$setViewValue(ngModel.$viewValue);
          });

          function validate(value) {
              var isValid = scope.$eval(attrs.sameAs) == value;

              ngModel.$setValidity('same-as', isValid);

              return isValid ? value : undefined;
          }

      }
  }
})
app.directive("metisMenu", function(){

    return {
      restrict: "A",
      link: function(scope,elem) {
          // console.log("calling metismenu")

          var $this = elem;

          $this.parent().find("li.active").has("ul").children("ul").addClass("collapse in");
          $this.parent().find("li").not(".active").has("ul").children("ul").addClass("collapse");

          var i = 0;
          $this.find("li").has("ul").children("a").on("click", clickHandler);
          $this.has("ul").children("a").on("click", clickHandler);

          function clickHandler(e) {
              // console.log("click handler "+i); i++;
              e.preventDefault();

              $(this).parent("li").toggleClass("active").children("ul").collapse("toggle");

              $(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide");

          }

      }
    };

});


app.directive("mcDashboard", ["templateDir",function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dashboard'),
      controller:  ["MonsterCloud","$scope", "mcConfig","serversFactory",function(MonsterCloud,$scope,mcConfig,serversFactory){
          $scope.certPurchaseServer = mcConfig.config.certPurchaseServer;
          serversFactory.all().then(function(servers){
            $scope.serversCount = servers.length;
          })
          
      }]
    };
}],{
   title: "Dashboard"
});

app.directive("sb2Root", ["templateDir",function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'sb2-root'),
      controller:  ["MonsterCloud","$scope", function(MonsterCloud,$scope){
          $scope.UserProfile = MonsterCloud.UserProfile;
      }]
    };
}]);
app.factory('UserProfileFactory', [function(){

  var re = {} // this is here outside purposefully

  var ore = function(auth) {
      angular.copy(auth, re)
      re.isLoggedIn =  re.token ? true : false
      re.setAuth = ore
      re.isSuperuser = re.s_grants ? re.s_grants.indexOf("SUPERUSER") > -1 : false;
      re.isServerOwner = re.s_grants ? re.s_grants.indexOf("SERVER_OWNER") > -1 : false;

      return re
  }

  return ore
}])

app.run(["mcSessionCookies","mcRoutesService", "mcConfig", function(mcSessionCookies, mcRoutesService){

    mcRoutesService.addExtraParams({uidi: mcSessionCookies.getUidi()})
  
}])

app.service("mcUserSession", ["mcSessionCookies", function(mcSessionCookies){


   var lib = {};

   lib.GetTokenArray = function(){
      return mcSessionCookies.GetTokenArray();
   }

   lib.hasRole = function(role) {
       // console.log("mcUserSession: hasRole?", role)
       var t = mcSessionCookies.getCurrent();
       console.log("mcUserSession: hasRole", role, t)
       if((t)&&(t.token)&&(t.s_grants)&&(t.s_grants.indexOf(role) > -1)) {
          return true;
       }
       return false;
   }

   lib.hasAnyRole = function(value){
      console.log("mcUserSession: hasAnyRole?", value);
      var rolesWanted = value.split(" ");
      var re = false;
      rolesWanted.some(function(role){
        if(lib.hasRole(role)) {
           re = true;
           return true;
        }
      })
      return re;
    }

    lib.findUidiForUserId = function(user_id) {

      console.log("mcUserSession: findUidiForUserId?", user_id)
      var t = lib.GetTokenArray()
      var found;
      t.some(function(e){
         if((e.token)&&(e.u_id == user_id)) {
            found = e;
            return true;
         }
      });
      return found ? found.uidi : undefined;
    }
    lib.findUidiForRole = function(role) {

      var t = lib.GetTokenArray();
      console.log("mcUserSession: findUidiForRole", role, t)
      var found;
      t.some(function(e){
         if((e.token)&&(e.s_grants)&&(e.s_grants.indexOf(role) > -1)) {
            found = e;
            return true;
         }
      });
      return found ? found.uidi : undefined;
    }

    return lib;

}])

app.factory('MonsterCloud', ["$http", "$window", "mcRoutesService", 'UserProfileFactory', 'mcConfig', 'showException', '$q', 'mcSessionCookies', '$rootScope', '$timeout', '$alertError',
                     function($http,   $window,   mcRoutesService,   UserProfileFactory,   mcConfig,   showException,   $q,   mcSessionCookies,   $rootScope, $timeout, $alertError){


  var UserProfile
  var session_lib = mcSessionCookies;

  UserProfile = UserProfileFactory(session_lib.GetCurrentTokenHash())

  // console.log("hey", UserProfile)

  function getBasePart(url) {
     var i = url.indexOf("?")

     var re = (i >= 0) ? url.substring(0, i) : url
     return re
  }

  function doLogoutToken(token){
     var p;
     if(!token)
        p = $q.when({});
     else
        p = re.doRequest({method:"POST", "url":"/logout", token: token, logoutRequest: true, data:{}})
              .catch(function(ex){ console.error("error while logging out (might be safe)", ex, token); })         

     return p.then(function(data){
            return fetch_if_defined(mcConfig.config.logout_hook, {noUriTransform: true, rawResult: true})
        })        
        .then(function(){
           // and removing the cookie locally
           var n = session_lib.getByToken(token);
           if(n) n.remove();
        })

  }

  var requestsSoFar = 0
  var re = {
    UserProfile: UserProfile,

    clearSessions: function(){
       session_lib.clearAll();
       UserProfile.setAuth({});
    },

    appendUriParams: function(page, params) {
       var sep = (page.indexOf("?") !== -1) ? "&" : "?"
       return  page + sep + params
    },

     frontPageByUidi: function(uid_index, redirectAfter, addNg){
        return re.appendUriParams(redirectAfter|| mcConfig.config.indexPageName,  (addNg ? "op=ng&" : "") + "uidi=" + uid_index)

    },
     appendUidi: function(url, uidi, addNg){
        return re.frontPageByUidi(typeof uidi != "undefined" ? uidi : re.getCurrentUidi(), url, addNg)

    },
    doLogin: function(cred, redirectAfter, loginPreredirectCallback){

      return re.doRequest({"method": "POST", "url": "/pub/account/authentication", "data": cred, "relayException": true})
        .then(function(d){
            return setReceivedAuthTokenForCurrentWindow(d, redirectAfter, loginPreredirectCallback)
        })
    },

    doReceiveToken: function(prom, redirectAfter) {
           return prom.then(function(d){
                return setReceivedAuthTokenForNewWindow(d, redirectAfter)
            })

    },

     // these two are the expected entry points for triggering logout
    logout: function(){

      // this is the logout callback
      console.log("logout callback called for", UserProfile)

      return doLogoutToken(UserProfile.token)
           .then(function(){

                 // removing this one just for sure
                 session_lib.removeCurrent();

                 redirectTo(mcConfig.config.indexPageName);
           })

    },
    logoutAll: function(){
        console.log("logoutAll initiated")
        var t = session_lib.GetTokenArray();
        var ps = [];
        t.forEach(function(e){
           ps.push(doLogoutToken(e.token));
        })
        return $q.all(ps).then(function(){
           // clearing the sessions just for sure
           session_lib.clearAll();
           redirectTo(mcConfig.config.indexPageName);
        })
    },

    getCurrentUidi: function(){
       return session_lib.getEffectiveUidi()
    },

     getCurrentAccount: function(){
       return re.get("/account")
     },

      doRequest: function(request){

         if(!request.method)
            request.method = request.data ? "POST" : "GET"

         if(request.method=="GET") {
            request.url += (request.url.indexOf("?") > 0 ? "&" :"?") + "rnd="+Math.random();
         }

         //testing:
         // var x = new Error("VALIDATION_ERROR"); x.params = { u_name: [ 'can\'t be blank' ] } ; showException(x)

         if((!request.noUriTransform)&&(request.url.substr(0,1) == '/'))
         {
            var redirected = false
            if((!request.noUriRedirectTransform)&&(typeof $window.mcRedirectHttpRequests === "object"))
            {
                var basePart = getBasePart(request.url)
                if($window.mcRedirectHttpRequests[basePart]) {
                  console.log("redirecting API request:", request.method, request.url)
                  request.url = $window.mcRedirectHttpRequests[basePart]
                  redirected = true
                }
            }

            if((typeof $window.mcMockedHttpResponses === "object")&&($window.mcMockedHttpResponses[request.method])&&(typeof $window.mcMockedHttpResponses[request.method][request.url] != "undefined"))
            {
                console.log("mocking API request:", request.method, request.url, request.data)
                return $q.when($window.mcMockedHttpResponses[request.method][request.url])
            }


            if((!redirected)&&(!request.noUriBaseTransform)) {
              if(mcConfig.config.api_uri_prefix)
                 request.url = mcConfig.config.api_uri_prefix + request.url

              if(mcConfig.config.api_url_base)
                 request.url = mcConfig.config.api_url_base + request.url
            }
         }

        if(!request.headers) request.headers = {}
        if(!request.noToken){
           var token = request.token || UserProfile.token;
           if(token)
              request.headers.MonsterToken = token;
        }


         //return Promise.resolve({"login":false,"forgotten":true,"registration":true})
         console.log("Sending",request.method,"request to API server:", request.url)
         requestsSoFar++

         var p = $http(request)
            .then(function(response){
               return request.rawResult ? response.data : response.data.result; //returning the important part only
            })

           if((!re.dontCatchExceptions)&&(!request.relayException)) {
             p = p.catch(function(ex){
                  var exMessage = "OTHER"
                  dontShowPopup = false
                  if((ex.status == -1)&&(ex.data === null)) dontShowPopup = true; // http request was aborted by the browser
                  console.error("exception",ex)
                  if((ex.data)&&(ex.data.result)&&(ex.data.result.error)&&(ex.data.result.error.message)) {
                     ex = ex.data.result.error
                     exMessage = ex.message
                  }

                  var wasCatpchaException = (exMessage == "CAPTCHA_MISMATCH")
                  if(wasCatpchaException) {
                     $alertError({message:'The security code you entered was invalid.'})
                     $rootScope.$broadcast("onCaptchaMismatch", wasCatpchaException)
                  }

                  if((!wasCatpchaException)&&(!dontShowPopup)) {

                     var tokenError = (exMessage.match(/^(MISSING_TOKEN|INVALID_TOKEN_OR_SESSION_EXPIRED|SESSION_EXPIRED|AUTH_FAILED)$/));

                     if(!tokenError) {
                        showException(ex);

                        // break the promise
                        throw ex;
                     }


                     if(mcConfig.config.session_lost_uri)
                        redirectTo(mcConfig.config.session_lost_uri)
                     else
                     if(!request.logoutRequest)
                        re.logout();

                  }


                  throw ex // we rethrow it so the original callback
             })
           }

           return p

      },

      get: function(uri){
        return re.doRequest({"method": "GET", url: uri})
      }
  }

    $.each(["post","put","delete","search"], function(k,q){
       re[q] = function(uri, data) {
         return re.doRequest({"method":q.toUpperCase(), "url": uri, "data": data})
       }

    })

    $timeout(function(){
       if(requestsSoFar > 1) return; // the limit is 1 because of the language json request which happens anyway
       if(!UserProfile.token) return; // filtering by existence of the token here, as it might be cleared by the legacy sync stuff

       console.log("No activity so far, sending ping")
       re.get("/ping")
    }, mcConfig.config.send_ping_after_inactivity || 60000)

  return re

  function redirectTo(page){
      $window.location.href= page
  }
  function openPage(page){
      $window.open( page )
  }

  function checkReceivedAuthToken(data, redirectAfter, addNg){
      if(!data.token) throw new Error("No token received, authentication failed")

      var new_uidi = session_lib.AddTokenHash(data)
      var frontPage = re.frontPageByUidi(new_uidi, redirectAfter || mcConfig.config.redirect_after_login, addNg)
      var x = { "index": new_uidi, "page": frontPage }
      console.log("new token hash added", data, new_uidi, x)
      return x
  }

  function commonAuthToken(data, p, loginPreredirectCallback){
        return fetch_if_defined(mcConfig.config.login_hook,
          {noUriTransform: true, rawResult: true, noToken: true, headers: {"MonsterUidi": p.index, "MonsterToken": data.token}})
         .then(function(){
             if(!loginPreredirectCallback) 
                return;

             return loginPreredirectCallback()
          })

  }

  function setReceivedAuthTokenForCurrentWindow(data, redirectAfter, loginPreredirectCallback) {


        var p = checkReceivedAuthToken(data, redirectAfter, mcConfig.config.pureNg )
        session_lib.forceIndex(p.index)
        UserProfile.setAuth(data)
        UserProfile.justLoggedIn = true

        return commonAuthToken(data,p,loginPreredirectCallback)
          .then(function(){
             if(!mcConfig.config.pureNg) 
                return redirectTo( p.page );

             console.log("setReceievedAuthToken", p, data);
             mcRoutesService.setSearch({"uidi": p.index});
             delete UserProfile.justLoggedIn;
             return;               
          })

    }
  function setReceivedAuthTokenForNewWindow(data, redirectAfter) {
        var p = checkReceivedAuthToken(data, redirectAfter)
        return commonAuthToken(data,p)
          .then(function(){
             return openPage( p.page )
          })
    }

    function fetch_if_defined(k, additional) {
        var req = angular.extend({"method":"POST", url: k, data: {}}, additional)
        return (!k) ? $q.when() : re.doRequest(req)
    }

}])

app.directive('mcLinkLogout', ["MonsterCloud",function(MonsterCloud) {
    return {
      restrict: "A",
      link: function(scope,elem) {
         elem.bind("click", function (event) {
            MonsterCloud.logout()
         })
      }
    };
}])
app.directive('mcLinkLogoutAll', ["MonsterCloud",function(MonsterCloud) {
    return {
      restrict: "A",
      link: function(scope,elem) {
         elem.bind("click", function (event) {
            MonsterCloud.logoutAll()
         })
      }
    };
}])

app.directive('mcLogout', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'logout'),
      scope: { },
      controller:  ["MonsterCloud", "$scope", function(MonsterCloud, $scope) {
            $scope.logout = MonsterCloud.logout
            $scope.logoutAll = MonsterCloud.logoutAll
      }]
    };
  }])


Array(
{
   n: "mcSb2Login",
   t: "sb2-login"
},
{
   n: "mcLogin",
   t: "login"
}).forEach(function(cat){
   app.directive(cat.n, ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,cat.t),
      scope: { "redirectAfter": "@" },
      controller:  showErrorsController({
         relayUserProfile: true,
         fireFunctionName: "login",
         dataName: "cred",
         dataFormName: "loginForm",
         isWorkingName: "isLoginInProgress",

         inject:["$alertError","mcConfig"],

         additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertError, mcConfig){
            if($scope.$parent.setClientHeight)
               $scope.$parent.setClientHeight()
            $scope.config = mcConfig.config
            $scope.cred = {}
            $scope.logout = MonsterCloud.logout
            $scope.logoutAll = MonsterCloud.logoutAll
            $scope.totpMode = false;

            $scope.totpAttempts = 0;

            $scope.cachedUsernamePassword = {};

            $scope.sendTotp = function(){
               if(!$scope.t.userToken) return;

               $scope.totpAttempts++;
               var data = angular.extend({}, $scope.t, $scope.cachedUsernamePassword);
               return $scope.doSendLogin(data);
            }

            $scope.cancelTotp = function(){
               $scope.totpMode = false;
               $scope.cred.userToken = "";
            }

            $scope.doSendLogin = function(data){
              return MonsterCloud.doLogin(data, $scope.redirectAfter)
               .catch(function(ex) {
                   console.error("Login failed", ex);

                   if($scope.totpAttempts >= 3) {
                      $scope.totpMode = false;
                   }

                   if((ex.data)&&(ex.data.result)&&(ex.data.result.error)&&(ex.data.result.error.message== "TOTP_REQUIRED")) {
                      angular.extend($scope.cachedUsernamePassword, $scope.cred);
                      $scope.totpMode = true;
                      return;
                   }

                   $alertError({message:'Login failed'})
               })

            }
         },

         fireCallback: function($scope, MonsterCloud,$q,$alertError){

             return $scope.doSendLogin($scope.cred);
         }
      })


    };
  }], {
     title: "Login"
  })
});




app.directive('mcLoginOrInProgress', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'login-or-in-progress'),
      scope: { "redirectAfter": "@", "legacyIntegration":"@", "legacyIntegrationUid":"@" },
      controller:  showErrorsController({
         inject: ["$element", "mcSessionCookies"],
         relayUserProfile: true,
         additionalInitializationCallback: function($scope, MonsterCloud,$q, $element, mcSessionCookies){
            $scope.setClientHeight = function(){
               var x = $element.find(".mcLoginOrInProgress")
               var h = x[0].clientHeight
               console.log("!!!! this is the mcLoginOrInProgress init callback", x, h, $element)
               x = $element.find(".mcLoginProgressDiv")
               x[0].setAttribute("style", "height:"+h+"px")

            }

            if($scope.legacyIntegration) {
               if(($scope.legacyIntegrationUid)&&($scope.legacyIntegrationUid != "0")) {
                  // the user is logged in the legacy system, lets see if we have any cookies here
                  var n = mcSessionCookies.getCurrent();
                  if((!n)||(""+n.u_id != ""+$scope.legacyIntegrationUid)) {
                     console.error("mcLoginOrInProgress: Legacy system and mc are out of sync! Logging out.", n, $scope.legacyIntegrationUid);
                     // out-of-sync! the legacy system needs to be logged out
                     MonsterCloud.logout();
                  }
               } else {
                  // the user is not logged in the legacy system, so we clear the sessions here as well
                  console.error("mcLoginOrInProgress: the user is not logged in the legacy system, sweeping out the cookies from mc");
                  MonsterCloud.clearSessions();
               }
            }

         }
      }),
    };
  }])

app.directive('mcLoginOrLogout', ["templateDir",function(templateDir) {
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'login-or-logout'),
      scope: { "redirectAfter": "@" },
      controller:  showErrorsController({
         relayUserProfile: true,
      })
    };
  }])


// see: https://github.com/angular/angular.js/issues/11495
app.directive('method', function () {
  return {
    priority: 1,   // To run before the default `form` directive
    restrict: 'A',
    compile: function (_, tAttrs) { tAttrs.action = undefined; }
  };
})

// see: https://github.com/angular/angular.js/issues/11495
app.directive('mcRequired', ["$parse",function ($parse) {
  return {
    priority: 1,   // To run before the default `form` directive
    restrict: 'A',
    link: function (scope, tAttrs, elem) {
       var n = tAttrs.mcRequired
       var v = $parse(n)(scope)
       console.log("hello!!!", scope, n, v, tAttrs)
       /*
       tAttrs.$set("required", true)
       $compile(elem)(scope);
       */

    }
  };
}])

app.service("mcFindRole", ["mcUserSession", function(mcUserSession){
   return function(role){
      return mcUserSession.findUidiForRole(role)
   }
}])

app.service("mcFindAnyRole", ["mcUserSession", function(mcUserSession){
   return function(value){
      var rolesWanted = value.split(" ");
      var re;
      rolesWanted.some(function(role){
         var i = mcUserSession.findUidiForRole(role);
         if(typeof i != "undefined"){
           re = i;
           return true;
         }
      })
      return re;
   }
}])

app.directive("mcIfRole", ['ngIfDirective','mcUserSession','mcFindAnyRole',function(ngIfDirective, mcUserSession, mcFindAnyRole){

  var ngIf = ngIfDirective[0];


  return {
    transclude: ngIf.transclude,
    priority: ngIf.priority,
    terminal: ngIf.terminal,
    restrict: ngIf.restrict,
    link: function($scope, $element, $attr) {
      var roles = $attr.mcIfRole;
      var hasRole = roles == "*";
      if(!hasRole){
        var local = $attr.localRole;
        if(local){
          hasRole = mcUserSession.hasAnyRole(roles);
        } else {
          hasRole = typeof mcFindAnyRole(roles) != "undefined";
        }
      }

      $attr.ngIf = function() {
        return hasRole;
      };
      ngIf.link.apply(ngIf, arguments);
    }
  };

}])



app.directive("mcSwitchRole", ["MonsterCloud","mcConfig",'mcFindRole',function(MonsterCloud,mcConfig,mcFindRole){

  return {
    restrict: "A",
    link: function($scope, $element, $attr) {

           var the_uidi = mcFindRole($attr.mcSwitchRole)
           if(typeof the_uidi != "undefined") {
              if($element[0].tagName === "A") {
                 $attr.$set('target',"_blank")
                 var url = MonsterCloud.appendUidi($attr.href || mcConfig.config.indexPageName, the_uidi)
                 $attr.$set('href', url)
              }
           }else{
              $attr.$set("class", "hidden")
           }

    }
  };

}])


app.factory('pagerFactory', ["mcConfig",function(mcConfig) {
   var re = function($scope, options) {
         if(!options) options = {}
         $scope[dataName()] = []
         $scope.pagination = {itemsPerPage: options.itemsPerPage || mcConfig.config.itemsPerPage, currentPage: 1}

          $scope.pager = function(sortOptions) {
               $scope.pagination.offset = ($scope.pagination.currentPage-1)*$scope.pagination.itemsPerPage

               var sqlHelper = angular.extend({limit: $scope.pagination.itemsPerPage, offset: $scope.pagination.offset}, sortOptions||{})

               $scope.pagination.fetchInProgress = true
               return options.query(sqlHelper)
                 .then(function(data){
                    $scope[dataName()] = data.rows

                    $scope.pagination.noResultsWarning = (data.rows.length <= 0)
                    $scope.pagination.fetchInProgress = false

                 })

          }

          $scope.sort = function(new_order, desc) {
             $scope.pager({order: new_order, desc: desc})
          }

          $scope.filter = function(){
             $scope[dataName()] = []
             $scope.pagination.currentPage = 1
             $scope.pagination.fetchInProgress = true

             return options.count()
               .then(function(data){
                      $scope.pagination.totalItems = data.count || 0
                      return $scope.pager()
               })

          }


          return $scope.filter

          function dataName() {
             return options.dataName ? options.dataName : "data"
          }

   }

   return re

}])

  app.directive("mcFeatures", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'features'),
     }

  }], {
     title: "Features",
     menuKey: templateDirectory,
  })


app.factory('pagerFactorySingleShot', ["pagerFactory","$q",function(pagerFactory, $q) {
   return function($scope, options) {
      options = options || {}
      if(!options.shot) throw new Error("shot not specified")
      var fullDataSet
      var filteredDataSet
      var fuse


      function fetchDataOnce(){
        return options.shot()
          .then(function(data){
             fullDataSet = data

             if(options.dataName)
                 $scope[options.dataName+"Ready"] = true

             if(options.fuse) {
                if(options.fuse.castToString)
                    data.forEach(function(d){
                       options.fuse.castToString.forEach(function(x){
                         d[x] = ""+d[x]
                       })
                    })

                var fuseOptions = angular.extend({
                    shouldSort: true,
                    tokenize: true,
                    matchAllTokens: true,
                    threshold: 0.2,
                    location: 0,
                    distance: 100,
                    maxPatternLength: 32,
                    minMatchCharLength: 1,
                }, options.fuse)

                fuse = new Fuse(fullDataSet, fuseOptions);
             }

             return $q.when(fullDataSet)
          })
      }

      options.count = function(){
          var p = fullDataSet ? $q.when(fullDataSet) : fetchDataOnce()

          return p.then(function(data){

                filteredDataSet = fuse && $scope.filterData && $scope.filterData.text ? fuse.search($scope.filterData.text) : fullDataSet

                return $q.when({count:filteredDataSet.length})

          })

      }
      options.query = function(sqlHelper) {
         var rows = filteredDataSet.slice(sqlHelper.offset, sqlHelper.offset+$scope.pagination.itemsPerPage)
         return $q.when({rows: rows})
      }

      return pagerFactory($scope, options)
   }

}])

app.directive('scope', function($sce) {
  function syntaxHighlight(json) {
    json = JSON.stringify(json, undefined, 2);
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
  }
  
  return {
    restrict: 'E',
    replace: true,
    terminal: true,
    template: '<pre class="syntaxHighlight" ng-bind-html="prettyScope()"></pre>',
    link: function(scope) {
      scope.prettyScope = function() {
        var json = {}, prop;
        for (prop in scope) {
          if (prop.indexOf('$') !== 0 && prop !== 'this') {
            json[prop] = scope[prop];
          }
        }
        return  $sce.trustAsHtml(syntaxHighlight(json));
      };
    }
  };
});


  
app.directive('mcGrid', ["ngIfDirective", "$compile","stConfig", function(ngIfDirective, $compile, stConfig) {

  var ngIf = ngIfDirective[0];
  return {
    priority: ngIf.priority,
    terminal: ngIf.terminal,
    restrict: 'A',
    link: function(scope, element, attrs) {

      // console.error("!!!", element.contents());


      var dataName = attrs.mcGrid;
      if(dataName){
        scope.mcGrid = scope[dataName];
        scope.$watchCollection(dataName, function(newValue){
           console.log("setting mcGrid", newValue)
           scope.mcGrid = newValue;
        });
      }

      var pipeMode = element.attr("st-pipe");

      element.wrap("<div class='table-responsive'></div>");
      element.find("tbody").attr("ng-show", "tablePaginated");
      element.addClass("table table-striped table-hover");
      element.attr("width", "100%");
      element.attr("st-table", "displayedCollection");
      if(pipeMode) {
         element.attr("st-pipe", "pipeHelper");
         scope.pipeHelper = function(tableState){

            if(!tableState.pagination.number)
               tableState.pagination.number = stConfig.pagination.itemsByPage;

            tableState.search.str = (tableState.search.predicateObject ? tableState.search.predicateObject['$'] : "");
            tableState.pagination.params = {
                        "limit":tableState.pagination.number,
                        "offset":tableState.pagination.start,
                        "order": tableState.sort.predicate,
                        "desc": tableState.sort.reverse,
            };
            var re = scope[pipeMode](tableState);
            if((re)&&(re.then)) {
               re = re.then(function(x){
                  tableState.pagination.numberOfPages = Math.ceil(tableState.pagination.totalItemCount / stConfig.pagination.itemsByPage);
                  // console.log("XXXXXXXXX", tableState.pagination, stConfig.pagination.itemsByPage);
                  return x;
               })
            }

            return re;
         }
      } else {
         element.attr("st-safe-src", "mcGrid");        
      }
      element.removeAttr("mc-grid");
      element.prepend('<thead><tr><th colspan=42><input st-search="" class="form-control" lng-placeholder="Search..." type="text"/></th></tr></thead>');
      element.prepend('<tfoot>'+
          (
          pipeMode ? 
          (
             '<tr ng-if="!displayedCollection"><th colspan="42" lng>The query is in progress...</th></tr>'+
             '<tr ng-if="displayedCollection.length == 0"><th colspan="42" lng>No items matched the criteria.</th></tr>'+
             '<tr ng-if="displayedCollection.length > 0"><td class="text-center" colspan="42">'+
                 '<span st-pagination="" ></span>'+
             '</td></tr>'
          ) 
          : 
          (
             '<tr ng-if="!mcGrid"><th colspan="42" lng>The query is in progress...</th></tr>'+
             '<tr ng-if="mcGrid.length > 0 && displayedCollection.length == 0"><th colspan="42" lng>No items matched the criteria.</th></tr>'+
             '<tr ng-if="mcGrid.length == 0"><th colspan="42" lng>The query returned no results.</th></tr>'+
             '<tr ng-if="mcGrid.length > 0"><td class="text-center" colspan="42">'+
                 '<span st-pagination="" ></span>'+
             '</td></tr>'
          )
          )
          +
      '</tfoot>');


      $compile(element)(scope);

      if(attrs.mcGridReady) {
        console.log("calling smart grid ready callback");
        scope[attrs.mcGridReady]();
      }

    }
  };
}])

   
   app.run(["stConfig", "mcConfig", "templateDir", function(stConfig, mcConfig, templateDir) {
	  stConfig.pagination.template = templateDir(templateDirectory,'st-grid-pagination');
	  stConfig.pagination.itemsByPage = mcConfig.config.itemsPerPage;
   }]);

		 


})()


function registerSwitcher(directiveName, postUri) {
  app.directive(directiveName, ['MonsterCloud',"$window", "mcConfig","mcUserSession", function(MonsterCloud, $window, mcConfig, mcUserSession){
    var re = {
      "restrict": "A",
      "scope": {},
      "link": function(scope, elem, attr) {

          var targetSite = elem.attr("href")
          if(targetSite == "#") targetSite = null // target site can be overridden via href, but only with meaningful ones

          elem.bind('click', function(){
            var targetUser = scope[directiveName]
            console.log("targetSite", targetSite, "targetUser", targetUser)

            var uidi = mcUserSession.findUidiForUserId(targetUser)
            if(typeof uidi != "undefined")
            {
               $window.open(MonsterCloud.appendUidi(targetSite || mcConfig.config.indexPageName, uidi))
            } else {
               var p = MonsterCloud.post(postUri, { account_id: targetUser })
               MonsterCloud.doReceiveToken(p, targetSite)
            }

            // need to return false to prevent default action
            return false
          });
      },
    }
    re.scope[directiveName] = "@"
    return re
  }])
}

function showErrorsController(config) {

  var re= ["$scope", "MonsterCloud", "$q"]

  if(config.inject)
     re = re.concat(config.inject)

  if(!config.fireCallback)
    config.fireCallback = function(){}


  re.push(
   function($scope, MonsterCloud, $q) {

              var injectionArguments = arguments

              if(config.relayUserProfile)
                $scope.UserProfile = MonsterCloud.UserProfile

        $scope[ config.fireFunctionName ? config.fireFunctionName : "fire" ] = function( ) {

           $scope.$broadcast('show-errors-check-validity');
           var dataFormName = config.dataFormName? config.dataFormName: "dataForm";
           console.log("dataForm validity:", $scope[dataFormName].$valid, $scope[dataFormName])
           if (!$scope[dataFormName].$valid) return
             $scope[getIsWorkingName()] = true


             var re = config.fireCallback.apply(this, injectionArguments)

             var p
             if((re)&&(re.then)) p= re; else {
           var deferred = $q.defer();
           deferred.resolve()
           p = deferred.promise
         }

             p.then(function(){
                  $scope.reset();
                  $scope[getIsWorkingName()] = false
                  $scope.$broadcast('show-errors-reset');
             })
             .catch(function(ex){
                  $scope[getIsWorkingName()] = false
             })

             return p
        };

        $scope.reset = function() {
          $scope.$broadcast('show-errors-reset');
          $scope[config.dataName ? config.dataName : "data"] = {  };
        }

        $scope.reset()

        if(config.additionalInitializationCallback)
           config.additionalInitializationCallback.apply(this, injectionArguments)

      })


  return re

  function getIsWorkingName(){
    return config.isWorkingName ? config.isWorkingName : "isInProgress"
  }
}
