(function(app){

  var templateDirectory = "ftp-superuser"

app.directive("mcFtpConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/ftp'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "FTP microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.service("mcFtpUrlSu", function(){
    var re = function(server, suffix) {
       return "/s/"+server+"/su/ftp/"+(suffix||"")
    }
    return re
  })

  app.directive("mcSuperuserFtpScoreboard", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-scoreboard'),
        scope: { "server":"@" },
        controller: ["$scope","MonsterCloud","mcFtpUrlSu","$timeout",
          function($scope,MonsterCloud,mcFtpUrlSu,$timeout) {
            var url = mcFtpUrlSu($scope.server,"scoreboard")

            $scope.reset = function(){
               return MonsterCloud.post(url+"/reset",{})
            }

            var pull;
            doWork()
            function doWork(){
               return MonsterCloud.get(url)
                 .then(function(data){

                 	$scope.scoreboard = data
                 	console.log("assigning:", $scope.scoreboard)

                 	pull = $timeout(doWork, 5000)
                 })
            }

            $scope.$on('$destroy', function(){
              // console.log("destroying scope", pull);
              if(pull)
                 $timeout.cancel(pull);
            });
        }]        
     }

  }],{
    title: "FTP scoreboard",
    paramsRequired: { "server": "@"  },    
    menuKey: templateDirectory,
    keywords:["proftpd"],
  })

	app.directive('mcSuperuserFtpList', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'ftp-list'),
	      scope: { "server": "@" },
	      controller: ["$scope", "MonsterCloud","mcFtpUrlSu", "accountLookup","webhostingLookup",
	        function($scope, MonsterCloud,mcFtpUrlSu,accountLookup,webhostingLookup){
	           $scope.ftps = []

            return MonsterCloud.get(mcFtpUrlSu($scope.server,"accounts"))
                 .then(function(data){
                    $scope.ftps = data
                    accountLookup($scope.ftps, "fa_user_id")
                    webhostingLookup($scope.ftps, $scope.server, null, "fa_webhosting")

                    $scope.ftpRows = data;
                 })




	       }]
	   }
	}],{
    title: "List of FTP accounts",
    paramsRequired: { "server": "@"  },    
    menuKey: templateDirectory,
    keywords:["proftpd"],
  })

	app.directive('mcSuperuserFtp', ["templateDir", function(templateDir){

	    return {
	      restrict: "E",
	      templateUrl: templateDir(templateDirectory,'ftp'),
	      scope: { "server": "@"  },
	   }
	}])


})(app)

