(function(app){

  var templateDirectory = "dbms"

  app.service("mcDbmsTask", ["$task","mcDbmsApiUrl",function($task,mcDbmsApiUrl){

         return function(title, server, prom, extendParams){
              var taskUrlBase = mcDbmsApiUrl(server)+"/tasks/"

              return prom.then(function(h){
                  return $task(angular.extend({title:title, url:taskUrlBase+h.id}, extendParams || {}))
              })
         }
  }])


  app.service("mcDbmsApiUrl", function(){
     return function(server, superuser) {
        return "/s/"+server+(superuser? "/su": "")+"/dbms"
     }
  })

app.directive("mcDbmsSlowlogList", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"slowlog"),
    // shared scope!
    "controller": ["$scope", "MonsterCloud", function($scope, MonsterCloud){

        if((!$scope.superuser)&&(!$scope.whId)) throw new Error("Either of these must be provided");

        var uriPrefix = "/s/"+$scope.server+"/" + ($scope.superuser ? "su/" : "") + "dbms/slowlog/" + ($scope.superuser ? "all" : "by-webhosting/"+$scope.whId)+"/";

        $scope.refetch = function(){
               var d = queryDetails();

               return MonsterCloud.post( uriPrefix + "search", d)
                 .then(function(data){
                    if($scope.postProcess)
                        $scope.postProcess(data);

                    $scope.slowlogs = data.events;
                 })          
        }

        $scope.refetch();

        function queryDetails() {
          var d = {}
          Array("notbefore","notafter","ds_query", "ds_database_name").forEach(function(v){
            if(($scope[v] !== "")&&($scope[v] !== null))
               d[v] = $scope[v]
          })
          return d
        }

    }]
  }
}])

app.directive("mcDbmsSlowlogListUser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-dbms-slowlog-list></mc-dbms-slowlog-list>",
  }
}], {
    title: "List of slow database queries",
    paramsRequired: { server:"@", whId: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcDbmsDatabaseList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-list'),
        scope: { server:"@", "databases":"=", "superuser":"@"},
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl",'mcDbmsTask','mcRoutesService','$element',
          function($scope, MonsterCloud, mcDbmsApiUrl,mcDbmsTask,mcRoutesService,$element){

           function getControlParams(name, record) {
  		        var p = {
  				      stateName: name,
  				      stateParams: {
         				      dbmsType: record.dm_dbms_type,
        					    server: $scope.server,
        					    whId: record.db_webhosting,
        					    databaseName: record.db_database_name
        				    }
        				}

      				if($scope.superuser)
      				  p.stateParams.superuser = true

      				return p
           }
           function setControl(name, record) {

		        mcRoutesService.jumpToState(getControlParams(name, record))
           }
           $scope.connectionInfo = function(record){
              mcRoutesService.jumpToState(getControlParams("mc-database-connection-info-"+record.dm_dbms_instance_name, record))
           }

           $scope.dbusers = function(record) {
                setControl("mc-dbms-dbusers", record)
           }
           $scope.import = function(record){
                setControl("mc-dbms-database-import", record)
           }
           $scope.export = function(record){
                setControl("mc-dbms-database-export", record)
           }
           $scope.repair = function(record){
                return mcDbmsTask(
                  "DBMS_TASK_TITLE_REPAIR_DATABASE",
                  $scope.server,
                  MonsterCloud.post(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+record.db_webhosting+"/"+record.db_database_name+"/repair", {})
                )
           }

           $scope.delete = function(record){


                return mcDbmsTask(
                  "DBMS_TASK_TITLE_DELETE_DATABASE",
                  $scope.server,
                  MonsterCloud.delete(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+record.db_webhosting+"/"+record.db_database_name)
                )
                .then(function(){
                    console.log("eliminating dbusers div")
                    $element.find("#databaseSpecific").html("")
                    $scope.$parent.$broadcast("refetch")


                })

           }

           $scope.reshot = function(){
               $scope.dbs = $scope.databases.data;
               // console.log("xxxxxxxxxxxxxxxxxx in reshot", $scope.databases)
           }
           $scope.$on("reshot", $scope.reshot)

           $scope.reshot()


        }]

     }

  }])



  app.directive("mcDatabaseConnectionInfoGeneral", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-connection-info-general'),
        scope: { server:"@", whId: "@",}, // note whId is only here to bind the control to webhostings and not to show up at other crumbs
        controller: ["$scope","mcServerHostname", function($scope, mcServerHostname){
           mcServerHostname.apply($scope.server, "dbms", $scope)
        }]
      }
  }])

  app.directive("mcDatabaseConnectionInfoMysql", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-connection-info-mysql'),
        controller: ["$scope","mcServerHostname", "mcConfig", "lngService", function($scope, mcServerHostname, mcConfig, lngService){
           mcServerHostname.apply($scope.server, "dbms", $scope)
           $scope.config = mcConfig.config;
           $scope.passwordLng = lngService.translate("Password");

           $scope.phpMyAdminUrl = mcConfig.config.dbms_phpmyadmin_url;
           if(!$scope.phpMyAdminUrl) {
              $scope.phpMyAdminUrl = "https://mysql."+$scope.server+"."+mcConfig.config.service_primary_domain;
           }
        }]
      }
  }],{
      title: "MySQL connection info",
      menuKey: templateDirectory,
      paramsRequired: { server:"@", whId: "@", databaseName:"@"},
  })

  app.directive("mcDbmsDatabaseExport", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-export'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl","$window",'mcConfig',function($scope, MonsterCloud, mcDbmsApiUrl,$window,mcConfig){

            $scope.do = function(){

               MonsterCloud.post(mcDbmsApiUrl($scope.server)+"/databases/"+$scope.whId+"/"+$scope.databaseName+"/export", {})
                 .then(function(h){
                     $window.location.href = mcConfig.config.api_uri_prefix + mcDbmsApiUrl($scope.server)+"/tasks/"+h.id
                 })

            }


        }]

     }

  }], {
     title: "Export database",
     menuKey: templateDirectory,
	 paramsRequired: { server:"@", "whId":"@", "databaseName":"@", "dbmsType": "@"},
	 paramsOptional: { superuser: "@"},
  })


  app.service("mcDbmsDatabaseImportService", ['mcDbmsApiUrl','MonsterCloud','mcDbmsTask',function(mcDbmsApiUrl,MonsterCloud,mcDbmsTask){
      return function(server, whId, databaseName, filename, content, filemanTask, doubleRequestMode) {
         return mcDbmsTask(
                  "DBMS_TASK_TITLE_IMPORT",
                  server,
                  MonsterCloud.post(mcDbmsApiUrl(server)+"/databases/"+whId+"/"+databaseName+"/import", {filename:filename, filemanTask: filemanTask}),
                  {fileInput: content, doubleRequestMode: doubleRequestMode}
                )
      }
  }])

  app.directive("mcDbmsDatabaseImport", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-import'),
     }

  }], {
     title: "Import database",
     menuKey: templateDirectory,
	 paramsRequired: { server:"@", "whId":"@", "databaseName":"@", "dbmsType": "@"},
  })

  app.directive("mcDbmsDatabaseImportViaUpload", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'database-import-via-upload'),
        scope: { server:"@", "whId":"@", "databaseName":"@", "dbmsType": "@"},
        controller: ["$scope",'mcDbmsDatabaseImportService',function($scope, mcDbmsDatabaseImportService){

            $scope.filename = ""

            $scope.do = function(){

               if(!$scope.content) return

                console.log("f:", $scope.filename)

                return mcDbmsDatabaseImportService($scope.server, $scope.whId, $scope.databaseName, $scope.filename, $scope.content, undefined, true)


            }


        }]

     }

  }])

  app.service("mcDbmsDatabasesService", ["MonsterCloud","mcDbmsApiUrl",function(MonsterCloud, mcDbmsApiUrl){
      return function(server, whId) {
          return MonsterCloud.get(mcDbmsApiUrl(server)+"/databases/"+whId)
      }
  }])

  app.directive("mcDbmsDatabases", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'databases'),
        controller: ["$scope",'mcDbmsDatabasesService',function($scope, mcDbmsDatabasesService){

           $scope.databases = {}

           $scope.refetch = function(){
              console.log("refetching database list, per webhosting")
              return mcDbmsDatabasesService($scope.server, $scope.whId)
                .then(function(data){
                    $scope.databases.data = data
                    $scope.$broadcast("reshot")
                })
           }


           $scope.$on("refetch",function(event){
               return $scope.refetch()
           });


           $scope.refetch()

        }]

     }

  }], {
      title: "Databases",
      menuKey: templateDirectory,
      paramsRequired: { "server":"@", "whId":"@" },
      keyword: ["database connection info"],
  })




  app.directive("mcDbmsDbusersList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'dbusers-list'),
        scope: { server:"@", "dbusers":"=", "whId":"@", "databaseName":"@", superuser: "@"},
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl",function($scope, MonsterCloud, mcDbmsApiUrl){
           var baseUrl = mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+$scope.whId+"/"+$scope.databaseName+"/user/"
           $scope.delete = function(record, forced){
              var c = {force: forced}
              return thenRefetch(MonsterCloud.delete(baseUrl+record.u_username, c))
           }
           function thenRefetch(prom) {
              return prom.then(function(){
                 $scope.$parent.$broadcast("refetch")
              })
           }
           $scope.lock = function(record){
              return thenRefetch(MonsterCloud.post(baseUrl+record.u_username+"/lock",{}))
           }
           $scope.unlock = function(record){
              return thenRefetch(MonsterCloud.post(baseUrl+record.u_username+"/unlock",{}))
           }

        }]

     }

  }])


  app.directive("mcDbmsDbusers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'dbusers'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl",function($scope, MonsterCloud, mcDbmsApiUrl){

           $scope.refetch = function(){
              console.log("refetching database users")
              MonsterCloud.get(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+$scope.whId+"/"+$scope.databaseName+"/users")
                .then(function(data){
                    $scope.dbusers = data
                })
           }



           $scope.$on("refetch",function(event){
               return $scope.refetch()
           });


           $scope.refetch()

        }]

     }

  }], {
     title: "Manage database users",
     menuKey: templateDirectory,
	 paramsRequired: {"server":"@", "whId":"@", databaseName: "@"},
	 paramsOptional: {superuser:"@"},
  })


  app.directive('mcDbmsRestoreFromFileman', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'restore-from-fileman'),
      controller:  showErrorsController({
         inject: ["mcDbmsDatabasesService","mcDbmsDatabaseImportService","mcFilemanUrl"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcDbmsDatabasesService, mcDbmsDatabaseImportService, mcFilemanUrl){
             $scope.dbbackupDir = "/dbbackup/"

             if(!$scope.databaseName) {
                 mcDbmsDatabasesService($scope.server,$scope.whId)
                    .then(function(databases){
                       $scope.databases = databases.map(function(x){return x.db_database_name;})
                    })
             } else {
                $scope.databases = [$scope.databaseName]
                $scope.backup = {database: $scope.databaseName}
             }


              $scope.filemanUrl = mcFilemanUrl($scope.server, $scope.whId)


         },
         fireCallback: function($scope, MonsterCloud, $q, mcDbmsDatabasesService, mcDbmsDatabaseImportService){

            return MonsterCloud.post($scope.filemanUrl+"/download", {src_file: $scope.dbbackupDir + $scope.backup.file})
              .then(function(d){
                 // console.log("returned fetching:", d)
                 return mcDbmsDatabaseImportService($scope.server, $scope.whId, $scope.backup.database, $scope.backup.file, null, d.id)
              })

         }

      })
    }
  }],{
      title: "Restore database from regular backup",
      menuKey: templateDirectory,
      paramsRequired: { "server":"@", "whId":"@" },
      paramsOptional: { "databaseName":"@"},
      keyword: ["mysql", "mariadb"],
  })

  app.directive('mcDbmsDbusersCreate', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      scope: { "server":"@", "whId":"@", databaseName: "@", "superuser":"@" },
      templateUrl: templateDir(templateDirectory,'dbusers-create'),
      controller:  showErrorsController({
         inject: ["mcDbmsApiUrl"],
         fireCallback: function($scope, MonsterCloud, $q, mcDbmsApiUrl){
           return MonsterCloud.put(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+$scope.whId+"/"+$scope.databaseName+"/users", $scope.dbuser)
              .then(function(){
                 $scope.dbuser = {}
                 $scope.$parent.$broadcast("refetch")
              })
         }

      })
    }
  }])

  app.directive('mcDbmsDatabaseCreate', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      scope: { "server": "@", whId: "@", createUser: "@", superuser:"@" },
      templateUrl: templateDir(templateDirectory,'database-create'),
      controller:  showErrorsController({
         inject: ["mcDbmsApiUrl"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcDbmsApiUrl){

           $scope.database = {}

           $scope.$on("refetch",function(event){
               return $scope.refetch()
           });

           $scope.refetch = function(){
              MonsterCloud.post(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+$scope.whId+"/supported")
                .then(function(d){
                    $scope.supportedDbmsTypes = d
                    $scope.databaseCanBeCreated = d.length > 0
                    $scope.databaseCannotBeCreated = !$scope.databaseCanBeCreated
                    if((!$scope.database.db_dbms)&&($scope.supportedDbmsTypes.length == 1)) $scope.database.db_dbms = $scope.supportedDbmsTypes[0]
                })

           }

           $scope.refetch()
         },
         fireCallback: function($scope, MonsterCloud, $q, mcDbmsApiUrl){
           return MonsterCloud.put(mcDbmsApiUrl($scope.server, $scope.superuser)+"/databases/"+$scope.whId, $scope.database)
              .then(function(){
                 if(!$scope.createUser) return $q.when()

                 return MonsterCloud.put(mcDbmsApiUrl($scope.server)+"/databases/"+$scope.whId+"/"+$scope.database.db_database_name+"/users",
                   {u_username: $scope.database.db_database_name, u_password: $scope.dbuser.u_password})
              })
              .then(function(){
                 $scope.database = {}
                 $scope.dbuser = {}
                 $scope.$parent.$broadcast("refetch")
              })
         }

      })
    }
  }])



})(app)
