(function(app){

  var templateDirectory = "fileman"

  app.service("mcFilemanTask", ["$task","mcFilemanUrl",function($task,mcFilemanUrl){

         return function(title, server, prom, extendParams){
              var taskUrlBase = mcFilemanUrl(server)+"/tasks/"

              return prom.then(function(h){
                  return $task(angular.extend({}, {title:title, url:taskUrlBase+h.id}, extendParams))
              })
         }
  }])

  app.service("mcFilemanDownload", ["$window","mcConfig","mcFilemanUrl",function($window,mcConfig,mcFilemanUrl){

         return function(server, prom){

                return prom.then(function(h){
                       $window.location.href = mcConfig.config.api_uri_prefix + mcFilemanUrl(server)+"/tasks/"+h.id
                })
         }
  }])


app.directive('mcWebhostingFileman', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     templateUrl: templateDir(templateDirectory,"webhosting-fileman"),
   }

}], {
     title: "File browser",
     menuKey: templateDirectory,
     paramsRequired: { "server": "@","whId": "@" },
     paramsOptional: { "path": "@" },
     keywords: ["files","browser"],
});


app.directive('mcFilemanSiteBackup', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     templateUrl: templateDir(templateDirectory,"site-backup"),

      controller:  showErrorsController({
         inject: ["mcFilemanUrl","mcFilemanDownload"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, mcFilemanUrl, mcFilemanDownload){
            $scope.distinct = {}
         },
         fireCallback: function($scope, MonsterCloud, $q, mcFilemanUrl, mcFilemanDownload){
              var baseUrl = mcFilemanUrl($scope.server, $scope.whId)
               var prom = MonsterCloud.post(baseUrl+"/tgz", {pathes:[$scope.distinct.selected]})
               return mcFilemanDownload($scope.server, prom)
         }

      })

   }

}], {
     title: "Backup website contents",
     menuKey: templateDirectory,
     paramsRequired: { "server": "@","whId": "@" },
     keywords: ["archive","backup","manual"],
});

app.service('mcFilemanDelService', ["MonsterCloud","mcFilemanUrl","mcFilemanTask",function (MonsterCloud,mcFilemanUrl,mcFilemanTask) {

    return function(server, whId, superuser, path_category){
       var baseUrl = mcFilemanUrl(server, whId, superuser, path_category);
       var removeUrl = baseUrl+"/remove";

       var re = {
          DelPromise: function(pathes){
             return MonsterCloud.post(removeUrl, {pathes: pathes, path_category: path_category})
          },
          Del: function(pathes){
                return mcFilemanTask(
                   "FILEMAN_REMOVE_TASK",
                   server,
                   re.DelPromise(pathes)
                )
          }
       }
       return re
    }
}])



app.directive('mcFilemanInterStoreCopyWizard', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     templateUrl: templateDir(templateDirectory,"inter-store-copy-wizard"),

      controller: ["$scope", "MonsterCloud","mcSessionCookies", "mcFilemanUrl", "$compile", "$element", "$task",
         function($scope, MonsterCloud, mcSessionCookies, mcFilemanUrl, $compile, $element, $task){
            $scope.tokensArray = mcSessionCookies.GetTokenArray();

            $scope.isDisabled=function(n){
               return n > $scope.pageMax
            }

            $scope["src_select_callback"] = function(dir, selectedFiles){
               $scope.copy.src.directory = dir;
               $scope.copy.src.files = selectedFiles;
               $scope.copy.src.pureFileNames = selectedFiles.map(function(x){return x.name;})
               $scope.nextDisabled[2] = selectedFiles.length <= 0;
            }
            $scope["dst_browse_callback"] = function(dir, files){
               $scope.copy.dst.directory = dir;
            }

            $scope.reset = function(){
              $scope.copy = {src:{}, dst:{directory:"/"}};

              $scope.opens = Array(5+1);
              $scope.nextDisabled = Array(5+1);
              for(var i = 0; i < $scope.nextDisabled.length; i++)
                $scope.nextDisabled[i] = true;
              $scope.nextDisabled[4] = false;

              $scope.pageMax = 1;
              setCurrentPage(1);

            }

            $scope.start = function(){
               var operation = ($scope.copy.src.webhosting.w_server_name == $scope.copy.dst.webhosting.w_server_name) ? "tar" : "tgz";
               var filesToFetch = $scope.copy.src.files.map(function(x){return x.fullPath});

               var srcUrl = mcFilemanUrl($scope.copy.src.webhosting.w_server_name, $scope.copy.src.webhosting.w_webhosting_id)+"/"+operation
               return MonsterCloud.doRequest({
                  token: $scope.copy.src.account.token,
                  url:srcUrl,
                  data: {
                    pathes:filesToFetch
                  }
               })
               .then(function(h){
                  var dstUrl = mcFilemanUrl($scope.copy.dst.webhosting.w_server_name, $scope.copy.dst.webhosting.w_webhosting_id)+"/un"+operation+"/transfer";

                  var prom = MonsterCloud.doRequest({
                    token: $scope.copy.dst.account.token,
                    url:dstUrl,
                    data:{
                      source_server: $scope.copy.src.webhosting.w_server_name,
                      source_download_id: h.id,
                      dst_dir_path: $scope.copy.dst.directory,
                    }
                  });

                  return prom;
               })
               .then(function(h){
                  return $task(angular.extend({}, {title:"FILEMAN_TITLE_COPY_FILES_BETWEEN_STORES", url:"/s/"+$scope.copy.dst.webhosting.w_server_name+"/tasks/"+h.id}))
               })
               .then(function(){
                  $scope.reset();
               })
            }

            $scope.next = function(){
                 var c = getCurrentPage()
                 if(c == 1) {
                    var t  = "<mc-fileman token='"+$scope.copy.src.account.token+"' select-callback='src_select_callback(dir, files)' show-checkbox='true' no-buttons='true' server='"+$scope.copy.src.webhosting.w_server_name+"' wh-id='"+$scope.copy.src.webhosting.w_webhosting_id+"'></mc-fileman>"
                    var el = $compile( t )( $scope );
                    var sl = $element.find("#src_fileman_placeholder")
                    sl.html( el );
                 }
                 if(c == 3) {
                    var t  = "<mc-fileman token='"+$scope.copy.src.account.token+"' browse-callback='dst_browse_callback(dir, files)' no-buttons='true' server='"+$scope.copy.dst.webhosting.w_server_name+"' wh-id='"+$scope.copy.dst.webhosting.w_webhosting_id+"'></mc-fileman>"
                    var el = $compile( t )( $scope );
                    var sl = $element.find("#dst_fileman_placeholder")
                    sl.html( el );
                 }
                 var n = setCurrentPage(c+1)
                 if(n > $scope.pageMax)
                   $scope.pageMax = n
            }

            $scope.accountChange = function(category){
                return MonsterCloud.doRequest({url:"/my/services/webhostings/details", token: $scope.copy[category].account.token})
                  .then(function(data){
                      $scope[category+"_webhostings"] = data;
                  })
            }
            $scope.webStoreChange = function(category){
               var index = (category == "src") ? 1 : 3;
               $scope.nextDisabled[index] = $scope.copy[category]["webhosting"] ? false : true;
            }

            $scope.reset();



            function getCurrentPage(){
              var n = 0
               for(var i = 0; i < $scope.opens.length; i++) {
                 if($scope.opens[i]) {
                   n = i
                   break;
                 }
               }
               return n
            }
            function setCurrentPage(n){
               for(var i = 0; i < $scope.opens.length; i++)
                 $scope.opens[i] = false

               $scope.opens[n] = true
               return n
            }

      }],

   }

}], {
     title: "Inter-store file copy wizard",
     menuKey: templateDirectory,
     paramsRequired: { },
});


app.directive('mcFilemanSiteDel', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     templateUrl: templateDir(templateDirectory,"site-del"),

      controller:  showErrorsController({
         inject: ["mcFilemanDelService"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q){
            $scope.distinct = {}
         },
         fireCallback: function($scope, MonsterCloud, $q, mcFilemanDelService){
            return mcFilemanDelService($scope.server, $scope.whId).Del([$scope.distinct.selected])
         }

      })

   }

}], {
     title: "Remove website contents",
     menuKey: templateDirectory,
     paramsRequired: { "server": "@","whId": "@" },
});

app.directive('mcDirlistSelectbox', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     scope: {
      "title": "@",
      "server": "@",
      "whId": "@",
      "directory": "@",
      "form": "=",
      "model": "=",
      "modelName": "@",
      "showFilesize": "@",
     },
     templateUrl: templateDir(templateDirectory,"dirlist-selectbox"),
     controller: ["$scope", "mcFilemanLister", "$q", function($scope, mcFilemanLister, $q){

          return mcFilemanLister({
            server:$scope.server,
            whId:$scope.whId,
            path:$scope.directory,
            withoutDirs: true,
            filesAsWell: true,
            filePropertiesAsWell: true,
          })
            .then(function(files){
               var re = Array()
               files.forEach(function(f){
                  var d = f.name
                  if($scope.showFilesize)
                    d += " ("+f.sizeHuman+")"
                  re.push({name: f.name, display: d})
               })
               $scope.files = re
            })

     }]
   }

}]);

  app.service("mcFilemanUrl", function(){
     return function(server, whId, superuser) {
        return "/s/"+server+(superuser? "/su" : "")+"/fileman"+(whId ? "/" + whId : "")
     }
  })

  app.service("mcFilemanLister", ["MonsterCloud","mcFilemanUrl", '$q', 'formatBytes', function(MonsterCloud,mcFilemanUrl, $q, formatBytes){
      return function(options) {
         Array("server","whId","path").forEach(function(k){
            if(!options[k]) throw new Error(k+" is mandatory")
         })

         var url = mcFilemanUrl(options.server, options.whId, options.superuser)
         var requestOptions = {
            method:"POST",
            url: url+"/list",
            data: {cwd: options.path, dirsOnly: !options.filesAsWell, filesOnly: options.withoutDirs, path_category: options.path_category},
            relayException: options.relayException
         };
         if(options.token) requestOptions.token = options.token;
         // console.log("listing!!!!!!!!!!!!!", requestOptions)
         return MonsterCloud.doRequest(requestOptions)
           .then(function(files){

                files.forEach(function(file){
                   file.isDirectory = (file.perm[0] == "d")
                   file.sizeHuman = formatBytes(file.size)
                   file.fullPath =  (options.path + "/"+file.name).replace("//","/")

                })


               if(!options.filePropertiesAsWell)
                 files = files.map(function(k){return (options.fullPathOnly ? k.fullPath : k.name)})

               return $q.when(files)
           })
      }
  }])

  app.directive("mcFilemanInProgress", ["mcFilemanUrl","$compile",function(mcFilemanUrl,$compile){
      return {
        restrict: "E",
        scope: { "server":"@", "whId":"@" },
        link: function(scope, elm, attrs, ctrl) {
              var tasksGetUrl = mcFilemanUrl(scope.server, scope.whId) + "/tasks"
              var taskUrl = mcFilemanUrl(scope.server) + "/tasks"
              var t  = "<mc-tasks-in-progress title='FILEMAN_TASK_IN_PROGRESS' task-url='"+taskUrl+"' tasks-get-url='"+tasksGetUrl+"' ></mc-tasks-in-progress>"
              var el = $compile( t )( scope );
              elm.html( el );
        }

     }

  }])


  app.service("mcReadonly", ["mcFilemanUrl","MonsterCloud","$q","mcFilemanTask",
    function(mcFilemanUrl,MonsterCloud,$q,mcFilemanTask){
      var cachedResult = {}
      var queryInProgress = {}
      return function(server,whId, superuser) {
        var cacheKey = server + "-" + whId
        var url = mcFilemanUrl(server, whId, superuser)
        var re = {
            isReadonly: function(){

                if(cachedResult[cacheKey]) return $q.when(cachedResult[cacheKey])

                if(!queryInProgress[cacheKey])
                   queryInProgress[cacheKey] = MonsterCloud.get(url+"/isreadonly")
                      .then(function(result){
                        delete queryInProgress[cacheKey]
                        if(!cachedResult[cacheKey]) cachedResult[cacheKey] = {}
                        cachedResult[cacheKey].isReadonly = result
                        return $q.when(cachedResult[cacheKey])
                      })

                return queryInProgress[cacheKey]

            },

            toReadonly: function(){
             doTask(MonsterCloud.post(url+"/toreadonly", {}))
               .then(function(){
                  cachedResult[cacheKey].isReadonly = true
               })
             },

            toReadwrite: function(){
             doTask(MonsterCloud.post(url+"/toreadwrite", {}))
               .then(function(){
                  cachedResult[cacheKey].isReadonly = false
               })
            }

        }
        return re

          function doTask(prom) {
             return mcFilemanTask(
               "FILEMAN_READONLY_TASK",
                server,
                prom
             )
          }

      }

  }])


  app.directive("mcReadonlyRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'readonly-row'),
      scope: { "server":"@","whId":"@"},
      controller: ["$scope", "mcReadonly", function($scope, mcReadonly){
          var r = mcReadonly($scope.server, $scope.whId)
          r.isReadonly().then(function(result){
            $scope.readonly = result
          })
          $scope.toReadonly = function(){
             r.toReadonly()
          }
          $scope.toReadwrite = function(){
             r.toReadwrite()
          }
      }]
   }

  }])

  app.service("mcFilemanUpload", ["$q","$task","mcFilemanUrl","MonsterCloud",function($q,$task,mcFilemanUrl,MonsterCloud){
     return function(server, whId, dir, files, urlOverride, path_category) {

        var taskUrlBase = mcFilemanUrl(server)+"/tasks/"
        var baseUrl = mcFilemanUrl(server, whId)

        console.log("SHIITTOO", server, "X", whId, "X", taskUrlBase, "X", baseUrl)

                if(files.length <= 0) return

                const title = "FILEMAN_UPLOAD_TASK"
                var d = {
                   title: title,
                   expectedTotalBytes: 0,
                   abortOnly: true,
                   doubleRequestMode: true,
                   expectedRequests: files.length,
                   urlFactory: function(index, appendText) {
                      console.log("urlFactory was called", index)
                      var file = files[index]
                      appendText("Uploading file: "+file.name)

                      var aData = {
                         doubleRequestMode: true,
                         title: file.name,
                      }

                      return file.readAsArrayBuffer()
                        .then(function(fileInput){
                            aData.fileInput = fileInput;
                            var payload = {dst_full_path: dir + file.name};
                            if(path_category) {
                               payload.path_category = path_category;                              
                            }
                            return MonsterCloud.post(urlOverride || baseUrl+ "/upload", payload);                       
                        })
                        .then(function(h){
                           aData.url = taskUrlBase + h.id
                           return $q.when(aData)
                        })
                   }
                }

                files.forEach(function(f){
                    d.expectedTotalBytes += f.size
                })

                console.log("triggering multiple file uploads", d)

                return $task(d)
     }
  }])


  app.directive("mcFileman", ["templateDir", function(templateDir){

      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'fileman'),
        scope: { server:"@", whId:"@", path: "@", noButtons: "@", "showCheckbox":"@", "superuser": "@", "pathCategory":"@", browseCallback: "&", selectCallback: "&", token: "@" },
        controller: ["$scope",'MonsterCloud',"mcFilemanUrl",'mcFilemanTask','mcFilemanLister','mcFilemanDownload',
        'mcReadonly','$task','$q',"$prompt",'mcFilemanDelService', '$element', 'mcFilesService','mcConfig',
        '$fileEditor','mcAutoCloseParams',"$location", 'mcLoadScript',"mcFilemanUpload",
          function($scope, MonsterCloud, mcFilemanUrl,mcFilemanTask,mcFilemanLister,mcFilemanDownload,
            mcReadonly, $task,$q,$prompt,mcFilemanDelService, $element, mcFilesService, mcConfig,
            $fileEditor, mcAutoCloseParams, $location, mcLoadScript, mcFilemanUpload){

            function cancel(e) {
              if (e.preventDefault) { e.preventDefault(); }
              return false;
            }

            mcLoadScript("ace", "/js/ace-min-noconflict/ace.js")

            $scope.smartTableReady = function(){
                console.log("smartTableReady was called");

                var filelist =  $element.find(".drop-files");
                filelist.on("dragover", function(){
                  console.log("dragover");
                  if(($scope.readonly)&&(!$scope.readonly.isReadonly))
                     filelist.addClass("hover")
                  return false;
                })
                Array("dragend", "dragleave").forEach(function(cat){
                    filelist.on(cat, function(){
                      console.log(cat);
                      filelist.removeClass("hover")
                      return false;
                    })

                })
                filelist.on("drop", function(e){
                    console.log("drop");
                    filelist.removeClass("hover")
                    e = e || window.event; // get window.event if e argument missing (in IE)
                    if (e.preventDefault) { e.preventDefault(); } // stops the browser from redirecting off to the image.
                    e = e.originalEvent || e; // dataTransfer is inside this stuff on Chrome

                    if(($scope.readonly)&&($scope.readonly.isReadonly)) return

                    var target =  e.dataTransfer || e.target
                    var files = target.files || target.files;
                    if(!files) return

                    $scope.doUpload(mcFilesService(files))

                });

            }


            var r = mcReadonly($scope.server, $scope.whId, $scope.superuser)
            r.isReadonly().then(function(result){
              $scope.readonly = result
            })

            var lastSortCol = "name"
            var lastSortDesc = false

            var taskUrlBase = mcFilemanUrl($scope.server)+"/tasks/"
            var baseUrl = mcFilemanUrl($scope.server, $scope.whId, $scope.superuser)
            $scope.dir = $scope.path || "/"
            $scope.files = []

            var pasteCutVars = []
            var pasteCopyVars = []
            recalcPasteVars()

            $scope.forgetSelection = function(){
              pasteCopyVars = []
              pasteCutVars = []
              recalcPasteVars()
            }

            $scope.cut = function(){
              if($scope.noneSelected()) return;
              pasteCutVars = getFullpathes(getSelectedFiles())
              pasteCopyVars = []
              recalcPasteVars()
            }
            $scope.copy = function(){
              if($scope.noneSelected()) return;
              pasteCopyVars = getFullpathes(getSelectedFiles())
              pasteCutVars = []
              recalcPasteVars()
            }

            function pasteCut(){
                return mcFilemanTask(
                   "FILEMAN_MOVE_TASK",
                   $scope.server,
                   MonsterCloud.post(baseUrl+"/move", {path_category: $scope.pathCategory, src_pathes:pasteCutVars,dst_dir_path:$scope.dir})
                )
            }
            function pasteCopy(){
                return mcFilemanTask(
                   "FILEMAN_COPY_TASK",
                   $scope.server,
                   MonsterCloud.post(baseUrl+"/copy", {path_category: $scope.pathCategory, src_pathes:pasteCopyVars,dst_dir_path:$scope.dir})
                )
            }

            $scope.paste = function(){
               if($scope.pasteNotPossible) return;
               var p = pasteCopyVars.length > 0 ? pasteCopy : pasteCut;
               return p().then(function(){
                  $scope.forgetSelection();
                  return $scope.refetch()
               })

            }

            function recalcPasteVars(){
                $scope.pastePossible = ((pasteCopyVars.length > 0) || (pasteCutVars.length > 0));
                $scope.pasteNotPossible = !$scope.pastePossible
            }

            function cwdUpPossible(){
                var x = $scope.dir.split("/")
                if(x.length < 3) return false
                x.shift()
                x.pop()
                x.pop()
                return ("/" + x.join("/") + "/").replace("//","/")
            }

            $scope.onSelect = function() {
                if($scope.selectCallback) {
                  $scope.selectCallback({dir:$scope.dir, files:getSelectedFiles()})
                }
            }

            function getSelectedFiles(){
               return $scope.files.filter(function(x){return x.checked})
            }
            function getFullpathes(arr){
               return arr.map(function(x){return x.fullPath})
            }
            function getNamepathes(arr){
               return arr.map(function(x){return x.name})
            }
            $scope.toggleAll = function(){
              console.log("toggleAll was called")
               $scope.files.forEach(function(file){
                console.log("toggling", file)
                  file.checked = !file.checked;
               })
               $scope.onSelect();
            }
            $scope.forceAll = function(){
               $scope.files.forEach(function(file){
                  file.checked = $scope.checkboxAll
               })
               $scope.onSelect();
            }
            $scope.cwdUp = function() {
                return doCwd(cwdUpPossible())
            }

            $scope.noneSelected = function(){
              var allUnchecked = true
              $scope.files.some(function(x){
                if(x.checked) {
                   allUnchecked = false
                   return true
                }
              })
              return allUnchecked
            }

            function getOnlyCheckedWithLengthConstraint(){
                var checkedOnes = 0
                var re = null
                $scope.files.some(function(x){
                  if(x.checked) {
                     checkedOnes++

                     if((!x.isDirectory)&& (x.size <= mcConfig.config.editableFileSizeLimitBytes))
                       re = x.fullPath

                  }
                })

                return checkedOnes == 1 ? re : null
            }

            $scope.editNotPossible = function(){

                return getOnlyCheckedWithLengthConstraint() == null
            }

            function editorTask(filename, prom){
               return $fileEditor({
                  title: "FILE_EDITOR_TITLE",
                  filename:filename,
                  textPromise: prom,
                  server: $scope.server,
                  path_category: $scope.pathCategory, 
                  uploadUrl: baseUrl+"/upload",
               })
                 .then(function(e){
                    if(e != "saved") return

                    return $scope.refetch()
                 })
            }

            $scope.newFile = function(){
                return $prompt({title: "FILEMAN_CREATE_FILE_TITLE", message: "FILEMAN_CREATE_FILE_MESSAGE", placeholder: "FILEMAN_CREATE_FILE_PLACEHOLDER"})
                  .then(function(filename){
                     var fullPath = $scope.dir+filename

                     return editorTask(fullPath, $q.when({text:""}))
                  })
            }
            $scope.editFile = function(){
               if($scope.editNotPossible()) return;

               var filename = getOnlyCheckedWithLengthConstraint()
               var prom = mcFilemanTask(
                         "FILEMAN_DOWNLOAD_FILE_TASK",
                         $scope.server,
                         MonsterCloud.post(baseUrl+"/download", {path_category: $scope.pathCategory, src_file: filename}),
                         mcAutoCloseParams({mcTaskStatusNotNeeded: true})
                      )

               return editorTask(filename, prom)
            }

            $scope.doRemove = function(){
               var sf = getFullpathes(getSelectedFiles())
               return mcFilemanDelService($scope.server, $scope.whId, $scope.superuser, $scope.pathCategory).Del(sf)
               .then(function(){
                  return $scope.refetch()
               })
            }

            function rewriteLocationThen(prom){
               return mcFilemanDownload($scope.server, prom)
            }

            function doCwd(path){
                  if(!path) return

                  $scope.dir = path
                  var s = $location.search()
                  if((!s.path)||(s.path != path)) {
                    s.path = path
                    $location.skipChangeOnce = true
                    $location.search(s)
                  }
                  return $scope.refetch()
            }

            $scope.downloadOrCwd = function(file){
              if(file.isDirectory)
              {
                  return doCwd($scope.dir + file.name + "/")

              } else {

                 rewriteLocationThen(MonsterCloud.post(baseUrl+"/download", {path_category: $scope.pathCategory, src_file: $scope.dir + file.name}))

              }
            }

            $scope.doTar = function(archive){
                 var sf = getNamepathes(getSelectedFiles());
                 if(sf.length <= 0) return

                 var payload = {path_category: $scope.pathCategory, pathes:sf, dst_dir_path: $scope.dir};

                 rewriteLocationThen(MonsterCloud.post(baseUrl+"/"+archive, payload))
            }

            $scope.doZip = function(){
                 var sf = getNamepathes(getSelectedFiles());
                 if(sf.length <= 0) return

                 return $prompt({title: "FILEMAN_ZIP_TITLE", message: "FILEMAN_ZIP_MESSAGE", placeholder: "FILEMAN_ZIP_PLACEHOLDER"})
                  .then(function(zipName){
                     var payload = {
                       path_category: $scope.pathCategory, 
                       pathes:sf, 
                       dst_dir_path: $scope.dir,
                       dst_zip_name: zipName
                     };
                     var p = MonsterCloud.post(baseUrl+"/zip", payload)

                     return mcFilemanTask(
                         "FILEMAN_ZIP_TASK",
                         $scope.server,
                         p
                      )

                  })
                  .then(function(){
                     return $scope.refetch()
                  })

            }

            $scope.mkdir = function() {
              // console.log("something")
                return $prompt({title: "FILEMAN_MKDIR_TITLE", message: "FILEMAN_MKDIR_MESSAGE", placeholder: "FILEMAN_MKDIR_PLACEHOLDER"})
                  .then(function(dir){
                    console.log({autoClose: true, dontDisplayUntilMs: mcConfig.config.tasksDontDisplayUntilMs}, mcConfig)
                      return mcFilemanTask(
                         "FILEMAN_MKDIR_TASK",
                         $scope.server,
                         MonsterCloud.post(baseUrl+"/mkdir", {path_category: $scope.pathCategory, "dst_dir_path": $scope.dir+dir}),
                         mcAutoCloseParams()
                      )

                  })
                  .then(function(d){
                     return $scope.refetch()
                  })

            }

            $scope.sort = function(col,desc){
              lastSortCol = col
              lastSortDesc = desc
              // console.log("crap", $scope.files)
              $scope.files.sort(function(a,b){
                   var re
                   if(a[col].localeCompare)
                     re = a[col].localeCompare(b[col]);
                   else
                     re = a[col] - b[col]
                   return re * (desc ? -1 : 1)
              })
            }

            $scope.doUpload = function(files){

                var p = mcFilemanUpload($scope.server, $scope.whId, $scope.dir, files, null, $scope.pathCategory)
                if(p) {
                  return p.then(function(){
                     $scope.refetch()
                  })
                }
            }

            $scope.doUploadExtract = function(file,data){
               var method = "untar"
               if(file.name.match(/.(zip|ZIP)$/))
                 method = "unzip"
               if(file.name.match(/.(gz|tgz|GZ|TGZ)$/))
                 method = "untgz"
               return mcFilemanTask(
                   "FILEMAN_UPLOAD_EXTRACT_TASK",
                   $scope.server,
                   MonsterCloud.post(baseUrl+"/"+method, {path_category: $scope.pathCategory, dst_dir_path: $scope.dir, filename: file.name}),
                   {fileInput: data, doubleRequestMode: true}
                )
               .then(function(){
                  $scope.refetch()
               })


            }

            $scope.refetch = function(){
                return mcFilemanLister({server:$scope.server,whId:$scope.whId,path_category: $scope.pathCategory, superuser: $scope.superuser, path:$scope.dir,filePropertiesAsWell:true,filesAsWell:true, token: $scope.token})
                  .then(function(files){
                      // console.log(files)
                      $scope.files = files
                      $scope.cwdUpPossible = cwdUpPossible()
                      $scope.sort(lastSortCol, lastSortDesc)

                      if($scope.browseCallback) {
                        $scope.browseCallback({dir:$scope.dir, files:$scope.files})
                      }
                  })
            }

            $scope.refetch()

            $scope.$on("refetch",function(event){
               return $scope.refetch()
           });

        }]

     }

  }])

  app.service("mcCronTransform", function(){
     return function(d){
         var re = []
         Object.keys(d.entries).forEach(function(k){
            d.entries[k].cron_id = k
            re.push(d.entries[k])
         })
         return re
     }
  })

  app.directive("mcCronListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'cron-list-and-add'),
        scope: { "server":"@", "whId":"@", "domain":"@", "display":"@",  },
        controller:  showErrorsController({
          inject: ["mcFilemanUrl", "mcDocrootHostentriesService","mcCronTransform"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcFilemanUrl, mcDocrootHostentriesService,mcCronTransform){

            $scope.monthNames = ["*","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            $scope.dayNames = ["*","Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

            $scope.cronTypes = ["url"]
            $scope.data = {}
            $scope.tmpExpr = {}
            $scope.tmp = {}

            $scope.baseUrl = mcFilemanUrl($scope.server, $scope.whId)+"/cron/"

            $scope.delete = function(cron){
               return MonsterCloud.delete($scope.baseUrl+cron.cron_id).then($scope.refetch)
            }

            $scope.urlPattern = /^https?:\/\/.+/;

            $scope.refetch = function(){
                return MonsterCloud.get($scope.baseUrl)
                  .then(function(d){
                     $scope.result = d

                     $scope.crontabs = mcCronTransform(d);
                  })

            }

            $scope.resetDaily = function(){
               $scope.tmpExpr = {
                  minute: 0,
                  hour: 5,
                  dom: '*',
                  month: '*',
                  dow: '*',
               }
            }
            $scope.resetHourly = function(){
               $scope.tmpExpr = {
                  minute: 0,
                  hour: '*',
                  dom: '*',
                  month: '*',
                  dow: '*',
               }
            }
            $scope.reset5min = function(){
               $scope.tmpExpr = {
                  minute: '*/5',
                  hour: '*',
                  dom: '*',
                  month: '*',
                  dow: '*',
               }
            }
            $scope.reset = function(){
              $scope.data = {}
              $scope.tmpExpr = {}
            }


            $scope.docrootChanged = function(){
               var p = "^https?:\/\/"+$scope.tmp.hostEntry.host+"\/"
               $scope.data.docroot_domain = $scope.tmp.hostEntry.domain
               console.log("regexp replaced", p)
               $scope.urlPattern = new RegExp(p)
               if(!$scope.data.data)
                 $scope.data.data = "http://"+$scope.tmp.hostEntry.host+"/"
            }

            $scope.buildCronExpression = function(){
              $scope.data.cronExpression = $scope.tmpExpr.minute+" "+
                $scope.tmpExpr.hour+" "+
                $scope.tmpExpr.dom+" "+
                $scope.tmpExpr.month+" "+
                $scope.tmpExpr.dow;
            }


            $scope.refetch()




            mcDocrootHostentriesService($scope.server, $scope.whId)
              .then(function(d){
                  $scope.hostentries = d
              })

          },
          fireCallback: function($scope, MonsterCloud, $q){

            $scope.buildCronExpression()

            return MonsterCloud.put($scope.baseUrl, $scope.data)
               .then(function(){
                  $scope.reset()
                  return $scope.refetch()
               })


          }

        })

     }

  }], {
     title: "Scheduled operations",
     menuKey: templateDirectory,
     paramsRequired: { "server": "@","whId": "@" },
     keywords: ["cron", "crontab"],
})



})(app)
