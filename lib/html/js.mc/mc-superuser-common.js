(function(app){

  var templateDirectory = "common-superuser"

  app.factory("serversFactory", ["MonsterCloud", "$q", function(MonsterCloud, $q) {
      var re = function(kind) {
         return MonsterCloud.get("/su/servers/"+(kind?kind:""))
      }

      re.all = function(){
         if((!MonsterCloud.UserProfile.isServerOwner)&&(!MonsterCloud.UserProfile.isSuperuser))
            return $q.when([]);
         return MonsterCloud.search("/su/servers/")
      }

      return re
  }])

app.factory('accountLookup', ["MonsterCloud", "$q", "mcMyGrants", function (MonsterCloud, $q, mcMyGrants) {
   var accountCache = {};
   return function(dataRows, user_id_field_name, user_name_field_name){
      if(!user_name_field_name) user_name_field_name = "u_name"
      var uidsHash = {}
      dataRows.forEach(function(row){
         var userIdCandidate = row[user_id_field_name];
         if(!userIdCandidate) return;
         var arrayMode = Array.isArray(userIdCandidate)
         // console.log("arraymode?", arrayMode)
         var userIdArray = arrayMode ? userIdCandidate : [userIdCandidate];         
         userIdArray.forEach(function(userId){
             var userNameFromCache = accountCache[userId];
             if(userNameFromCache) {
                // console.log("got it from cache", userId);
                if(arrayMode) {
                  if(!row[user_name_field_name])
                    row[user_name_field_name] = Array();
                  row[user_name_field_name].push(userNameFromCache);
                }
                else
                  row[user_name_field_name] = userNameFromCache;

                return;
             }
             uidsHash[userId] =  1;
         })

      })

      var uids = Object.keys(uidsHash)
      if(uids.length <= 0) return $q.when();

      var p;
      if(!MonsterCloud.UserProfile.isSuperuser)
      {
          // expected respoonse: {"16384":"Monster Media","16454":"4P Agro LXV Kft"}
          p = mcMyGrants().then(function(rows){

             // rows = {"u_id":123,"u_name":"Your referer","u_primary_email":"supervisor@1234.hu"},
             var re = {};
             rows.forEach(function(r){
                re[r.u_id] = r.u_name;
             });

             return re;
          })
      } 
      else
        p = MonsterCloud.post("/su/accountapi/accounts/lookup/id", {ids: uids});

      return p
        .then(function(re){
           dataRows.forEach(function(row){
              var userIdCandidate = row[user_id_field_name];
              if(!userIdCandidate) return;
              if(Array.isArray(userIdCandidate)) {

                row[user_name_field_name] = Array();
                userIdCandidate.forEach(function(userId){
                    row[user_name_field_name].push(re[userId] || userId);
                    if(!accountCache[userId]) {
                        accountCache[userId] = re[userId];
                        // console.log("cache now looks like", accountCache)
                    }

                })

              } else {
                var userId = userIdCandidate;
                row[user_name_field_name] = re[userId];
                if(!accountCache[userId]) {
                    accountCache[userId] = re[userId];
                    // console.log("cache now looks like", accountCache)
                }

              }

           })
        })
   }
}])

app.factory('webhostingLookup', ["MonsterCloud", "$q", function (MonsterCloud, $q) {
   var webhostingCache = {};
   return function(dataRows, server_name, server_field_name, webhosting_id_field_name, webhosting_name_field_name, webhosting_userid_field_name){
      if(!webhosting_name_field_name) webhosting_name_field_name = "wh_name"
      var whidsPerServer = {}
      dataRows.forEach(function(row){
         var aServer = server_name || row[server_field_name]
         var whId = row[webhosting_id_field_name];
         if((webhostingCache[aServer])&&(webhostingCache[aServer][whId])) {
            // console.log("got it from cache", aServer, whId, webhostingCache[aServer][whId]);
            var v = webhostingCache[aServer][whId];
            row[webhosting_name_field_name] = v.name;
            if(webhosting_userid_field_name)
               row[webhosting_userid_field_name] = v.user;
            return;
         }

         if(!whidsPerServer[aServer]) whidsPerServer[aServer] = {}
         whidsPerServer[aServer][whId] =  1
      })

      var servers = Object.keys(whidsPerServer)
      if(servers.length <= 0) return $q.when()

      var ps = []
      servers.forEach(function(server){
          ps.push(
            MonsterCloud.post("/s/"+server+"/su/webhosting/lookup/id", {ids: Object.keys(whidsPerServer[server])})
            .then(function(re){
               dataRows.forEach(function(row){
                  var aServer = server_name || row[server_field_name]
                  if(aServer != server) return
                  var whId = row[webhosting_id_field_name];
                  if(re[whId]) {
                    row[webhosting_name_field_name] = re[whId].name
                    if(webhosting_userid_field_name)
                      row[webhosting_userid_field_name] = re[whId].user

                    if(!webhostingCache[server]) webhostingCache[server] = {};
                    if(!webhostingCache[server][whId]) {
                      webhostingCache[server][whId] = re[whId];
                      console.log("webhosting cache now looks like this", webhostingCache);                      
                    }
                  }
               })
            })
          )
      })

      return $q.all(ps)

   }
}])

  app.directive("mcSuperuserServersList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'servers-list'),
        scope: { "control": "@", "kind": "@", "display": "@" },
        controller: ["$scope","serversFactory",function($scope, serversFactory){

           serversFactory($scope.kind).then(function(d){
              $scope.servers = d
           })

        }]
     }

  }])

  app.directive("mcSuperuserUserSelector", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'user-selector'),
        scope: { "form": "=", "data": "=", "callback": "=", "byRole": "@" },
        controller: ["$scope","MonsterCloud","$q","mcMyGrants",function($scope, MonsterCloud, $q, mcMyGrants){

              $scope.options = []
              $scope.onSelect = function($item, $model, $label) {
                 console.log("match selected", $item, $model, $label);
                 if($scope.callback)
                    $scope.callback($item, $model, $label);
              }
              $scope.getUser = function(value) {

                  var payload = {"where": value, limit: 10};

                  var p;
                  if(MonsterCloud.UserProfile.isSuperuser)
                  {
                      var url;
                      if($scope.byRole) {
                         url = "/su/accountapi/accounts/by-role/"+$scope.byRole;
                      } else {
                         url = "/su/accountapi/accounts/search";
                      }

                      p = MonsterCloud.post(url, payload);

                  }
                  else
                  {
                      // expected respoonse: [{"u_id": 1, "u_name": "username1"}]
                      p = mcMyGrants().then(function(rows){
                          return {accounts: rows};
                      })
                  }


                  return p.then(function(d){
                       $scope.options = d.accounts
                       return $scope.options;
                    })
              }

              $scope.formatLabel = function(model) {
                 for (var i=0; i< $scope.options.length; i++) {
                   if (model === $scope.options[i].u_id) {
                     return $scope.options[i].u_name;
                   }
                 }
              };

        }]
     }

  }])


  app.directive("mcSuperuserMain", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'main'),
        controller: ["$scope","serversFactory","mcRoutesConfig",function($scope, serversFactory, mcRoutesConfig){

           var mappingReference = {
              "database": ["mc-dbms-superuser-databases"],
              "tapi": ["mc-tapi-list"],
              "webhosting": [
                "mc-docroot-superuser-server",
                "mc-superuser-certificates",
                "mc-superuser-ftp-scoreboard",
                "mc-superuser-ftp-list",
                "mc-superuser-git-list",
                "mc-superuser-python-list",
                "mc-superuser-webhosting-list",
                "mc-superuser-iptables-bans",
                "mc-superuser-test-commander",
              ],
              "email": ["mc-superuser-email-accounts"],
              "installatron": ["mc-superuser-installatron"],
              "": ["mc-superuser-docker-containers", "mc-eventlog-superuser"],
           }

           $scope.serverNames
           $scope.mappings = {}
           $scope.controls = []
           var controlTmp = {}
           Object.keys(mappingReference).forEach(function(role){
              mappingReference[role].forEach(function(control){
                 if(controlTmp[control]) return
                 controlTmp[control] = true
                 $scope.controls.push(control)
              })
           })

           $scope.controlTitles = {}
           $scope.controls.forEach(function(control){
             var title = mcRoutesConfig.stateTemplates[control] ? mcRoutesConfig.stateTemplates[control].title : control
             $scope.controlTitles[control] = title
           })

           serversFactory.all().then(function(d){
              $scope.serverNames = d.map(function(l){return l.s_name})
              d.forEach(function(server){

                  server.s_roles.forEach(function(role){
                    processRoles(server, role)
                  })
                  processRoles(server, "")
              })

              // console.log("here.", $scope.serverNames, $scope.controls, $scope.controlTitles, $scope.mappings)
           })

           function processRoles(server, role){
                if(!mappingReference[role]) return
                mappingReference[role].forEach(function(control){
                   $scope.mappings[server.s_name+control] = true
                })
           }

        }]
     }

  }], {
    title: "Superuser servers list",
    paramsOptional:{  },
    menuKey: templateDirectory,
  })


  app.directive("mcSuperuserWie", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'wie'),

        controller: ["$scope","MonsterCloud","$q","serversFactory","$compile","$element","$task", "mcConfig",'mcTapiDnsRecordReplacer','mcStorageDisks',
         function($scope, MonsterCloud,$q,serversFactory, $compile, $element, $task, mcConfig,mcTapiDnsRecordReplacer, mcStorageDisks){
              $scope.backup = {completeBackup: true};
              $scope.restore = {subsystems:[]};
              $scope.szarRestorePossible = false;

               serversFactory().then(function(d){
                  $scope.servers = d
               })

              var operationInProgress = false;

              var lastBackupServer;
              var lastRestoreServer;
              function onCompleteBackupChange(restoreOnly){
                 var html;
                console.log("onCompleteBackupChange!", restoreOnly)
                adjustFeedingNeeded();
                 if($scope.backup.completeBackup) {
                   html = "";
                   $element.find("#completeBackup").html("");
                   $element.find("#completeRestore").html("");

                 }
                 else {

                     if(!restoreOnly) {
                        html = '<mc-global-webhosting-selector per-server="true" server="{{::backup.server}}" model="backup" ></mc-global-webhosting-selector>'
                        $element.find("#completeBackup").html($compile( html )( $scope ));

                     }

                     html = '<mc-global-webhosting-selector per-server="true" change="destWhChanged()" server="{{::restore.server}}" model="restore" ></mc-global-webhosting-selector>'
                     $element.find("#completeRestore").html($compile( html )( $scope ));

                 }

                 fetchSubsystems();
                 lastRestoreServer = $scope.restore.server;
              }

              $scope.destWhChanged = function(){
                 adjustFeedingNeeded();
              }

              $scope.completeBackupChange = function(){
                return onCompleteBackupChange();
              }
              $scope.restoreServerChange = function(){
                  fetchDisks();
                  return onCompleteBackupChange(true);
              }
              function fetchDisks() {
                  return mcStorageDisks($scope.restore.server)
                    .then(function(disks){
                       $scope.diskStorages = disks;
                    })                
              }
              $scope.backupServerChange = function(){

                 // console.log("CRRRRAP", $scope.restore.server, $scope.backup.server, lastBackupServer)
                 if((!$scope.restore.server)||($scope.restore.server == lastBackupServer)) {
                   $scope.restore.server = $scope.backup.server;
                   fetchDisks();
                 }
                 lastBackupServer = $scope.backup.server;

                 return onCompleteBackupChange();
              }

              $scope.dbFreshBackupRestoreChange = function(){
                 if($scope.restore.dbFreshBackupRestore) $scope.restore.dbRestoreLast = false;
              }
              $scope.dbRestoreLastChange = function() {
                 if($scope.restore.dbRestoreLast) $scope.restore.dbFreshBackupRestore = false;
              }

              function restoreDisabled(){
                  if(operationInProgress) return true;
                  if(!$scope.restore.server) return true;
                  if(!$scope.backup.completeBackup) {
                     if(!$scope.backup.webhosting_id) return true;
                     if((!$scope.restore.webhosting_id) && (!$scope.restore.createNewWebstore)) return true;
                     if(($scope.restore.createNewWebstore)&&(!$scope.restore.newWebstoreStorage)) return true;
                  } 
                  // we dont do this anymore, as the control could be used to simple transfer files only
                  //if($scope.restore.subsystems.length <= 0) return true;

                  if(($scope.szarRestorePossible) && ($scope.restore.restoreSzarFirst) && (!$scope.backup.szarCheckpoint)) return true;

                  return false;
              }
              function fetchBackupDisabled(){
                  if(operationInProgress) return true;
                  if(!$scope.backup.server) return true;
                  if((!$scope.backup.completeBackup) && (!$scope.backup.webhosting_id)) return true;

                  return false;
              }
              $scope.fetchDisabled = function(){
                  return fetchBackupDisabled();
              }
              $scope.backupDisabled = function(){
                  return fetchBackupDisabled();
              }
              $scope.importDisabled = function(){
                  return (fetchBackupDisabled()) || (restoreDisabled());
              }

              function fetchSubsystems(){
                if(lastRestoreServer == $scope.restore.server) return;
                return MonsterCloud.get("/s/"+$scope.restore.server+"/su/wie/config/subsystems/")
                  .then(function(d) {
                      $scope.subsystems = d;
                  })
              }

              function getUrlInt(destination, component, suffix) {
                 return "/s/"+$scope[destination].server+"/su/wie/"+component+"/"+suffix;
              }

              function getUrl(destination, component) {
                 return getUrlInt(destination, component, ($scope.backup.completeBackup ? "" : $scope[destination].webhosting_id));
              }

              function getBackupUrl(component) {
                return getUrl("backup", component);
              }
              function getRestoreUrl(component, webstore_id){
                if(component == "restore")
                   return getUrlInt("restore", component, webstore_id||"");
                return getUrl("restore", component);
              }


              $scope.fireBackup = function(){

                $scope.response = "...";
                operationInProgress = true;

                return MonsterCloud.post(getBackupUrl("backup"), {})
                  .then(function(d) {
                      $scope.response = d;
                      operationInProgress = false;
                  })

              }

              $scope.fireFetch = function(){

                $scope.response = "...";
                operationInProgress = true;

                return MonsterCloud.get(getBackupUrl("fetch"))
                  .then(function(d) {
                      $scope.response = "";
                      var blob = new Blob([JSON.stringify(d)], {type: "text/json;charset=utf-8"});
                      var ts = new Date().toISOString().split('T')[0];
                      saveAs(blob, $scope.backup.server+"-backup-"+ts+".json");
                      operationInProgress = false;
                  })

              }

              var checkpointCache = {};
              function adjustFeedingNeeded(){
                $scope.sameServer = ($scope.backup.server == $scope.restore.server);

                $scope.feedingNeeded =
                  (
                   (!$scope.sameServer)
                   ||
                   (
                      (!$scope.backup.completeBackup) && 
                      (
                         ($scope.backup.webhosting_id != $scope.restore.webhosting_id)
                         ||
                         ($scope.restore.createNewWebstore)
                      )
                      
                   )
                  );

                 // note these expressions are here by design:
                 $scope.webFilesFromSourcePossible = $scope.feedingNeeded;
                 // as of now, szarRestorePossible needs the very same logical decision as feedingNeeded
                 $scope.szarRestorePossible = ((!$scope.feedingNeeded) && (!$scope.restore.createNewWebstore));
                 $scope.szarWebRestorePossible = $scope.szarRestorePossible;


                 $scope.showDnsRecordsOptions = !$scope.sameServer;

                 if($scope.szarRestorePossible) {
                    var server = $scope.backup.server;
                    $scope.checkpoints = checkpointCache[server];
                    if(!$scope.checkpoints) {

                        MonsterCloud.post("/s/"+$scope.backup.server+"/su/webhosting/mc/szar/checkpoints")
                          .then(function(cps){
                             $scope.checkpoints = checkpointCache[server] = cps;
                          })

                    }

                 }

              }

              $scope.fireImport = function(){

                var restorePayload = {subsystems: angular.copy($scope.restore.subsystems) || []};

                var state = "kickoff";
                operationInProgress = true;

                var databasesToBeRestored = [];
                var webstoresToBeRestored;
                var aTargetWebstore;
                var aSrcWebstoreId;
                var restoreServerPrefix;
                var restoreUrlPrefix;
                var standardPerWebstoreSuffixMsg;
                var restoreDbFiles = [];
                var aDatabase;
                var readonlyRestoreIsNeeded;
                if(!$scope.backup.completeBackup)
                   webstoresToBeRestored = [ $scope.restore.createNewWebstore ? "todo-create" : $scope.restore.webhosting_id ];

                var new_server_ip;
                var old_server_ip;
                var servicePrimaryDomain = mcConfig.config.service_primary_domain;
                var newFullHost = $scope.restore.server +"."+ servicePrimaryDomain;
                var oldFullHost = $scope.backup.server +"."+ servicePrimaryDomain;

                const title = "WEBSTORE_RESTORE_TASK";
                var d = {
                   title: title,
                   abortOnly: true,
                   undeterministic: true,
                   urlFactory: function(index, appendText, lastPromiseResult) {
                      console.log("urlFactory was called", index)

                      var pureAdata = {
                         title: title
                      };
                      var aData = {
                         title: title,
                         mcDontDisplay: true,
                         mcTaskStatusNotNeeded: true,
                      }


                      if(state == "kickoff") {
                         appendText("Starting");

                         if(($scope.szarRestorePossible)&&($scope.restore.restoreSzarFirst))
                           state = "fetchSzarWieBackup";
                         else if($scope.feedingNeeded)
                           state = "fetchConfigForFeeding";
                         else if($scope.backup.completeBackup)
                           state = "fetchWebstoreListForCompleteRestore"; // we fetch this one as we are about to restore webstores one by one
                         else {
                           // we are restoring only one single webhosting in place, without any config sorcery
                           state = "dnsPrepare";
                         }

                        if((!$scope.backup.completeBackup)&&($scope.restore.createNewWebstore)) {

                           appendText("Creating new webstore on the target server "+ $scope.restore.server);

                           aData.prom = MonsterCloud.get("/s/"+$scope.backup.server+"/webhosting/"+$scope.backup.webhosting_id)
                             .then(function(srcData){
                                 var createPayload = angular.copy(srcData);
                                 Object.keys(createPayload).forEach(function(k){
                                    if([
                                      "wh_tally_mail_storage",
                                      "wh_tally_web_storage",
                                      "wh_tally_db_storage",
                                      "wh_tally_mail_storage_mb",
                                      "wh_tally_web_storage_mb",
                                      "wh_tally_db_storage_mb",
                                      "wh_tally_sum_storage_mb",
                                      "extras",
                                      "created_at",
                                      "updated_at",
                                      "wh_id",
                                      "wh_secretcode",
                                      "wh_is_readonly",
                                      "wh_is_expired",
                                      "template"
                                    ].indexOf(k) > -1) {
                                       delete createPayload[k];
                                    }
                                 });

                                 // will be created on the selected backend storage array
                                 createPayload.wh_storage = $scope.restore.newWebstoreStorage;

                                 return MonsterCloud.put("/s/"+$scope.restore.server+"/wizard/webhosting", createPayload);

                             })
                             .then(function(h){
                                webstoresToBeRestored = [h.wh_id];
                                $scope.restore.webhosting_id = h.wh_id;

                                appendText("Webstore created on target server "+ $scope.restore.server +" with id "+h.wh_id);

                             })


                        } else {
                           aData.prom = $q.when();
                        }

                        return $q.when(aData);

                      }

                      if(state == "processWebstoreListForCompleteRestore"){
                         webstoresToBeRestored = angular.copy(lastPromiseResult);
                         state = "dnsPrepare";
                      }



                      // ---------------------------------------------------------------

                      if(state == "fetchSzarWieBackup") {
                         appendText("Restoring wie configuration files from szar-backup for server: "+$scope.backup.server+($scope.backup.completeBackup? "": " (for the selected webstore)"));
                         state = $scope.backup.completeBackup ? "fetchWebstoreListForCompleteRestore" : "dnsPrepare";
                         return MonsterCloud.post("/s/"+$scope.backup.server+"/su/webhosting/wie/szar/restore/"+($scope.backup.completeBackup?"":$scope.backup.webhosting_id), {checkpoint: $scope.backup.szarCheckpoint})
                           .then(function(h){
                                pureAdata.url = "/s/"+$scope.backup.server+"/webhosting/tasks/"+h.id;
                                return pureAdata;
                           })

                      }                      
                      else if(state == "fetchConfigForFeeding") {
                         appendText("The destination server needs configuration to be fed.");
                         appendText("Fetching from the source: "+$scope.backup.server+" "+($scope.backup.completeBackup ? "(complete)" : "("+$scope.backup.webhosting_id+")" ));
                         aData.prom = MonsterCloud.get(getBackupUrl("fetch"));
                         state = "feedConfig";

                      }
                      else if(state == "feedConfig") {
                         if($scope.backup.completeBackup)
                            webstoresToBeRestored = Object.keys(lastPromiseResult);
                         appendText("Feeding destination ("+$scope.restore.server+") with wie configuration.");
                         aData.prom = MonsterCloud.post(getRestoreUrl("feed"), lastPromiseResult);
                         state = "dnsPrepare";
                      }
                      else if(state == "fetchWebstoreListForCompleteRestore"){
                         appendText("A complete restore was requested.");
                         appendText("Fetching list of webstores covered by wie configurations from: "+$scope.backup.server);
                         aData.prom = MonsterCloud.get(getBackupUrl("webstores"));
                         state = "processWebstoreListForCompleteRestore";
                      }
                      else if(state == "dnsPrepare") {

                        state = "restore";
                        if(($scope.showDnsRecordsOptions) && ($scope.restore.replaceDnsRecords)) {
                          appendText("Querying the DNS zone to learn the IP addresses");
                          aData.prom = MonsterCloud.get("/su/tapi/domain/"+mcConfig.config.service_primary_domain)
                            .then(function(r){
                                r.records.forEach(function(record){
                                   if(Array("A","A/PTR").indexOf(record.type) < 0) return;
                                   if(record.host == $scope.backup.server) {
                                      old_server_ip = record.ip;
                                   }
                                   else
                                   if(record.host == $scope.restore.server) {
                                      new_server_ip = record.ip;
                                   }
                                })

                                if(!new_server_ip) {
                                   throw new Error("IP address for the destination server not found: "+$scope.restore.server);
                                }

                                if(!old_server_ip) {
                                   throw new Error("IP address for the source server not found: "+$scope.backup.server);
                                }

                                appendText("IP addresses: "+old_server_ip+" -> "+new_server_ip);

                            })
                        }
                        else
                          aData.prom = $q.when();

                      }
                      else if(state == "restore") {
                        readonlyRestoreIsNeeded = false;
                        aTargetWebstore = webstoresToBeRestored.shift();
                        aSrcWebstoreId = $scope.backup.completeBackup ? aTargetWebstore : $scope.backup.webhosting_id;
                        restoreServerPrefix = "/s/"+$scope.restore.server+"/";
                        restoreUrlPrefix = restoreServerPrefix+"su/webhosting/"+aTargetWebstore+"/szar/restore/";

                        appendText("");
                        if(!aTargetWebstore) {
                          console.log("we are done!")
                          appendText("The opererations have finished successfully.");
                          return $q.when()
                        }

                        standardPerWebstoreSuffixMsg = " on server "+$scope.restore.server+" for webstore: "+aTargetWebstore;

                        appendText("Restore cycle"+standardPerWebstoreSuffixMsg);
                        appendText("(remaining in the queue: "+webstoresToBeRestored.length+")");


                        state = "restoreCleanWebFiles";
                        if(restorePayload.subsystems.length <= 0) {
                           appendText("Skipping restoration of wie configurations"+standardPerWebstoreSuffixMsg);
                           aData.prom = $q.when();
                        } else {
                           appendText("Restoring wie configuration"+standardPerWebstoreSuffixMsg);
                           aData.prom = MonsterCloud.post(getRestoreUrl("restore", aTargetWebstore), restorePayload);
                        }                        


                      }
                      else if(state == "restoreCleanWebFiles") {
                        state = "restoreWebFiles";
                        if(!$scope.restore.cleanWebFiles) {
                           appendText("Skipping cleaning of webstore files"+standardPerWebstoreSuffixMsg);
                           aData.prom = $q.when();
                        }else{
                           appendText("Cleaning of webstore files"+standardPerWebstoreSuffixMsg);
                           return cleanupLogic("web");
                        }
                      }
                      else if(state == "restoreWebFiles") {

                         state = "restoreDatabase";

                         if(($scope.feedingNeeded) &&($scope.restore.webFilesFromSource)) {

                            appendText("Transferring website files from the selected source.");
                            return doTransferFiles("web");

                         }
                         else if((!$scope.feedingNeeded) &&($scope.restore.webFilesFromSzar)) {

                            appendText("Restoring website files from szar backup.");
                            return restoreTaskTail(MonsterCloud.post(restoreUrlPrefix+"web", {dir: "/"}))

                         }
                         else {
                            appendText("Skipping restoration of webstorage files"+standardPerWebstoreSuffixMsg);
                            aData.prom = $q.when();
                         }

                      }
                      // this one is not in use as the databases would need to be recreated then once again (they are created by wie restoration by default)
                      else if(state == "restoreCleanupDatabase") {

                         state = "restoreDatabase";

                        if(!$scope.restore.dbWipeExisting) {
                           appendText("Skipping removal of existing databases"+standardPerWebstoreSuffixMsg);
                           aData.prom = $q.when();
                        }else{
                           appendText("Wiping existing databases"+standardPerWebstoreSuffixMsg);
                           return MonsterCloud.delete(restoreServerPrefix+"su/dbms/databases/"+aTargetWebstore)
                             .then(function(h){
                                pureAdata.url = restoreServerPrefix+"dbms/tasks/"+h.id;
                                return pureAdata;
                             })
                        }

                      }
                      else if(state == "restoreDatabase") {


                        if($scope.restore.dbRestoreLast) {
                           appendText("Restoring latest databases from /dbbackup"+standardPerWebstoreSuffixMsg);
                           aData.prom = listSourceDatabases();
                           state = "restoreDbRestoreLastListDbbackupFiles";
                        }
                        else if(($scope.feedingNeeded)&&($scope.restore.dbFreshBackupRestore)) {
                           appendText("Generating fresh database snapshots on source storage to restore them"+standardPerWebstoreSuffixMsg);
                           aData.prom = listSourceDatabases();
                           state = "restoreDbFreshBackupRestore";
                        }
                        else {
                           appendText("Skipping removal of existing databases"+standardPerWebstoreSuffixMsg);
                           aData.prom = $q.when();
                           state = "restoreCleanMailFiles";
                        }

                      }
                      else if(state == "restoreDbRestoreLastListDbbackupFiles"){
                           appendText("Listing files in /dbbackup"+standardPerWebstoreSuffixMsg);

                           aData.prom = MonsterCloud.post(restoreServerPrefix+"su/fileman/"+aTargetWebstore+"/list", {cwd: "/dbbackup"})
                             .then(function(list){
                                restoreDbFiles = list;
                             })

                           state = "restoreDbRestoreLast";

                      }
                      else if(state == "restoreDbRestoreLast"){

                        var aDatabaseObject = databasesToBeRestored.shift();
                        if(!aDatabaseObject) {
                          console.log("we are done with dbbackup based database restorations!")
                          appendText("Databases have been restored with snapshots found in /dbbackup.");
                          state = "restoreCleanMailFiles";
                          aData.prom = $q.when();
                          return $q.when(aData);
                        }

                        aDatabase = aDatabaseObject.db_database_name;

                        appendText("Restoring database "+aDatabase+" with brand new snapshot"+standardPerWebstoreSuffixMsg);
                        var snapshotFile = findLatestSnapshotForDatabase(aDatabase);
                        if(!snapshotFile)
                           return $q.reject({message: "Snapshot for database "+aDatabase+" not found!"});

                        appendText("Selected snapshot: "+snapshotFile);

                        // restore by file
                        return MonsterCloud.post(restoreServerPrefix+"su/fileman/"+aTargetWebstore+"/download", {src_file: "/dbbackup/" + snapshotFile})
                          .then(function(filemanTask){
                              return MonsterCloud.post(restoreServerPrefix+"su/dbms/databases/"+aTargetWebstore+"/"+aDatabase+"/import", {filename:snapshotFile, filemanTask: filemanTask.id});
                          })
                          .then(function(h){
                             pureAdata.url = restoreServerPrefix + "dbms/tasks/"+h.id;
                             return pureAdata;
                          })

                      }
                      else if(state == "restoreDbFreshBackupRestore") {

                        var aDatabaseObject = databasesToBeRestored.shift();
                        if(!aDatabaseObject) {
                          console.log("we are done with fresh database restorations!")
                          appendText("Databases have been restored with brand new snapshots.");
                          state = "restoreCleanMailFiles";
                          aData.prom = $q.when();
                          return $q.when(aData);
                        }

                        aDatabase = aDatabaseObject.db_database_name;

                        // do a fresh cycle
                        appendText("Restoring database "+aDatabase+" with brand new snapshot"+standardPerWebstoreSuffixMsg);

                        // restore by export/import chain
                        return MonsterCloud.post("/s/"+$scope.backup.server+"/su/dbms/databases/"+aSrcWebstoreId+"/"+aDatabase+"/export", {})
                          .then(function(dbmsTask){
                              return MonsterCloud.post(restoreServerPrefix+"su/dbms/databases/"+aTargetWebstore+"/"+aDatabase+"/import", {filename:aDatabase+".sql.gz", source_server: $scope.backup.server, dbmsTask: dbmsTask.id});
                          })
                          .then(function(h){
                             pureAdata.url = restoreServerPrefix + "dbms/tasks/"+h.id;
                             return pureAdata;
                          })

                      }
                      else if(state == "restoreCleanMailFiles") {

                         state = "restoreMailFiles";

                        if(!$scope.restore.cleanMailFiles) {
                           appendText("Skipping cleaning of mail files"+standardPerWebstoreSuffixMsg);
                           aData.prom = $q.when();
                        }else{
                           appendText("Cleaning of mail files"+standardPerWebstoreSuffixMsg);
                           return cleanupLogic("mail");
                        }

                      }
                      else if(state == "restoreMailFiles") {

                         state = "restoreReadonly";

                         if(($scope.feedingNeeded) &&($scope.restore.mailFilesFromSource)) {

                            appendText("Transferring of mail files from the selected source.");
                            return doTransferFiles("mail");

                         }
                         else if((!$scope.feedingNeeded) &&($scope.restore.mailFilesFromSzar)) {

                            appendText("Restoring of mail files from szar backup.");
                            return restoreTaskTail(MonsterCloud.post(restoreUrlPrefix+"mail", {dir: "/"}))

                         }
                         else {
                            appendText("Skipping restoration of mail files"+standardPerWebstoreSuffixMsg);
                            aData.prom = $q.when();
                         }

                      }
                      else if(state == "restoreReadonly") {

                         state = "restoreReadonly2";

                         if($scope.restore.restoreReadonly) {

                            appendText("Querying the read-only status of the old storage");
                            aData.prom = MonsterCloud.get("/s/"+$scope.backup.server+"/su/fileman/"+aSrcWebstoreId+"/isreadonly")
                              .then(function(isReadOnly){
                                 if(!isReadOnly) {
                                   readonlyRestoreIsNeeded = false;
                                   appendText("The source storage is not read-only, proceeding");
                                   return; 
                                 }

                                 readonlyRestoreIsNeeded = true;

                              })


                         }
                         else {
                            appendText("Skipping querying of readonly status"+standardPerWebstoreSuffixMsg);
                            aData.prom = $q.when();
                         }

                      }
                      else if(state == "restoreReadonly2") {

                         state = "restoreWebstoreDomainBindings";

                         if(readonlyRestoreIsNeeded) {
                             appendText("Setting the new storage as read-only");

                             return MonsterCloud.post(restoreServerPrefix+"su/fileman/"+aTargetWebstore+"/toreadonly", {})
                               .then(function(r){
                                   pureAdata.url = restoreServerPrefix+"fileman/tasks/"+r.id;
                                   return pureAdata;                                    
                               })

                         } else {
                            appendText("Skipping restoration of the readonly status");
                            aData.prom = $q.when();

                         }


                      }
                      else if(state == "restoreWebstoreDomainBindings") {

                         state = "restoreDnsRecords";

                         if($scope.restore.webstoreDomainBindings) {

                            appendText("Querying the webstore-domain bindings of the old storage");
                            aData.prom = MonsterCloud.post("/su/accountapi/webhostingdomains/by-wh/", {wd_server: $scope.backup.server, wd_webhosting: aSrcWebstoreId})
                              .then(function(bindings){

                                 var newBindings = angular.copy(bindings);
                                 newBindings.forEach(function(b){
                                    b.wd_server = $scope.restore.server;
                                    b.wd_webhosting = aTargetWebstore;
                                 });

                                 appendText("Setting the domain bindings on the new webstore");

                                 return MonsterCloud.put("/su/accountapi/webhostingdomains/mass", newBindings)
                              })


                         }
                         else {
                            appendText("Skipping restoration of webstore domain bindings"+standardPerWebstoreSuffixMsg);
                            aData.prom = $q.when();
                         }

                      }
                      else if(state == "restoreDnsRecords") {

                         state = "restore"; // jump back to the first state and kick off restoration of the next selected webstore

                         if(($scope.showDnsRecordsOptions) &&($scope.restore.replaceDnsRecords)) {

                            appendText("Replacing DNS records to point to the new server.");
                            appendText("Querying domain names that belong to this webstore...");

                            aData.prom = MonsterCloud.get(restoreServerPrefix+"webhosting/"+aTargetWebstore+"/domains")
                              .then(function(adomains){
                                 var domains = [];
                                 adomains.forEach(function(d){
                                   domains.push({kind: "domain", name: d});
                                 })
                                 return mcTapiDnsRecordReplacer(domains, [
                                   {dnsRecordType: ["A","A/PTR"], field_name: "ip", old_value: old_server_ip, new_value: new_server_ip},
                                   {dnsRecordType: ["MX"], field_name: "mailserver", old_value: servicePrimaryDomain, new_value: newFullHost},
                                   {dnsRecordType: ["MX"], field_name: "mailserver", old_value: oldFullHost, new_value: newFullHost},
                                   {dnsRecordType: ["CNAME"], field_name: "cname", old_value: oldFullHost, new_value: newFullHost},
                                   {dnsRecordType: ["CNAME"], field_name: "cname", old_value: servicePrimaryDomain, new_value: newFullHost},
                                   {dnsRecordType: ["CLONE"], field_name: "parentHost", old_value: $scope.backup.server, new_value: $scope.restore.server},
                                 ], 
                                 {
                                  appendText: appendText, 
                                  robust: true
                                 })


                              })
                              .then(function(stats){
                                  if(stats.recordsReplaced > 0)
                                      appendText("Replaced "+stats.recordsReplaced+" records altogether.");

                                  appendText("DNS query operation ready.");
                              })

                         }
                         else {
                            appendText("Skipping adjustment of DNS records"+standardPerWebstoreSuffixMsg);
                            aData.prom = $q.when();
                         }


                      }

                      return $q.when(aData);

                      function findLatestSnapshotForDatabase(dbName){
                         var re;
                         var r = new RegExp("(.+)-([0-9]+)\.sql(\.gz)?$");
                         restoreDbFiles.forEach(function(dbRow){
                            var fn = dbRow.name;
                            var m = r.exec(fn);
                            if(!m) return;
                            if(m[1] != dbName) return;
                            if((!re)|| (fn > re)) re = fn;
                         })

                         return re;
                      }


                       function listSourceDatabases(){
                          appendText("Listing databases on source storage");
                          return MonsterCloud.get("/s/"+$scope.backup.server+"/su/dbms/databases/"+aSrcWebstoreId)
                            .then(function(dbs){
                                databasesToBeRestored = angular.copy(dbs);
                            })
                       }

                       function doTransferFiles(path_category) {

                                return MonsterCloud.post("/s/"+$scope.backup.server+"/su/fileman/"+aSrcWebstoreId+"/tgz-all", {path_category: path_category})
                                   .then(function(h){
                                      var dstUrl = restoreServerPrefix + "fileman/"+aTargetWebstore+"/untgz/transfer";

                                      return MonsterCloud.doRequest({
                                        url:dstUrl,
                                        data:{
                                          source_server: $scope.backup.server,
                                          source_download_id: h.id,
                                          dst_dir_path: "/",
                                          path_category: path_category,
                                        }
                                      });
                                   })
                                   .then(function(h){
                                      pureAdata.url = restoreServerPrefix + "tasks/"+h.id; // no fileman in the url here, since this task is relayed by the relayer service via eventemitter
                                      return pureAdata;
                                   })

                       }

                       function cleanupLogic(category){

                               return MonsterCloud.post(restoreServerPrefix+"su/fileman/"+aTargetWebstore+"/cleanup", {path_category: category})
                                 .then(function(r){
                                    pureAdata.url = restoreServerPrefix+"fileman/tasks/"+r.id;
                                    return pureAdata;
                                 })
                       }

                       function restoreTaskTail(p){
                          return p.then(function(r){
                                 pureAdata.url = restoreServerPrefix+"webhosting/tasks/"+r.id;
                                 return pureAdata;
                              })
                       }
                   }

                }

                console.log("triggering undeterministic task chain", d)

                return $task(d)
                  .then(function(){
                      operationInProgress = false;
                  })
              }




        }]

     }

  }], {
    title: "Webstore config import/export and file transfer",
    paramsOptional: {},
    menuKey: templateDirectory,
    keywords: ["Webstore config", "import","export", "wie"]
  })


  app.directive("mcSuperuserWieMail", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'wie-mail'),

        controller:  showErrorsController({
          inject:["serversFactory",'mcConfig', "$compile","$element","$task"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, serversFactory,mcConfig,$compile,$element,$task){
              $scope.operationInProgress = false;
              $scope.notification = {complete:true};

               serversFactory().then(function(d){
                  $scope.servers = d
               })

              var previousFullHostname="";
              $scope.serverChange = function(){
                  var fullHostname = $scope.notification.server+ "."+ mcConfig.config.service_primary_domain;

                  // console.log("hey!", fullHostname, "X", previousFullHostname, "Y", $scope.notification.old_server)

                  if((!$scope.notification.old_server)||($scope.notification.old_server == previousFullHostname))
                      $scope.notification.old_server = fullHostname;
                  if((!$scope.notification.current_server)||($scope.notification.current_server == previousFullHostname))
                      $scope.notification.current_server = fullHostname;

                  previousFullHostname = fullHostname;

                  $scope.completeChange();
              }

              var previousMysqlUrl = "";
              var previousRoundcubeUrl = "";
              function mysqlRoundcubeUrls(fullHostname){
                  var phpMyAdminUrl = "https://mysql."+fullHostname;
                  var roundCubeUrl = "https://webmail."+fullHostname;

                  if((!$scope.notification.phpmyadmin_url)||($scope.notification.phpmyadmin_url == previousMysqlUrl))
                      $scope.notification.phpmyadmin_url = phpMyAdminUrl;

                  if((!$scope.notification.roundcube_url)||($scope.notification.roundcube_url == previousRoundcubeUrl))
                      $scope.notification.roundcube_url = roundCubeUrl;

                  previousMysqlUrl = phpMyAdminUrl;
                  previousRoundcubeUrl = roundCubeUrl;

              }

              $scope.newServerChange=function(){
                 mysqlRoundcubeUrls($scope.notification.new_server);
              }

              $scope.futureServerChange=function(){
                 mysqlRoundcubeUrls($scope.notification.future_server);
              }

              $scope.completeChange = function(){
                 console.log("onCompleteChange!");
                 if($scope.notification.complete) {
                   $element.find("#complete").html("");
                   // delete $scope.dataForm.webhosting_selector;
                 }
                 else {

                    html = '<mc-global-webhosting-selector per-server="true" server="{{::notification.server}}" model="notification" complete-model="true" ></mc-global-webhosting-selector>'
                    $element.find("#complete").html($compile( html )( $scope ));
                 }

              }


          },
          fireCallback: function($scope, MonsterCloud, $q, serversFactory, mcConfig, $compile,$element,$task){
                $scope.operationInProgress = true;

                var awh;
                var emailAccountsToProcess;
                var webstoresToProcess;
                var scheduledTemplateStr = ($scope.notification.finished?"done":"scheduled");
                var serverPrefix = "/s/"+$scope.notification.server+"/su/";
                var payload;
                var state = "kickoff";
                const title = "WEBSTORE_RELOCATION_EMAIL_NOTIFICATION_TASK";
                var d = {
                   title: title,
                   abortOnly: true,
                   undeterministic: true,
                   urlFactory: function(index, appendText, lastPromiseResult) {
                       var aData = {
                         title: title,
                         mcDontDisplay: true,
                         mcTaskStatusNotNeeded: true,
                       }
                       if(state == "kickoff") {
                          appendText("Starting");
                          if($scope.notification.complete) {
                             appendText("Fetching list of webstores from: "+$scope.notification.server);
                             aData.prom = MonsterCloud.get(serverPrefix+"webhosting/")
                               .then(function(webstores){
                                  webstoresToProcess = angular.copy(webstores);
                               })
                             ;
                          } else {
                            webstoresToProcess = [$scope.notification.webhosting];
                            aData.prom = $q.when();
                          }
                          state = "process";
                       }
                       else if(state == "process"){
                         appendText("");
                         awh = webstoresToProcess.shift();
                         if(!awh){
                            console.log("we are done!")
                            appendText("The opererations have finished successfully.");
                            return $q.when()
                         }

                         appendText("Processing webstore: "+awh.wh_name+" ("+awh.wh_id+")");
                         appendText("(remaining in the queue: "+webstoresToProcess.length+")");

                         appendText("Sending the main email");

                          state = "queryEmailAccounts";

                          payload = {
                             target_webstore_id: awh.wh_id,
                             target_webstore_name: awh.wh_name,
                             business_justification: $scope.notification.business_justification,
                             business_justification: $scope.notification.business_justification,
                             phpmyadmin_url: $scope.notification.phpmyadmin_url,
                             roundcube_url: $scope.notification.roundcube_url,
                          };
                          if(!$scope.notification.finished) {
                             payload.current_server = $scope.notification.current_server;
                             payload.future_server = $scope.notification.future_server;
                             payload.schedule = $scope.notification.schedule_str+ " ("+ $scope.notification.schedule.toISOString()+")";
                          }else{
                             payload.old_server = $scope.notification.old_server;
                             payload.new_server = $scope.notification.new_server;
                          }
                          aData.prom = MonsterCloud.post(serverPrefix+"mailer/sendmail", {
                             "toAccount": { user_id: awh.wh_user_id, emailCategory: "TECH" },
                             "template": "webhosting/relocating-"+scheduledTemplateStr,
                             "context": payload,
                          });
                       }
                       else if(state == "queryEmailAccounts") {
                          state = "processEmailAccounts";
                          appendText("Querying individual mailboxes for webstore "+awh.wh_id);
                          aData.prom = MonsterCloud.get(serverPrefix+"email/webhostings/"+awh.wh_id+"/accounts")
                            .then(function(res){
                               emailAccountsToProcess = angular.copy(res.accounts);
                            })                             
                       }
                       else if(state == "processEmailAccounts") {
                          var aemailaccountArr = emailAccountsToProcess.shift();
                          if(!aemailaccountArr) {
                            state = "process"; // next webstore
                            aData.prom = $q.when();
                            return $q.when(aData);
                          }
                          var aemailaccount = aemailaccountArr.ea_email;

                          appendText("Sending e-mail notification to "+aemailaccount);

                          payload.target_email_address = aemailaccount;
                          delete payload.phpmyadmin_url;
                          delete payload.roundcube_url;

                          aData.prom = MonsterCloud.post(serverPrefix+"mailer/sendmail", {
                             "localeAsAccount": awh.wh_user_id,
                             "to": aemailaccount,
                             "template": "webhosting/relocating-email-account-"+scheduledTemplateStr,
                             "context": payload,
                          });
                       }
                       return $q.when(aData);
                   }

                }

                console.log("triggering undeterministic task chain", d)

                return $task(d)
                  .then(function(){
                      $scope.operationInProgress = false;
                  })
          }
        })


     }

  }], {
    title: "E-mail notification about webstore relocations",
    paramsOptional: {},
    menuKey: templateDirectory,
    keywords: ["transfer", "relocation", "wie"]
  })



  app.directive("mcSuperuserSyncDatabase", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'sync-database'),

        controller:  showErrorsController({
          inject:["serversFactory"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, serversFactory){

              $scope.microservices=["tapi","accountapi"];
              $scope.backup = {microservice: "tapi"};

              $scope.servers = {};

              $scope.microservices.forEach(function(k){
                 serversFactory(k).then(function(re){
                    $scope.servers[k] = re;
                 });
              });


          },
          fireCallback: function($scope, MonsterCloud, $q, serversFactory){

              $scope.result = "";

              return MonsterCloud.post("/su/sync/"+$scope.backup.microservice, {src: $scope.backup.sourceServer})
                .then(function(re){
                    $scope.result = re;
                })

          }
        })


     }

  }], {
    title: "Synchronizing database between microservices",
    paramsOptional: {},
    permissions: "SUPERUSER",
    menuKey: templateDirectory,
    keywords: ["tapi", "accountapi", "tinydns", "djbdns"]
  })



  app.directive("mcSuperuserTimemachine", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'timemachine'),
        controller: ["$scope","MonsterCloud","$timeout", function($scope, MonsterCloud, $timeout){

           var baseUrl = "/s/"+$scope.server+"/su/timemachine/";
           var timestamps;
           var latestTimeout;

           $scope.minTs = 0;
           MonsterCloud.get(baseUrl+"list")
             .then(function(d){
                timestamps = d;
                $scope.maxTs = timestamps.length - 1;
                $scope.currentTsI = $scope.maxTs;
                $scope.sliderChange();
             })

           $scope.sliderChange = function(){
              $scope.currentTs = timestamps[$scope.currentTsI];
              $scope.currentTsIso = new Date($scope.currentTs * 1000).toISOString();
              // $scope.content = "...";

              if(latestTimeout) {
                $timeout.cancel(latestTimeout);
              }

              latestTimeout = $timeout(function(){
                latestTimeout = null;
                return MonsterCloud.get(baseUrl + "fetch/"+$scope.currentTs)
                  .then(function(c){
                      $scope.content = c;
                  })
              }, 500)

           }

        }]
     }

  }], {
    title: "Time machine",
    paramsRequired:{ "server": "@" },
    menuKey: templateDirectory,
    keywords: ["scoreboard"]
  })




  app.directive("mcSuperuserSimpleConfigEditor", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'simple-editor'),
        scope: {
           showConfig: "@",
           url: "@",
        },
        controller: showErrorsController({
          inject: ["$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q){
              $scope.operationInProgress = false;

              $scope.config = {};
              $scope.candidate = {};

              if($scope.showConfig) {
                 MonsterCloud.post($scope.url+"/config/defaults")
                   .then(function(d){
                      $scope.config.defaults = d;
                      return MonsterCloud.post($scope.url+"/config/file")
                   })
                   .then(function(d){
                      $scope.config.file = d;
                   })
              }


              $scope.edit = function(filename){
                 return MonsterCloud.post($scope.url+"/fetch", {filename: filename})
                   .then(function(d){
                      $scope.candidate.data = d.data;
                      $scope.candidate.filename = filename;
                   })
              }

              $scope.delete = function(filename){
                 return MonsterCloud.post($scope.url+"/delete", {filename: filename})
                   .then(function(d){
                      return $scope.refetch();
                   })
              }

              $scope.refetch = function(){
                 return MonsterCloud.post($scope.url+"/list")
                   .then(function(d){
                      return $scope.files = d;
                   })

              }


              $scope.showSaveAsNew = function(){
                if(!$scope.candidate.filename) return true;
                if(!$scope.files) return true;
                return $scope.files.indexOf($scope.candidate.filename) < 0;
              }

              $scope.showEdit = function(){
                if(!$scope.candidate.filename) return false;
                if(!$scope.files) return false;
                return $scope.files.indexOf($scope.candidate.filename) > -1;
              }

              return $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
              return MonsterCloud.post($scope.url+"/save", $scope.candidate)
                .then(function(){
                   return $alertSuccess();
                })
          }
        })


     }

  }])




})(app)

