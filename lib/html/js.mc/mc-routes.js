(function(angular){

    var templateDirectory = "route"

    var mcRoutes = angular.module("mc-routes", ["mc-lng"])
      .provider("mcRoutesConfig", [function(){
         this.registeredTemplates = { }

         this.registerStateTemplate = function(stateName, options) {
           // console.log("registering template", stateName, options)
           var snake_state_name = snake_case(stateName)
           // options could be template, paramsRequired, paramsOptional, title, heading, keywords
           // console.error('"'+options.title+'":"'+options.title+'",')
           this.registeredTemplates[snake_state_name] = options
         }
         this.$get = [function(){
            return { stateTemplates: this.registeredTemplates }
         }]

         // this.registerStateTemplate("mc-route-test", {title: "mc Route test", heading:"Hello mc state route!"})
      }])
      .service("mcRouteSearch", ['mcRoutesConfig','lngService','$q','$location',function(mcRoutesConfig, lngService, $q, $location){
         var translatedResult = {};
         var translatedProm = {};
         var re = function(searchPhrase, discardWhereParamsMissing, skipSuperuser){
            if(!skipSuperuser) skipSuperuser = false;

            return getTranslatedRoutes(skipSuperuser)
              .then(function(translatedRoutes){
                  var currentParams = $location.search()

                  var options = {
                    shouldSort: true,
                    tokenize: true,
                    matchAllTokens: true,
                    threshold: 0.6,
                    location: 0,
                    distance: 100,
                    maxPatternLength: 32,
                    minMatchCharLength: 1,
                    keys: [ "stateName", "title", "heading", "keywords" ]
                  };
                  var fuse = new Fuse(translatedRoutes, options); // "list" is the item array
                  var x = fuse.search(searchPhrase);
                  var re = []


                  x.forEach(function(translatedState){
                      var state = mcRoutesConfig.stateTemplates[translatedState.stateName]
                      if(state.paramsRequired){
                        translatedState.paramsMissing = false
                        Object.keys(state.paramsRequired).some(function(requiredParamName){
                           if(!currentParams[requiredParamName]) {
                              translatedState.paramsMissing = true
                              return true
                           }
                        })
                      }
                      if((!discardWhereParamsMissing)||(!translatedState.paramsMissing))
                        re.push(translatedState)
                  })


                  re.sort(function(a, b){
                    if(a.paramsMissing == b.paramsMissing) return 0
                    return a.paramsMissing ? 1 : -1
                  });

                  return $q.when(re)
              })
         }
         return re


         function getTranslatedRoutes(skipSuperuser){
            if(translatedResult[skipSuperuser])
              return $q.when(translatedResult[skipSuperuser])
            if(!translatedProm[skipSuperuser])
               translatedProm[skipSuperuser] = lngService.then(function(){
                  var translated = []
                  Object.keys(mcRoutesConfig.stateTemplates).forEach(function(stateName){
                     var state = mcRoutesConfig.stateTemplates[stateName]
                     var a = {stateName: stateName,menuKey: state.menuKey}
                     Array("title","heading","keywords").forEach(function(k){
                        if(!state[k]) return
                        if(Array.isArray(state[k])) {
                           a[k] = []
                           state[k].forEach(function(w){
                              a[k].push(lngService.translate(w))
                           })
                        } else {
                           a[k] = lngService.translate(state[k])
                        }

                     })
                     translated.push(a)
                  })

                  translatedProm[skipSuperuser] = null
                  translatedResult[false] = translated

                  // now lets filter them based on superuserness
                  translatedResult[true] = [];
                  translated.forEach(function(st){
                    console.log(st);
                     if((!st.menuKey)||(!isSuperuserMenuKey(st.menuKey)))
                       translatedResult[true].push(st);
                  })

                  return $q.when(translatedResult[skipSuperuser])
              })

            return translatedProm[skipSuperuser]
         }
      }])
      .service("mcRoutesService", ["$compile","$rootScope","$location",'mcRoutesConfig','$window','lngService',function($compile, $rootScope, $location, mcRoutesConfig, $window,lngService){
         var opRestriction = "ng"
         var targetViews = {}
         var lastUrl
         var lastHash
         var wasInNgMode

         var current_state;
         var current_params;

         function isSameUrl(newUrl){
            var p = newUrl.indexOf("#")
            var newHash = ""
            if(p > -1) {
               newUrl = newUrl.substr(0, p)
               newHash = newUrl.substr(p)
            }
            /*
            console.log("absurl", $location.absUrl())
            console.log("url", $location.url())
            */
            if((newUrl == lastUrl))//&&(newHash != lastHash))
               return true
            lastUrl = newUrl
            lastHash = newHash
            return false
         }

         function setPageTitle(p){

            return lngService.then(function(){
                var stateName = p.stateName;
                var snake_state_name = snake_case(stateName)
                var state = mcRoutesConfig.stateTemplates[snake_state_name]
                if(!state) return stateName;
                if(!state.title) return stateName;
                return lngService.translate(state.title) || state.title;              
            })
            .then(function(title){
                $rootScope.pageTitle = (p.stateParams.whId ? p.stateParams.whId+"@" : "") +  (p.stateParams.server ? p.stateParams.server+": " : "") + title;
            })
         }



         $rootScope.$on("$locationChangeSuccess", function(event, p){
            if($location.skipChangeOnce) {
              delete $location.skipChangeOnce
              return
            }
            if(isSameUrl(p))
              return
            console.log("!!!!!!! $locationChangeSuccess", event, p, location ? true : false)
            re.renderStateByLocation()
         })

         var extraParams = {}

         var defaultView;
         var viewFilter = [];
         var mcNoBreadcrumbs = {};

         var alreadyBound = false;
         var stateChangeCallbacks = [];

         var lastChildScope;

         var re = {

            setSearch: function(p, newState) {
               re.addExtraParams(p);
               var x = angular.extend({}, p, {op: opRestriction}, {state: newState});
               $location.search(x);
            },

            setFilter: function(newViewFilter){
               viewFilter = newViewFilter;
            },
            setDefaultView: function(viewName){
               defaultView = viewName;
            },          
            bindView: function($element, name, mcNoBreadcrumb){
               if(!name) name = ""
               if(targetViews[name]) console.warn("View with the same name already bound, overriding")
               targetViews[name] = $element;
               
               mcNoBreadcrumbs[name] = mcNoBreadcrumb;

               if((!name)&&(!current_state)&&(!alreadyBound)) {
                 alreadyBound = true;
                 return re.renderStateByLocation();
               }
            },
            renderStateByLocation: function(){

               var s = getLocationSearch();
               console.log("location parameters are", s, defaultView)
               if((opRestriction)&&(s.op != opRestriction)) {
                  console.log("URL is not in ng mode")
                  if(wasInNgMode) {
                     console.log("but it was, meaning an ng site was just left for a legacy one via navigation controls")
                     $window.location.reload();
                     return;
                  }
               }


               if(!s.state) {
                  if(!defaultView) {
                     console.log("Default view was not configured, so leaving the view as is");
                     return;
                  }

                  s.state = defaultView;              
               }


               var stateName = s.state
               delete s.state
               delete s.op

               if((viewFilter.length > 0)&&(0 > viewFilter.indexOf(stateName))) {
                  console.log("state not supported in this view");
                  if(defaultView)
                     re.jumpToState({stateName: defaultView});
                  return;
               }

               try{
                  return re.renderState({stateName: stateName, stateParams: s})
               }catch(ex){
                  console.error("Unable to render state", ex)
               }
            },
            renderState: function(p){
               var template = commonChecks(p)

               var element = targetViews[p.targetView]
               if(!element) {
                 console.error("Target view not (yet?) found")
                 return;
               }

               setPageTitle(p);

               var childScope = (p.parentScope||$rootScope).$new(true)
               childScope.stateParams = p.stateParams

               var el = $compile( template )( childScope );
               element.html( el );

               current_state = p.stateName ? snake_case(p.stateName) : "???"
               current_params = p.stateParams;
               wasInNgMode = true;

               stateChangeCallbacks.forEach(function(cb){
                  cb(p);
               })

               if((lastChildScope) && (!p.parentScope)) {
                  console.log("destroying previous scope");
                  lastChildScope.$destroy();
               }

               lastChildScope = childScope;

            },
            getCurrentState: function(){
              return angular.extend({stateName: current_state, stateParams: current_params}, mcRoutesConfig.stateTemplates[current_state])
            },
            addExtraParams: function(p){
               if(typeof p == "object") {
                 Object.keys(p).forEach(function(k){
                    if(typeof p[k] == "undefined")
                       delete p[k];
                 })
               }
               angular.extend(extraParams, p)

               if($) {
                 var extraStr = paramsToSearchString(extraParams, true)
                 if(!extraStr) {
                   console.log("not appending anything, the extra params are worthless")
                   return
                 }
                 console.log("adding extrastr:", extraStr)

                 $('[mc-sref]').each(function(){
                     var oldHref = $(this).attr("href")
                     if(!oldHref) {
                        console.log("Skip adding extraParams, no href set yet (will be done by the angular directive)", this)
                        return
                     }
                     console.log("appending url",  oldHref)
                     $(this).attr("href", oldHref + "&" + extraStr)
                 })
               }

            },
            /*
            // not supported anymore, href calculation would be screwed
            setExtraParams: function(p){
               extraParams = p || {}
            },
            */
            calculateSearch: function(p, keepCurrentParams){
              commonChecks(p)

              if(!p.stateName) throw new Error("destination state was not specified")

              var re = {state: p.stateName}
              if(opRestriction)
                  re.op = opRestriction
              if(keepCurrentParams)
                  p.stateParams = getLocationSearch()

              // we need some special treatment for booleans:
              Object.keys(p.stateParams).forEach(function(pname){
                 var v = p.stateParams[pname]
                 var t = typeof v
                 // console.log("XXX", pname, v, t)
                 if(t == "boolean"){
                   if(v)
                     p.stateParams[pname] = "true"
                   else
                     delete p.stateParams[pname]
                 }

              })
              
              var filteredStateParams = {};
              Object.keys(p.stateParams).forEach(function(n){
                 var v = p.stateParams[n];
                 if(typeof v == "undefined") return;
                 filteredStateParams[n] = v;
              });
              var search = angular.extend({}, extraParams, filteredStateParams, re)

              return search
            },
            calculateHrefFromSearch: function(search, wantFullUrl) {
                  var searchStr = paramsToSearchString(search)
                  var fullUrl = $location.absUrl()
                  var path = $location.path()
                  var fullNewUri = path + "?" + searchStr

                  if(!wantFullUrl) return fullNewUri

                  var regexp = /^(https?:\/\/.+?)\//
                  var m = regexp.exec(fullUrl)

                  if((m)&&(m[1])) {
                     var fullNewUrl = m[1] + fullNewUri
                     return fullNewUrl
                  }

                  // else undef
            },
            calculateHref: function(p, keepCurrentParams, wantFullUrl) {
               var search = re.calculateSearch(p, keepCurrentParams)
               return re.calculateHrefFromSearch(search, wantFullUrl)

            },
            registerStateChangeCallback: function(callback){
                stateChangeCallbacks.push(callback);
            },
            jumpToState: function(p, keepCurrentParams){

              var search = re.calculateSearch(p, keepCurrentParams)

              console.log("setting location search to", search, p.targetView)
              lastUrl = ""
              if(p.targetView =="_blank") {
                  var fullNewUrl = re.calculateHrefFromSearch(search, true)

                  if(fullNewUrl) {
                     return $window.open(fullNewUrl)
                  }
                  return false
              }
              else {
                  console.log("jumpToState changing search parameters", search);
                  $location.search(search)
                  if(!$rootScope.$$phase)
                     $rootScope.$apply()
              }
            }
         }
         return re

         function paramsToSearchString(p, skipUndefined){
            var re = []
            Object.keys(p).forEach(function(x){
                var v = p[x]
                if((skipUndefined)&&(typeof v == "undefined"))
                   return
                re.push(encodeURIComponent(x)+"="+encodeURIComponent(v))
            })
            return re.join("&")
         }

         function getLocationSearch(){
            return angular.copy($location.search())
         }

         function commonChecks(p){
             if(!p.stateParams) p.stateParams = {}
             if(!p.targetView) p.targetView = ""

             console.log("commonCheck", p)

             var t = ""
             if(p.stateName) {
                console.log("trying to resolve", p.stateName)
                var snake_state_name = snake_case(p.stateName)
                var state = mcRoutesConfig.stateTemplates[snake_state_name]
                if(state)
                {
                   console.log("state found", state)

                   if((state.menuKey)&&(!mcNoBreadcrumbs[p.targetView])) {
                      t += "<mc-menu-breadcrumb></mc-menu-breadcrumb>"
                   }

                   if(state.heading)
                      t += "<h3>"+state.heading+"</h3>"

                   if(state.template)
                      t += state.template
                   else
                   {
                      var params = ""
                      Array(state.paramsRequired,state.paramsOptional).forEach(function(c){
                        if(!c) return
                        Object.keys(c).forEach(function(camelName){

                           var snake_name = snake_case(camelName)
                           var type = c[camelName]
                           var valueRef = "stateParams['"+camelName+"']"
                           if(type == "@") {
                             if(typeof p.stateParams[camelName] == "undefined")
                               return // skipping this one

                             value = "{{::"+valueRef+"}}"
                           }
                           else if(type == "=")
                             value = valueRef
                           else if((type == "&")&&(c == state.paramsOptional)) {
                             return; // lets skip this one
                           }
                           else
                             throw new Error("Invalid parameter type")

                           params += " "+snake_name+"=\""+value+"\""
                        })
                      })
                      t += "<"+snake_state_name+params+"></"+snake_state_name+">"
                   }

                   console.log("redirecting", snake_state_name, t)

                }
             }
             else
               t = p.template

             if(!t)  throw new Error("Template could not be resolved");

             return t
         }

      }])

      .directive("mcStateServer", [function(){
          return {
            restrict: "E",
            scope: {},
            template: "<span ng-if='server'>: {{server}}</span>",
            controller: ["$scope", "mcRoutesService", function($scope, mcRoutesService){
                mcRoutesService.registerStateChangeCallback(rebuild);
                rebuild(mcRoutesService.getCurrentState());

                function rebuild(p){
                    $scope.server = (p.stateParams || {}).server;
                }
                
            }]
         }
      }])



      .directive("mcIfRouteParams", ['ngIfDirective','mcRoutesService',function(ngIfDirective,mcRoutesService){

        var ngIf = ngIfDirective[0];

        return {
          transclude: ngIf.transclude,
          priority: ngIf.priority,
          terminal: ngIf.terminal,
          restrict: ngIf.restrict,
          link: function($scope, $element, $attr) {
            var expectedParams = $attr.mcIfRouteParams.split(",");

            $attr.ngIf = function() {
              var p = mcRoutesService.getCurrentState();
              var currentParams = p.stateParams || {};
              var found = 0;
              expectedParams.forEach(function(expectedParam){
                  // console.log("comparing", expectedParam, "inside", currentParams )
                  if(currentParams[expectedParam]) {
                    found++;
                  }
              });

              // console.log("being evaluated", currentParams, expectedParams, found);
              
              if (found < expectedParams.length) return false;

              if(typeof $attr.mcIfRegularWebhosting == "undefined") return true;

              var superuserCurrently = isSuperuserMenuKey(p.menuKey);

              return !superuserCurrently;

            };
            ngIf.link.apply(ngIf, arguments);
          }
        };

      }])

      .directive("mcView", ["mcRoutesService", function(mcRoutesService){
          return {
            restrict: "A",
            scope: {"mcView":"@","mcViewDefault":"@","mcAllowedViews":"@", "mcNoBreadcrumb": "@"},
            link: function(scope, elem, attrs){
                var viewFilter = (scope.mcAllowedViews ? scope.mcAllowedViews.split("\|") : []);
                mcRoutesService.setFilter(viewFilter);
                if(scope.mcViewDefault) {
                   mcRoutesService.setDefaultView(scope.mcViewDefault);
                }
                mcRoutesService.bindView(elem, scope.mcView, scope.mcNoBreadcrumb);
            }
         }
      }])

      .controller("mcMenuController", ["$scope", "mcRoutesService", "mcRoutesConfig", "$attrs", "mcUserSession", function($scope,mcRoutesService,mcRoutesConfig, $attrs, mcUserSession){

                var currentState
                var currentMenuKey
                var currentParams

                mcRoutesService.registerStateChangeCallback(rebuild);

                $scope.jump = function(key){
                   mcRoutesService.jumpToState({stateName:key}, true)
                }

                rebuild()

                function hasAllParams(candidateState){
                   var candidateParams = candidateState.paramsRequired || {}
                   var allmatched = true
                   Object.keys(candidateParams).some(function(candidateParamName){
                      if(!currentParams[candidateParamName]) {
                         allmatched = false
                         return true
                      }
                   })

                   return allmatched
                }

                function rebuild(){
                    $scope.hasAnything = false
                    currentState = mcRoutesService.getCurrentState() || {}
                    currentMenuKey = currentState.menuKey || ""


                    if(typeof $attrs.menuSuperuserMode != "undefined")
                      $scope.isSuperuser = $attrs.menuSuperuserMode == "true";
                    else
                      $scope.isSuperuser = ((!currentMenuKey) || (isSuperuserMenuKey(currentMenuKey)));

                    currentParams = currentState.paramsRequired || {}

                    var all = {}
                    Object.keys(mcRoutesConfig.stateTemplates).forEach(function(stateCandidateName){
                        var stateCandidate = mcRoutesConfig.stateTemplates[stateCandidateName]
                        if(!stateCandidate.menuKey) return

                        if((stateCandidate.menuKey != currentMenuKey)&&(!$scope.showOthers)){
                           if((!$scope.isSuperuser)||(!isSuperuserMenuKey(stateCandidate.menuKey)))
                             return
                        }


                        if(!hasAllParams(stateCandidate)) return

                        if(stateCandidate.permissions) {
                           if(!mcUserSession.hasAnyRole(stateCandidate.permissions))
                              return;
                        }

                        if(!all[stateCandidate.menuKey])
                          all[stateCandidate.menuKey] = {}

                        all[stateCandidate.menuKey][stateCandidateName] = stateCandidate.title
                    })

                    $scope.hasAnything = sumOptions(all) > 1

                    if(currentState) {
                      $scope.currentTitle = currentState.title
                      $scope.current = all[currentMenuKey]
                      delete all[currentMenuKey]

                    }

                    $scope.permissions = "SERVER_OWNER"

                    if(Object.keys(all).length > 0)
                       $scope.others = all

                }

                function sumOptions(all){
                   var re = 0
                   Object.keys(all).forEach(function(k){
                     re += Object.keys(all[k]).length
                   })
                   return re
                }

            }])

      .directive("mcSb2SuperuserMenu", ["templateDir",function(templateDir){
          return {
            restrict: "A",
            templateUrl: templateDir(templateDirectory,'sb2-superuser-menu'),
            controller: "mcMenuController"
          };
      }])

      .directive("mcMenuBreadcrumb", ["templateDir", function(templateDir){
          return {
            restrict: "E",
            scope: {showOthers:"@"},
            templateUrl: templateDir(templateDirectory, "menu-breadcrumb"),
            controller: "mcMenuController"
         }
      }])

      .directive("mcStateSearch", ["templateDir",function(templateDir){
          return {
            restrict: "E",
            scope: {targetView:"@", "skipSuperuser": "@", "discardWhereParamsMissing": "@"},
            templateUrl: templateDir(templateDirectory, "state-search"),
            controller: ["$scope", "mcRoutesService", "mcRouteSearch", function($scope,mcRoutesService, mcRouteSearch){
               $scope.onSelect = function($item, $model, $label){
                 console.log("onSelect", $item, $model, $label)
                 if($item.paramsMissing) return false
                 var d = angular.copy($item)
                 if($scope.targetView)
                    d.targetView = $scope.targetView
                 mcRoutesService.jumpToState(d, true)
                 $scope.stateSearch = ""
                 return false
               }
               $scope.getStates = function(searchText){
                 return mcRouteSearch(searchText, $scope.discardWhereParamsMissing, $scope.skipSuperuser)
               }
            }]
         }
      }])

      Array("mcSref","mcState").forEach(function(c){
          mcRoutes.directive(c, ["mcRoutesService",function(mcRoutesService){
              var re = {
                restrict: "A",
                scope: {},
                link: function(scope, elem, attrs) {

                  var p = {stateName:scope.mcSref, stateParams: scope.mcSrefParams, targetView: scope.mcSrefTarget, parentScope: scope.mcSrefTarget ? scope : null}

                  var href = (c == "mcSref" ? mcRoutesService.calculateHref(p) : "")
                  attrs.$set("href", href);

                  elem.bind('click', function() {
                     console.log("clicked!", p)

                     mcRoutesService[c == "mcSref" ? "jumpToState" : "renderState"](p)

                     // this is needed in order to close some bootstrap submenus:
                     if($) {
                       $('li').removeClass("open");
                       $('.dropdown-backdrop').remove();
                     }

                     return false
                  });

                }
             }
             re.scope[c] = "@"
             re.scope[c+"Params"] = "="
             re.scope[c+"Target"] = "@"
             return re
          }])

      })

    var originalAngularModule = angular.module
    angular.module = function(){
       var module = originalAngularModule.apply(this, arguments)
       var originalDirective = module.directive
       module.directive = function(directiveName, directiveDetails, stateParams){
          if(!stateParams)
             return originalDirective(directiveName, directiveDetails)

          var originalCallback
          var customDirectiveDetails
          if(Array.isArray(directiveDetails)) {
             originalCallback = directiveDetails[directiveDetails.length-1]
             customDirectiveDetails = directiveDetails
             customDirectiveDetails[customDirectiveDetails.length-1] = newCallback
          } else {
             originalCallback = directiveDetails
             customDirectiveDetails = newCallback
          }

          var directiveScope
          if(!stateParams.paramsShared)
            directiveScope = angular.extend({}, stateParams.paramsRequired, stateParams.paramsOptional)

          module.config(['mcRoutesConfigProvider',  function(mcRoutesConfigProvider) {

                  mcRoutesConfigProvider.registerStateTemplate(
                    directiveName,
                    stateParams
                  )
          }])

          return originalDirective(directiveName, customDirectiveDetails)

          function newCallback(){
            var r = originalCallback.apply(this, arguments)

            if((!r.scope)&&(directiveScope))
               r.scope = directiveScope

            return r
          }
        }

       return module

    }

       function snake_case(name, separator) {
         if(!name) return;
         var SNAKE_CASE_REGEXP = /[A-Z]/g;
         separator = separator || '-';
         return name.replace(SNAKE_CASE_REGEXP, function(letter, pos) {
           return (pos ? separator : '') + letter.toLowerCase();
         });
       }

        function isSuperuserMenuKey(menuKey) {
           return menuKey.indexOf("-superuser") > -1
        }


})(angular)
