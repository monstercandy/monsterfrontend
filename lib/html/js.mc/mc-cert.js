(function(app){

  var templateDirectory = "cert"

  app.service("mcPurchasesUrl", function(){
     return function(server, pcId, superuser){
        return "/s/"+server+(superuser?"/su": "")+"/cert/purchases/my"+(pcId?"/item/"+pcId:"")+"/"

     }
  })
  app.service("mcCertUrl", function(){
     return function(server, whId, superuser){
        return "/s/"+server+(superuser?"/su": "")+"/cert/certificates/"+(whId ? "by-webhosting/"+whId : "my") + "/"

     }
  })
  app.service("mcCertContactsUrl", function(){
     return function(server){
        return "/s/"+server+"/cert/contacts/my/"

     }
  })

  app.service("mcCertTask", ["$task",function($task){
    return function(server, title, prom, extraParams){
      return prom.then(function(h){
       var taskUrl = "/s/"+server+"/cert/tasks/"+h.id
        return $task(angular.extend({}, {title:title, "url":taskUrl}, extraParams))
      })
    }
  }])

  app.service("mcRawCertificateInfo", ["mcCertUrl","MonsterCloud",function(mcCertUrl,MonsterCloud){
     var re = function(server, certId, whId, superuser){
        var url = re.Url(server, certId, whId, superuser)
        return MonsterCloud.get(url)
     }

     re.Url = function(server, certId, whId, superuser){
        return mcCertUrl(server, whId, superuser) +"item/"+certId
     }

     return re
  }])

  function certificateParser(MonsterCloud, baseUrl, $q, data) {
        return MonsterCloud.post(baseUrl+"certificates", {certificatesText: data})
            .then(function(data){
               var re = []
               data.forEach(function(c){
                  var a = {}
                  Array("Subject","Issuer","X509v3 Subject Alternative Name").forEach(function(k){
                    a[k] = (c[k] || {}).text|| "(Missing)"
                  })
                  Array("Not After","Not Before").forEach(function(k){
                    a[k] = (c[k] || {}).iso || "(Missing)"
                  })
                  re.push(a)
               })
               return $q.when({display:re, full: data})
            })
  }

   Array(
    {
      "n": "mcCertCsr",
      "t": "c-csr",
      "fn_name": "chgCsr",
      "key": "csr",
      "l": function(MonsterCloud, baseUrl, $q, data){
          return MonsterCloud.post(baseUrl+"certificate-signing-request", {csr: data})
            .then(function(data){
               return $q.when({display:data.Subject.text, full: data})
            })
      },
    },
    {
      "n": "mcCertCertificate",
      "t": "c-certificate",
      "fn_name": "chgCert",
      "key": "certificate",
      "l": certificateParser
    },
    {
      "n": "mcCertPrivateKey",
      "t": "c-private-key",
      "fn_name": "chgPrivateKey",
      "key": "privateKey",
      "l": function(MonsterCloud, baseUrl, $q, data){
          return MonsterCloud.post(baseUrl+"private-key", {privateKeyText: data})
            .then(function(res){
               return Promise.resolve({display:res, full: res})
            })
      }
    },
    {
      "n": "mcCertIntermediate",
      "t": "c-intermediate",
      "fn_name": "chgInter",
      "key": "intermediate",
      "l": certificateParser
    }
   ).forEach(function(x){

        app.directive(x.n, ["templateDir", function(templateDir){
            return {
                restrict: "E",
                templateUrl: templateDir(templateDirectory, x.t),
                // shared scope!
                controller: ["$scope", "$timeout", "MonsterCloud", "$q", function($scope, $timeout, MonsterCloud, $q){
                   var chgProm
                   var chgLastValue
                   var baseUrl = "/s/"+$scope.server+"/cert/info/"
                   $scope[x.fn_name] = function(){
                      if(chgProm)
                         $timeout.cancel(chgProm)

                      Array("info", "infoFull").forEach(function(x){
                        if(!$scope[x])
                            $scope[x] = {}
                        else
                           delete $scope[x][x.key]
                      })

                      if(!$scope.cert) return
                      if(!$scope.cert[x.key]) return

                      var a = $scope.cert[x.key].trim()

                      if((!chgLastValue)||(a != chgLastValue)) {
                        chgLastValue = a

                        chgProm = $timeout(function(){
                           return x.l(MonsterCloud, baseUrl, $q, a)
                             .then(function(res){
                                 console.log("returneed", res)
                                 $scope.infoFull[x.key] = res.full
                                 $scope.info[x.key] = res.display

                                 if($scope["on_change_"+x.key])
                                   $scope["on_change_"+x.key]($scope.info[x.key], $scope.infoFull[x.key], a)
                             })
                        }, 1000)
                      }
                   }
                }]

            }
        }])


   })


app.directive("mcDocrootCertificateRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'docroot-certificate-row'),
      scope: { "server":"@","whId":"@","domain": "@" },
      controller: ["$scope", "MonsterCloud", "mcDocroot", "mcWebhostingFactory",
                   'mcRawCertificateInfo', 'mcRoutesService', 'mcCertTask',
        function($scope, MonsterCloud, mcDocroot, mcWebhostingFactory,
            mcRawCertificateInfo, mcRoutesService, mcCertTask){

          $scope.openCert = function(){
              var p = {stateName: "mcCertInfo", stateParams: $scope.certSrefParams}
              console.log("doing the jumpState", p)

              return mcRoutesService.jumpToState(p)
          }

          $scope.letsencryptOn = function(){
             $scope.inProgress = true
             var p = MonsterCloud.put("/s/"+$scope.server+"/cert/purchases/my/letsencrypt", {
                    // domains:[mainDomain,"www."+mainDomain],
                    wh_id: $scope.whId,
                    attach: {server: $scope.server, webhosting_id: $scope.whId, docroot_domain: $scope.domain}
                  });
             return mcCertTask($scope.server, "LETSENCRYPT_ENROLLMENT_IN_PROGRESS", p)
               .then(function(){
                  $scope.inProgress = false
                  return refetch(true)
               })

          }

          return refetch()

          function refetch(force){
            return mcDocroot.Lookup($scope.server, $scope.whId,  $scope.domain, force)
              .then(function(result){
                $scope.lookupFinished = true
                $scope.docroot = result

                if(result.certificate_id)
                  return mcRawCertificateInfo($scope.server, result.certificate_id, $scope.whId, $scope.superuser)
                   .then(function(cert){
                      $scope.cert = cert
                      $scope.certSrefParams = {cId: cert.c_id, server: $scope.server, whId: $scope.whId}

                      // console.log("cert is", cert, $scope.certSrefParams)
                   })

                return mcWebhostingFactory($scope.server, $scope.whId).Lookup()
                  .then(function(wh){
                    // console.log(wh)
                    if((wh)&&(wh.template)&&(wh.template.t_x509_certificate_letsencrypt_allowed))
                      $scope.letsencryptCanBeActivated = true
                  })

              })

          }

      }]
   }

}])

  app.directive("mcCertListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'list-and-add'),
        controller:  showErrorsController({
          inject:["mcCertUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcCertUrl){

               $scope.baseUrl = mcCertUrl($scope.server, $scope.whId)


               $scope.delete = function(cert){

                  // the backend cert server will submit a request to the docroot subsystem to deassign it from the web server
                  return MonsterCloud.post($scope.baseUrl+"item/"+cert.c_id+"/disable",{})
                    .then(function(){
                        return $scope.refetch()

                    })

               }


               $scope.refetch = function(){

                    return MonsterCloud.get($scope.baseUrl)
                      .then(function(r){
                         $scope.result = r
                         $scope.certs = r.certificates;
                      })

               }

               $scope.reset = function(){
                  $scope.cert = {}
                  $scope.info = {}
               }


               $scope.reset()
               return $scope.refetch()


          },
          fireCallback: function($scope, MonsterCloud, $q){

             return MonsterCloud.put($scope.baseUrl, $scope.cert)
               .then(function(){

                  $scope.reset()

                  return $scope.refetch()

               })


          }
        })
     }

  }], {
       title:"Manage certifciates",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@"},
       paramsOptional: { "whId":"@"},
  })

  app.directive("mcCertInfoHead", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'info-head'),
        // shared scope!
        controller: ["$scope","mcRawCertificateInfo", "mcDocrootCertificateQuery", "mcRoutesService", function($scope, mcRawCertificateInfo, mcDocrootCertificateQuery, mcRoutesService){
               $scope.baseUrl = mcRawCertificateInfo.Url($scope.server, $scope.cId, $scope.whId, $scope.superuser)

               $scope.keysToShow = [
                 "c_id",
                 "c_product",
                 "c_alias",
                 "c_issuer_organization",
                 "c_issuer_common_name",
                 "c_subject_common_name",
                 "c_subject_organization_unit",
                 "c_begin",
                 "c_end",
                 "c_webhosting",
                 "c_comment",
               ]

               function jumpTo(stateName){
                  var re = {server:$scope.server,cId:$scope.cId}
                  if($scope.data.c_webhosting)
                     re.whId=$scope.data.c_webhosting
                  if($scope.superuser)
                    re.superuser = true

                   return mcRoutesService.jumpToState({stateName:stateName,stateParams: re})
               }

               $scope.view = function() {
                   return jumpTo("mc-cert-info")
               }
               $scope.edit = function() {
                   return jumpTo("mc-cert-mod")
               }


               $scope.cert = {}

                  return mcRawCertificateInfo($scope.server, $scope.cId, $scope.whId, $scope.superuser)
                    .then(function(data){
                        $scope.data = data

                        var a_wh_id = $scope.whId || data.c_webhosting
                        if((a_wh_id)&&(a_wh_id != "0"))
                          return mcDocrootCertificateQuery($scope.server, a_wh_id, $scope.superuser)
                        else
                          return Promise.resolve()
                    })
                    .then(function(docrootCerts){
                        $scope.docrootCerts = docrootCerts
                        if((docrootCerts)&&($scope.onResolved))
                          return $scope.onResolved()
                    })
        }]

     }

  }])


  app.directive("mcCertInfo", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'info'),
        controller:["$scope", function($scope){
            $scope.viewMode = true
            $scope.download = function(data, ext) {
               var fullFilename = $scope.data.c_subject_common_name+ext;
               var d = (typeof data != "string") ? data = data.join("\n") : data;
               var blob = new Blob([d], {type: "text/json;charset=utf-8"});
               saveAs(blob, fullFilename);
            }
        }]
     }

  }], {
       title:"Certificate info",
       menuKey: templateDirectory,
       paramsRequired: { "cId":"@", "server":"@"},
       paramsOptional: { "whId":"@", "superuser":"@"},
  })

  app.directive("mcPurchaseActivationInfo", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'activation-info'),
        controller:["$scope", "mcPurchasesUrl", "MonsterCloud", function($scope, mcPurchasesUrl, MonsterCloud){
               var baseUrl = mcPurchasesUrl($scope.server, $scope.pcId)
               return MonsterCloud.get(baseUrl+"activation")
                 .then(function(d){
                    $scope.activation = d
                 })
        }]
     }

  }], {
       title:"Certificate activation info",
       menuKey: templateDirectory,
       paramsRequired: { "pcId":"@", "server":"@"},
  })



  app.directive("mcCertMod", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'mod'),
        controller:  showErrorsController({
          inject:["mcDocrootCertificateQuery","mcRoutesService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcDocrootCertificateQuery, mcRoutesService){


               $scope.x = {}

               $scope.copyDest = {}

               $scope.copy = function(){
                console.log("hey, here", $scope.copyDest)
                  if(!$scope.copyDest.webhosting_id) return;

                  $scope.isCopyInProgress = true;
                  return MonsterCloud.post($scope.baseUrl+"/copy", $scope.copyDest)
                    .then(function(cert){
                       $scope.isCopyInProgress = false;
                       return mcRoutesService.jumpToState({
                         stateName:"mc-cert-mod",
                         stateParams:{
                          server:$scope.copyDest.server,
                          cId: cert.c_id,
                          whId: $scope.copyDest.webhosting_id
                         }
                       })
                    })

               }

               $scope.certDetach = function(){
                  return MonsterCloud.delete("/s/"+$scope.server+($scope.superuser?"/su":"")+"/docrootapi/certificates/"+$scope.cId)
                    .then(function(){

                       if(!$scope.docrootCerts[""]) $scope.docrootCerts[""] = []
                       $scope.docrootCerts[""] = $scope.docrootCerts[""].concat($scope.docrootCerts[$scope.cId])
                       delete $scope.docrootCerts[$scope.cId]

                    })
               }
               $scope.certAttach = function(){
                  var domain = $scope.x.docrootToBeAttachedTo
                  // console.log("crapshit!?", domain)
                  if(!domain) return
                  return MonsterCloud.post("/s/"+$scope.server+($scope.superuser?"/su":"")+"/docrootapi/docroots/"+$scope.hasWebhostingId+"/"+domain+"/certificate", {certificate_id: $scope.cId})
                    .then(function(){
                        if(!$scope.docrootCerts[$scope.cId]) $scope.docrootCerts[$scope.cId] = []
                        $scope.docrootCerts[$scope.cId].push(domain)

                        var index = $scope.docrootCerts[""].indexOf(domain)
                        if(index > -1) $scope.docrootCerts[""].splice(index, 1)
                    })
               }

               $scope.onResolved = function(){
                   $scope.hasWebhostingId = $scope.whId || $scope.data.c_webhosting
                   if(($scope.docrootCerts[''])&&($scope.docrootCerts[''].indexOf($scope.data.c_subject_common_name) > -1))
                      $scope.x.docrootToBeAttachedTo = $scope.data.c_subject_common_name

               }


          },
          fireCallback: function($scope, MonsterCloud, $q){

            if((!$scope.cert.privateKey)&&(!$scope.cert.intermediate))
               return

            var ps = []
             if($scope.cert.privateKey)
               ps.push(MonsterCloud.put($scope.baseUrl+"/private-key", {privateKey: $scope.cert.privateKey}))
             if($scope.cert.intermediate)
               ps.push(MonsterCloud.put($scope.baseUrl+"/intermediates", {intermediates: $scope.cert.intermediate}))

             return $q.all(ps)
               .then(function(){
                  if($scope.cert.privateKey)
                     $scope.data.c_private_key = 1
                  if($scope.cert.intermediate)
                     $scope.data.c_intermediate_chain = 1

                  $scope.cert = {}
               })


          }
        })
     }

  }], {
       title:"Modify certificate",
       menuKey: templateDirectory,
       paramsRequired: { "cId":"@", "server":"@"},
       paramsOptional: { "whId":"@", "superuser":"@"},
  })


  app.directive("mcCertActivationGeneric", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'activation-generic'),
      }

  }], {
       title:"Certificate activation",
       menuKey: templateDirectory,
       paramsRequired: { "cId":"@", "server":"@"},
       paramsOptional: { "whId":"@"},
  })





  app.directive("mcCertContactListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'contact-list-and-add'),
        controller:  showErrorsController({
          inject:["mcCertContactsUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcCertContactsUrl){

               $scope.baseUrl = mcCertContactsUrl($scope.server)

               MonsterCloud.get("/s/"+$scope.server+"/cert/config/countries")
                 .then(function(data){
                    $scope.countries = data
                 })


               $scope.delete = function(contact){

                  return MonsterCloud.delete($scope.baseUrl+"item/"+contact.co_id,{})
                    .then(function(){
                        return $scope.refetch()

                    })

               }
               $scope.select = function(contact){
                  if(!$scope.selectMode) return

                  $scope.selectedCoId = contact.co_id

                  if($scope.selectCallback) {
                     console.log("calling callback with", contact)
                     $scope.selectCallback({contact:contact})
                  }
               }
               $scope.selectForModify = function(contact){
                  $scope.selectedCoId = contact.co_id
                  $scope.contact = contact
               }


               $scope.refetch = function(){
                  return MonsterCloud.get($scope.baseUrl)
                    .then(function(r){
                       $scope.contacts = r.contacts;
                    })
               }

               $scope.reset = function(){
                  $scope.contact = {}
                  $scope.selectedCoId = 0
                  $scope.$broadcast('show-errors-reset');
               }

               $scope.saveAsNew = function(){
                  $scope.kind = "new"
                  return $scope.fire()
               }
               $scope.modify = function(){
                  $scope.kind = "modify"
                  return $scope.fire()
               }


               $scope.reset()
               return $scope.refetch()


          },
          fireCallback: function($scope, MonsterCloud, $q){

             var data = angular.copy($scope.contact)
             return MonsterCloud.put($scope.baseUrl, data)
             .then(function(x){
              data.co_id = x.co_id

                 if($scope.kind=="modify")
                       return MonsterCloud.delete($scope.baseUrl+"item/"+$scope.selectedCoId, {})

                 return $q.when()
             })
             .then(function(){

                $scope.reset()

                return $scope.refetch()

             })
             .then(function(){

                $scope.select(data)

             })

          }
        })
     }

  }], {
       title:"Manage certifciate contacts",
       menuKey: templateDirectory,
       paramsRequired: { "server":"@"},
       paramsOptional: { "selectMode":"@", "selectCallback": "&"},
  })


  app.directive("mcCertPurchases", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'purchases'),
        controller:  ["$scope", '$q', "MonsterCloud", "mcPurchasesUrl",'mcRoutesService', 'mcWebhostingServices', 'mcConfig',
         function($scope, $q, MonsterCloud, mcPurchasesUrl, mcRoutesService, mcWebhostingServices, mcConfig){



               $scope.doAction = function(purchase, action){
                  if(action == "view-certificate")
                    return mcRoutesService.jumpToState({stateName:"mc-cert-info",stateParams: {server:purchase.server,cId:purchase.pc_certificate}})
                  if(action == "view-activation")
                    return mcRoutesService.jumpToState({stateName:"mc-purchase-activation-info",stateParams: {server:purchase.server, pcId: purchase.pc_id}})

                  mcRoutesService.jumpToState({stateName:"mc-cert-action-"+action,stateParams: {server:purchase.server,pcId:purchase.pc_id, pcSubjectCommonName: purchase.pc_subject_common_name}})

               }

               $scope.refetch = function(){

                  var serversCheckedAlready = {};
                  var mcCertPurchasesCount = 0; 

                  return ((!$scope.server) ? mcWebhostingServices() : $q.when([{w_server_name: $scope.server}]))
                    .then(function(whList){
                        var servers = whList.map(function(x){return x.w_server_name;})
                        var ps = [];
                        $scope.purchases = [];

                        if((mcConfig.config.certPurchaseServer)&&(servers.indexOf(mcConfig.config.certPurchaseServer)))
                           servers.push(mcConfig.config.certPurchaseServer);

                        servers.forEach(function(serverName){
                           var baseUrl = mcPurchasesUrl(serverName, $scope.superuser);
                           if(serversCheckedAlready[serverName]) return;
                           serversCheckedAlready[serverName] = true;

                           ps.push(
                            MonsterCloud.get(baseUrl)
                              .then(function(r){
                                 var purchases = angular.copy(r.purchases);
                                 mcCertPurchasesCount += purchases.length;
                                 purchases.forEach(function(purchase){
                                    var actions = [];
                                    purchase.server = serverName;
                                    purchase.actions.forEach(function(action){
                                       var a = action.split("-")
                                       a.shift()
                                       var r = a.join("-")
                                       actions.push([r, action])
                                    })

                                    if(purchase.pc_certificate)
                                       actions.push(["view-certificate", "view-certificate"])
                                    if(purchase.pc_activation)
                                       actions.push(["view-activation", "view-activation"])

                                    // console.log("hey", actions)
                                    purchase.actions = actions

                                    $scope.purchases.push(purchase);
                                 })

                             })
                           )
                           return $q.all(ps);
                        })
                    })
                    .then(function(){
                        if($scope.$parent)
                           $scope.$parent.mcCertPurchasesCount = mcCertPurchasesCount;

                    })


               }
               return $scope.refetch()

        }]
     }

  }], {    
       title:"Purchased certificates",
       menuKey: templateDirectory,
       paramsRequired: { },
       paramsOptional: { "server":"@", "checkboxMode":"@","noHead":"@"},
  })

  app.service("mcBackToCertPurchases", ["mcRoutesService","mcCertTask",function(mcRoutesService,mcCertTask){
     return function(server, promise, asTask, jumpToStateParams){

        if(asTask)
          promise = mcCertTask(server, "CERTIFICATE_OPERATION_IN_PROGRESS", promise)

        return promise.then(function finisher(){
            if(jumpToStateParams)
              return mcRoutesService.jumpToState(jumpToStateParams);

            return mcRoutesService.jumpToState({stateName:"mc-cert-purchases", stateParams:{server:server}})
        });
     }
  }])

  app.service("mcCertActivationLocal", ["mcPurchasesUrl","MonsterCloud","mcBackToCertPurchases",function(mcPurchasesUrl, MonsterCloud, mcBackToCertPurchases){
     return function($scope) {
           var url = mcPurchasesUrl($scope.server, $scope.pcId)+"activate/task"
           var payLoad = {local:true,attach:{server: $scope.model.server, webhosting_id:$scope.model.webhosting_id,docroot_domain: $scope.model.docroot_domain}}

           return mcBackToCertPurchases($scope.server, MonsterCloud.post(url, payLoad), true)
     }
  }])
  app.service("mcCertReissueLocal", ["mcPurchasesUrl","MonsterCloud","mcBackToCertPurchases",function(mcPurchasesUrl, MonsterCloud, mcBackToCertPurchases){
     return function($scope) {
             var url = mcPurchasesUrl($scope.server, $scope.pcId)+"reissue/task"

             return mcBackToCertPurchases($scope.server,MonsterCloud.post(url, {}), true)

     }
  }])

  app.directive("mcCertActivateLocal", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'activate-local'),
        // scope: shared!
        controller: ["$scope", function($scope){
            $scope.model = {}
        }]
     }

  }])

  Array("mcCertActionVendor1Activate","mcCertActionVendor2ActivateLocal").forEach(function(controlName){
    app.directive(controlName, ["templateDir", function(templateDir){
        return {
          restrict: "E",
          templateUrl: templateDir(templateDirectory,'action-activate-local'),

          controller: showErrorsController({
            inject: ["mcCertActivationLocal"],
            fireCallback: function($scope, MonsterCloud, $q, mcCertActivationLocal){

               return mcCertActivationLocal($scope)

            }
          })

       }

    }], {
         title:"Activate local certificate",
         paramsRequired: { "server":"@", "pcId":"@" },
    })

  })

  Array("mcCertActionVendor1Reissue","mcCertActionVendor2ReissueLocal").forEach(function(controlName){
    app.directive(controlName, ["templateDir", function(templateDir){
        return {
          restrict: "E",
          templateUrl: templateDir(templateDirectory,'action-reissue-local'),

          controller: showErrorsController({
            inject: ["mcCertReissueLocal"],
            fireCallback: function($scope, MonsterCloud, $q, mcCertReissueLocal){

               return mcCertReissueLocal($scope)

            }
          })

       }

    }], {
         title:"Reissue local certificate",
         paramsRequired: { "server":"@", "pcId":"@" },
    })
  })

  app.directive("mcCertCsrErrors", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        // shared scope!
        templateUrl: templateDir(templateDirectory,'cert-csr-errors'),
      }
  }]);

  Array("Activate", "Reissue").forEach(function(controlName){
        app.directive("mcCertActionVendor2"+controlName+"General", ["templateDir", function(templateDir){
            return {
              restrict: "E",
              templateUrl: templateDir(templateDirectory,'action-vendor2-activate-general'),
              controller: ["$scope", "MonsterCloud", "mcPurchasesUrl", "mcBackToCertPurchases", 'mcRoutesService', function($scope, MonsterCloud, mcPurchasesUrl, mcBackToCertPurchases,mcRoutesService){
                // {csr:csr, ApproverEmail: "foobar@foo.fo", Admin_contact_id: co_id}

                  $scope.controlName = controlName

                  var pcBaseUrl = mcPurchasesUrl($scope.server, $scope.pcId)

                  $scope.csrError = {}

                  $scope.opens = Array(5+1)
                  $scope.pageMax = 2

                  $scope.subScope = {}

                  $scope.selectedContact

                  $scope.approver = {approvalType:"email"}

                  $scope.approvalTypes = ["email","http","dns"]
                  $scope.approverEmails = []
                  $scope.approverHttp = {}

                  $scope.purchase = {}

                  $scope.wildcardExpected

                  $scope.on_change_csr = function(dataDisplay, dataFull, csr){
                    console.log("changed", dataFull)

                    $scope.csrError = {}
                    if((dataFull.Subject)&&(dataFull.Subject.CN)) {
                      var domainName = dataFull.Subject.CN
                      if($scope.wildcardExpected){
                         if(!domainName.match(/^\*./))
                           $scope.csrError.WildcardExpected = true
                      }
                      else
                      if(domainName.match(/^www./))
                         $scope.csrError.WithoutWwwExpected = true


                      return MonsterCloud.post(pcBaseUrl+"dispatch/query_validation", {DomainName: domainName, csr: csr})
                        .then(function(res){
                            $scope.approverEmails = res.email_validation
                            $scope.csr = csr
                            $scope.csrFull = dataFull
                        })


                    } else
                      $scope.csrError.InvalidCsr = true
                  }

                  $scope.isDisabled=function(n){
                     return n > $scope.pageMax
                  }

                  $scope.next = function(){

                     var c = getCurrentPage()
                     if(c == 2) {
                         $scope.$broadcast('show-errors-check-validity');
                         if (!$scope.subScope.csr.$valid) return
                     }
                     if(c == 3) {
                         $scope.$broadcast('show-errors-check-validity');
                         if (!$scope.subScope.approval.$valid) return
                     }

                     $scope.$broadcast('show-errors-reset');

                     var n = setCurrentPage(c+1)
                     if(n > $scope.pageMax)
                       $scope.pageMax = n
                  }

                  $scope.finish = function(){
                     $scope.isInProgress = true
                     var payload = {csr: $scope.csr, approvalType:$scope.approver.approvalType, Admin_contact_id: $scope.selectedContact.contact.co_id}
                     if($scope.approver.approvalType == "email")
                        payload.ApproverEmail= $scope.approver.email

                     var p
                     if(controlName == "Activate")
                       p = MonsterCloud.post(pcBaseUrl+"activate/task", payload)
                     else
                       p = MonsterCloud.post(pcBaseUrl+"reissue/task", payload)

                     if($scope.approver.approvalType == "email")
                        return mcBackToCertPurchases($scope.server, p, true)

                     return mcBackToCertPurchases($scope.server, p, true, {stateName:"mc-purchase-activation-info", stateParams:{server:$scope.server, pcId: $scope.pcId}})

                  }

                  $scope.contactSelected = function(c){
                     console.log("c was", c)
                     $scope.selectedContact = c
                     $scope.next()
                  }

                  setCurrentPage(1)


                  return MonsterCloud.get(pcBaseUrl)
                     .then(function(d){
                         $scope.purchase = d
                         if($scope.purchase.pc_product.match(/Wildcard/i))
                           $scope.wildcardExpected = true
                     })

                  function getCurrentPage(){
                    var n = 0
                     for(var i = 0; i < $scope.opens.length; i++) {
                       if($scope.opens[i]) {
                         n = i
                         break;
                       }
                     }
                     return n
                  }
                  function setCurrentPage(n){
                     for(var i = 0; i < $scope.opens.length; i++)
                       $scope.opens[i] = false

                     $scope.opens[n] = true
                     return n
                  }

              }]
           }

        }], {
             title:"Comodo "+controlName,
             paramsRequired: { "server":"@", "pcId":"@" },
        })

  })


    app.directive("mcCertActionVendor2ResendApprover", ["templateDir", function(templateDir){
        return {
          restrict: "E",
          templateUrl: templateDir(templateDirectory,'action-vendor2-resend-approver'),

          controller: showErrorsController({
            inject: ["mcPurchasesUrl", "$alertSuccess"],
            additionalInitializationCallback: function($scope, MonsterCloud, $q, mcPurchasesUrl){
               var baseUrl = mcPurchasesUrl($scope.server, $scope.pcId)
               $scope.dispatchUrl = baseUrl+"dispatch/resend_approver_email"

               return MonsterCloud.get(baseUrl+"activation")
                 .then(function(d){
                    $scope.activation = d
                 })
            },
            fireCallback: function($scope, MonsterCloud, $q, mcPurchasesUrl, $alertSuccess){

               return MonsterCloud.post($scope.dispatchUrl, {})
                 .then(function(){
                    return $alertSuccess({timeout:0,message:'The approval e-mail has been resent successfully.'})
                 })

            }
          })

       }

    }], {
         title:"Resend approver email for certificate activation",
         paramsRequired: { "server":"@", "pcId":"@" },
    })


    Array("mcCertActionVendor1Revoke","mcCertActionVendor2Revoke").forEach(function(controlName){
      app.directive(controlName, ["templateDir", function(templateDir){
          return {
            restrict: "E",
            templateUrl: templateDir(templateDirectory,'action-revoke'),

            controller: showErrorsController({
              inject: ["mcPurchasesUrl", "mcBackToCertPurchases"],
              fireCallback: function($scope, MonsterCloud, $q, mcPurchasesUrl, mcBackToCertPurchases){
                 var url = mcPurchasesUrl($scope.server, $scope.pcId)+"revoke"

                 return mcBackToCertPurchases($scope.server, MonsterCloud.post(url, {}))

              }
            })

         }

      }], {
           title:"Revoking certificate",
           paramsRequired: { "server":"@", "pcId":"@" },
           paramsOptional: { "pcSubjectCommonName":"@" },
      })

    })

    app.directive("mcCertActionVendor2Manual", ["templateDir", function(templateDir){
        return {
          restrict: "E",
          templateUrl: templateDir(templateDirectory,'action-vendor2-manual'),
          controller: ["$scope","mcConfig", function($scope,mcConfig){
              $scope.config = mcConfig.config
          }]
       }

    }], {
         title:"Revoking certificate",
         paramsRequired: { "server":"@", "pcId":"@" },
    })

})(app)
