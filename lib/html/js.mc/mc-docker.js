(function(app){

  var templateDirectory = "docker"


  app.service("mcDockerTask", ["$q","$task",function($q, $task){
    return function(server, title, prom) {
        return prom.then(function(r){
            if(!r.id) return $q.when(r)

            var url = "/s/"+server+"/docker/tasks/"+r.id
            return $task({title:title, "url":url})

        })
    }
  }])

  app.directive("mcDockerContainers", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'containers'),
        controller: showErrorsController({
          inject: ["$alertSuccess","mcConfig"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcConfig){
             $scope.config  = mcConfig.config;

             $scope.baseUrl = "/s/"+$scope.server+"/docker/webhosting/"+$scope.whId+"/";
              var url = $scope.baseUrl + "containers/list";

              $scope.fetchWebapps = function(){
                 return MonsterCloud.get("/s/"+$scope.server+"/docrootapi/docroots/webapps/"+$scope.whId)
                   .then(function(webapps){
                      $scope.webapps = webapps;
                      tryToMatch(webapps.default);
                      // console.log("matching, webapps", webapps);
                      Object.keys(webapps.vhosts).forEach(function(vhostName){
                         var candidate = webapps.vhosts[vhostName];
                         tryToMatch(candidate);
                      })
                   })

                 function tryToMatch(candidate) {
                    // console.log("matching?", candidate, $scope.webApplications)
                    if(candidate.container) {
                       // a container is referenced by name! lets find its id.
                       $scope.webApplications.some(function(webapp){
                          if(webapp.PrimaryName == candidate.container) {
                             candidate.match = webapp.Id;
                             return true;
                          }
                       })

                    } else {
                       // clasic stuff, falling back to type.
                       candidate.match = candidate.type;
                    }
                 }
              }

              $scope.saveWebApp = function(vhostName, containerIdOrType){
                var p;
                if(Array("default", "none", "host-legacy").indexOf(containerIdOrType) > -1) {
                  // special case, needs to be routed to docrootapi
                  var url = "/s/"+$scope.server+"/docrootapi/docroots/webapps/"+$scope.whId+"/"+(vhostName?vhostName:"");
                  p = MonsterCloud.post(url, {type: containerIdOrType});
                } else {
                  // container case, needs to be routed to dockerapi
                  var url = $scope.baseUrl + "container/"+containerIdOrType+"/"+ (vhostName?"attach-to-vhost":"set-as-default-web");
                  p = MonsterCloud.post(url, {vhost: vhostName});
                }

                return p.then($alertSuccess).then($scope.fetchWebapps);
              }


              $scope.refetch = function(){
                  return MonsterCloud.post(url)
                     .then(function(containers){
                        $scope.containers = containers;
                        $scope.webApplications = [];

                        $scope.containers.map(function(x){
                           x.name_joined = x.Names.join(", ");
                           x.webApplication = (x.Labels["com.monster-cloud.mc-webapp"] ? true : false);
                           if(x.webApplication) {
                              $scope.webApplications.push(x);
                           }
                           if(x.shutdownAt){
                              x.shutdownAtHuman = new Date(x.shutdownAt*1000).toISOString();
                           }
                           if(x.webshellSecret) {
                              x.webshellUrl = "https://"+$scope.server+"."+mcConfig.config.service_primary_domain+"/api/s/"+$scope.server+"/docker/shellinabox/"+x.webshellSecret+"/";
                           }
                        })
                     })
                     .then(function(){
                        return MonsterCloud.post($scope.baseUrl+"images/available")
                     })
                     .then(function(a){
                        $scope.availableImages = a;
                        a.images.map(function(x){
                           x.debugSupported = x.Labels['com.monster-cloud.mc-params'] && x.Labels['com.monster-cloud.mc-params'].debug;
                           x.imageType = x.Labels['com.monster-cloud.mc-app'];
                        })

                        return $scope.fetchWebapps();
                     })
              }

              $scope.action= function(container, action) {
                 var d = {};
                 if(action == "remove") {
                   d.tryToStopFirst = true;
                 }

                 return MonsterCloud.post($scope.baseUrl+"container/"+container.Id+"/"+action, d)
                   .then(function(){
                      return $alertSuccess();
                   })
                   .then(function(){
                      return $scope.refetch();
                   })
              }

              $scope.resetCl = function(){
                  $scope.cl = {start: true};
              }

              $scope.resetCl();
              $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcDockerTask){
             var pl = angular.copy($scope.cl);
             if(pl.debug) {
               pl.debug = 120*60; // 2 hours by default
             }
             else {
               delete pl.debug; // removing false value
             }

             pl.image = pl.image.PrimaryTag;
             
             return MonsterCloud.post($scope.baseUrl+"containers/new", pl)
               .then(function(re){
                  $scope.lastCreateResponse = re;

                  $scope.resetCl();

                  return $alertSuccess();
               })
               .then(function(){

                  return $scope.refetch();
               })
          }
      })

     }
  }],
   {
       title:"Docker containers",
       menuKey: templateDirectory,
       paramsRequired:{"server":"@","whId":"@"},
  });




  app.directive("mcSshAccess", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ssh-access'),
        controller: showErrorsController({
          inject: ["$alertSuccess","mcWebhostingFactory","mcConfig"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcWebhostingFactory, mcConfig){

              $scope.config  = mcConfig.config;
              $scope.cl = {};

              $scope.baseUrl = "/s/"+$scope.server+"/docker/webhosting/"+$scope.whId+"/";
              var url = $scope.baseUrl + "containers/list";

              $scope.refetch = function(){
                  return MonsterCloud.post(url)
                     .then(function(containers){
                        $scope.containers = containers;
                        containers.forEach(function(x){
                           x.human = x.PrimaryName+ " ("+x.Image+")";
                        })
                     })
                     .then(function(){
                        return MonsterCloud.get($scope.baseUrl+"ssh")
                     })
                     .then(function(x){
                        angular.extend($scope.cl, x);
                     })
              }

              var f = mcWebhostingFactory($scope.server, $scope.whId)
              return f.Lookup()
                .then(function(result){
                  $scope.wh = result;

                  $scope.sshAllowed = ($scope.wh.template.t_allowed_dynamic_languages||[]).indexOf("ssh") > -1;

                  if($scope.sshAllowed)
                    return $scope.refetch();

                })


          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
             var pl = angular.copy($scope.cl);
             
             return MonsterCloud.post($scope.baseUrl+"ssh/authz", {authz: pl.authz})
               .then(function(re){

                  return MonsterCloud.post($scope.baseUrl+"ssh/target", {targetContainer: pl.targetContainer})
               })
               .then(function(re){
                  return $alertSuccess();
               })
          }
      })

     }
  }],
   {
       title:"SSH access to the webstore",
       menuKey: templateDirectory,
       paramsRequired:{"server":"@","whId":"@"},
  });


  app.directive("mcDockerContainerExec", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'container-exec'),
        controller: showErrorsController({
          inject: ["$alert", "$alertSuccess"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alert, $alertSuccess){


              $scope.showUpgrade = function(container){
                var data = {dontCopy: true, target: container.Image, targets: container.compatibleImages};
                return $alert(data, {
                    templateUrl: templateDir(templateDirectory, "upgrade-dialog"),
                })
                .then(function(){
                    // console.log("returned", r, data)
                    return MonsterCloud.post($scope.baseUrl+"upgrade", {destination_image: data.target})
                })
               .then($alertSuccess)
               .then(function() {
                  var p = {stateName: "mcDockerContainers", stateParams: {whId: $scope.whId, server: $scope.server}};
                  return mcRoutesService.jumpToState(p)
               });
              }

             $scope.pl = {};
             $scope.baseUrl = "/s/"+$scope.server+"/docker/webhosting/"+$scope.whId+"/container/"+$scope.cId+"/";
             var url = $scope.baseUrl+"info";
             $scope.refetch = function(){
               return MonsterCloud.get(url)
                 .then(function(info){
                    $scope.info = info;

                    $scope.info.showRebuild = info.compatibleImages.length == 1;
                    $scope.info.showUpgrade = info.compatibleImages.length > 1;

                    return MonsterCloud.post($scope.baseUrl+"logs")
                 })
                 .then(function(logs){
                    $scope.logs = logs;
                 })

             }


             $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcDockerTask){
             var command = $scope.pl.command.split(" ");
             return MonsterCloud.post($scope.baseUrl+"exec/fast", {command: command})
               .then(function(re){
                  $scope.lastCmd = re;
               })
          }
      })

     }
  }],
   {
       title:"Docker containers",
       menuKey: templateDirectory,
       paramsRequired:{"server":"@","whId":"@","cId":"@"},
  })



})(app)
