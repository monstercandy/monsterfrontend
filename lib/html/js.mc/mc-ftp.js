(function(app){

  var templateDirectory = "ftp"

  app.service("mcFtpUrl", function(){
    var re = function(server, whId, suffix) {
       return "/s/"+server+"/ftp/accounts/"+whId+"/"+(suffix||"")
    }

    return re
  })
  app.directive("mcFtpListAndAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-list-and-add'),
     }

  }], {
    title: "FTP account management",
    menuKey: templateDirectory,
    paramsRequired: { "server":"@", "whId":"@" },
    keywords: ["proftpd"],
  })

  app.directive("mcFtpAccountInfo", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-account-info'),
        scope: { "server":"@", whId: "@" },
        controller: ["$scope","mcServerHostname",
          function($scope,mcServerHostname) {
 		    mcServerHostname.apply($scope.server, "ftp", $scope)
          }]
     }
  }])

  app.directive("mcFtpAdminipWarning", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-adminip-warning'),
        scope: { "server":"@", "whId":"@" },
        controller: ["$scope","mcFtpSiteService",function($scope, mcFtpSiteService){
            mcFtpSiteService($scope.server, $scope.whId).Lookup().then(function(d){
              $scope.lookup = d
            })
        }]
     }

  }])

  app.service("mcFtpSiteService", ["MonsterCloud", "mcFtpUrl", "$q", function(MonsterCloud, mcFtpUrl, $q){
     var cachedResult = {}
     var cachedAccounts = {}
     var cachedProm = {}
     var cachedAccountsProm = {}
     return function(server, whId){
        var key = server+"-"+whId
        var urlBase = "/s/"+server+"/ftp/sites/"+whId+"/"
        var usersUrl = mcFtpUrl(server,whId)

        var re = {
           SaveIpAcls: function(){
              return MonsterCloud.post(urlBase+"acls", {reset:cachedResult[key].wh_ip_acl_whitelist})
           },
           AddIpAcl: function(newIp){
              return MonsterCloud.put(urlBase+"acls", {ip:newIp})
                .then(function(){
                    cachedResult[key].wh_ip_acl_whitelist.push(newIp)
                })
           },
           DelIpAcl: function(newIp){
              return MonsterCloud.delete(urlBase+"acls", {ip:newIp})
                .then(function(){
                    var index = cachedResult[key].wh_ip_acl_whitelist.indexOf(newIp)
                    cachedResult[key].wh_ip_acl_whitelist.splice(index, 1)

                })
           },
           GetUsersEx: function(){
              if(cachedAccounts[key]) return $q.when(cachedAccounts[key])

              if(!cachedAccountsProm[key])
                  cachedAccountsProm[key] = MonsterCloud.search(usersUrl)
                    .then(function(lookup){
                       delete cachedAccountsProm[key]
                       cachedAccounts[key] = lookup
                       return $q.when(lookup)

                    })

              return cachedAccountsProm[key]
           },
           GetUsers: function(){
              return re.GetUsersEx()
                .then(function(lookup){
                    return $q.when(lookup.accounts)
                })

           },
           DeleteUserCache: function(){
              delete cachedAccounts[key]
           },
           AddUser: function(d){
             re.DeleteUserCache()
             return MonsterCloud.put(usersUrl, d)
           },
           DeleteUser: function(username){
              re.DeleteUserCache()
              return MonsterCloud.delete(usersUrl+username)
           },
           Lookup: function(){
                if(cachedResult[key]) return $q.when(cachedResult[key])

                if(!cachedProm[key])
                  cachedProm[key] = MonsterCloud.get(urlBase)
                    .then(function(d){
                       delete cachedProm[key]
                       cachedResult[key] = d
                       return $q.when(d)
                    })

                return cachedProm[key]

           }
        }


        Array(
              {q:"admin-login",d:"wh_last_admin_login_enabled",n:"AdminLogin"},
              {q:"acls",d:"wh_ip_acl_effective",n:"IpAcls"},
              {q:"public",d:"wh_public_login",n:"Public"}
        ).forEach(function(k){
            re["Status"+k.n] = function(status){
               return MonsterCloud.post(urlBase+k.q, {status: status})
                .then(function(){
                    if(cachedResult[key]) cachedResult[key][k.d] = status
                })
            }

        })


        return re
     }
  }])

  app.directive("mcFtpAclControls", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-acl-controls'),

        controller:  showErrorsController({
          inject: ["mcFtpSiteService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcFtpSiteService){

            $scope.ftpService = mcFtpSiteService($scope.server, $scope.whId)
            $scope.ftpService.Lookup().then(function(d){
              $scope.lookup = d
            })

            $scope.doStatus = function(kind, status){
              $scope.ftpService["Status"+kind](status)
            }

            $scope.delete = function(ip){
               return $scope.ftpService.DelIpAcl(ip)
            }

          },
          fireCallback: function($scope, MonsterCloud, $q, mcFtpSiteService){

              return $scope.ftpService.AddIpAcl($scope.ip)
               .then(function(){
                  delete $scope.ip
               })

          }

        })

     }
  }], {
    title: "FTP access control lists",
    menuKey: templateDirectory,
    paramsRequired: { "server":"@", "whId":"@" },
    keywords: ["proftpd","acl"],
  })


app.directive('mcFtpTotp', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'ftp-2fa'),
      controller:  showErrorsController({

        inject: ["$alertSuccess", "$alertError"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){

            $scope.urlBase = "/s/"+$scope.server+"/ftp/accounts/"+$scope.whId+"/"+$scope.faUsername+"/totp/";

            $scope.reinit = function(){
                var url = $scope.urlBase + "status";
                return MonsterCloud.post(url, {})
                  .then(function(totp){
                          $scope.totp = totp;
                          if(!$scope.totp.fa_totp_active) {
                              var qrcode = new QRCode(document.getElementById("qrcode"));
                              qrcode.makeCode($scope.totp.fa_totp_url);
                          }

                  })
            }


            return $scope.reinit();
        },
        fireCallback: function($scope, MonsterCloud,$q,$alertSuccess,$alertError){

           var data = {userToken: $scope.t.userToken};

           var action = $scope.totp.fa_totp_active ? "inactivate" : "activate";

           return MonsterCloud.doRequest({method:"POST", url: $scope.urlBase+action, data: data, relayException: true})
              .then(function(){
                   $scope.totp.fa_totp_active = !$scope.totp.fa_totp_active;
                   return $alertSuccess()
              })
              .then(function(){
                   return $scope.reinit();
              })
              .catch(function(){
                   return $alertError();
              })
        }

      })
    }
}],
       {
       title:"FTP account two factor authentication",
     menuKey: templateDirectory,
       keywords: ["mfa", "2fa", "authy", "authenticator", "otp", "totp", "hotp"],
       paramsRequired: { "server":"@", "whId":"@", "faUsername":"@" },
      })

  app.directive("mcFtpList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-list'),
        scope: { "server":"@", "whId":"@" },
        controller: ["$scope","MonsterCloud","mcFtpSiteService",
          function($scope,MonsterCloud,mcFtpSiteService) {

            var ftp = mcFtpSiteService($scope.server, $scope.whId)

            $scope.refetch = function(){
                console.log("refetching for list control")
                return ftp.GetUsers()
                   .then(function(ftps){
                      $scope.showListing = true;
                      $scope.ftps = ftps;
                   })

            }

            $scope.delete = function(ftpRow){
               return ftp.DeleteUser(ftpRow.fa_username)
                 .then(function(){
                    return $scope.refetch();
                 })
                 .then(function(){
                    return $scope.$parent.$broadcast("refetch");                  
                 })
            }

            $scope.$on("refetch",function(event){
               return $scope.refetch()
            });

            return $scope.refetch()

        }]
     }

  }])

app.directive('mcFtpSshAuthz', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'ftp-ssh-authz'),
      controller:  showErrorsController({

        inject: ["$alertSuccess", "$alertError"],
        additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess){

            $scope.urlBase = "/s/"+$scope.server+"/ftp/accounts/"+$scope.whId+"/"+$scope.faUsername+"/ssh-authz/";

            $scope.authz = "";

            var elem = document.getElementById('authz');
            elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');

            $scope.reinit = function(){
                return MonsterCloud.get($scope.urlBase)
                  .then(function(r){
                        $scope.authz = r.authz;
                  })
            }


            return $scope.reinit();
        },
        fireCallback: function($scope, MonsterCloud,$q,$alertSuccess,$alertError){

           var data = {authz: $scope.authz};

           return MonsterCloud.post($scope.urlBase, data)
              .then(function(){
                   return $alertSuccess()
              })
        }

      })
    }
}],
       {
       title:"Public key authentication for SFTP",
     menuKey: templateDirectory,
       keywords: ["sftp", "public key"],
       paramsRequired: { "server":"@", "whId":"@", "faUsername":"@" },
      })

  app.directive("mcFtpAdd", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'ftp-add'),
        scope: { "server":"@", "whId":"@"},
        controller:  showErrorsController({
          inject: ["mcFtpSiteService"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcFtpSiteService){
             $scope.ftpService = mcFtpSiteService($scope.server, $scope.whId)

          	 $scope.resetForm = function(){
                $scope.ftp = {fa_subdir:"/"}
          	 }


             $scope.refetch = function(){
                 console.log("refetching for add control")
                 $scope.resetForm()
                 $scope.ftpService.GetUsersEx()
                 .then(function(lookup){
                    $scope.accountLookup = lookup
                 })
             }


             $scope.refetch()

             $scope.$on("refetch",function(event){
               return $scope.refetch()
             });
          },
          fireCallback: function($scope, MonsterCloud, $q){

             return $scope.ftpService.AddUser($scope.ftp)
               .then(function(){

                  return $scope.$parent.$broadcast("refetch")

               })


          }

        })

     }

  }])


})(app)
