(function(app){

  app.controller('PromptModalController', ["$scope", "$uibModalInstance", "data", "$q", function ($scope, $uibModalInstance, data, $q) {
    $scope.data = angular.copy(data);
    $scope.text = $scope.data.text || ""

    $scope.ok = function () {
       $scope.$broadcast('show-errors-check-validity');
       if (!$scope.dataForm.$valid) return

      $uibModalInstance.close($scope.text);
    };

    $scope.cancel = function () {
      if($scope.data.sendFalseOnCancel) return $uibModalInstance.close(false);
      $uibModalInstance.dismiss()
    };


  }])
  .factory('$prompt', ["$uibModal", 'templateDir', function ($uibModal, templateDir) {
    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir('common','prompt'),    
        controller: 'PromptModalController',
      }

      settings = angular.extend(defaults, (settings || {}));
      
      if ('templateUrl' in settings && 'template' in settings) {
        delete settings.template;
      }

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings).result;
    };
  }])


})(app)
