var mcLng = angular.module("mc-lng", [])

  .service("lngService", ["$http", "mcConfig", '$q', function($http, mcConfig, $q) {
   var lng = {}
   var lngToUse = angular.element("html").attr("lang") || getBrowserLanguage() || mcConfig.config.default_language

   var p;
   if(lngToUse) {

       p = httpGet(getLngUrl(lngToUse))
            .then(function(data){
              lng = data

              if((mcConfig.config.default_language)&&(mcConfig.config.default_language != lngToUse))
                return httpGet(getLngUrl(mcConfig.config.default_language))
              else
                return $q.when({})
           })
            .then(function(defaultLanguageKeys){
               // assign default keys
               Object.keys(defaultLanguageKeys).forEach(function(k){
                  if(!lng[k])
                    lng[k] = defaultLanguageKeys[k]
               })

               return $q.when(lng)
            })
   }
   else
       p = $q.when({})

   p.language = lngToUse;
   
   p.translate = function(key) {
       key = key.trim()
       if(!key) return ""
       if(typeof lng[key] === "undefined") {
         if(lngToUse != "en") console.warn("\""+key+"\":\""+key+"\",")
         return key.replace(/{/g,"{{").replace(/}/g,"}}")
       }
       return lng[key]
   }

   return p

   function getBrowserLanguage(){
       var s = navigator.language || navigator.userLanguage;
       if(s)
          s = s.substr(0, 2);

       return s;
   }

   function getLngUrl(l) {
      return "/languages.mc/lng-"+l+".json?" + (mcConfig.config.template_version ? mcConfig.config.template_version : new Date())
   }

   function httpGet(url){
      return $http.get(url)
        .then(function(response){
            return $q.when(response.data)
        })
   }

}])
.directive('lng', ["lngService","$parse","$interpolate",function(lngService,$parse,$interpolate) {
  return {
    // restrict: 'E', // no restriction, it can be an attribute or an element
    priority: -1,
    link: function (scope, element, attrs) {
        return lngService.then(function(){

              var originalKey = element.text().trim()
              var key = attrs.lng || originalKey

                  if("lngPreProcess" in attrs)
                     key = $interpolate(key)(scope)

                  if("lngParse" in attrs)
                     key = $parse(key)(scope)

                  var translated = lngService.translate(key)


                   if(("lngPostProcess" in attrs) && (attrs["lngPostProcess"] != "false")){
                       // console.log("contents", element.contents())
                       // console.log(attrs["lngPostProcess"], attrs)
                       translated = $interpolate(translated)(scope);
                  }

                  if(originalKey != translated)
                       element.html(translated);


              // scope.$destroy();
               })
    },
  }
}])
.run(["$rootScope","lngService", function($rootScope, lngService) {

   lngService.then(function(lng){

      $rootScope.lngReady = true
      $rootScope.lng = lngService.translate
   })
}])


Array("title","placeholder").forEach(function(q){
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

  var importScope = {}
  var dir = "lng"+capitalizeFirstLetter(q)
  mcLng.directive(dir, ["lngService",'$interpolate',function(lngService, $interpolate) {
    return {
    restrict: 'A',
    link: function (scope, element, attrs) {
       lngService.then(function(){
        var text = attrs[dir]
        var translated = lngService.translate(text)


        if(dir+"PostProcess" in attrs) {
             // console.log("interpolating stuff!", translated)
               translated = $interpolate(translated)(scope);
         }

        element.attr(q, translated);
       })
    },
    }
  }]);
})
