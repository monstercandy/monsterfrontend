(function(app){

  app.controller('TosModalController', ["$scope", "$uibModalInstance", "data", "$sce", function ($scope, $uibModalInstance, data, $sce) {
    $scope.data = angular.copy(data);
    $scope.data.tos = $sce.trustAsHtml($scope.data.tos)

    $scope.ok = function () {
       $uibModalInstance.close();
    };

    $scope.abort = function () {
       $uibModalInstance.dismiss('did not accept');
    };


  }])
  .factory('$mcTos', ["$uibModal", 'templateDir', function ($uibModal, templateDir) {
    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir('common','tos'),    
        controller: 'TosModalController',

         // these two is to prevent dismissing the dialog box
         backdrop  : 'static',
         keyboard  : false,
      }

      settings = angular.extend(defaults, (settings || {}));
      

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings).result;
    };
  }])
  .directive('mcTos', function () { 
    return {
      priority: 1,
      scope: {
        redirectOnFailure: "@",
      },
      controller: ["$mcTos", "$scope", "$window", '$element', function($mcTos, $scope, $window, $element){
         var data = {tos: $element.html()}
         $mcTos(data, {})

           .catch(function(ex){
              console.log("exception", ex)
              if($scope.redirectOnFailure)
                 $window.location = $scope.redirectOnFailure
           })
           ;

      }]
    }
  });

})(app)
