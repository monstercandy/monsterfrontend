(function(app){

  var templateDirectory = "installatron"

  app.directive("mcInstallatronKickoff", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'installatron-kickoff'),

        controller: ["$scope","MonsterCloud",'mcFtpSiteService','mcDbmsDatabasesService',function($scope,MonsterCloud,mcFtpSiteService,mcDbmsDatabasesService){

            var installatron_name

            mcFtpSiteService($scope.server, $scope.whId).GetUsers()
              .then(function(ftpaccounts){
                  $scope.ftpaccounts = ftpaccounts;
                  // $scope.ftpUsernamesStr = $scope.ftpaccounts.map(function(x){return x.fa_username}).join(", ");
              })
            mcDbmsDatabasesService($scope.server, $scope.whId).then(function(databases){
               $scope.databases = databases;
               // $scope.databaseNamesStr = $scope.databases.map(function(x){return x.db_database_name}).join(", ");
            })

            $scope.installatronLink = "";

            $scope.kickoffDisabled = false;

            function getInstallatronLink(){
                $scope.kickoffDisabled = true;
            }

            $scope.start = function(){

                return MonsterCloud.post(getUrl("kickoff"), {server:$scope.server,wh_id:$scope.whId})
                 .then(function(r){
                    $scope.installatronLink = r.url;
                    $scope.kickoffDisabled = false;
                    console.log("link saved", $scope.installatronLink)
                 })
            }


            function getUrl(rest){
               return "/s/[this]/wizard/webhosting/installatron/"+(rest||"")
            }

        }],

     }
  }],
   {
       title:"Installatron panel",
       menuKey: templateDirectory,
       paramsRequired:{"server":"@","whId":"@"},
  })


})(app)
