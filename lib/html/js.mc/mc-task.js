(function(app){

  app.service("TaskService", ["MonsterCloud","$q","mcConfig", function(MonsterCloud, $q, mcConfig){


      return function(oData, feedback){

        // 10kbyte by default:
        var maxNumberOfBytesToReadBeforeRewrap = oData.taskMaxNumberOfBytesToReadBeforeRewrap || mcConfig.config.taskMaxNumberOfBytesToReadBeforeRewrap || 100*1024;

        return $q(function(mainResolve, mainReject){

            if(!feedback) feedback = {}

            var abortPromiseStructs = []


            var readRequests = 0
            var sofarLoadedBefore = 0
            var scopeTextBefore = ""
            feedback.sofarRequests = 0
            feedback.sofarLoaded = 0
            feedback.data = oData;
            feedback.text = ""

            return isFactory() ?
                   nextFromTheFactory() :
                   doTheHardWork(oData)

            function setText(data){
                 feedback.text = scopeTextBefore + data
            }
            function appendText(data){
                 feedback.text += data + "\n"
            }


            function abortAllRequests(){
               abortPromiseStructs.forEach(function(abortPromiseStruct){
                   abortPromiseStruct.doAbort()
               })
            }

            function addAbortPromiseStruct(){
              var abortPromiseStruct = {}
              abortPromiseStruct.promise = $q(function(resolve,reject){
                 abortPromiseStruct.doAbort = resolve
              })
              abortPromiseStructs.push(abortPromiseStruct)
              return abortPromiseStruct
            }
            function abortAndRemoveAbortPromiseStruct(promStruct){
               promStruct.doAbort()
               var i = abortPromiseStructs.indexOf(promStruct)
               abortPromiseStructs.splice(i, 1)
            }


            function doTheHardWork(data){

                  sofarLoadedBefore = feedback.sofarLoaded
                  scopeTextBefore = feedback.text || ""
                  feedback.status = "in_progress"

                  var lastProgressText = null;
                  var inputSenderSpawned = false
                  var eventResolved = false
                  var numberOfTailReaders = 0
                  var p
                  var url = data.url
                  if(!url) {
                     if((data.prom)&&(data.prom.then)) {
                        console.log("Running in simple promise mode (no task fetching)");
                        p = data.prom;
                     }
                     else
                        p = $q.reject(new Error("Url not provided"));
                  }

                  feedback.abort = function () {
                    console.log("doing abort...")
                    feedback.abortDisabled = true
                    feedback.aborted = true
                    MonsterCloud.get(url+"/abort")
                    abortAllRequests()
                  };


                  if(!p)
                  {
                    function getRequestOptions(firstRequest){
                      var aurl = url
                      if(data.doubleRequestMode)
                        aurl += "/" + (firstRequest ? "output" : "input")

                      var abortPromiseStruct = addAbortPromiseStruct()
                      var requestOptions = {
                         relayException: true,
                         readRequest: readRequests++,
                         abortPromiseStruct: abortPromiseStruct,
                         url:aurl,
                         transformRequest: [],
                         transformResponse: [],
                         eventHandlers: {
                           readystatechange:function(p){
                              // console.log("readystatechange event", firstRequest, p.target.readyState, p)
                              var rs = p.target.readyState

                              if(rs != XMLHttpRequest.HEADERS_RECEIVED) return

                              if(requestOptions.abortPrevious) {
                                // we do it this way, since we dont want to loose closing message due to race condition

                                console.log("aborting previous request", requestOptions.abortPrevious.readRequest)

                                // aborting just this one
                                requestOptions.abortPrevious.abortedNumberOfBytesReached = true
                                abortAndRemoveAbortPromiseStruct(requestOptions.abortPrevious.abortPromiseStruct)
                                console.log("number of stuffs in abortAllRequests:", abortPromiseStructs.length)
                              }

                              if(!data.doubleRequestMode) return
                              if(!firstRequest) return
                              if(inputSenderSpawned) return
                              inputSenderSpawned = true


                              // kicking off the upload
                              var uploadRequest = getRequestOptions(false)
                              configureUploadOptions(uploadRequest)

                              console.error("headers received for the output reader, starting the upload task", uploadRequest);

                              MonsterCloud.doRequest(uploadRequest)
                           },
                           progress: function(evt) {

                             if(!firstRequest) return
                             // console.info("task PROGRESS", evt)

                             lastProgressText = evt.target.responseText;
                             if(requestOptions.timer) return;
                             requestOptions.timer = setTimeout(function(){
                                 requestOptions.timer = null;

                                 if(feedback.status != "in_progress") return;

                                 if(lastProgressText) {
                                   if(lastProgressText.indexOf("Task not found.") == 0) return
                                   if(lastProgressText.trim() == "::: output reader attached") return

                                   setText(lastProgressText)
                                 }

                                 if(lastProgressText.length >= maxNumberOfBytesToReadBeforeRewrap) {

                                   appendText("::: Output limit reached, reconnecting to relax browser memory usage :::")

                                   // refetch
                                   var newRequestOptions = getRequestOptions(true);
                                   console.error("output limit reached, sending new request", lastProgressText.length, maxNumberOfBytesToReadBeforeRewrap, newRequestOptions);
                                   var p = MonsterCloud.doRequest(newRequestOptions);
                                   newRequestOptions.abortPrevious = requestOptions;
                                   p.requestOptions = newRequestOptions;
                                   attachProcessingTail(p);
                                 }

                             }, 1000);

                           }
                         },
                         timeout: abortPromiseStruct.promise
                      }
                      if(data.noUriTransform)
                          requestOptions.noUriTransform = true

                      return requestOptions
                    }

                    function configureUploadOptions(requestOptions) {

                        if(data.fileInput)
                            requestOptions.data = new Uint8Array(data.fileInput)
                        if(data.stringInput)
                            requestOptions.data = data.stringInput

                        if((!oData.expectedTotalBytes)&&(requestOptions.data))
                            oData.expectedTotalBytes = requestOptions.data.length

                        if(oData.expectedTotalBytes) {
                            requestOptions.uploadEventHandlers = {
                                  progress: function (e) {
                                      // console.error(" uploadEventHandlers progress", e)
                                      feedback.sofarLoaded = sofarLoadedBefore + (e.loaded || 0)
                                      if (e.lengthComputable)
                                         feedback.actProgress = (e.loaded / e.total) * 100;

                                      feedback.sofarProgress = Math.round((feedback.sofarLoaded / oData.expectedTotalBytes) * 100);
                                  }
                            }

                        }
                        if(data.uploadEventHandlers)
                            requestOptions.uploadEventHandlers = data.uploadEventHandlers

                    }

                    var r = getRequestOptions(true)
                    if(!data.doubleRequestMode)
                      configureUploadOptions(r)

                    // console.log("hey", data, $scope.data)
                    p = MonsterCloud.doRequest(r)
                    p.requestOptions = r

                  }

                  console.error("calling attachProcessingTail in main", p);
                  return attachProcessingTail(p)


                  function attachProcessingTail(p) {
                     var thisTailReader = ++numberOfTailReaders

                     return p.then(function(d){

                      if((p.requestOptions)&&(p.requestOptions.timer)) {
                         console.error("Cancelling the attached timeouts", p);
                         clearTimeout(p.requestOptions.timer);
                         p.requestOptions.timer = null;
                      }

                      // console.info("task then", d)
                      if((d) && (!data.mcDontDisplay)) {
                         setText(d);                        
                      }

                      console.error("tail processor was invoked:", thisTailReader, d, p)

                      // we are invoked sometimes with null
                      if(!d) {
                         feedback.text = lastProgressText || feedback.text;
                      }


                      var myRegexp  = /::: mc task status: (.+?) :::/
                      var m = myRegexp.exec( feedback.text );
                      if((!m) && (data.mcTaskStatusNotNeeded))
                         m = ["-","successful"]

                      if(!m) {
                          console.error("There is no task status response, number of tail readers:", numberOfTailReaders)
                          if(numberOfTailReaders == 1)
                             throw new Error("Invalid response, no mc task status found")

                          return $q.when()
                      }

                      console.error("task status received:", m)

                      feedback.status = m[1];
                      if(feedback.status == "successful") {
                        feedback.text = feedback.text.replace(m[0], "")


                        if(isFactory()) {
                          if(lastProgressText)
                             lastProgressText = lastProgressText.replace(m[0], "");
                          return nextFromTheFactory(d);
                        }
                           

                      } else if(data.doubleRequestMode) {
                        console.log("request was not successful, aborting everything")
                        feedback.abort()
                      }

                    })
                    .catch(function(ex){
                      console.error("there was an exception while attachProcessingTail", thisTailReader, ex, p.requestOptions ? p.requestOptions.abortedNumberOfBytesReached : null)
                      if(thisTailReader > 1) return $q.when() // we discard exceptions for nonstarter processes
                      if((p.requestOptions)&&(p.requestOptions.abortedNumberOfBytesReached)) return $q.when()

                      var exMsg = feedback.aborted ? "aborted" : ((ex && ex.message) || JSON.stringify(ex) );
                      appendText("\n\nEXCEPTION: "+exMsg)
                      feedback.status = feedback.aborted ? "aborted" : "error"
                    })
                    .then(function(){
                      numberOfTailReaders--
                      if(feedback.status != "in_progress")
                         feedback.abortDisabled = true
                      else
                         return $q.when() // it is still in progress, so we wont resolve.

                      // we dont want the event to be resolved multiple times
                      if(eventResolved) return
                      eventResolved = true

                      console.error("Resolving event.")

                      mainResolve(feedback)
                    })
                  }


            }

            function isFactory(){
                return (
                         ((oData.expectedRequests)||(oData.undeterministic))
                         &&
                         (oData.urlFactory)
                       )
            }

            function thereAreMore(){
                return ((oData.expectedRequests)&&(feedback.sofarRequests < oData.expectedRequests))
                       ||
                       (oData.undeterministic);
            }
            function nextFromTheFactory(lastPromResult){
                console.log("nextFromTheFactory")
                if(!thereAreMore()) return $q.when()
                var oStatus = feedback.status;
                feedback.status = "in_progress"

                console.log("calling urlFactory")

                return oData.urlFactory(feedback.sofarRequests++, appendText, lastPromResult)
                  .then(function(data){
                      console.error("a promise was just resolved by the urlFactory", data);
                      // undeterministic task chains may terminate this way:
                      if(!data) {
                        console.log("Undeterministic task chain has just finished!");
                        feedback.status = oStatus;
                        return $q.when();
                      }

                      console.log("received data!", data)
                      doTheHardWork(data)
                  })

            }



        })


      }

  }])

  app.controller('TaskModalController', ["$scope", "$uibModalInstance", "data", "TaskService", function ($scope, $uibModalInstance, oData, TaskService) {

    $scope.feedback = oData.feedback || {}
    $scope.close = function () {
      $uibModalInstance.close($scope.feedback);
    };

    return (oData.taskStartedAlready || TaskService(oData, $scope.feedback))
      .then(function(feedback){
          if((oData.autoClose)&&(feedback.status == "successful"))
            $scope.close()
      })

  }])
  .factory('$task', ["$uibModal", 'templateDir', 'TaskService', '$q', '$timeout', function ($uibModal, templateDir, TaskService, $q, $timeout) {
    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir('common','task'),
        controller: 'TaskModalController',
        size: "lg",
        backdrop  : 'static',
        keyboard  : false,
      }

      settings = angular.extend({}, defaults, settings);

      if ('templateUrl' in settings && 'template' in settings) {
        delete settings.template;
      }

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      var windowDisplayedAlready = false

      if(data.dontDisplayUntilMs) {
         console.log("delaying the task window for a while:", data.dontDisplayUntilMs)
         data.abortOnly = true
         data.feedback = {}

         var timer = $timeout(function(){
            // no response til a while, lets display the window
            timer= null
            return showWindowIfNotYet()
         }, data.dontDisplayUntilMs)

         data.taskStartedAlready = TaskService(data, data.feedback)
           .then(function(feedback){
              console.log("taskservice just returned", feedback)

              if(timer) $timeout.cancel(timer)
              if(feedback.status != "successful")
                 showWindowIfNotYet()
              return $q.when(feedback)
           })

         return data.taskStartedAlready
      }
      else
         return showWindowIfNotYet()

     function showWindowIfNotYet(){
        if(windowDisplayedAlready) return
        windowDisplayedAlready = true
        return $uibModal.open(settings).result;
     }

    };
  }])


})(app)
