(function(app){

  function resetDnsZone(domain, mcConfig){
       return MonsterCloud.post("/tapi/domain/"+domain, {"template": mcConfig.config.domain_dns_records_reset_template})
  }

  function deleteRecord(MonsterCloud, domain, kind, rec) {
     return MonsterCloud.delete("/tapi/"+kind+"/"+domain+"/"+rec.hash, {"limit":1})  //
  }

  function isRegularDomain(kind) {
     return kind == "domain"
  }

  function doRefetch($scope, x){
     console.log("broadcasting refetch", $scope)
     $scope.$parent.$broadcast('refetchNeeded', x);
  }

  var templateDirectory = "tapi"

  app.directive('notBindZoneName', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.notBindZoneName = function(modelValue, viewValue) {
          // console.log("hey", modelValue, viewValue, attrs.endsWith, ctrl)

          if(!viewValue) return true

            console.log("shit", scope)

          return !viewValue.endsWith(".");
        };
      }
    };
  });

  app.directive("mcDnsCopyFrom", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'dns-copy-from'),
        scope: { "domain": "@", },
        controller:  showErrorsController({
          inject:["mcAccountDomains"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcAccountDomains){
             $scope.copyFrom = []
             if(!isRegularDomain) return

             return mcAccountDomains()
               .then(function(d){
                  d.forEach(function(q){
                     if(q.d_domain_name == $scope.domain) return;

                     $scope.copyFrom.push(q)
                  })
                  $scope.copyFrom.sort(function(a, b){return a.d_domain_name.localeCompare(b.d_domain_name);} )

                  // console.log("na itten e", $scope.copyFrom)
               })
          },
          fireCallback: function($scope, MonsterCloud, $q){

             MonsterCloud.get("/tapi/domain/"+$scope.selectedCopyFrom)
               .then(function(d){
                  return MonsterCloud.post("/tapi/domain/"+$scope.domain, {"reset": d.records})
               })
               .then(function(){
                  doRefetch($scope)

               })


          }

        })
     }

  }])



app.directive("mcTapiDnsZoneOnOffRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'tapi-dns-zone-on-off-row'),
      scope: { "domain": "@", createZoneAllowed: "=",  },
      controller: ["$scope", "mcDnsZone", function($scope, mcDnsZone){
          // console.log("xx", $scope.createZoneAllowed, $scope.domain)
          return mcDnsZone.Lookup($scope.domain)
            .then(function(result){
              $scope.lookupFinished = true
              $scope.zoneLookup = result
            })
      }]
   }

}])

app.service('mcTapiSupportedDnsRecordTypes', ["mcConfig", function(mcConfig){
   return function(){
      var re = ['A', 'A/PTR', 'AAAA', 'CNAME', 'MX', 'NS/A', 'PTR', 'SOA', 'SOA/NS/A', 'SRV', 'CAA', 'TXT'];
      if (mcConfig.config.clone_record_support)
         re.push('CLONE');
      return re;
   }
}])

app.service('mcTapiDnsEditLinkService', ["mcDnsZone", function(mcDnsZone){
   return function($scope){
      return mcDnsZone.Lookup($scope.domain)
        .then(function(result){
          $scope.zoneLookup = result
        })
   }
}])
app.directive('mcTapiDnsEditLink', ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'tapi-dns-edit-link'),
      scope: { "domain": "@", link: "@" },
      controller: ["$scope", "mcTapiDnsEditLinkService", function($scope, mcTapiDnsEditLinkService){
          return mcTapiDnsEditLinkService($scope)
      }]
   }

}])

app.directive('mcTapiDnsEditLink2', ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'tapi-dns-edit-link2'),
      scope: { "domain": "@", kind: "@", "domainId": "@" , 'externalRegistrar': "=", "display": "@", 'template': "@" },
      controller: ["$scope", "mcTapiDnsEditLinkService", function($scope, mcTapiDnsEditLinkService){
          $scope.params = {
             'external-registrar':$scope.externalRegistrar,
             'kind':$scope.kind,
             'display':$scope.display,
             'domain':$scope.domain,
             'domainId':$scope.domainId,
             'template': $scope.template
          }
          return mcTapiDnsEditLinkService($scope)
      }]
   }

}])

app.directive('mcDnsAdmin', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dnsadmin'),
      controller: ["$scope", function($scope){
          if(!$scope.kind) $scope.kind = "domain"
          if(!$scope.display) $scope.display = $scope.domain

          $scope.dnsRecord = {}
          $scope.showCopy =  isRegularDomain($scope.kind)

      }]
   }
}],
      {
       title:"DNS record admin",
       menuKey: templateDirectory,
       paramsRequired:{"domain":"@"},
       paramsOptional:{"kind":"@","display":"@","external-registrar":"=","domainId":"@", 'template': "@"},
       keywords: ["dns record", "copy", "restore"]
      }
)

app.directive('mcDnsIpv4Input', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dns-ipv4-input'),
      scope: { "dnsRecord": "=", form: "=", required:"=" },
   }
}])


app.service("mcDnsZone", ["MonsterCloud", "mcConfig", '$window', '$q', function(MonsterCloud, mcConfig, $window, $q){

    var cache = {}
    var inProgress = {}

    var re = {
        "Lookup": function(domain) {
             // console.log("call", domain, cache, inProgress)
             if(cache[domain]) return $q.when(cache[domain])

             if(!inProgress[domain])
               inProgress[domain] = MonsterCloud.search("/tapi/domain/"+domain, {})
                 .then(function(data){
                     delete inProgress[domain]
                     cache[domain] = {zoneExists: data}
                     return $q.when(cache[domain])
                 })

             return inProgress[domain]

        },
        "ResetTemplate": function(forceTemplate) {
            return forceTemplate || mcConfig.config.domain_dns_records_reset_template;
        },
        "Resettable": function(forceTemplate) {
            return re.ResetTemplate(forceTemplate) ? true : false;
        },
        "Reset": function(domain, forceTemplate, relayException){
            var template = re.ResetTemplate(forceTemplate);
            if(!template) throw new Error("Not configured");           

            return MonsterCloud.doRequest({
                method:"POST", 
                url: "/tapi/domain/"+domain, 
                data: {"template":template},
                relayException: relayException,
              })
              .then(function(d){
                  if(cache[domain]) cache[domain].zoneExists = true
                  return $q.when(d)

              })
        },
        "Remove": function(domain) {
            return MonsterCloud.delete("/tapi/domain/"+domain, {})
              .then(function(d){
                  if(cache[domain]) cache[domain].zoneExists = false
                  return $q.when(d)

              })
        }

    };
    return re;
}])

app.directive('mcDnsZoneOn', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dns-zone-on'),
      scope: { "domain": "@", noReload: "@" },
      controller: ["$scope", "mcDnsZone", "$window", function($scope, mcDnsZone, $window){
         $scope.fire = function(){
            mcDnsZone.Reset($scope.domain)
             .then(function(){
                 if(!$scope.noReload)
                    $window.location.reload()
             })
         }
      }]
   }
}])

app.directive('mcDnsZoneOff', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dns-zone-off'),
      scope: { "domain": "@", noReload:"@" },
      controller: ["$scope", "mcDnsZone", "$window", function($scope, mcDnsZone, $window){
         $scope.fire = function(){
            mcDnsZone.Remove($scope.domain)
             .then(function(){
                 if(!$scope.noReload)
                   $window.location.reload()
             })
         }
      }]
   }
}])


app.directive('mcDnsRecordList', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'dns-record-list'),
      scope: { "domain": "@", externalRegistrar: "=", kind: "@", "domainId": "@", "dnsRecord": "=", "template": "@" },
      controller: ["$scope", "MonsterCloud", "mcConfig", "$window", 'mcDnsZone', function($scope, MonsterCloud, mcConfig, $window, mcDnsZone){

           var firstTime = true
           $scope.records = {}
           if(!$scope.domainId) $scope.domainId = $scope.domain

           $scope.showResetButton =  isRegularDomain($scope.kind) && mcDnsZone.Resettable($scope.template);
           $scope.showChangeNsButton = (!$scope.externalRegistrar);

           $scope.select = function(rec) {
              console.log("itten", rec)
              $scope.dnsRecord = angular.copy(rec)
           }

           $scope.changens = function(){
              var r = mcConfig.config.changeNsHref
              r = MonsterCloud.appendUriParams(r, "da_id="+$scope.domainId)
              r = MonsterCloud.appendUidi(r)
              console.log(r)
              $window.location.href = r
           }

           $scope.delete = function(rec){
              return deleteRecord(MonsterCloud, $scope.domain, $scope.kind, rec)
                .then(function(){
                    return $scope.refetch();
                })
           }
           $scope.reset = function() {
               if($scope.kind != "domain") return;

               return mcDnsZone.Reset($scope.domain, $scope.template)
                 .then(function(data){
                    $scope.refetch()
                 })

           }
           $scope.refetch = function(jumpToId) {
               return MonsterCloud.get("/tapi/"+$scope.kind+"/"+$scope.domain)
                 .then(function(data){
                     transformRecords(data)
                     $scope.records = data.records

                     if(!firstTime)
                        $window.location.href = "#"+ (jumpToId ? jumpToId : "dns_records_root_table")

                     firstTime = false
                 })

           }

          $scope.$on('refetchNeeded', function(e, args) {
              console.log("refetch event received")
              $scope.refetch(args)
          });

           $scope.refetch()




              function transformRecords(jd){
                      var len = jd["records"].length;
                      for (i=0; i<len; ++i) {
                              jd["records"][i]["full_hostname"] = $scope.domain
                              if(jd["records"][i]["host"])
                                  jd["records"][i]["full_hostname"] = jd["records"][i]["host"] + "." + jd["records"][i]["full_hostname"]

                              jd["records"][i]["human_value"] = jd["records"][i]["nameserver"] ||
                                                                jd["records"][i]["mailserver"] ||
                                                                jd["records"][i]["cname"] ||
                                                                jd["records"][i]["text"] ||
                                                                jd["records"][i]["ip"] ||
                                                                jd["records"][i]["target"] ||
                                                                jd["records"][i]["value"] ||
                                                                jd["records"][i]["human_value"] ||
                                                                ""

                              if(jd["records"][i]["human_value"].length > 30)
                                 jd["records"][i]["human_value"] = jd["records"][i]["human_value"].substr(0,30)+"..."
                      }
              }



       }]
   }
}])


app.directive('mcEditDnsRecord', ["templateDir", "mcConfig", function(templateDir, mcConfig){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'edit-dns-record'),
      scope: { "domain": "@", kind: "@", dnsRecord: "=", externalRegistrar: "=" },
      controller:  showErrorsController({
        inject:["mcTapiSupportedDnsRecordTypes"],
        additionalInitializationCallback: function($scope, MonsterCloud,$q,mcTapiSupportedDnsRecordTypes){

             $scope.supportedDnsRecordTypes = mcTapiSupportedDnsRecordTypes();
             $scope.nameservers = mcConfig.config.nameservers || ""
             $scope.dnsRecord = {}

             $scope.caaFlagOptions = [{value:0, label:"non-critical"},{value:1,label:"critical"}];
             $scope.caaTagOptions = ["issue","issuewild","iodef"];


             $scope.add = function(){
                $scope.mode = "add"
                $scope.fire()
             }
             $scope.mod = function(){
                $scope.mode = "del"
                $scope.fire()
             }

        },
        fireCallback: function($scope, MonsterCloud, $q){

             var p = $q.when()

             if($scope.mode == "del")
               p = deleteRecord(MonsterCloud, $scope.domain, $scope.kind, $scope.dnsRecord)

             p.then(function(){
                return MonsterCloud.put("/tapi/"+$scope.kind+"/"+$scope.domain, $scope.dnsRecord)
             })
             .then(function(data){
                 $scope.dnsRecord = {}
                 doRefetch($scope, data.hash[0])
             })


             return p

        }
      })

   }

}])




})(app)
