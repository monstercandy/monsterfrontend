(function(app){

  var templateDirectory = "webhosting-superuser"

  var webhostingApi = "/su/webhosting/"


app.factory("mcSuperuserWebhostingUrl", function(){
   return function(server, suffix) {
      return "/s/"+server+webhostingApi+(suffix||"")
   }
})

app.directive("mcWebhostingConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/webhosting'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Webhosting microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

app.directive("mcWebhostingSlowlogListSuperuser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-webhosting-slowlog-list ></mc-webhosting-slowlog-list>",
    "controller": ["$scope","webhostingLookup","accountLookup",function($scope, webhostingLookup,accountLookup){
        $scope.superuser = true;

        $scope.postProcess = function(data){
            accountLookup(data.events, "sl_user_id")
            webhostingLookup(data.events, $scope.server, null, "sl_webhosting")
        }

    }]
  }
}], {
    title: "List of slow web application requests",
    paramsRequired: { server:"@"},
    menuKey: templateDirectory,
})

app.directive("mcWebhostingAccesslogListSuperuser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-webhosting-accesslog-list ></mc-webhosting-accesslog-list>",
    "controller": ["$scope","webhostingLookup","accountLookup","MonsterCloud","$alertSuccess",function($scope, webhostingLookup,accountLookup,MonsterCloud,$alertSuccess){
        $scope.superuser = true;

        $scope.process = function(){
            $scope.processDisabled = true;
            return MonsterCloud.post($scope.uriPrefix+"process", {})
              .then(function(){
                   return $alertSuccess();
              })
              .then(function(){
                   $scope.processDisabled = false;
                   return $scope.filter();
              });
        }

        $scope.postProcess = function(data){
            webhostingLookup(data.events, $scope.server, null, "al_webhosting", null, "user_id")
            .then(function(){
                return accountLookup(data.events, "user_id")
            });
        }

    }]
  }
}], {
    title: "Web application request statistics",
    paramsRequired: { server:"@"},
    menuKey: templateDirectory,
})


app.directive('mcSuperuserWebhostingServerActions', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-server-actions'),
      controller: ["$scope","MonsterCloud","mcSuperuserWebhostingUrl","mcWebhostingTask","$alertSuccess",
        function($scope, MonsterCloud, mcSuperuserWebhostingUrl,mcWebhostingTask,$alertSuccess){

            $scope.szarBackupAll = function(cat){
                return mcWebhostingTask($scope.server, "TITLE_WEBHOSTING_SZAR_BACKUP",
                  MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/szar/backup/"+cat,{})
                )
            }

            $scope.phpFpmRegenAll = function(){
               MonsterCloud.post(mcSuperuserWebhostingUrl($scope.server, "php/fpm/config"), {})
                 .then(function(){
                    return $alertSuccess()

                 })
            }

            $scope.phpFpmRehashAll = function(){
               MonsterCloud.post(mcSuperuserWebhostingUrl($scope.server, "php/fpm/rehash"), {})
                 .then(function(){
                    return $alertSuccess()
                 })
            }

      }]
    }
}],
      {
       title:"Webhosting server actions",
       menuKey: templateDirectory,
       paramsRequired: {server:"@"},
       keywords: ["Regenerate all PHP-FPM configs", "Rehash all PHP-FPM servers", "Szar backup"]
      }
)

app.service("mcStorageDisks", ["MonsterCloud", "mcSuperuserWebhostingUrl", function(MonsterCloud, mcSuperuserWebhostingUrl){
    return function(server){
       return MonsterCloud.get(mcSuperuserWebhostingUrl(server, "supported/storages"))      
    }
}])

app.directive('mcSuperuserWebhostingAdd', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-add'),
      controller:  showErrorsController({
         inject: ["mcSuperuserWebhostingTemplatesFactory", "mcStorageDisks","$alertSuccess","mcRoutesService"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcSuperuserWebhostingTemplatesFactory, mcStorageDisks){
            $scope.webhosting = {template_override:{}};

            mcStorageDisks($scope.server)
                .then(function(d){
                   $scope.diskStorages = d
                })


            mcSuperuserWebhostingTemplatesFactory()
              .then(function(d){
                  $scope.webhostingTemplates = d
              })
         },
         fireCallback: function($scope, MonsterCloud, $q, mcSuperuserWebhostingTemplatesFactory, mcStorageDisks,$alertSuccess,mcRoutesService){

           return MonsterCloud.put("/s/"+$scope.server+"/wizard/webhosting" , angular.extend({w_server_name: $scope.server}, $scope.webhosting))
              .then(function(){
                 return $alertSuccess()
              })
              .then(function(){
                 return mcRoutesService.jumpToState({stateName:"mc-superuser-webhosting-list", stateParams:{server:$scope.server}})
              })
         }

      })
    }
}],
      {
       title:"Add webhosting",
       menuKey: templateDirectory,
       paramsRequired: {server:"@"},
      }
)

app.directive('mcSuperuserWebhostingList', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-list'),
      controller: ["$scope", "MonsterCloud", '$q', 'mcSuperuserWebhostingUrl', "accountLookup",'mcRoutesService', '$task',
        function($scope, MonsterCloud, $q, mcSuperuserWebhostingUrl, accountLookup,  mcRoutesService, $task){
           $scope.webhostings = []

          $scope.select = function(webhosting) {

              return mcRoutesService.jumpToState({stateName:"mc-superuser-webhosting", stateParams:{server:$scope.server,whId:webhosting.wh_id}})
          }

          $scope.removeWebstore = function(webhosting){
               var prom = MonsterCloud.delete("/wizard/webhosting", {wh_id: webhosting.wh_id, wh_user_id: webhosting.wh_user_id, server: $scope.server});
                return prom.then(function(r){

                    var url = "/tasks/"+r.id
                    return $task({title:"WEBSTORE_TASK_REMOVE", "url":url}).then($scope.refetch);
                })
          }
          $scope.removeLowLevel = function(webhosting){
               return MonsterCloud.delete(mcSuperuserWebhostingUrl($scope.server, webhosting.wh_id))                 
                 .then(function(data){
                    var url = "/s/"+$scope.server+"/webhosting/tasks/"+r.id
                    return $task({title:"WEBSTORE_TASK_REMOVE", "url":url}).then($scope.refetch);
                 })
          }
           $scope.refetch = function() {
                 return MonsterCloud.get(mcSuperuserWebhostingUrl($scope.server))
                   .then(function(data){
                      $scope.webhostings = data
                      accountLookup($scope.webhostings, "wh_user_id")

                      $scope.whs = data;
                   })

           }

           $scope.refetch()

       }]
   }
}],
      {
       title:"List webhostings",
       menuKey: templateDirectory,
       paramsRequired: {server:"@"},
      }
)


app.directive('mcSuperuserWebhostingFlags', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-flags'),

      // shared scope!

      controller:  showErrorsController({
         inject: ["mcWebhostingTask","mcWebhostingSzarWebAdmin","$alertSuccess", "$prompt"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcWebhostingTask,mcWebhostingSzarWebAdmin, $alertSuccess, $prompt){
             var configured = false
             $scope.$watch('webhosting', function(newValue, oldValue) {

                if(!$scope.webhosting) return
                if(configured) return

                configured = true
                $scope.wh_docker_hints_str = JSON.stringify($scope.webhosting.wh_docker_hints || {});
             });


             function doWalk(q) {
                 return mcWebhostingTask($scope.server, "TITLE_WEBHOSTING_WALK", MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/"+$scope.webhosting.wh_id+"/"+q,{}))
             }

              $scope.szarWebAdmin = function(cat){
                  return mcWebhostingSzarWebAdmin($scope.server, $scope.webhosting.wh_id, cat)
              }

              $scope.szarBackup = function(cat){
                  return mcWebhostingTask(
                    $scope.server,
                    "TITLE_WEBHOSTING_SZAR_BACKUP",
                    MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/"+$scope.webhosting.wh_id+"/szar/backup/"+cat,{})
                  )
              }

              $scope.changeEmailServicePassword = function(){
                return $prompt({title: "TITLE_CHANGE_EMAIL_SERVICE_ACCOUNT_PASSWORD", message: "MESSAGE_CHANGE_EMAIL_SERVICE_ACCOUNT_PASSWORD", placeholder: "PLACEHOLDER_CHANGE_EMAIL_SERVICE_ACCOUNT_PASSWORD"})
                  .then(function(password){
                     return MonsterCloud.post(
                        "/s/"+$scope.server+"/su/webhosting/"+$scope.webhosting.wh_id,
                        {wh_mail_service_account_password: password}
                     );                    
                  })
                  .then(function(){
                      return $alertSuccess();
                  });

              }

             $scope.walk = function() {
                 return doWalk("walk")
             }

             $scope.walkAndStore = function() {
                 return doWalk("walk-and-store")

             }

             $scope.rehashPhpFpm = function() {
                 return MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/php/fpm/rehash/"+$scope.webhosting.wh_php_version, {})
                   .then(function(d){
                      return $alertSuccess()
                   })
             }

             $scope.displayPhpFpm = function() {
                 return MonsterCloud.get("/s/"+$scope.server+"/su/webhosting/"+$scope.webhosting.wh_id+"/php/fpm/config", {})
                   .then(function(d){
                      $scope.response = d
                   })
             }
             $scope.regenPhpFpm = function() {
                 return MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/"+$scope.webhosting.wh_id+"/php/fpm/config", {})
                   .then(function(){
                      return $alertSuccess()
                   })
             }

             $scope.sendTallyWarning = function() {
                 return MonsterCloud.post("/s/"+$scope.server+"/webhosting/"+$scope.whId+"/tallywarning", {})
                   .then(function(){
                      return $alertSuccess()
                   })
             }


             $scope.setExpired = function(expired) {
                 return MonsterCloud.post("/s/"+$scope.server+"/wizard/webhosting/expired", {wh_id: $scope.whId, wh_is_expired: expired})
                   .then(function(){
                      $scope.webhosting.wh_is_expired = expired
                      return $alertSuccess()
                   })
             }

         },
         fireCallback: function($scope, MonsterCloud, $q, mcWebhostingTask,mcWebhostingSzarWebAdmin, $alertSuccess){

           var payload = {
              wh_max_execution_second:$scope.webhosting.wh_max_execution_second || 0,
              wh_php_fpm_conf_extra_lines:$scope.webhosting.wh_php_fpm_conf_extra_lines || "",
              wh_docker_hints: JSON.parse($scope.wh_docker_hints_str),
           }
           return MonsterCloud.post("/s/"+$scope.server+"/su/webhosting/"+$scope.whId , payload)
              .then(function(){
                 return $alertSuccess()
              })
         }


      })

   }
}])


app.directive('mcSuperuserWebhostingParams', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-params'),
      // shared scope!

      controller: ["$scope", "MonsterCloud","$alertSuccess",
        function($scope,  MonsterCloud, $alertSuccess){

          var configured = false
          $scope.$watch('webhosting', function(newValue, oldValue) {

            if(!$scope.webhosting) return
            if(configured) return

            configured = true
            $scope.webhosting.template_override = JSON.parse($scope.webhosting.wh_template_override || "{}")
          });

         $scope.clearParams = function(){
            $scope.webhosting.template_override = {}
         }

         $scope.setWebhostingTemplate = function() {
             var p = {}
             Array("wh_name", "wh_template", "template_override").forEach(function(x){
                p[x] =  $scope.webhosting[x]
             })

             return $scope.webhostingFactory.Change(p, true)
               .then(function(){
                return $alertSuccess()
               })
         }

       }]
   }
}])


app.directive('mcSuperuserWebhosting', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting'),

      controller: ["$scope", "mcWebhostingFactory", 'mcSuperuserWebhostingUrl', 'mcSuperuserWebhostingTemplatesFactory',
        function($scope, mcWebhostingFactory, mcSuperuserWebhostingUrl, mcSuperuserWebhostingTemplatesFactory){


          mcSuperuserWebhostingTemplatesFactory()
            .then(function(d){
                $scope.webhostingTemplates = d
            })

          $scope.webhostingFactory = mcWebhostingFactory($scope.server, $scope.whId, true)


         return $scope.webhostingFactory.Lookup()
           .then(function(d){
              $scope.webhosting = d
           })


       }]
   }
}],{
  title: "Edit webhosting package",
  menuKey: templateDirectory,
  paramsRequired: { "server":"@", "whId":"@" },
})


app.directive('mcSuperuserWebhostingEditLink', ["templateDir", function(templateDir){

    return {
      templateUrl: templateDir(templateDirectory,'webhosting-edit-link'),
      scope: {server:"@", whId:"@"},
      controller: ["$scope", "mcFindRole",
        function($scope, mcFindRole){
          $scope.targetUidi = mcFindRole("SUPERUSER")
          $scope.show = typeof $scope.targetUidi != "undefined"
          $scope.params = {server: $scope.server, whId: $scope.whId, uidi: $scope.targetUidi}
          // console.log("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",$scope.params)

       }]
   }
}])



app.directive("mcSuperuserWebstoreAddDomain", ["templateDir", function(templateDir){
  return {
      templateUrl: templateDir(templateDirectory,'webstore-add-domain'),
      controller:  showErrorsController({
         inject:["$alertSuccess","mcWebhostingFactory"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcWebhostingFactory) {

            mcWebhostingFactory($scope.server, $scope.whId, true).Lookup()
              .then(function(wh){
                 $scope.wh = wh;
              })
         },
         fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){
             return MonsterCloud.put("/su/accountapi/account/"+$scope.wh.wh_user_id+"/domains", {domain: $scope.d.domainName})
               .then(function(){
                   return MonsterCloud.put("/su/accountapi/webhostingdomains/", {"domain": $scope.d.domainName, "wd_server": $scope.server, "wd_webhosting": $scope.whId})
               })
               .then(function(){
                   var ps = [];
                   var docroot = "/"+$scope.d.domainName+"/pages";
                   var url = "/s/"+$scope.server+"/su/docrootapi/docroots/"+$scope.whId+"/"+$scope.d.domainName+"/entries/";
                   ps.push(MonsterCloud.put(url, {host: $scope.d.domainName, docroot: docroot}));
                   ps.push(MonsterCloud.put(url, {host: "www."+$scope.d.domainName, docroot: docroot}));
                   return $q.all(ps);
               })
               .then(function(){
                  if(!$scope.d.attachToEmail) return;
                  return MonsterCloud.put("/s/"+$scope.server+"/su/email/domains", {do_domain_name: $scope.d.domainName, do_webhosting: $scope.whId, account: $scope.wh.wh_user_id})
               })
               .then(function(){
                   return $alertSuccess();
               })

         }
      })

  }
}], {
    title: "Attach domain to the webstore",
    paramsRequired: { server:"@", whId:"@"},
    menuKey: templateDirectory,
})



app.directive('mcStorageStats', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'storage-stats'),
      controller: ["$scope", "MonsterCloud", function($scope, MonsterCloud){

          var baseUrl = "/s/"+$scope.server+"/su/webhosting/system/";

          function refreshProc(k){
              return MonsterCloud.get(baseUrl+"proc/"+k)
                .then(function(d){
                   $scope[k] = d;
                })
          }
          $scope.refreshSmart = function(disk){
             $scope.smarts[disk] = {output: "..."};
             return MonsterCloud.get(baseUrl+"bin/smartctl/"+disk)
               .then(function(d){
                   var s = angular.extend({disk: disk}, d);
                   $scope.smarts[disk] = s;
               });
          }
          $scope.refreshSmarts = function(){
              $scope.smarts = {};
              $scope.partitions.disks.forEach($scope.refreshSmart);
          }

          $scope.refreshMdstat = function(){
              return refreshProc("mdstat");
          }
          $scope.refreshPartitions = function(){
              return refreshProc("partitions");
          }
          $scope.refreshSwaps = function(){
              return refreshProc("swaps");
          }
          $scope.refreshFreespace = function(){
              return MonsterCloud.get(baseUrl+"bin/df")
                .then(function(d) {
                    $scope.df = d;
                })
          }

          return $scope.refreshFreespace()
            .then($scope.refreshMdstat)
            .then($scope.refreshSwaps)
            .then($scope.refreshPartitions)
            .then($scope.refreshSmarts)
      }]
   }
}], {
   title: "Storage statistics",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@" },
})


})(app)

