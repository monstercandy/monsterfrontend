(function(app){

  var templateDirectory = "installatron-superuser"


app.directive("mcInstallatronConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/installatron'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Installatron microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcSuperuserInstallatron", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'installatron'),
        controller: ["$scope","MonsterCloud","$window",
          function($scope,MonsterCloud,$window) {
             var url = "/s/"+$scope.server+"/su/installatron/guixfer"
             $scope.start = function(){
                return MonsterCloud.post(url,{})
                  .then(function(r){
                      $window.open(r.url)
                  })
             }

          }]        
     }

  }], {
     title: "Installatron admin interface",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"}
  })

})(app)

