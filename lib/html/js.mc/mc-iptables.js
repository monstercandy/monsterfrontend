(function(app){

  var templateDirectory = "iptables"
  var apiPrefix = "/iptables/"

  app.service("mcFirewallService", ["MonsterCloud", "$q", "$task", function(MonsterCloud,$q,$task){
     var resultCache = {}
     var promCache = {}
     return function(server, whId){
        var url = "/s/"+server+apiPrefix+"whextra/"+whId+"/firewallrules/";
        var key = "" + server + whId;
        var re = {
           firewallOn: function(){
              return setStatus(true);
           },
           firewallOff: function(){
              return setStatus(false);
           },
           openFirewallLog: function(){
              return MonsterCloud.post(url+"logging",{})
                .then(function(r){
                    var url = "/s/"+server+"/iptables/tasks/"+r.id
                    return $task({title:"TITLE_FIREWALL_LOG", "url":url})

                })
           },
           Remove: function(entry){
             return MonsterCloud.delete(url+"rule", entry)
               .then(function(){
                  return re.Lookup(true)
               })
           },
           Insert: function(data){
              return MonsterCloud.put(url, data)
               .then(function(){
                  return re.Lookup(true)
               })
           },
           Lookup: function(force){
              if((!force)&&(resultCache[key])) return $q.when(resultCache[key]);
              if(!promCache[key]) {
                promCache[key] = MonsterCloud.get(url)
                 .then(function(data){
                     delete promCache[key];
                     resultCache[key] = angular.extend(resultCache[key] || {}, data);
                     return $q.when(resultCache[key])
                 })
               }

              return promCache[key];
           }
        }

        return re;

        function setStatus(active){
          return MonsterCloud.post(url+"status", {active:active})
            .then(function(){
                if(resultCache[key])
                  resultCache[key].active = active;
                return $q.when()
            })
        }
     }
  }])

  app.directive("mcFirewallRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'firewall-row'),
      scope: { "server":"@","whId":"@"},
      controller: ["$scope", "mcFirewallService", function($scope, mcFirewallService){

          var f = mcFirewallService($scope.server, $scope.whId)
          f.Lookup()
            .then(function(result){
              $scope.firewall = result
            })

          $scope.firewallOn = function(){
             f.firewallOn()
          }
          $scope.firewallOff = function(){
             f.firewallOff()
          }
      }]
   }
}])


app.directive("mcWebhostingFirewall", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-firewall'),
      controller:  showErrorsController({
          inject: ["mcFirewallService","mcWebhostingTask"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q,mcFirewallService,mcWebhostingTask){
              $scope.f = mcFirewallService($scope.server, $scope.whId)

              $scope.f.Lookup().then(function(d){
                 $scope.firewall = d
              })
              $scope.actions= ["REJECT","ACCEPT"]


              $scope.openFirewallLog = function(){
                  return $scope.f.openFirewallLog();
              }
              $scope.remove = function(entry){
                  return $scope.f.Remove(entry);
              }

          },
          fireCallback: function($scope, MonsterCloud, $q){
             return $scope.f.Insert($scope.firewallrule)
               .then(function(d){
                   $scope.firewallrule = {}
               })
          }
      })
   }
}], {
   title: "Firewall for web applications",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@","whId":"@"},
   keywords: ["iptables"],
})


})(app)
