var mcConfig = angular.module("mc-config", [])
  .service("mcConfig", ['$window', function($window){
     var c = angular.extend({
                 "indexPageName": "index.html",
                 "default_country_code": "36",
                 "api_uri_prefix": "/api",
                 "alert_timeout_ms": 10000,
                 "tasksDontDisplayUntilMs": 5000,
                 "editableFileSizeLimitBytes": 100*1024,
               }, $window.mcConfiguration)

     var re = {
         "config": c
     }

     if(!re.config.mailServiceDomains)
        re.config.mailServiceDomains = [".svc"];

     return re;

  }])

 .factory("templateDir", ["mcConfig", function(mcConfig){
   return function(dir, tpl){
      // console.log("template:",mcConfig.config,dir, tpl)
      return  (mcConfig.config.template_directory||"/template.mc") + "/"+dir+"/tpl-"+tpl+".html?" + (mcConfig.config.template_version ? mcConfig.config.template_version : new Date())
   }
  }])
 .service("mcServerHostname", ["mcConfig", function(mcConfig){
     var re = function(server, forceKey){
        // console.log("template:",tpl)
        return mcConfig.config["force_"+forceKey+"_hostname"] || (server + "." + mcConfig.config.service_primary_domain)
     }
     re.apply = function(server, forceKey, $scope){
        $scope.hostname = re(server, forceKey)
     }
     return re
  }])
