(function(app){

  var templateDirectory = "dbms-superuser"


app.directive("mcDbmsConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/dbms'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "DBMS microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcDbmsSuperuserServer", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        scope: {server: "@"},
        templateUrl: templateDir(templateDirectory,'server'),
     }

  }])


app.directive("mcDbmsSlowlogListSuperuser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-dbms-slowlog-list ></mc-dbms-slowlog-list>",
    "controller": ["$scope","webhostingLookup","accountLookup",function($scope, webhostingLookup,accountLookup){
        $scope.superuser = true;

        $scope.postProcess = function(data){
            accountLookup(data.events, "ds_user_id")
            webhostingLookup(data.events, $scope.server, null, "ds_webhosting")
        }

    }]
  }
}], {
    title: "List of slow database queries",
    paramsRequired: { server:"@"},
    menuKey: templateDirectory,
})

  app.directive("mcDbmsSuperuserStats", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'stats'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl","webhostingLookup","accountLookup","mcDbmsTask",
        function($scope, MonsterCloud, mcDbmsApiUrl,webhostingLookup,accountLookup,mcDbmsTask){

           function toResponse(p){
               return p.then(function(d){
                   $scope.response = d
               })
           }

           $scope.displayStoredStats = function(){
               return toResponse(MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/databases/stats"))
           }

           $scope.displayLiveStats = function(){
               return toResponse(MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/stats/live"))
           }

           $scope.dbbackup = function(record){
                return mcDbmsTask(
                  "DBMS_TASK_TITLE_DBBACKUP",
                  $scope.server,
                  MonsterCloud.post(mcDbmsApiUrl($scope.server,true)+"/databases/"+(record?record.ws_webhosting+"/":"")+"dbbackup", {})
                )
           }

           $scope.reprocessStats = function(record){
               var p = record ? {onlyWebhosting:record.ws_webhosting} : {}
               return MonsterCloud.post(mcDbmsApiUrl($scope.server,true)+"/stats/get-and-process", p)
                 .then(function(){
                    $scope.refetch()
                 })
           }

           $scope.unlock = function(record){
              doLocking(record, false)
           }
           $scope.lock = function(record){
              doLocking(record, true)
           }

           function doLocking(record, newLockState) {
               MonsterCloud.post(mcDbmsApiUrl($scope.server,true)+"/stats/"+record.ws_webhosting, {ws_locked: newLockState})
                 .then(function(){
                    // record.ws_locked = newLockState
                    $scope.refetch()
                 })
           }

           $scope.refetch = function(){

                    return MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/stats/")
                        .then(function(data){
                            $scope.stats = data
                            accountLookup($scope.stats, "ws_user_id")
                            webhostingLookup($scope.stats, $scope.server, null, "ws_webhosting")
                            $scope.consumption = data;
                        })

           }

           $scope.refetch()
        }]
      }
  }],
  {
     title: "Database storage consumption statistics per webhosting",
     paramsRequired: { "server": "@" },
     menuKey: templateDirectory,
     keywords:["recalculation", "backup"],
  })




  app.directive("mcDbmsSuperuserDbms", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'dbms'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl","$compile","$element",'$window','mcDbmsTask',
          function($scope, MonsterCloud, mcDbmsApiUrl, $compile, $element,$window,mcDbmsTask){

           function toResponse(p){
               return p.then(function(d){
                   $scope.response = d
               })
           }

           $scope.edit = function(record){
                var t  = "<mc-dbms-superuser-dbms-edit server=\""+$scope.server+"\" dbms-name=\""+record.dm_dbms_instance_name+"\"></mc-dbms-superuser-dbms-edit>"
                var el = $compile( t )( $scope );
                var sl = $element.find("#dmbsSpecific")
                // console.log("sub element", sl)
                sl.html( el );
                $window.location.href = "#dmbsSpecific"

           }

           $scope.delete = function(record){
              return MonsterCloud.delete(mcDbmsApiUrl($scope.server,true)+"/dbms/"+record.dm_dbms_instance_name,{})
                .then(function(){
                    $scope.refetch()
                })
           }

           $scope.test = function(record){
              return toResponse(MonsterCloud.post(mcDbmsApiUrl($scope.server,true)+"/dbms/"+record.dm_dbms_instance_name+"/test",{}))
           }

           $scope.repairAllDatabases = function(record){
                return mcDbmsTask(
                  "DBMS_TASK_TITLE_REPAIR_DATABASE",
                  $scope.server,
                  MonsterCloud.post(mcDbmsApiUrl($scope.server, true)+"/dbms/"+record.dm_dbms_instance_name+"/repair", {})
                )
           }


           $scope.refetch = function(){
              MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/dbms/")
                .then(function(data){
                    $scope.dbms = data
                })
           }

           $scope.$on("refetch",function(event){
                $element.find("#dmbsSpecific").html("")
               return $scope.refetch()
           });


           $scope.refetch()

        }]
      }
  }],
  {
     title: "DBMS list",
     paramsRequired: { "server": "@" },
     menuKey: templateDirectory,
  })





  app.directive("mcDbmsSuperuserScoreboard", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'scoreboard'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl","$timeout",
          function($scope, MonsterCloud, mcDbmsApiUrl, $timeout){

            var pull;

          $scope.fields = [
            "dm_dbms_instance_name","Id","User","Host","db","Command","Time","State","Info","Progress"
          ]

              $scope.$on('$destroy', function(){
                // console.log("destroying scope", pull);
                if(pull)
                   $timeout.cancel(pull);
              });
              
           var url = mcDbmsApiUrl($scope.server, true)+"/stats/scoreboard"
           refetch()

           function refetch(){
              return MonsterCloud.get(url)
                .then(function(data){
                   $scope.scoreboard = data
                   pull = $timeout(refetch, 10000);
                })
           }

        }]
      }
  }],
  {
     title: "DBMS scoreboard",
     paramsRequired: { "server": "@" },
     menuKey: templateDirectory,
  })



  app.directive('mcDbmsConnstrMysql', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      scope: { "connstr": "=" },
      templateUrl: templateDir(templateDirectory,'dbms-connstr-mysql'),
        controller: ["$scope",function($scope, MonsterCloud, mcDbmsApiUrl, $compile, $element){
            // console.log("HEY!", $scope.connstr)
            $scope.conn = angular.fromJson($scope.connstr)
            $scope.$watch('conn', function(newValue, oldValue) {
              $scope.connstr = angular.toJson(newValue)
            }, true);
        }]
    }
  }])


  app.directive('mcDbmsSuperuserDbmsAdd', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      scope: { "server": "@" },
      templateUrl: templateDir(templateDirectory,'dbms-add'),
      controller:  showErrorsController({
         inject: ["mcDbmsApiUrl"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcDbmsApiUrl){
            MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/config/supported/dbmstypes")
              .then(function(d){
                 $scope.supportedDbmsTypes = d
              })

            $scope.resetDbms = function(){
              $scope.dbms = { dm_dbms_type: "" }

            }

            $scope.resetDbms()

         },
         fireCallback: function($scope, MonsterCloud, $q, mcDbmsApiUrl){

           return MonsterCloud.put(mcDbmsApiUrl($scope.server,true)+"/dbms", $scope.dbms)
              .then(function(){
                 $scope.resetDbms()
                 $scope.$parent.$broadcast("refetch")
              })
         }

      })
    }
  }])



  app.directive('mcDbmsSuperuserDbmsEdit', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      scope: { "server": "@", dbmsName: "@" },
      templateUrl: templateDir(templateDirectory,'dbms-edit'),
      controller:  showErrorsController({
         inject: ["mcDbmsApiUrl"],
         additionalInitializationCallback: function($scope, MonsterCloud,$q,mcDbmsApiUrl){
            MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/dbms/"+$scope.dbmsName)
              .then(function(d){
                  $scope.dbms = d
              })
         },
         fireCallback: function($scope, MonsterCloud, $q, mcDbmsApiUrl){
           var nd = {dm_dbms_instance_name: $scope.dbms.dm_dbms_instance_name, dm_connection_string: $scope.dbms.dm_connection_string}
           return MonsterCloud.post(mcDbmsApiUrl($scope.server,true)+"/dbms/"+$scope.dbmsName, nd)
              .then(function(){
                 $scope.$parent.$broadcast("refetch")
              })
         }

      })
    }
  }])




    app.directive("mcDbmsSuperuserDatabases", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'databases'),
        controller: ["$scope",'MonsterCloud',"mcDbmsApiUrl",'accountLookup','webhostingLookup',
        function($scope, MonsterCloud, mcDbmsApiUrl,accountLookup,webhostingLookup){

           $scope.databases = {}

           $scope.refetch = function(){
              console.log("refetching database list, superuser")

              return MonsterCloud.get(mcDbmsApiUrl($scope.server,true)+"/databases/")
                .then(function(data){
                    $scope.databases.data = data
                    accountLookup(data, "db_user_id")
                    webhostingLookup(data, $scope.server, null, "db_webhosting")
                    $scope.$broadcast("reshot")
                })

           }

           $scope.$on("refetch",function(event){
               return $scope.refetch()
           });


           $scope.refetch()

        }]
      }
  }],
  {
     title: "Manage databases",
     paramsRequired: { "server": "@" },
     menuKey: templateDirectory,
  })

})(app)

