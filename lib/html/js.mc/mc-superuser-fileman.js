(function(app){

  var templateDirectory = "fileman-superuser"

app.directive("mcFilemanConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/fileman'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Fileman microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

  app.directive("mcSuperuserCronList", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'cron-list'),
        controller: ["$scope","MonsterCloud","mcCronTransform","accountLookup","webhostingLookup",
          function($scope,MonsterCloud,mcCronTransform,accountLookup,webhostingLookup) {

            $scope.baseUrl = "/s/"+$scope.server+"/su/fileman/cron/"

            $scope.delete = function(cron){
               return MonsterCloud.delete($scope.baseUrl+cron.cron_id).then($scope.refetch)
            }

            $scope.refetch = function(){

                   return MonsterCloud.get($scope.baseUrl)
                      .then(function(d){
                         $scope.result = d

                         var re = mcCronTransform(d)

                         accountLookup(re, "user_id")
                         webhostingLookup(re, $scope.server, null, "wh_id")

                         $scope.crontabs = re;
                      });

            }

            $scope.refetch()

          }]
     }

  }], {
     title: "List of cron entries",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"},
     keywords: ["cron","crontab"],
  })

  Array("Web", "Mail").forEach(function(cat){
    var lcat = cat.toLowerCase();
    app.directive("mcSuperuserFileman"+cat, [function(){
        return {
          restrict: "E",
          template: "<mc-fileman server='{{::server}}' wh-id='{{::whId}}' path-category='"+lcat+"' superuser='true'></mc-fileman>",
       }

    }], {
       title: "File manager ("+lcat+")",
       menuKey: templateDirectory,
       paramsRequired: {"server": "@", "whId": "@"},
    })

  })


})(app)

