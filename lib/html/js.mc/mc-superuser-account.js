(function(app){
  const templateDirectory = "accountapi-superuser"
  const apiPrefix = "/su/accountapi"

  const permissions = "SUPERUSER ADMIN_ACCOUNTAPI";

app.directive('mcLoginlogSuperuser', ["templateDir", function(templateDir){
    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'loginlog-superuser'),
    }
}], {
    "title": "Global login log",
    "menuKey": templateDirectory,
    "permissions": permissions,
})

app.directive("mcAccountapiConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/accountapi'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "Accountapi microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
    permissions: permissions,
})


app.directive("mcSmsList", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"sms-list"),
    "controller": ["$scope", "MonsterCloud", function($scope, MonsterCloud){

        // $scope.subsystems = []  // it is with one-time prefix in the template, so we should nto initialize it here!

        $scope.statuses = ["","failed", "sent", "sent_and_received"];

        var uriPrefix = apiPrefix + "/sms/";

        $scope.refetch = function(){
               var d = queryDetails();

               return MonsterCloud.post( uriPrefix + "search", d)
                 .then(function(data){
                    $scope.smss = data;
                 })

        }

        $scope.refetch();

        function queryDetails() {
          var d = {}
          Array("notbefore","notafter","s_status", "s_message", "s_recipient").forEach(function(v){
            if(($scope[v] !== "")&&($scope[v] !== null))
               d[v] = $scope[v]
          })
          return d
        }

    }]
  }
}], {
    title: "List of SMS messages",
    paramsRequired: {},
    menuKey: templateDirectory,
    permissions: permissions,
})

app.directive("mcWebstoreDomainBindings", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"webstore-domain-bindings"),
    "controller": showErrorsController({
          inject:["webhostingLookup", "serversFactory", "$compile", "$element"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, webhostingLookup, serversFactory, $compile, $element){

              $scope.uriPrefix = apiPrefix + "/webhostingdomains/";

              $scope.nwd = {webhosting:{}};

               serversFactory().then(function(d){
                  $scope.servers = d
               })

               $scope.serverChange = function(){
                     var html = '<mc-global-webhosting-selector per-server="true" server="{{::nwd.wd_server}}" not-required="true" model="nwd.webhosting" ></mc-global-webhosting-selector>'
                     $element.find("#webhosting").html($compile( html )( $scope ));
               }

              $scope.del = function(wd){
                  var x = {wd_server: wd.wd_server, wd_webhosting: wd.wd_webhosting, domain: wd.wd_domain_canonical_name};
                  return MonsterCloud.delete($scope.uriPrefix, x)
                       .then(function(data){
                          var i = $scope.wds.indexOf(wd);
                          $scope.wds.splice(i, 1);
                       })
              }

              $scope.refetch = function(){

                     return MonsterCloud.get($scope.uriPrefix)
                       .then(function(data){
                          webhostingLookup(data, null, "wd_server", "wd_webhosting")
                          $scope.wds = data;
                       })

              }

              $scope.refetch();

          },
          fireCallback: function($scope, MonsterCloud){
             var x = {wd_server: $scope.nwd.wd_server, domain: $scope.nwd.domain, wd_webhosting: $scope.nwd.webhosting.webhosting_id};
             return MonsterCloud.put($scope.uriPrefix, x)
               .then(function(){
                  delete $scope.nwd.domain;
                  $scope.refetch();
               })
          }
    })
  }
}], {
    title: "Webstore domain bindings",
    paramsRequired: {},
    menuKey: templateDirectory,
    permissions: permissions,
})

Array(
  {n:"mcTelList",u:"tels",t:"List of telephone numbers", p:"t", ef: "t_human"},
  {n:"mcEmailList",u:"emails",t:"List of e-mail addresses", p:"e", ef: "e_email"}
).forEach(function(category){
  app.directive(category.n, ["templateDir", function(templateDir){
    return {
      "templateUrl": templateDir(templateDirectory,category.u),
      "controller": ["$scope", "MonsterCloud", 'accountLookup', '$prompt', '$alertSuccess',
        function($scope, MonsterCloud, accountLookup, $prompt, $alertSuccess) {

          $scope.booleanOptions = ["", "0", "1"];
          $scope.tfilter = {};

          if(category.n=="mcTelList") {
             $scope.sendSms = function(tel){

                return $prompt({title: "SEND_SMS_TITLE", message: "SEND_SMS_MESSAGE", placeholder: "SEND_SMS_PLACEHOLDER"})
                  .then(function(message){
                      var sms = { s_message: message, s_country_code: tel.t_country_code, s_area_code: tel.t_area_code, s_phone_no: tel.t_phone_no};

                      return MonsterCloud.put("/su/accountapi/sms", sms)
                  })
                  .then(function(){
                      return $alertSuccess({message: "SMS_SUBMITTED_MESSAGE"});
                  });
            }
          }

          $scope.setEscalate = function(tel, to){
             var d = {};
             d[category.ef] = tel[category.ef];
             d[category.p+"_escalate"] = to;
             return MonsterCloud.post("/su/accountapi/"+category.u+"/set-escalate", d)
               .then($scope.refetch);
          }

          $scope.refetch = function(){
                  var d  = angular.copy($scope.tfilter);
                  Object.keys(d).forEach(function(x){
                     if(d[x] === "")
                      delete d[x];
                  });

                  return MonsterCloud.post("/su/accountapi/"+category.u+"/search", d)
                      .then(function(data){
                          accountLookup(data, category.p+"_user_id")

                          $scope[category.u] = data;
                      })

          }

          $scope.refetch();


      }]
    }
  }], {
      title: category.t,
      paramsRequired: {},
      menuKey: templateDirectory,
      permissions: permissions,
  });

})

  app.directive("mcSuperuserSmsSending", ["templateDir", function(templateDir){
    return {
      "templateUrl": templateDir(templateDirectory,"sms-send"),
        controller:  showErrorsController({
          inject:["$alertSuccess", "mcCountryCodes"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess, mcCountryCodes){

            $scope.sms = {};

            MonsterCloud.get("/su/accountapi/config/sms/providers")
              .then(function(providers){
                  $scope.providers = providers;
                  $scope.providers.push("");
              })


            $scope.sms.s_country_code = mcCountryCodes.defaultCode();

            mcCountryCodes.initScope($scope)


          },
          fireCallback: function($scope, MonsterCloud, $q, $alertSuccess){


            var sms = angular.copy($scope.sms);

            return MonsterCloud.put("/su/accountapi/sms", sms)
                .then(function(){
                    return $alertSuccess({message: "SMS_SUBMITTED_MESSAGE"});
                });

          }
        })

    }
  }], {
      title: "SMS sending",
      paramsRequired: {},
      menuKey: templateDirectory,
      permissions: permissions,
  });

  app.directive("mcSuperuserAccountSmsSeemeCredentials", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'seeme-credentials'),
        controller:  showErrorsController({
          inject:["mcSuperuserConfigUrl"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcSuperuserConfigUrl){

               $scope.baseUrl = apiPrefix+"/config/sms/seeme/credentials";


               $scope.delete = function(cert){

                  return MonsterCloud.delete($scope.baseUrl)
                    .then(function(){
                      $scope.creds = {}
                      $scope.thereIs= false
                      return $alertSuccess()
                    })
               }

               $scope.refetch = function(){
                  return MonsterCloud.get($scope.baseUrl)
                    .then(function(r){
                       $scope.current = r
                       if(r.configured)
                         $scope.thereIs= true
                    })

               }

               $scope.creds = {};
               $scope.refetch();


          },
          fireCallback: function($scope, MonsterCloud, $q, mcSuperuserConfigUrl,$alertSuccess){

             return MonsterCloud.post($scope.baseUrl, $scope.creds)
               .then(function(){
                  delete $scope.creds.ApiKey
                  $scope.thereIs= true
                  return $scope.refetch();

               })


          }
        })
     }

  }], {
       title:"SeeMe account",
       menuKey: templateDirectory,
       permissions: permissions,
       paramsRequired: {},
       keywords: ["sms", "provider"],
  })


app.directive("mcSuperuserAccountCredentials", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-credentials"),
    "scope": {
        "Account": "=edit",
    },
    controller:  showErrorsController({
         inject: ["CredentialLogic", "$alertSuccess", "serversFactory"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, CredentialLogic, $alertSuccess, serversFactory){
             $scope.supportedPermissions = []
             $scope.cred = {}
             $scope.updateMode = false;

             $scope.serverNames = [];
             serversFactory.all().then(function(d){
                $scope.serverNames = [""].concat(d.map(function(l){return l.s_name}))
             })



             $scope.del = function(credential){
               var user = credential.c_username
               return MonsterCloud.delete(apiPrefix+"/account/"+$scope.Account.u_id+"/credential/"+user, {})
                  .then(function(){
                      return $scope.refetch();
                  })


             }
             $scope.updatePermissions = function(){
                 return MonsterCloud.post(apiPrefix+"/account/"+$scope.Account.u_id+"/credential/"+$scope.cred.c_username, {c_grants:$scope.cred.c_grants})
                   .then(function(){
                       $scope.refetch()
                       return $alertSuccess()
                   })
             }
             $scope.changePassword = function(){
                 $scope.$broadcast('show-errors-check-validity');
                 if (!$scope.dataForm.$valid) return

                 return MonsterCloud.post(apiPrefix+"/account/"+$scope.Account.u_id+"/credential/"+$scope.cred.c_username+"/password", {new_password:$scope.cred.c_password})
                   .then(function(){
                       return $alertSuccess()
                   })
             }
             $scope.select = function(credential) {
                $scope.cred = angular.copy(credential)
                $scope.updateMode = true
             }
             $scope.refetch = function(){
                 return MonsterCloud.get(apiPrefix+"/account/"+$scope.Account.u_id+"/credentials")
                   .then(function(data){
                       $scope.credentials = CredentialLogic(data)
                   })
             }

             MonsterCloud.get(apiPrefix+"/config/credentials/permissions")
               .then(function(data){
                   $scope.supportedPermissions = data
               })

             $scope.refetch()

         },
         fireCallback: function($scope, MonsterCloud) {

             var tmp = {c_password: $scope.cred.c_password, c_grants: $scope.cred.c_grants, c_uuid: $scope.cred.c_uuid, c_server: $scope.cred.c_server};

             return MonsterCloud.put("/su/accountapi/account/"+$scope.Account.u_id+"/credentials", tmp)
               .then(function(data){
                   $scope.cred = {}
                   $scope.updateMode = false
                   return $scope.refetch()
               })
         }

    })
  }
}])

app.directive("mcSuperuserAccountConnectedDomains", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-connected-domains"),
    "scope": {
        "Account": "=edit",
    },
    controller:  showErrorsController({
         additionalInitializationCallback: function($scope, MonsterCloud){
           $scope.domains = []

           $scope.data_process = function(data){
                data.forEach(function(q){
                  q.human = (q.d_domain_name)
                  if(q.d_domain_canonical_name != q.d_domain_name)
                     q.human += " ("+q.d_domain_canonical_name+")"
                })

                $scope.domains = data
           }

           $scope.del=function(domain){
               return MonsterCloud.delete(apiPrefix+"/account/"+$scope.Account.u_id+"/domain/"+domain.d_domain_canonical_name, {})
                  .then(function(){
                      $scope.domains = $scope.domains.filter(function(e){return e !== domain})

                  })

           }

           $scope.refilter = function(){
              return MonsterCloud.get(apiPrefix+"/account/"+$scope.Account.u_id+"/domains")
                .then(function(data){
                   $scope.data_process(data)
                })

           }

           $scope.refilter()

         },
         fireCallback: function($scope, MonsterCloud) {
           var data = {  }
           return MonsterCloud.put(apiPrefix+"/account/"+$scope.Account.u_id+"/domains", $scope.domain)
              .then(function(){
                $scope.domain = {}
                $scope.refilter()
              })
         }

    })
  }
}])


app.directive("mcSuperuserAccountConnectedWebhostings", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-connected-webhostings"),
    "scope": {
        "Account": "=edit",
    },
    controller:  showErrorsController({
         inject: ["serversFactory"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, serversFactory){
           $scope.webhostings = []

           $scope.del=function(webhosting){
               return MonsterCloud.delete(apiPrefix+"/account/"+$scope.Account.u_id+"/webhosting/"+webhosting.w_server_name+"/"+webhosting.w_webhosting_id, {})
                  .then(function(){
                      return $scope.refilter();
                  })

           }

           $scope.refilter = function(){
              return MonsterCloud.get(apiPrefix+"/account/"+$scope.Account.u_id+"/webhostings")
                .then(function(data){
                    $scope.webhostings = data

                })

           }

           $scope.refilter()

           serversFactory("webhosting").then(function(d){
              $scope.webhostingServers = d
           })

         },
         fireCallback: function($scope, MonsterCloud) {
           var data = {  }
           return MonsterCloud.put(apiPrefix+"/account/"+$scope.Account.u_id+"/webhostings", $scope.webhosting)
              .then(function(){
                $scope.webhosting = {}
                $scope.refilter()
              })
         }

    })
  }
}])


app.directive("mcSuperuserAccountForcePassword", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-force-password"),
    "scope": {
        "Account": "=edit",
    },
    controller:  showErrorsController({
         inject: ["$alertSuccess"],
         additionalInitializationCallback: function($scope, MonsterCloud) {
            $scope.user = {}
            $scope.tmp = {}
         },
         fireCallback: function($scope, MonsterCloud, $q, $alertSuccess) {
           return MonsterCloud.post(apiPrefix+"/account/"+$scope.Account.u_id+"/force_password", $scope.user)
              .then(function(){
                 $scope.user = {}
                 $scope.tmp = {}
                 return $alertSuccess({message:'Password changed successfully'})
              })
         }

    })
  }
}])

app.directive("mcSuperuserAccountEdit", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-edit"),
    "scope": {
        "Account": "=edit",
    },
      controller:  showErrorsController({
         inject:["$alertSuccess"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, $alertSuccess) {
            $scope.justDeleted = false
            $scope.delete = function(){
               MonsterCloud.delete(apiPrefix+"/account/"+$scope.Account.u_id, {})
                 .then(function(){
                     $scope.justDeleted = true
                 })
            }

            $scope.forceOffTwoFactorAuth = function(){
               return MonsterCloud.post(apiPrefix+"/account/"+$scope.Account.u_id+"/totp/force-off", {})
                 .then(function(){
                    return $alertSuccess();
                 });
            }
         },
         fireCallback: function($scope, MonsterCloud,$q,$alertSuccess){
           var data = { "u_name": $scope.Account.u_name, "u_comment": $scope.Account.u_comment }
           return MonsterCloud.post(apiPrefix+"/account/"+$scope.Account.u_id, data)
              .then(function(){
                 return $alertSuccess()
              })
         }

      })
  }
}])


app.directive("mcSuperuserAccountSelect", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-select"),
    controller: ["$scope","MonsterCloud","$compile", "$element",function($scope,MonsterCloud,$compile, $element){
        $scope.selectedUser = {u_id: $scope.uId};
        $scope.jump = function(n){

          var t = "<"+n+" edit='selectedUser'></"+n+">";
          $element.find("#template").html($compile( t )( $scope ));

        }
        return MonsterCloud.get(apiPrefix+"/account/"+$scope.uId)
          .then(function(data){
             angular.extend($scope.selectedUser, data);
             $scope.jump("mc-superuser-account-edit");
          })


    }]
  }
}], {
  "title": "Edit account",
  "menuKey": templateDirectory,
  "permissions": permissions,
  "paramsRequired": {"uId":"@"},
})

app.directive("mcSuperuserAccountList", ["$window", "templateDir", function($window, templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-account-list"),
    "scope": {},
    "controller": ["$scope", "MonsterCloud", 'mcRoutesService', function($scope, MonsterCloud, mcRoutesService){

        $scope.deletedStates = ["","0","1"]
        $scope.deleted = "0"

        $scope.refetch = function(tableState){
          
               var d = { deleted: $scope.deleted, where: tableState.search.str};
               $scope.displayedCollection = null;
               return MonsterCloud.post(apiPrefix+"/accounts/count", d)
                 .then(function(data){
                     tableState.pagination.totalItemCount = data.count;

                     var p = angular.extend(d, tableState.pagination.params);
                     return MonsterCloud.post(apiPrefix+"/accounts/search", p)
                 })
                 .then(function(data){
                        $scope.displayedCollection = data.accounts;
                 })

        }


    }]
  }
}],{
  "title": "Manage accounts",
  "menuKey": templateDirectory,
  "permissions": permissions,
})



app.directive("mcSuperuserAccountServers", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-servers"),
    "scope": {},
    "controller":  showErrorsController({
         inject:["accountLookup"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, accountLookup) {


           MonsterCloud.get(apiPrefix+"/config/servers/roles")
                .then(function(data){
                   $scope.availableServerRoles = data;
                })

           $scope.deleteUser = function(user) {
              var i = $scope.x.s_users_merged.indexOf(user);
              if(i > -1)
                 $scope.x.s_users_merged.splice(i, 1);
           }

           $scope.addUser = function(user) {
              $scope.x.s_users_merged.push({u_id: user.u_id, u_name: user.u_name});
           }

           $scope.refetch = function(){
              $scope.x = {s_users_merged: []};
              return MonsterCloud.get(apiPrefix+"/servers")
                .then(function(data){
                   $scope.servers = data;
                   accountLookup(data, "s_users");
                })

           }

           $scope.select = function(server) {
              $scope.selected = server.s_name
              $scope.x = angular.copy(server);
              $scope.x.s_users_merged = [];
              if(!$scope.x.s_users) $scope.x.s_users = [];
              for(var i = 0; i < $scope.x.s_users.length; i++) {
                $scope.x.s_users_merged.push({u_id: $scope.x.s_users[i], u_name: $scope.x.u_name[i]});
              }
           }

           $scope.saveRoles = function(server) {
              if(!$scope.selected) return
              return MonsterCloud.post(apiPrefix+"/server/"+$scope.selected, $scope.getPayload())
                .then(function(){
                   $scope.selected = null
                   return $scope.refetch()
                })
           }

           $scope.delete = function(server){
              return MonsterCloud.delete(apiPrefix+"/server/"+server.s_name)
                .then(function(data){
                   return $scope.refetch()
                })
           }

           $scope.refetch()

           $scope.getPayload = function(){
              var d = angular.copy($scope.x);
              d.s_users = d.s_users_merged.map(function(x){return x.u_id;});
              delete d.s_users_merged;
              delete d.u_name;
              return d;
           }

         },
         fireCallback: function($scope, MonsterCloud){
           
           return MonsterCloud.put(apiPrefix+"/servers", $scope.getPayload()).then($scope.refetch)
         }

      })

  }
}],{
    "title": "Manage servers",
    "menuKey": templateDirectory,
    "permissions": permissions,    
})


app.service("mcSuperuserWebhostingTemplatesFactory", ["MonsterCloud", function(MonsterCloud) {
  return function() {
     // note: we do not send this via the /su route as ones with SERVER_OWNER role will access it as well
     return MonsterCloud.get("/accountapi/webhostingtemplates")
  }
}])

app.directive("mcSuperuserWebhostingTemplateOptions", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"webhosting-template-options"),
    "scope": { "form": "=", "template":"=", "optional": "=", "server": "@" },
    "controller": ["$scope","MonsterCloud", "mcSuperuserCluebringerPolicies", function($scope, MonsterCloud, mcSuperuserCluebringerPolicies){

        $scope.$on("reset-template-params", function(){
          resetTemplateParams()
        })

        $scope.$on("select-template-params", function(){
          $scope.tmp.t_bandwidth_limit = JSON.stringify($scope.template.t_bandwidth_limit);
        })

        $scope.tmp = {};

        $scope.setBandwidthRules=function (){
          try{
             $scope.template.t_bandwidth_limit = JSON.parse($scope.tmp.t_bandwidth_limit);
          }catch(ex){
             $scope.template.t_bandwidth_limit = {};
          }

        }

        $scope.addBandwidthRule = function(){
          var dh = parseInt($scope.tmp.t_bandwidth_limit_hours, 10);
          if(isNaN(dh)) return;
          var db = parseInt($scope.tmp.t_bandwidth_limit_bytes, 10);
          if(isNaN(db)) return;

          $scope.template.t_bandwidth_limit[dh] = db;
          $scope.tmp.t_bandwidth_limit_hours = "";
          $scope.tmp.t_bandwidth_limit_bytes = "";
          $scope.tmp.t_bandwidth_limit = JSON.stringify($scope.template.t_bandwidth_limit);
        }

        resetTemplateParams()

            // note: we dont send this via the su route, as ones with SERVER_OWNER role will access it as well
            MonsterCloud.get("/accountapi/config/webhostingtemplates/dynamiclanguages")
              .then(function(data){
                 $scope.dynamiclanguages = data
              })

            if($scope.server) {
               mcSuperuserCluebringerPolicies($scope.server)
                .then(function(d){
                    $scope.emailPolicies = d;
                })              
            }

            function resetTemplateParams(){
                if(!$scope.optional)
                {
                   if(!$scope.template) $scope.template = {}
                   $scope.template.t_bandwidth_limit = {}
                   Array("t_awstats_allowed","t_x509_certificate_external_allowed","t_x509_certificate_letsencrypt_allowed","t_public").forEach(function(q){
                      if(!$scope.template[q]) $scope.template[q] = 0
                   })
                   if(!$scope.template.t_allowed_dynamic_languages) $scope.template.t_allowed_dynamic_languages = []
                }

            }
    }]
  }
}])

app.directive("mcSuperuserWebhostingTemplates", ["templateDir",function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"superuser-webhosting-templates"),
    "scope": {},
    "controller":  showErrorsController({
         inject: ["mcSuperuserWebhostingTemplatesFactory"],
         additionalInitializationCallback: function($scope, MonsterCloud, $q, mcSuperuserWebhostingTemplatesFactory){
           $scope.webhostingtemplates = []



           $scope.del=function(template){
               return MonsterCloud.delete(apiPrefix+"/webhostingtemplate/"+template.t_name, {})
                  .then(function(){
                      return $scope.refilter()
                  })

           }
           $scope.update = function(){
               $scope.op = "update"
               $scope.fire()
           }
           $scope.saveAsNew = function(){
               $scope.op = "save_as_new"
               $scope.fire()
           }
           $scope.select=function(template){
               $scope.template = angular.copy(template)
               $scope.template.t_original_name = $scope.template.t_name
               $scope.$broadcast("select-template-params")
           }
           $scope.refilter = function(){
              return mcSuperuserWebhostingTemplatesFactory()
                .then(function(data){
                   $scope.webhostingtemplates = data
                })

           }

           $scope.refilter()

           $scope.afterSubmit = function(prom) {
              return prom.then(function(){
                Object.keys($scope.template).forEach(function(q){delete $scope.template[q]});
                console.log("broadcasting reset template params")
                $scope.$broadcast("reset-template-params")
                return $scope.refilter()
              })

           }


         },
         fireCallback: function($scope, MonsterCloud) {

           var p
           if($scope.op == "save_as_new")
             p = MonsterCloud.put(apiPrefix+"/webhostingtemplates", $scope.template)
           if($scope.op == "update")
             p = MonsterCloud.post(apiPrefix+"/webhostingtemplate/"+$scope.template.t_original_name, $scope.template)

           return p
              .then(function(){
                $scope.template = {}
                // Object.keys($scope.template).forEach(function(q){delete $scope.template[q]});
                console.log("broadcasting reset template params")
                $scope.$broadcast("reset-template-params")
                return $scope.refilter()
              })

         }

    })
  }
}], {
    "title": "Webhosting package configurations",
    "menuKey": templateDirectory,
    "permissions": "SUPERUSER SERVER_OWNER",
})


registerSwitcher("mcSwitchUserSuperuser", apiPrefix+"/authentication/switch_user")


})(app)
