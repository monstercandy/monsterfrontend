(function(app){

  var templateDirectory = "test-superuser"

  app.directive("mcSuperuserFilemanInProgress", ["$compile",function($compile){
      return {
        restrict: "E",
        scope: { "server":"@" },
        link: function(scope, elm, attrs, ctrl) {
              var tasksGetUrl = "/s/"+scope.server+"/su/fileman/tests/tasks"
              var taskUrl = "/s/"+scope.server+"/fileman/tasks"
              var t  = "<mc-tasks-in-progress title='FILEMAN_TASK_TITLE_IN_PROGRESS' task-url='"+taskUrl+"' tasks-get-url='"+tasksGetUrl+"' ></mc-tasks-in-progress>"
              var el = $compile( t )( scope );
              elm.html( el );
        }

     }

  }])

  app.directive("mcSuperuserTestCommander", ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'commander'),
        controller: ["$scope","MonsterCloud","mcFilemanUpload","mcFilemanDownload","mcFilemanTask",
          function($scope,MonsterCloud,mcFilemanUpload,mcFilemanDownload,mcFilemanTask) {
             var baseUrl = "/s/"+$scope.server+"/su/fileman/tests/"
             $scope.doUpload = function(files){
                return mcFilemanUpload($scope.server, null, "foobar/", files, baseUrl+"upload")
             }
             $scope.doLongReadSimple = function(){
                var prom = MonsterCloud.post(baseUrl+"longread-simple", {})
                return mcFilemanTask(
                   "COMMANDER_LONG_READ_TEST",
                   $scope.server,
                   prom
                )

             }
             $scope.doLongReadDouble = function(){
                var prom = MonsterCloud.post(baseUrl+"longread-upload", {})
                return mcFilemanTask(
                   "COMMANDER_LONG_READ_TEST",
                   $scope.server,
                   prom,
                   {doubleRequestMode: true, stringInput: $scope.content}
                )
             }
             $scope.doDownload = function(){
               var prom = MonsterCloud.post(baseUrl+"download", {})
               return mcFilemanDownload($scope.server, prom)
             }

          }]
     }

  }], {
     title: "Commander manual tests",
     menuKey: templateDirectory,
     paramsRequired: {"server": "@"}
  })

})(app)

