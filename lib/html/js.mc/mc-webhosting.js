(function(app){

  var templateDirectory = "webhosting"
  var apiPrefix = "/webhosting/"

  app.service("mcWebhostingTask", ["$q","$task",function($q, $task){
    return function(server, title, prom) {
        return prom.then(function(r){
            if(!r.id) return $q.when(r)

            var url = "/s/"+server+"/webhosting/tasks/"+r.id
            return $task({title:title, "url":url})

        })
    }
  }])

  app.service("mcWebhostingSzarWebAdmin", ["MonsterCloud", "$window", function(MonsterCloud,$window){
    return function(server, whId, cat) {
       var url = "/s/"+server+"/webhosting/"+whId+"/szar/jump/"+(cat||"web")
       return MonsterCloud.post(url, {})
         .then(function(u){
            $window.open(u.url)
         })
    }
  }])


app.directive("mcAttachDomain", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory,"attach-domain"),
    controller: [
       "$scope", "MonsterCloud", "$q", "mcWebhostingFactory","mcAccountDomains",
       "mcAccountWebhostingDomains", "mcConfig", "mcDnsZone", "mcRoutesService", 
       "$alertSuccess", "$alertError","mcDocrootApiUrl","mcEmailStatusService",
       "$timeout",
     function($scope, MonsterCloud, $q, mcWebhostingFactory, mcAccountDomains, 
              mcAccountWebhostingDomains, mcConfig, mcDnsZone, mcRoutesService, 
              $alertSuccess, $alertError, mcDocrootApiUrl, mcEmailStatusService,
              $timeout){

          $scope.initDocroot = true;
          $scope.initMailSubsystem = true;

          var f;

          function getServerFullName(server){
             return server+"."+mcConfig.config.service_primary_domain;
          }

          $scope.fetchWebhostingInfo = function(){
             f = mcWebhostingFactory($scope.server, $scope.whId);            
             return $scope.refetch();
          }

          $scope.refetch= function(){
            return f.Lookup()
              .then(function(result){
                $scope.wh = result;
              })            
          }
          
          $scope.normalizeDomain = function(host){
              host = host.toLowerCase();

              var r = new RegExp("(?:www.)+(.+)");
              var m = r.exec(host);
              if((m)&&(m[1]))
                return m[1];

              return host;
          }

          $scope.whSelected = function(){
             console.log("whSelected!", $scope.whInc)
             $scope.resetStuff = true;
             $scope.whId = $scope.whInc.webhosting_id;
             $scope.server = $scope.whInc.server;

             $timeout(function(){
                   $scope.resetStuff = false;
             }, 100);

             return $scope.fetchWebhostingInfo();

          }

          function showError(){
              return $alertError({
                 timeout: 60000, 
                 message: "An unexpected error encountered while working on the DNS zones. Proceeding with attaching the domain name, but you'll want to review the DNS settings manually."
              })            
          }

          if((!$scope.server)||(!$scope.whId)) {
            $scope.predefinedServerWhId = false;
            $scope.whInc = {};
            /*
            mcAccountWebhostings.then(function(wds){
                // [{w_server_name: "s1", w_webhosting_id: 12345}],
                $scope.
            })
            */
          }
          else {
            $scope.predefinedServerWhId = true;
            $scope.fetchWebhostingInfo();
          }

          $scope.learnRealPrefix = function(fullHostname, bareDomain) {
              // www.foo.hu 10
              // foo.hu 6
              // expected: www 3 

              // console.log("XXXXXXXXX", fullHostname, "Y", bareDomain)
              return fullHostname.substr(0, fullHostname.length - bareDomain.length - 1);
          }

          $scope.resetZone = function(domain, server){
             var re = true;
             return mcDnsZone.Reset(domain, getServerFullName(server), true)
               .catch(function(ex){
                  re = false;
                  return showError();
               })
               .then(function(){
                  return re;
               })
          }

          $scope.onDomainAdded = function(hostname, skipRedirect){
             mcAccountDomains.Reset();
             mcAccountWebhostingDomains.Reset();
             f.Reset();

             var docrootPath = "/"+hostname+"/pages/";

             return $q.when()
               .then(retrier(function(){
                  if(!$scope.initDocroot) return;

                  var payload = {
                       dst_dir_path: docrootPath
                  };
                  var uri = "/s/"+$scope.server+"/fileman/"+$scope.whId+"/";

                  if(mcConfig.config.putWebhostingDomainFilemanTemplate) {
                    payload.template = mcConfig.config.putWebhostingDomainFilemanTemplate;
                    return MonsterCloud.post(uri+"template", payload);

                  }else{
                    return MonsterCloud.post(uri+"mkdir", payload);
                  }

               }))
               .then(retrier(function(){
                  if(!$scope.initDocroot) return;

                  var url = mcDocrootApiUrl($scope.server, false)+"/docroots/"+$scope.whId+"/"+hostname+"/entries/";

                  return addStuff(hostname)
                    .then(function(){
                       return addStuff("www."+hostname);
                    })

                  function addStuff(ahostname){
                    var payload = {
                       host: ahostname,
                       docroot: docrootPath
                    };

                    return MonsterCloud.put(url, payload);
                  }
               }))
               .then(retrier(function(){
                  if(!$scope.initMailSubsystem) return;

                  var d = {
                     server: $scope.server,
                     whId: $scope.whId,
                     domain: hostname
                  }
                  return mcEmailStatusService(d).then(function(){
                    return d.toOn();
                  });
                
               }))
               .then(retrier(function(){
                   if(!mcConfig.config.putWebhostingDomainExternalCallback) return;
                   var req = {
                      method: "POST",
                      url: mcConfig.config.putWebhostingDomainExternalCallback, 
                      data: {hostname: hostname, server: $scope.server, whId: $scope.whId},
                      noUriTransform: true, 
                      rawResult: true,
                   };
                   return MonsterCloud.doRequest(req);
               }))
               .then(function(){
                   if(!skipRedirect) {
                     return $alertSuccess().then($scope.jumpBack);
                   }
                   else
                     return $scope.refetch();              
               })


              function retrier(callback, count){
                  if(!count) count = 3;

                  MonsterCloud.dontCatchExceptions = true;

                  var re = callback()
                  if(!re) return;

                  return re.catch(function(ex){
                    console.log("exception?!!", ex)
                    var msg = "-";
                    if((ex.data)&&(ex.data.result)&&(ex.data.result.error)&&(ex.data.result.error.message)) {
                      msg = ex.data.result.error.message;
                    }
                    if((count > 1)&&(msg == "PERMISSION_DENIED")) {
                       return  $timeout(function(){
                          return retrier(callback, count -1)
                       })
                    }

                    MonsterCloud.dontCatchExceptions = false;

                    throw ex;
                  })
                  .then(function(){
                    MonsterCloud.dontCatchExceptions = false;
                  })
              }

          }

          $scope.addSubDomainCnames = function(host, domain, server) {
              var re = true;
              var cname = getServerFullName(server);
              return addRecord(host).then(function(){
                return addRecord("www."+host);
              })
              .catch(function(ex){
                  re = false;
                  return showError();
              })
              .then(function(){
                  return re;
              })

              function addRecord(ahostname){
                   return MonsterCloud.doRequest({
                    method:"PUT", 
                    relayException: true,
                    url:"/tapi/domain/"+domain, 
                    data: {
                     type: "CNAME",
                     cname: cname,
                     host: ahostname
                   }})                
              }

          }

          $scope.jumpBack = function(){
              return mcRoutesService.jumpToState({
                 stateName:"mc-webhosting-info",
                 stateParams: {
                   server: $scope.server,
                   whId: $scope.whId,
                 }
              });
          }


 
    }]
  }
}], {
    title: "Attach domain to the webstore",
    paramsOptional: { server:"@", whId: "@", domain: "@"},
    menuKey: templateDirectory,
})

app.directive("mcAttachDomainAlreadyAuthorized", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory,"attach-domain-already-authorized"),
    scope: {server:"@", whId: "@", domain: "@"},
    controller:  showErrorsController({
          inject: ["mcAccountDomains","mcAccountWebhostingDomains"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcAccountDomains, mcAccountWebhostingDomains){
              $scope.ready = false;
              $scope.resetDns = true;
              var i = 0;

              mcAccountDomains().then(function(doms){
                 if($scope.domain) {
                    var found = [];
                    doms.some(function(d){
                      if((d.d_domain_name == $scope.domain)||(d.d_domain_canonical_name == $scope.domain)) {
                         found.push(d);
                         return true;
                      }
                    })
                    $scope.allDomains = found;
                 }
                 else
                    $scope.allDomains = doms;
                 i++;
                 readyLogic();
              })

              mcAccountWebhostingDomains().then(function(wds){
                 $scope.wds = wds;
                 i++;
                 readyLogic();
              })

              $scope.recalc = function(){
                readyLogic();
              }


              function readyLogic(){
                 if(i != 2) return;
                 $scope.ready = true;
                 $scope.domains = [];
                 var alreadyAttached = {};
                 // console.log("HERE", $scope.wds, $scope.allDomains);
                 $scope.wds.forEach(function(wd){
                    alreadyAttached[wd.wd_domain_name] = 1;
                 });
                 $scope.allDomains.forEach(function(d){
                    if(!$scope.showAllDomains) {
                       if(alreadyAttached[d.d_domain_name])
                         return;
                    }
                    $scope.domains.push(d.d_domain_name);
                 });
              }
          },
          fireCallback: function($scope, MonsterCloud, $q, mcAccountDomains, mcAccountWebhostingDomains){
              var payload = {
                  wd_server: $scope.server,
                  wd_webhosting: $scope.whId,
                  wd_domain_name: $scope.domain
              };

              return MonsterCloud.put("/account/webhostingdomains/", payload)
                .then(function(){
                    if(!$scope.resetDns) return;
                    return $scope.$parent.resetZone($scope.domain, $scope.server);
                })
                .then(function(){
                    return $scope.$parent.onDomainAdded($scope.domain)
                })
          }
    })
  }
}])

app.directive("mcAttachDomainSubdomain", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory,"attach-domain-subdomain"),
    scope: {server:"@", whId: "@",domain: "@"},
    controller:  showErrorsController({
          inject: ["mcAccountDomains"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcAccountDomains){

              mcAccountDomains().then(function(doms){
                 $scope.ready = true;
                 var domainNames = doms.map(function(x){return x.d_domain_name});
                 if($scope.domain) {
                    $scope.domains = domainNames.indexOf($scope.domain) > -1 ? [$scope.domain] : [];
                    if($scope.domains.length)
                       $scope.mainDomain = $scope.domain;
                 }
                 else
                    $scope.domains = domainNames;
              })

          },
          fireCallback: function($scope, MonsterCloud, $q){
              $scope.subDomain = $scope.$parent.normalizeDomain($scope.subDomain);

              var payload = {
                  wd_server: $scope.server,
                  wd_webhosting: $scope.whId,
                  wd_domain_name: $scope.subDomain + "." + $scope.mainDomain
              };

              return MonsterCloud.put("/account/webhostingdomains/", payload)
                .then(function(re){
                    var prefix = $scope.$parent.learnRealPrefix(payload.wd_domain_name, re.bareDomain);
                    if(prefix)
                       return $scope.$parent.addSubDomainCnames(prefix, re.bareDomain, $scope.server);
                })
                .then(function(){
                    return $scope.$parent.onDomainAdded(payload.wd_domain_name)
                })

          }
    })
  }
}])

app.directive("mcAttachDomainExternal", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory,"attach-domain-external"),
    scope: {server:"@", whId: "@"},
    controller:  showErrorsController({
          inject: ["mcConfig"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcConfig){
             $scope.dnsServers = [];
             if(mcConfig.config.nameservers) {
                var i = 0;
                mcConfig.config.nameservers.split(", ").forEach(function(n){
                   i++;
                   $scope.dnsServers.push({
                      i: i,
                      hostname: n
                   })
                })
             }
             $scope.config = mcConfig.config;
             MonsterCloud.post("/account/config/webhostingdomains/txtrecords/ex", {server: $scope.server})
               .then(function(r){
                  $scope.txtrecords = r;
               })
          },
          fireCallback: function($scope, MonsterCloud, $q, mcConfig){
              $scope.domain = $scope.domain.toLowerCase();
              var payload = {
                  wd_server: $scope.server,
                  wd_webhosting: $scope.whId,
                  wd_domain_name: $scope.domain
              };
              if(($scope.txtrecords.required)&&(!$scope.txtCode)) {
                  return MonsterCloud.post("/account/webhostingdomains/txt-record-code", payload)
                    .then(function(re){
                       $scope.txtCode = re; 
                    })
              }

              var putResponse;
              return MonsterCloud.put("/account/webhostingdomains/", payload)
                .then(function(re){
                    putResponse = re;
                    return $scope.$parent.resetZone($scope.txtCode.bareDomain, $scope.server);
                })
                .then(function(success){
                    $scope.dnsSuccess = success;

                    if(!success) return;

                    var prefix = $scope.$parent.learnRealPrefix($scope.domain, $scope.txtCode.bareDomain);
                    if(prefix)
                       return $scope.$parent.addSubDomainCnames(prefix, $scope.txtCode.bareDomain, $scope.server);

                })
                .then(function(){
                    return $scope.$parent.onDomainAdded($scope.domain, true)
                })
                .then(function(){
                    $scope.showSuccess = true;
                })
          }
    })
  }
}])

app.directive("mcWebhostingSlowlogList", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"slowlog"),
    // shared scope!
    "controller": ["$scope", "MonsterCloud", "mcConfig", "mcFilemanLister", function($scope, MonsterCloud, mcConfig, mcFilemanLister){

        if((!$scope.superuser)&&(!$scope.whId)) throw new Error("Either of these must be provided");

        $scope.config = mcConfig.config;
        if(mcConfig.config.legacyPhpSlowlog) {
            var uriPrefix = "/s/"+$scope.server+"/" + ($scope.superuser ? "su/" : "") + "webhosting/slowlog/" + ($scope.superuser ? "all" : "by-webhosting/"+$scope.whId)+"/";

            $scope.refetch = function(){
                   var d = queryDetails();

                   return MonsterCloud.post( uriPrefix + "search", d)
                     .then(function(data){
                        if($scope.postProcess)
                            $scope.postProcess(data);

                        $scope.slowlogs = data.events;
                     })
            }

            $scope.refetch();

            function queryDetails() {
              var d = {}
              Array("notbefore","notafter","sl_dump", "sl_path").forEach(function(v){
                if(($scope[v] !== "")&&($scope[v] !== null))
                   d[v] = $scope[v]
              })
              return d
            }
        } else {
           $scope.fetch = function(){
              var path = $scope.phpSlowlogFile;
              $scope.content = "Fetching "+path+"...";

              return MonsterCloud.post("/s/"+$scope.server+"/fileman/"+$scope.whId+"/fetch", {src_file: path})
                .then(function(re){
                    $scope.content = re.data;
                })
           }

           return mcFilemanLister({server: $scope.server, whId: $scope.whId, path: "/php-fpm-logs", filePropertiesAsWell: true})
             .then(function(files){
                $scope.phpSlowlogFiles = files.filter(function(x){ return x.name.match(/\.slow\./); });
             })
        }

    }]
  }
}])

app.directive("mcWebhostingSlowlogListUser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-webhosting-slowlog-list></mc-webhosting-slowlog-list>",
  }
}], {
    title: "List of slow web application requests",
    paramsRequired: { server:"@", whId: "@"},
    menuKey: templateDirectory,
})





app.directive("mcWebhostingAccesslogList", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"accesslog"),
    // shared scope!
    "controller": ["$scope", "MonsterCloud", function($scope, MonsterCloud){

        if((!$scope.superuser)&&(!$scope.whId)) throw new Error("Either of these must be provided");

        $scope.uriPrefix = "/s/"+$scope.server+"/" + ($scope.superuser ? "su/" : "") + "webhosting/accesslog/" + ($scope.superuser ? "all" : "by-webhosting/"+$scope.whId)+"/";

        $scope.refetch = function(){
               var d = queryDetails();

               return MonsterCloud.post( $scope.uriPrefix + "search", d)
                 .then(function(data){
                    if($scope.postProcess)
                        $scope.postProcess(data);

                    $scope.accesslogs = data.events;
                 })

        }

        $scope.refetch();

        function queryDetails() {
          var d = {}
          Array("notbefore","notafter","al_vhost").forEach(function(v){
            if(($scope[v] !== "")&&($scope[v] !== null))
               d[v] = $scope[v]
          })
          return d
        }

    }]
  }
}])

app.directive("mcWebhostingAccesslogListUser", ["templateDir", function(templateDir){
  return {
    "template": "<mc-webhosting-accesslog-list></mc-webhosting-accesslog-list>",
  }
}], {
    title: "Web application request statistics",
    paramsRequired: { server:"@", whId: "@"},
    menuKey: templateDirectory,
})

app.directive("mcSb2WebhostingMenu", ["templateDir", function(templateDir){
  return {
    templateUrl: templateDir(templateDirectory, "mc-sb2-webhosting-menu"),
    scope: {},
    controller: ["$scope", "mcRoutesService", function($scope, mcRoutesService){
        var p = mcRoutesService.getCurrentState();
        $scope.params = {server: p.stateParams.server, whId: p.stateParams.whId};
        $scope.protectedDirParams = angular.extend({}, $scope.params, {showWarningAboutDomains: true});
    }]
  }
}])

app.directive("mcStateWebhostingName", [function(){
      return {
        restrict: "E",
        scope: {},
        template: "<span ng-if='whName'>: {{whName}}</span>",
        controller: ["$scope", "mcRoutesService", "mcWebhostingFactory", function($scope, mcRoutesService, mcWebhostingFactory){
            mcRoutesService.registerStateChangeCallback(rebuild);
            rebuild(mcRoutesService.getCurrentState());

            function rebuild(p){
                $scope.whName = "";
                var params = (p.stateParams || {});
                if((params.server)&&(params.whId)) {
                  var f = mcWebhostingFactory(params.server, params.whId)
                  return f.Lookup()
                    .then(function(result){
                      $scope.whName = result.wh_name;
                    })

                }
                
            }
            
        }]
     }
  }])


/**
 This one returns all webhosting services belonging to the current account including all its parameters
*/
  app.service("mcWebhostingServices", ["MonsterCloud", "mcCachedLookupProvider",
    function(MonsterCloud, mcCachedLookupProvider
    ){
       return mcCachedLookupProvider.Factory(function(){
           return MonsterCloud.get("/my/services/webhostings/details");

/*
// should return something like this:
                  [
                    {w_server_name: "s1", w_webhosting_id: 12345, wh: {wh_name: "foobar"}},
                    ...
                  ]
*/

       })
  }]);

  app.factory("mcAllWebstoresPerServer", ["MonsterCloud", "mcCachedLookupProvider",
    function(MonsterCloud, mcCachedLookupProvider
    ){
       return function(server){
          return mcCachedLookupProvider.Factory(function(){
              return MonsterCloud.get("/s/"+server+"/su/webhosting/")
                .then(function(whs){
                    var re = [];

                    whs.forEach(function(wh){
                        re.push({
                           w_server_name: server,
                           w_webhosting_id: wh.wh_id,
                           wh: wh
                        })
                    })
                    return re;
                })

              })()
       }

/*
// should return something like this:
                  [
                    {w_server_name: "s1", w_webhosting_id: 12345, wh: {wh_name: "foobar"}},
                    ...
                  ]
*/

  }])

    app.directive("mcGlobalWebhostingSelector", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      scope: {model:"=", "server": "@", change: "&", excludeWhId:"@", excludeServer: "@", completeModel: "@", perServer: "@", notRequired: "@"},
      templateUrl: templateDir(templateDirectory,'global-webhosting-selector'),
      controller: ["$scope", "mcWebhostingServices", "mcAllWebstoresPerServer", function($scope, mcWebhostingServices, mcAllWebstoresPerServer){

        $scope.required = $scope.notRequired ? false : true;

        $scope.onChange = function(){
           $scope.model.webhosting_id = $scope.local.w_webhosting_id
           $scope.model.server = $scope.local.w_server_name
           if($scope.completeModel)
              $scope.model.webhosting = $scope.local.wh;

           console.log("model set to", $scope.model)
           if($scope.change) {
              console.log("calling onchange")
              $scope.change()
           }
        }

        if(($scope.perServer)&&(!$scope.server))
          throw new Error("In per-server mode you should specify both global and server attributes");

        var p = $scope.perServer ? mcAllWebstoresPerServer($scope.server) : mcWebhostingServices();

        return p.then(function(data){
          console.log("it finished.", data, $scope.server)
            $scope.items = []
            data.forEach(function(item){
                if(($scope.server)&&(item.w_server_name != $scope.server)) return;
                if(($scope.excludeServer)&&($scope.excludeWhId)&&(item.w_server_name == $scope.excludeServer)&&(item.w_webhosting_id==$scope.excludeWhId)) return;

                item.label = item.w_server_name + " / " + item.w_webhosting_id +": "+ ((item.wh||{}).wh_name);

                $scope.items.push(item)
            })

        })



      }]
   }
}])


    app.directive("mcWebhostingSecurity", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-security'),
      controller: ["$scope", "mcDocrootWebhostingDomains", function($scope, mcDocrootWebhostingDomains){
          return mcDocrootWebhostingDomains($scope.server, $scope.whId)
            .then(function(domains){
                $scope.webhostingDomains = domains;
            })
      }]
   }
}], {
  title:"Webhosting security settings",
  menuKey: templateDirectory,
  paramsRequired: {server:"@","whId":"@"},
})

    app.directive("mcWebhostingScriptWatcherRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'script-watcher-row'),
      scope: { "server":"@","whId":"@"},
      controller: ["$scope", "mcWebhostingFactory", function($scope, mcWebhostingFactory){
          var f = mcWebhostingFactory($scope.server, $scope.whId)
          f.Lookup()
            .then(function(result){
              $scope.lookupFinished = true
              $scope.webhosting = result
            })

          $scope.turn = function(newState){
             f.Change({wh_notify_script_differences:newState})
          }
      }]
   }
}])


app.service("mcWebhostingDomainsService", ["mcAccountWebhostingDomains", function(mcAccountWebhostingDomains){
  var re = function(server, whId, returnObject) {
     return mcAccountWebhostingDomains().then(function(rows){
        var re = [];
        rows.forEach(function(row){
           if((row.wd_webhosting == whId)&&(row.wd_server == server)) 
           {
              if(returnObject)
                  re.push(row);
              else
                  re.push(row.wd_domain_canonical_name);
           }
        });

        return re;
     })

  }

  re.Reset = function(){
    mcAccountWebhostingDomains.Reset();
  }

  return re;
}])


app.directive("mcWebhostingDomainRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'webhosting-domain-row'),
      scope: { "server":"@","whId":"@", "twoColumns": "@"},
      controller: ["$scope", "mcWebhostingFactory", "MonsterCloud", "mcWebhostingDomainsService",
       function($scope, mcWebhostingFactory,MonsterCloud, mcWebhostingDomainsService){
          var f = mcWebhostingFactory($scope.server, $scope.whId)
          f.Lookup()
            .then(function(result){
              $scope.webhosting = result;
              return mcWebhostingDomainsService($scope.server, $scope.whId)
            })
            .then(function(domains){
              $scope.domains_count = domains.length;
              if($scope.$parent) {
                 $scope.$parent.domainsExceeded = $scope.domains_count >= $scope.webhosting.template.t_domains_max_number_attachable; 
              }
              $scope.lookupFinished = true;
            })

      }]
   }
}])

    app.directive("mcWebhostingRenamerRow", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'webhosting-renamer-row'),
      scope: { "server":"@","whId":"@", "extraCall": "@"},
      controller: ["$scope", "mcWebhostingFactory", "$q", "MonsterCloud", "showException", "$prompt", function($scope, mcWebhostingFactory,  $q, MonsterCloud, showException, $prompt){
          var f = mcWebhostingFactory($scope.server, $scope.whId)
          f.Lookup()
            .then(function(result){
              $scope.lookupFinished = true;
              $scope.webhosting = result;
              $scope.wh_name = $scope.webhosting.wh_name;
            })

          $scope.rename = function(){
             var newName;
             return $prompt({text: $scope.wh_name, title: "RENAME_WEBSTORE_TITLE", message: "RENAME_WEBSTORE_MESSAGE", placeholder: "RENAME_WEBSTORE_PLACEHOLDER"})
               .then(function(data){
                   newName = data;
                   // console.log("doing shit", $scope.wh_name);
                   var p = $q.when("ok");

                   if($scope.extraCall) {
                      p = MonsterCloud.doRequest({"method":"POST", url: $scope.extraCall, data: {wh_id: $scope.whId, wh_name: newName}, noUriTransform: true, rawResult: true})
                   }

                   return p.then(function(d)
                     {
                        console.log("returned", d)
                        if(d != "ok")
                           throw new Error("Error while renaming the storage in the legacy DB");
                        return MonsterCloud.post("/s/"+$scope.server+"/webhosting/"+$scope.whId, {wh_name: newName})
                     })
                     .then(function(){
                        $scope.webhosting.wh_name = $scope.wh_name = newName;
                     })
                     .catch(function(ex){
                        return showException(ex);
                     })

               })
          }
      }]
   }
}])


  app.directive("mcWebhostingDomains", ["templateDir", function(templateDir){
  return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-domains'),
      scope: { "server":"@","whId":"@", "panel": "@"},
      controller: ["$scope", "MonsterCloud", "$q", "$task", "mcWebhostingDomainsService", 
        function($scope, MonsterCloud, $q, $task, mcWebhostingDomainsService){
           $scope.detach = function(domain){

                var state = "docroot";
                var title = "TASK_DETACHING_DOMAIN";
                var pureAdata = {
                         title: title
                      };

                var d = {
                   title: title,
                   abortOnly: true,
                   undeterministic: true,
                   urlFactory: function(index, appendText, lastPromiseResult) {
                       var aData = {
                         title: title,
                         mcDontDisplay: true,
                         mcTaskStatusNotNeeded: true,
                       }

                       if(state == "docroot") {
                          appendText("Removing docroot entries");
                          aData.prom = MonsterCloud.delete("/s/"+$scope.server+"/docrootapi/protected-dirs/"+$scope.whId+"/"+domain)
                             .then(function(webstores){
                                return MonsterCloud.delete("/s/"+$scope.server+"/docrootapi/docroots/"+$scope.whId+"/"+domain)
                             });

                          state = "email";
                       }
                       else if(state == "email") {
                          appendText("Removing email boxes");

                          state = "wd";

                          return MonsterCloud.doRequest({
                              method: "DELETE", 
                              url: "/s/"+$scope.server+"/email/webhostings/"+$scope.whId+"/domains/domain/"+domain, 
                              relayException: true,
                              data: {}
                           })
                           .then(function(h){
                                pureAdata.url = "/s/"+$scope.server+"/email/tasks/"+h.id;
                                return pureAdata;
                           })
                           .catch(function(ex){
                              if(
                                (ex)&&(ex.data)&&(ex.data.result)&&
                                (ex.data.result.error)&&
                                (ex.data.result.error.message == "DOMAIN_NOT_FOUND")
                                ) {
                                appendText("No mailboxes to remove");
                                aData.prom = $q.when();
                                return aData;                                
                              }

                              // unexpected error:
                              throw ex;
                           })

                       }
                       else if(state == "wd"){
                          appendText("Removing the webstore-domain binding");

                          state = "finish";

                          aData.prom = MonsterCloud.delete("/wizard/webhostingdomains/", {wd_server: $scope.server, wd_webhosting: $scope.whId, wd_domain_name: domain});

                       }
                       else if(state == "finish"){
                          appendText("The opererations have finished successfully.");
                          return $q.when()
                       }

                       return $q.when(aData);
                   }

                }

              console.log("detaching", domain);

                return $task(d)
                  .then(function(){
                      mcWebhostingDomainsService.Reset();
                      return $scope.refetch();
                  })

           }

           $scope.refetch = function(){
             return mcWebhostingDomainsService($scope.server, $scope.whId)
               .then(function(dom){
                  $scope.domains = dom;
               })

           }

           $scope.refetch();
      }]
   }
}])

  app.directive("mcSzarLinkWeb", ["templateDir", function(templateDir){
  return {
      restrict: "A",
      templateUrl: templateDir(templateDirectory,'szar-link-web'),
      scope: { "server":"@","whId":"@"},
      controller: ["$scope", "mcWebhostingSzarWebAdmin", function($scope, mcWebhostingSzarWebAdmin){
          $scope.szarWebAdmin = function(){
             mcWebhostingSzarWebAdmin($scope.server, $scope.whId)
          }
      }]
   }
}])

      app.directive("mcWebhostingStatChart", ["templateDir", function(templateDir){

        return {
            restrict: "E",
            templateUrl: templateDir(templateDirectory,"stat-chart"),
            scope: { "server":"@","whId":"@", "panel": "@"},
            controller: ["mcLoadScript","$scope","mcWebhostingFactory", "$element", "$window", "$timeout",
             function(mcLoadScript, $scope, mcWebhostingFactory, $element, $window, $timeout) {

               var webhostingData
               var maxMb
               var freeSpaceMb
               var chartArea
               var ctx
               var container

               var lastChart

               $scope.checked = {"mail":true,"web":true,"db":true,"free":false}

               $scope.toggle = function(cat){
                  $scope.checked[cat] = !$scope.checked[cat]
                  rebuildPie()
               }

               var wh = mcWebhostingFactory($scope.server, $scope.whId)
               mcLoadScript("Chart", "/js/Chart.min.js")
                  .then(function(){
                     return wh.Lookup()
                  })
                  .then(function(result){

                     webhostingData = result

                     maxMb = result.template.t_storage_max_quota_soft_mb
                     if(!maxMb) return

                     freeSpaceMb = maxMb - webhostingData.wh_tally_sum_storage_mb
                     if(freeSpaceMb < 0) freeSpaceMb = 0
                     webhostingData.wh_tally_free_storage = {"free space": freeSpaceMb}

                     var web_sum = 0
                     Object.keys(webhostingData.wh_tally_web_storage).forEach(function(d){
                        web_sum += webhostingData.wh_tally_web_storage[d]
                     })
                     var diff = webhostingData.wh_tally_web_storage_mb - web_sum
                     if(diff > 0)
                       webhostingData.wh_tally_web_storage.other_files = diff

                     if(!window.Chart) return

                     $scope.canShow = true
                     Chart.defaults.global.responsive = true;
                     Chart.defaults.global.maintainAspectRatio = true

                     chartArea = $element.find("#chart-area")[0]
                     ctx = chartArea.getContext("2d");
                     container = $element.find("#chart-area-container")[0]

                     $timeout(rebuildPie, 1000)


                      /*
                     angular.element($window).bind("resize", windowResize)
                     windowResize()

                     function windowResize(){
                        ctx.canvas.width = 300;
                        ctx.canvas.height = 300;
                     }
                     */

                 })

               function rebuildPie() {

                   $scope.pieData  = []
                   Object.keys($scope.checked).forEach(function(cat){
                      var wantDetails = $scope.checked[cat]
                      if(wantDetails) {

                        var o = webhostingData["wh_tally_"+cat+"_storage"]
                        Object.keys(o).forEach(function(k){
                           var v = parseFloat(o[k]).toFixed(2);
                           $scope.pieData.push({label:k, value: v})
                        })

                      } else {
                        var v = webhostingData["wh_tally_"+cat+"_storage_mb"]
                        if(typeof v != "undefined")
                           $scope.pieData.push({label:cat, value: v})
                      }

                   })

                   if(lastChart)
                      lastChart.destroy()

                   lastChart = new Chart(ctx).Pie($scope.pieData);
               }

            }]


         }

      }])

  Array(
     {n:"mcWebhostingMailpolicyRow",t:'mailpolicy-row'},
     {n:"mcWebhostingQuotaRow",t:'quota-row'},
     {n:"mcWebhostingPhpRow",t:'php-row'},
     {n:"mcWebhostingDbmsDatabasesRow",t:'dbms-databases-row'},
     {n:"mcWebhostingDbmsSizesRow",t:'dbms-size-row'},
     {n:"mcWebhostingFtpAccountsRow",t:'ftp-accounts-row'},
     {n:"mcWebhostingEmailAccountsRow",t:'email-accounts-row'},
     {n:"mcWebhostingEmailAliasesRow",t:'email-aliases-row'}
  ).forEach(function(r){

      app.directive(r.n, ["templateDir", function(templateDir){
        return {
            restrict: "A",
            templateUrl: templateDir(templateDirectory,r.t),
            scope: { "server":"@","whId":"@" },
            controller: ["$scope", "mcFtpSiteService", "mcWebhostingFactory", "mcDbmsDatabasesService", "mcRoutesService", "mcEmailAccountService", "mcEmailAliasService", "EMAIL_SERVICE_ACCOUNT_TLD",
              function($scope, mcFtpSiteService, mcWebhostingFactory, mcDbmsDatabasesService, mcRoutesService, mcEmailAccountService, mcEmailAliasService, EMAIL_SERVICE_ACCOUNT_TLD){
               var wh = mcWebhostingFactory($scope.server, $scope.whId)
               wh.Lookup()
                  .then(function(result){
                    $scope.lookupFinished = true
                    $scope.webhosting = result
                    if(r.n == "mcWebhostingFtpAccountsRow") {
                       var ftp = mcFtpSiteService($scope.server, $scope.whId)
                       return ftp.GetUsers()
                         .then(function(users){
                            $scope.ftp_accounts_count = users.length

                         })
                    }

                    if(r.n == "mcWebhostingEmailAccountsRow") {
                       return mcEmailAccountService($scope.server, $scope.whId).Lookup()
                         .then(function(are){
                            $scope.email_accounts_count = are.result.accounts.length
                         })
                    }

                    if(r.n == "mcWebhostingEmailAliasesRow") {
                       return mcEmailAliasService($scope.server, $scope.whId).Lookup()
                         .then(function(are){
                            $scope.email_aliases_count = are.result.aliases.length
                         })
                    }

                    if(r.n == "mcWebhostingDbmsDatabasesRow") {
                       return mcDbmsDatabasesService($scope.server, $scope.whId).then(function(dbs){
                           $scope.dbms_length = dbs.length
                       })
                    }

                    if(r.n == "mcWebhostingMailpolicyRow") {
                       $scope.mailList = function(){
                           var domain = $scope.whId+'.'+$scope.server+EMAIL_SERVICE_ACCOUNT_TLD;
                           var mailer = "mailer@"+domain;
                           return mcRoutesService.jumpToState({
                             stateName:"mc-email-account-cluebringer",
                             stateParams: {
                               server: $scope.server,
                               whId: $scope.whId,
                               eaEmail: mailer,
                               domain: domain
                             }
                           })
                       }
                    }

                  })

            }]
         }

      }])

  })



var directoryPickerInstances = 0

app.directive('mcDirectoryPicker', ["templateDir",function (templateDir) {
   return {
     restrict: "E",
     scope: {
      "title": "@",
      "default": "@",
      "placeholder": "@",
      "server": "@",
      "whId": "@",
      "form": "=",
      "model": "=",
      "modelName": "@",
     },
     templateUrl: templateDir(templateDirectory,"directory-picker"),
     controller: ["$scope", "mcFilemanLister", "$q", function($scope, mcFilemanLister, $q){

             // $scope.modelName = "directoryPicker"+(directoryPickerInstances++)

             if($scope.default) {
                   $scope.model = {}
                   $scope.model[$scope.modelName] = $scope.default
             }

             var cacheListerPath
             var cacheListerProm

             $scope.getLocation = function(val) {

                  val = transformLogic(val)

                  if(val == cacheListerPath)
                     return cacheListerProm

                  cacheListerPath = val
                  cacheListerProm = mcFilemanLister({server:$scope.server,whId:$scope.whId,path:val,fullPathOnly: true, relayException: true})
                    .catch(function(ex){
                       if(ex.message.indexOf("NOT_FOUND") > -1)
                          return $q.when([])
                       throw ex
                    })

                  return cacheListerProm
             }


             function transformLogic(val){
                val = val.trim()
                if(!val) return "/"
                if(val.match(/\/$/)) return val
                var c = val.split("/")
                c.pop()
                return c.join("/") || "/"

             }


     }]
   }

}]);

app.directive('mcWebhostingPhpSelector', ["templateDir", function(templateDir){
      return {
        restrict: "E",
        templateUrl: templateDir(templateDirectory,'webhosting-php-selector'),
        controller:  showErrorsController({
          inject: ["mcWebhostingFactory",'$alertSuccess','mcConfig'],
          additionalInitializationCallback: function($scope, MonsterCloud, $q, mcWebhostingFactory, $alertSuccess, mcConfig){

             $scope.config = mcConfig.config;

             $scope.phpUserIniUrl = "http://php.net/manual/en/configuration.file.per-user.php";

             $scope.versionChange = function() {
                 $scope.supportedVersions.some(function(version){
                    if(version.version == $scope.mywebhosting.wh_php_version) {
                       $scope.showVersionWarning=version.deprecated
                       return true
                    }
                 })
             }

             MonsterCloud.get("/s/"+$scope.server+apiPrefix+"supported/php")
               .then(function(d){
                  $scope.supportedVersions = d
               })


             mcWebhostingFactory($scope.server, $scope.whId).Lookup().then(function(d){
                $scope.mywebhosting = d
             })
          },
          fireCallback: function($scope, MonsterCloud, $q, mcWebhostingFactory, $alertSuccess){

             var d = {wh_php_version: $scope.mywebhosting.wh_php_version, wh_id: $scope.whId}
             return MonsterCloud.post("/s/"+$scope.server+"/wizard/webhosting/php", d)
               .then(function(){
                  return $alertSuccess({message:'Version info submitted. Changes are activated in a few minutes.'})

               })


          }

        })
     }
}], {
   title: "Select PHP version",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@", "whId":"@"},
})


app.directive('mcWebhostingList', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-list'),
      scope: { },
      controller: ["$scope", "MonsterCloud", '$q', 'mcWebhostingFactory', "mcAccountWebhostings", function($scope, MonsterCloud, $q, mcWebhostingFactory, mcAccountWebhostings){
           $scope.webhostings = []


           $scope.refetch = function() {
               return mcAccountWebhostings()
                 .then(function(data){
                     var ps = []
                     if($scope.$parent)
                       $scope.$parent.mcWebhostingListCount = data.length;
                     data.forEach(function(row){
                         var p = mcWebhostingFactory(row.w_server_name, row.w_webhosting_id).Lookup()
                           .then(function(d){
                              d.w_server_name = row.w_server_name;
                              d.unique = row.w_server_name+"."+d.wh_id;
                              $scope.webhostings.push(d);
                              show();
                           })

                         ps.push(p)
                     })

                     return $q.all(ps)
                 })
                 .then(function(){
                    $scope.webhostings.sort(function(a,b){
                        if(a.unique < b.unique) return -1;
                        if(a.unique > b.unique) return 1;
                        return 0;
                    })
                    return show()
                 })

           }

           $scope.refetch()

           function show(){
               $scope.showListing = $scope.webhostings.length > 0
           }


       }]
   }
}], {
   title: "List of webhosting packages",
   menuKey: templateDirectory,
})

app.service("mcWebhostingFactory", ["MonsterCloud",'$q',function(MonsterCloud,$q){
  var inProgress = {}
  var cached = {}

  return function(server, wh_id, superUser) {
     var key = server+wh_id
     var url = "/s/"+server+(superUser?"/su":"")+apiPrefix+wh_id

     return {
       Change: function(changes, drop) {
          return MonsterCloud.post(url, changes)
            .then(function(d){
                if(cached[key]) {
                   if(!drop) {
                       Object.keys(changes).forEach(function(lkey){
                          cached[key][lkey] = changes[lkey]
                       })
                   }else{
                     console.log("deleting cached webhosting lookup key")
                     delete cached[key]
                   }
                }

                return $q.when(d)
            })

       },
       Reset: function(){
          delete cached[key];
       },
       Lookup: function() {
         if(cached[key]) return $q.when(cached[key])

         if(!inProgress[key]) {
           console.log("querying webhosting", wh_id)
           inProgress[key] = MonsterCloud.get(url)
             .then(function(d){
                 delete inProgress[key]
                 cached[key] = d
                 d.estimated_databases_count = Object.keys(d.wh_tally_db_storage||{}).length
                 return $q.when(d)
             })

         }

         return inProgress[key]
       }
     }
  }

}])

app.directive('mcWebhostingInfo', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-info'),
      transclude: true,
      link: function(scope, tElement, attrs, control, transclude) {
              transclude(scope, function (clone) {
                  if(!clone.length > 1)
                     return;
                  var target = tElement.find("[transclude-next]");

                  if(!clone[1]) return;

                  var source = angular.element(clone[1].innerHTML);
                  console.log("mcWebhostingInfo: element found?:", target, source)
                  source.insertAfter(target);
              });
      },      
      controller: ["$scope", "mcWebhostingFactory", "mcConfig", "mcReadonly", function($scope, mcWebhostingFactory, mcConfig, mcReadonly){
         $scope.rowsNotSupported = mcConfig.config.rowsNotSupported;
         return mcWebhostingFactory($scope.server, $scope.whId).Lookup()
           .then(function(d){
              $scope.webhosting = d;
              return mcReadonly($scope.server, $scope.whId).isReadonly()
           })
           .then(function(isReadonlyResult){
              $scope.is_readonly = isReadonlyResult.isReadonly;
           })
       }]
   }
}], {
   title: "Webhosting info",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@", "whId":"@" },
   paramsOptional: { "renameExtraCall":"@" },
})


app.directive('mcWebhosting', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting'),
      controller: ["$scope", "mcWebhostingFactory", function($scope, mcWebhostingFactory){
         return mcWebhostingFactory($scope.server, $scope.whId).Lookup()
           .then(function(d){
              $scope.webhosting = d
           })


       }]
   }
}], {
   title: "Webhosting PHP version and firewall",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@", "whId":"@" },
})

app.directive('mcWebhostingRestore', ["templateDir", function(templateDir){

    return {
      restrict: "E",
      templateUrl: templateDir(templateDirectory,'webhosting-restore'),
      controller:  showErrorsController({
          inject: ["mcReadonly","mcFilemanDelService","$task", "mcWebhostingSzarWebAdmin", "$alertError"],
          additionalInitializationCallback: function($scope, MonsterCloud, $q,mcReadonly, mcFilemanDelService, $task, mcWebhostingSzarWebAdmin){

              $scope.url = "/s/"+$scope.server+"/webhosting/"+$scope.whId+"/szar/"
              $scope.cat = $scope.category || "web"

              $scope.filesToDelete = []
              $scope.browseCallback = function(dir, files){
                 $scope.lastDir = dir
                 $scope.filesToDelete = files.map(function(x){ return x.fullPath; })
                 // console.log("browseCallback!", dir, files)
              }

              $scope.shouldRestoreButtonBeDisabled = function(){
                 return ((!$scope.lastDir)||($scope.lastDir == "/"))
              }

              $scope.szarWebAdmin = function(){
                 mcWebhostingSzarWebAdmin($scope.server, $scope.whId)
              }

              MonsterCloud.post($scope.url+"checkpoints/"+$scope.cat, {})
                .then(function(re){
                   $scope.checkpoints = re
                })

              var r = mcReadonly($scope.server, $scope.whId)
              r.isReadonly().then(function(result){
                $scope.readonly = result
                // console.log($scope.readonly)
              })

          },
          fireCallback: function($scope, MonsterCloud, $q, mcReadonly, mcFilemanDelService, $task, mcWebhostingSzarWebAdmin, $alertError){

            if($scope.shouldRestoreButtonBeDisabled()) return $alertError({message:"Please navigate first to the folder to be restored!"})

            var restoreUrl = $scope.url+"restore/"+$scope.cat

            var deleteFirst = $scope.filesToDelete.length > 0

            var taskUrlBase = "/s/"+$scope.server+"/"

            const title = "WEBHOSTING_RESTORATION_TASK"
            var d = {
               title: title,
               abortOnly: true,
               expectedRequests: (deleteFirst ? 2 : 1),
               urlFactory: function(index, appendText) {
                  console.log("urlFactory was called", index)

                  var aData = {
                     title: title
                  }

                  var p;
                  if((deleteFirst)&&(index == 0)) {
                    appendText("Removing files from the selected directory: "+$scope.lastDir)
                    aData.taskUrl = "fileman"
                    p = mcFilemanDelService($scope.server, $scope.whId).DelPromise($scope.filesToDelete)
                  }
                  else
                  if((!deleteFirst)||(index > 0)) {
                    appendText("Starting the restoration process: "+$scope.lastDir+" "+$scope.checkpoint)
                    aData.taskUrl = "webhosting"
                    p = MonsterCloud.post($scope.url+"restore/"+$scope.cat, {checkpoint:$scope.checkpoint, dir: $scope.lastDir})
                  }
                  else
                    throw new Error("Something is wrong with the restoration logic")

                  return p.then(function(h){
                       aData.url = taskUrlBase + aData.taskUrl + "/tasks/" + h.id;
                       return aData;
                    })

               }
            }

            return $task(d)
              .then(function(){
                  // we refresh the file listing via this broadcast:
                  $scope.$broadcast("refetch")
              })



          }
        })
   }
}], {
   title: "Restore webhosting contents from regular backup",
   menuKey: templateDirectory,
   paramsRequired: { "server":"@", "whId":"@" },
   paramsOptional: { "category":"@" },
   keywords: ["szar"],
})

})(app)
