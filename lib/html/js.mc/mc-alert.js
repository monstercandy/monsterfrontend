(function(app){

  const templateDirectory = "common"

  app.controller('AlertModalController', ["$scope", "$interval", "$uibModalInstance", "data", function ($scope, $interval, $uibModalInstance, data) {
    $scope.data = data.dontCopy ? data : angular.copy(data);

        if(data.lngParams) {
           $scope.lngPostProcess = "true"
           $scope = angular.extend($scope, data.lngParams)
        }
        else
          $scope.lngPostProcess = "false"

    var baseOk = $scope.okText = "Ok"

    $scope.ok = function () {
      $uibModalInstance.close();
    };
    $scope.cancel = function () {
      $uibModalInstance.dismiss();
    };

    if(data.timeout) {
      $interval(function(){
        data.timeout -= 1000
        var remainingSec = parseInt(data.timeout/1000)
        if(remainingSec <= 0)
          return $scope.ok()
        $scope.okText = baseOk + " ("+(remainingSec)+")"
      }, 1000)
    }


  }])
  .factory('$alert', ["$uibModal", 'templateDir', function ($uibModal, templateDir) {
    return function (data, settings) {
      var defaults = {
        templateUrl: templateDir(templateDirectory,'alert-dialog'),
        controller: 'AlertModalController',
      }

      settings = angular.extend(defaults, (settings || {}));

      if ('templateUrl' in settings && 'template' in settings) {
        delete settings.template;
      }

      settings.resolve = {
        data: function () {
          return data;
        }
      };

      return $uibModal.open(settings).result;
    };
  }])
  .factory('$alertSuccess', ['$alert', 'mcConfig', function ($alert, mcConfig) {
    return function(params){
       return $alert(angular.extend({timeout: mcConfig.config.alert_timeout_ms || 10000, alertType:'success', message: 'Operation completed successfully!', heading: 'Success'}, params))
    }
  }])
  .factory('$alertError', ['$alert', 'mcConfig', function ($alert, mcConfig) {
    return function(params){
       return $alert(angular.extend({timeout: mcConfig.config.alert_timeout_ms || 10000, alertType:'danger', message: 'There was an error while executing this operation.', heading: 'Error'}, params))
    }
  }])
  .directive("mcAlertBox",["templateDir", function(templateDir){

   return {
     restrict: "E",
     scope: {
      "showPermanent": "=",
      "showTimeout": "=",
      "params": "=",
      "alertType": "@",
      "message": "@",
      "heading": "@",
     },
     templateUrl: templateDir(templateDirectory,"alert-box"),

     controller: ["$scope","$timeout", 'mcConfig', '$window','$rootScope', function($scope,$timeout, mcConfig, $window, $rootScope){
        if(!$scope.alertType) $scope.alertType = "danger"

        if($scope.params) {
           $scope.lngPostProcess = "true"
           $scope = angular.extend($scope, $scope.params)
        }
        else
          $scope.lngPostProcess = "false"


     }]
   }
}])

})(app)
