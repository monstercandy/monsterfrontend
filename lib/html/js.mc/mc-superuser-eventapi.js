(function(app){
  var templateDirectory = "eventapi-superuser"
  var apiPrefix = "/su/eventapi"

app.directive("mcEventapiConfigEditor", ["templateDir", function(templateDir){
  return {
    "template": "<mc-superuser-simple-config-editor show-config='true' url='/s/{{server}}/su/editor/eventapi'></mc-superuser-simple-config-editor>",
  }
}], {
    title: "EventApi microservice config editor",
    paramsRequired: {server: "@"},
    menuKey: templateDirectory,
})

app.directive("mcEventlogSuperuser", ["templateDir", function(templateDir){
  return {
    "templateUrl": templateDir(templateDirectory,"eventlog"),
    "controller": ["$scope", "MonsterCloud", 'pagerFactory', '$q', function($scope, MonsterCloud, pagerFactory, $q){

        // $scope.subsystems = [] // it has one time prefix in the template so we should not initalize here!
        // $scope.eventtypes = [] // it has one time prefix in the template so we should not initalize here!

        $scope.successfullness = ["","0","1"]

        var baseUrl = "/s/"+$scope.server+apiPrefix


        $scope.refetch = function(tableState){

           $scope.displayedCollection = null;
           var d = queryDetails(tableState);
           return MonsterCloud.post( baseUrl+"/events/count", d)
             .then(function(data){
                tableState.pagination.totalItemCount = data.count;

                var p = angular.extend(d, tableState.pagination.params);
                return MonsterCloud.post( baseUrl+"/events/search", d)
             })             
             .then(function(data){
                 $scope.displayedCollection = data.events;
             })          
        }


       return MonsterCloud.get( baseUrl+"/config/subsystems")
         .then(function(data){
             $scope.subsystems = Array("").concat(data)
             return MonsterCloud.get( baseUrl+"/config/eventtypes")
         })
         .then(function(data){
             $scope.eventtypes = data
         })


        function queryDetails(tableState) {
          var d = {where: tableState.search.str};
          Array("notbefore","notafter","e_ip","e_subsystem", "e_event_type", "e_other", "e_username").forEach(function(v){
            if(($scope[v] !== "")&&($scope[v] !== null))
               d[v] = $scope[v]
          })
          return d
        }

    }]
  }
}], {
   title: "Global event log",
   menuKey: templateDirectory,
   paramsRequired: { "server": "@" },

})


})(app)
