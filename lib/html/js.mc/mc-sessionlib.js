angular.module("mc-sessionlib", ["ngCookies"])//
  .service("mcSessionCookies", ["$cookies", "$location", function($cookies, $location){
   var prefix = "mc_";
   var prefixMatcher = new RegExp("^"+prefix);
   var forcedIndex = -1;
   var lib = {
      // returns a cookie object for the newly added token
      add: function(data){
        if((!data)||(!data.token)) {
           throw new Error("Token must be present");
        }
        var o = lib.getByToken(data.token);
        if(!o) {
          var newIndex = findNextIndex();
          console.log("next Index", newIndex);
          o = toObject(newIndex, data);
        }
        o.change(data);
        return o;
      },
      AddTokenHash: function(data){ // compatitibilty function
        var n = lib.add(data);
        return n.uidi;
      },
      clearAll: function(){
        var uidis = getAllUidis();
        uidis.forEach(function(uidi){
          $cookies.remove(prefix+uidi);
        })
      },
      getByIndex: function(index){
         var n = getRaw(index);
         console.log("mcCookies getByIndex", index, n)
         return getObjectOrNull(index, n);
      },
      getByToken: function(token){
         var uidis = getAllUidis();
         var foundData;
         var foundUidi;
         uidis.some(function(uidi){
          var d = getRaw(uidi);
          if(d.token == token) {
            foundData = d;
            foundUidi = uidi;
            return true;
          }
         });

         return getObjectOrNull(foundUidi, foundData);
      },
      forceIndex: function(index){
         forceIndex = index;
      },
      removeCurrent: function(){
        var n = lib.getCurrent();
        if(n) n.remove();
      },
      getCurrent: function(){
         return lib.getByIndex(lib.getEffectiveUidi());
      },
      GetCurrentTokenHash: function(){ // compatibility function
        return lib.getCurrent();
      },
      getUidi: function(){
         return forcedIndex != -1 ? forcedIndex : getUidiFromQueryString();
      },
      getEffectiveUidi: function(){
         return lib.getUidi() || 0;
      },

      // this is a compatibility function which returns the uidi: data mappings
      GetTokenArray: function(){
         var uidis = getAllUidis();
         var re = [];
         uidis.forEach(function(uidi){
          re.push(lib.getByIndex(uidi));
         })
         return re;
      }
   }
   return lib;

   function qs(key){
      var s = $location.search()
      return s[key]
   }
   function getUidiFromQueryString(){
      var re = qs("uidi");
      // workaround for broken urls like http://monster/index.html?uidi=0&uidi=0&op=ng
      if((Array)&&(Array.isArray)&&(Array.isArray(re))) re = re[0];
      return re;
   }
   function getObjectOrNull(index, d){
         if(!d) return;
         return toObject(index, d);     
   }

   function getRaw(index){
      return $cookies.getObject(prefix+index);
   }

   function toObject(index, stuff){
      stuff.uidi = index;
      stuff.remove = function(){
         $cookies.remove(prefix+stuff.uidi);
      }
      stuff.change = function(newData){
         angular.extend(stuff, newData);
         putRawCookie(stuff.uidi, stuff);
      }

      return stuff;
   }

   function putRawCookie(index, object){
      var clone = angular.copy(object);
      delete clone.uidi;
      $cookies.putObject(prefix+index, clone, {samesite: "strict"});
   }

   // returns the biggest uidi or -1 if there are none
   function getAllUidis(){
      var cookieNames = Object.keys($cookies.getAll());
      var uidis = [];
      cookieNames.forEach(function(name){
         if(!name.match(prefixMatcher)) return;
         console.log("getAllUidis", name);
         var index = name.substr(3);
         uidis.push(index);
      })
      return uidis;
   }
   function findBiggestIndex(){
      var uidis = getAllUidis();
      console.log("all uidis", uidis);
      if(!uidis.length) return -1;
      return Math.max.apply(null, uidis);
   }
   function findNextIndex(){
     return findBiggestIndex()+1;
   }
}])
