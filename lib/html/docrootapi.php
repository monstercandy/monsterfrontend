<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

     <a href mc-sref="mc-docroot-vts-list" mc-sref-params="{server:'s1',whId:'22001'}">vts list</a>

     <a href mc-sref="mc-docroot-webhosting-docroots" mc-sref-params="{server:'s1',whId:'12345',domain:'femforgacs.hu',display:'fémforgács.hu'}">docroots</a>

     <a href mc-sref="mc-docroot-webhosting-protected-dirs-and-accounts" mc-sref-params="{server:'s1',whId:'12345',domain:'femforgacs.hu',display:'fémforgács.hu','showWarningAboutDomains':true}">protected dirs</a>


     <a href mc-sref="mc-docroot-redirects" mc-sref-params="{server:'s1',domain:'femforgacs.hu',display:'fémforgács.hu','showRedirectVsDocrootAlert':true}">redirects (http)</a>

     <a href mc-sref="mc-docroot-frames" mc-sref-params="{server:'s1',domain:'femforgacs.hu',display:'fémforgács.hu','showRedirectVsDocrootAlert':false}">redirects (frame)</a>

     <a href mc-sref="mc-docroot-domains-of-webstore" mc-sref-params="{server:'s1',whId:'12345'}">list of virtual hosts</a>
     

     <div mc-view>

       <!-- this includes a protected dir control as well, we dont use it anymore
      <mc-docroot-webhosting server="s1" display="fémforgács.hu" domain="femforgacs.hu"  webhosting="12345" ></mc-docroot-webhosting>
       -->       
       
     </div>


      <!-- misc controls: -->
      <h3>misc controls</h3>
      <form method="post" novalidate onsubmit="return false">
            <mc-docroot-hostentries server="s1" wh-id="12345" model="abcd"></mc-docroot-hostentries>

            <div>Current: <mc-docroot-current server="s1" wh-id="12345" domain="femforgacs.hu" ></mc-docroot-current></div>

            <div>Distinct docroot selector: <mc-docroot-distinct-selectbox server="s1" wh-id="12345" ></mc-docroot-distinct-selectbox></div>

            <li mc-awstats-link server="s1" wh-id="12345" domain="femforgacs.hu" link="/index.php?something=1"></li>

            <mc-docroot-settings server="s1" wh-id="12345" domain="femforgacs.hu" awstats-link="/index.php?something=1"></mc-docroot-settings>


            <input type="submit">
      </form>
      <!-- 
      this one is not needed anymore as there is a dedicated control that works via fileman api
      <mc-docroot-distinct-remove remove-url-prefix="/index.php?op=tha-torles&amp;uidi=0&amp;th=16420&amp;to_path=" server="s1" webhosting="12345" ></mc-docroot-distinct-remove>
      -->


  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
