<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

     <a href mc-sref="mc-fileman-site-backup" mc-sref-params="{server:'s1',whId:'12345'}">site backup</a>
     <a href mc-sref="mc-fileman-site-del" mc-sref-params="{server:'s1',whId:'12345'}">site del</a>
     <a href mc-sref="mc-webhosting-fileman" mc-sref-params="{server:'s1',whId:'12345','path':'/'}">fileman wh</a>
     <a href mc-sref="mc-cron-list-and-add" mc-sref-params="{server:'s1',whId:'12345'}">cron</a>

     <a href mc-sref="mc-fileman-inter-store-copy-wizard" >inter-store file copy wizard</a>

    <div mc-view>
    </div>


<form name="dataForm1">
        <mc-dirlist-selectbox title="Dbbackup" form="dataForm1" model="pl" model-name="docroot" server="s1" wh-id="12345" directory="/dbbackup"></mc-dirlist-selectbox>
</form>


<form name="dataForm2">
        <mc-directory-picker title="Document root" form="dataForm2" model="pl" model-name="docroot" server="s1" wh-id="12345" placeholder="placeholder" default="/"></mc-directory-picker>
</form>


            <table>
            <tr mc-readonly-row server="s1" wh-id="12345"></tr>
            </table>


  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
