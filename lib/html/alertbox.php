<!DOCTYPE html>
<html lang="en">
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body ng-app="app" class="padding-small">

Current: {{ foo }}

  <button ng-click="foo=!foo">Toggle </button> <button ng-click="foo=true">Turn on</button> <button ng-controller="test" ng-click="cl(true)">broadcast true</button>  <button ng-controller="test" ng-click="cl(false)">broadcast false</button> 
    
  <mc-alert-box alert-type="success" message="Operation completed successfully!" heading="Success" ></mc-alert-box>
	<mc-alert-box message="Operation completed successfully!" heading="Success" ></mc-alert-box>

  <button ng-controller="test" ng-click="serviceSuccess()">success</button>
  <button ng-controller="test" ng-click="serviceError()">error</button>

  <?include("js.mc/loader.php");?>

  <script>
  app.controller("test", function($rootScope, $scope, $alertSuccess, $alertError){
    $scope.serviceSuccess=function(){
       $alertSuccess()
    }
    $scope.serviceError=function(){
       $alertError()
    }

})
</script>
    
</body>
</html>
