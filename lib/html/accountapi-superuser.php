<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>
      <a href mc-sref="mc-superuser-account-servers">servers</a>

      <a href mc-sref="mc-superuser-webhosting-templates">templates</a>

      <a href mc-sref="mc-superuser-account-list">superuser accounts</a>

      <a href mc-sref="mc-loginlog-superuser">loginlog</a>

      <div mc-view></div>

          <a href="/foobar.php"  mc-switch-user-superuser="{{user.u_id}}">switch user and override the jump target</a>

  </div>

  <?include("js.mc/loader.php");?>

    
</body>
</html>
