<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div ng-hide="::lngReady">Loading...</div>
  <div>

     <a href mc-sref="mc-dbms-restore-from-fileman" mc-sref-params="{server:'s1',whId:'12345'}">restore from fileman</a>  

     <a href mc-sref="mc-dbms-databases" mc-sref-params="{server:'s1',whId:'12345'}">databases</a>  

	    <div mc-view>

	    </div>

  </div>

  
  <?include("js.mc/loader.php");?>


    
</body>
</html>
