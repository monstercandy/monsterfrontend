<!DOCTYPE html>
<html lang="en">
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body ng-app="app" class="padding-small">

    <mc-tos class="hidden" redirect-on-failure="/something-else.php">
     <div>Felhívjuk rá a figyelmét, hogy az Anonim Domainregisztráció szolgáltatás nem kíván semmilyen illegális tevékenységet támogatni.</div>
     <div>Hatósági megkeresés esetén a regisztrációval kapcsolatos összes információt a rendelkezésükre bocsájtjuk.</div>
     <br>
     <div><a href="/index.php?op=aszf" target="_blank">Általános Szerződési Feltételek</a>,
          <a href="/index.php?op=aukcio-szabalyzat" target="_blank">Domain aukciós szabályzat</a></div>
  </mc-tos>

    <mc-registration-or-show-account uf-tos="true" redirect-after="index3.php!"></mc-registration-or-show-account>


  <?include("js.mc/loader.php");?>

    
</body>
</html>
