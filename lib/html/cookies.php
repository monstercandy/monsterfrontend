<!DOCTYPE html>
<html lang="hu" ng-app="app" >
<head>
	<title>AccountAPI via RelayAPI test</title>
	<link rel="stylesheet" href="css.mc/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


</head>
<body class="padding-small">
  <div>

    <mc-cookie>
    	<pre>{{current|json}}</pre>
    	<button ng-click="mcAdd()">Add</button>
    	<button ng-click="mcRemove()">Remove current</button>
    </mc-cookie>

    <br/><br/>

    <raw-cookie>
    	<pre>{{cookieKeys|json}}</pre>
    	<button ng-click="rawAdd()">Add</button>
    	<button ng-click="rawRemove()">Remove all</button>
    </raw-cookie>

  </div>

  <?include("js.mc/loader.php");?>

<script>  

app.directive('mcCookie', function() {
  return {
    controller: ["mcSessionCookies", "$scope", function(mcSessionCookies, $scope) {

        var i = 0;
    	$scope.mcRemove = function(){
    		console.log("mc remove?")
    		$scope.current.remove();
    	}
    	$scope.mcAdd = function(){
    		console.log("mc add?")
    		i++;
    		var data = {some: i, token: "token"+i};
    		$scope.current = mcSessionCookies.add(data);
    	}
    	$scope.current = mcSessionCookies.getCurrent();

    }]
  };
})

app.directive('rawCookie', function() {
  return {
    controller: ["$cookies", "$scope", function($cookies, $scope) {
    	function re(){
         	$scope.cookieKeys = Object.keys($cookies.getAll());    		
    	}

    	var i = 0;
    	$scope.rawRemove = function(){
    		console.log("raw remove");
        var names = Object.keys($cookies.getAll());
    		names.forEach(function(name){
          console.log("name", name);
          if(!name.match(/^mc_/)) return;
    			$cookies.remove(name);
    		});
    		re();
    	}
    	$scope.rawAdd = function(){
    		console.log("raw add");
    		i++;
    		$cookies.putObject("mc_"+i, {"foo": i}, {samesite: "lax"})
    		re();
    	}

    	re();
    }]
  };
})

</script>
    
</body>
</html>
