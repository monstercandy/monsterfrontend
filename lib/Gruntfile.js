module.exports = function(grunt) {

    var base_dir = "/var/lib/monster/frontend/";
    var tmp_dir = base_dir+"html.tmp/"
    var dest_dir = base_dir+"html.release/"

    var js_dir_name = "js.mc"
        var js_dir_src = "html/"+js_dir_name+"/"

    var css_dir_name = "css.mc"
        var css_dir_src = "html/"+css_dir_name+"/"

        var loader_php = dest_dir+'js.mc/loader.php'; // this is still in use by monster
        var index_html = dest_dir+'index.html';


    var now = Date.now()
        var footer_script_line = '<script src="site.mc/config.js"></script><script src="'+js_dir_name+'/official.min.js"></script><script src="'+js_dir_name+'/mc.min.js"></script><script src="'+js_dir_name+'/mc-superuser.min.js"></script><link rel="stylesheet" href="'+css_dir_name+'/mc.min.css"><link rel="stylesheet" href="'+css_dir_name+'/official-extra.min.css">'


    var config = {
    pkg: grunt.file.readJSON('package.json'),

        clean: {
                release: [dest_dir],
                tmp: [tmp_dir],
        },

        cssmin: {
          options: {
                shorthandCompacting: false,
                roundingPrecision: -1
          },
          target: {
                files: {
                  '/var/lib/monster/frontend/html.tmp/passfield.min.css': [css_dir_src+'passfield.css'],

                  // appended below!
                }
          }
        },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        sourceMap: true,
                sourceMapIncludeSources: true,
                sourceMapName: function(p){
                        var path = require("path")
                        var dest = dest_dir+"_private/"+path.basename(p)+".map"
                        // console.log("FOOOO", p, dest); process.process.reallyExit();
                        return dest
                }
      },
          official: {
                 src: [
                   js_dir_src+'showErrors.forked.js',
                   js_dir_src+'angular-cookies.js',
                   js_dir_src+'smart-table.js', // this one has been patched, so we do the minifying ourselves!
                   js_dir_src+'Filesaver.js',
                   js_dir_src+'mc-confirm.js',

                   js_dir_src+'passfield.js',
                   js_dir_src+'passfield-locales.js',

                 ],
                 dest: tmp_dir+'official-extra.min.js',
          },

      pub: {
                 src: [
                   tmp_dir+'mc-sessionlib.js',
                   tmp_dir+'mc-lng.js',
                   tmp_dir+'mc-config.js',
                   tmp_dir+'mc-routes.js',
                   tmp_dir+'mc-script.js',

                   tmp_dir+'mc-common.js',
                   tmp_dir+'mc-task.js',
                   tmp_dir+'mc-file-editor.js',
                   tmp_dir+'mc-tos.js',
                   tmp_dir+'mc-alert.js',
                   tmp_dir+'mc-prompt.js',
                   tmp_dir+'mc-account.js',
                   tmp_dir+'mc-git.js',
                   tmp_dir+'mc-tapi.js',
                   tmp_dir+'mc-docrootapi.js',
                   tmp_dir+'mc-dbms.js',
                   tmp_dir+'mc-fileman.js',
                   tmp_dir+'mc-iptables.js',
                   tmp_dir+'mc-webhosting.js',
                   tmp_dir+'mc-ftp.js',
                   tmp_dir+'mc-email.js',
                   tmp_dir+'mc-email-public.js',
                   tmp_dir+'mc-installatron.js',
                   tmp_dir+'mc-docker.js',
                   tmp_dir+'mc-cert.js',
                 ],
                 dest: dest_dir+'js.mc/mc.min.js',
          },
          superuser: {
            src: [
                  tmp_dir+'mc-superuser-common.js',
                  tmp_dir+'mc-superuser-account.js',
                  tmp_dir+'mc-superuser-git.js',
                  tmp_dir+'mc-superuser-eventapi.js',
                  tmp_dir+'mc-superuser-tapi.js',
                  tmp_dir+'mc-superuser-docrootapi.js',
                  tmp_dir+'mc-superuser-dbms.js',
                  tmp_dir+'mc-superuser-fileman.js',
                  tmp_dir+'mc-superuser-iptables.js',
                  tmp_dir+'mc-superuser-webhosting.js',
                  tmp_dir+'mc-superuser-ftp.js',
                  tmp_dir+'mc-superuser-email.js',
                  tmp_dir+'mc-superuser-installatron.js',
                  tmp_dir+'mc-superuser-docker.js',
                  tmp_dir+'mc-superuser-test.js',
                  tmp_dir+'mc-superuser-cert.js',
                ],
                dest: dest_dir+js_dir_name+"/mc-superuser.min.js",
          }

    },

        concat: {
                dist_js: {
                  options: {
                          separator: ";\n",
                  },
                  src: [
                    js_dir_src+'fuse.min.js',
                    js_dir_src+'jquery.min.js',
                        js_dir_src+'bootstrap.min.js',
                    js_dir_src+'angular.min.js',
                        js_dir_src+'ui-bootstrap-tpls-2.0.1.min.js',
                        tmp_dir+'official-extra.min.js',
                        js_dir_src+'sb-admin-2.min.js',
                        js_dir_src+'qrcode.min.js',
                  ],
                  dest: dest_dir+''+js_dir_name+'/official.min.js',
                },
                dist_css: {
                  src: [
                    tmp_dir+'passfield.min.css',
                    css_dir_src+ 'metisMenu.min.css',
                        css_dir_src+ 'sb-admin-2.min.css',               // ------------- if you ever update this script, dont forget to strip the last semicolon (;)
                        css_dir_src+ 'fontawesome-all.min.css',

                  ],
                  dest: dest_dir+''+css_dir_name+'/official-extra.min.css',
                },

          },



                copy: {
                  js_to_tmp: {
                        files: [
                          {expand: true, cwd: js_dir_src, src: [
                                'mc-sessionlib.js',
                                'mc-lng.js',
                                'mc-config.js',
                                'mc-routes.js',
                                'mc-script.js',

                                'mc-common.js',
                                'mc-task.js',
                                'mc-file-editor.js',
                                'mc-tos.js',
                                'mc-alert.js',
                                'mc-prompt.js',
                                'mc-account.js',
                                'mc-iptables.js',
                                'mc-webhosting.js',
                                'mc-ftp.js',
                                'mc-email.js',
                                'mc-email-public.js',
                                'mc-installatron.js',
                                'mc-docker.js',
                                'mc-cert.js',
                                'mc-tapi.js',
                                'mc-git.js',
                                'mc-docrootapi.js',
                                'mc-dbms.js',
                                'mc-fileman.js',
                                'mc-superuser-common.js',
                                'mc-superuser-account.js',
                                'mc-superuser-iptables.js',
                                'mc-superuser-webhosting.js',
                                'mc-superuser-eventapi.js',
                                'mc-superuser-ftp.js',
                                'mc-superuser-email.js',
                                'mc-superuser-cert.js',
                                'mc-superuser-installatron.js',
                                'mc-superuser-docker.js',
                                'mc-superuser-test.js',
                                'mc-superuser-tapi.js',
                                'mc-superuser-docrootapi.js',
                                'mc-superuser-git.js',
                                'mc-superuser-dbms.js',
                                'mc-superuser-fileman.js',
                          ],
                          dest: tmp_dir+''},
                        ],

                  },

                  rest: {
                        files: [
                          // includes files within path
                          {
                            expand: true, cwd: "html",
                            src: [
                               css_dir_name+'/bootstrap.min.css',
                               'fonts/**',
                               'webfonts/**',
                               'languages.mc/**',
                               'js/**',
                               'img.mc/**',
                               'template.mc/**',
                               'index.html',
                               js_dir_name+'/*.php' ],
                            dest: dest_dir+''
                          },
                        ],
                  },
                },

          removelogging: {
                dist: {
                  src: tmp_dir+"*.js" // Each file will be overwritten with the output!
                }
          },

          removeLoggingCalls: {
                  files: [tmp_dir+"*.js"],
          },

          replace: {


              jsproduction: {
                        src: [tmp_dir+'mc-common.js'],
                        overwrite: true,
                    replacements: [
                      {
                                  from: "//ENABLE THIS ONE IN PRODUCTION",
                                  to: "",
                          }     ,
              {
                                  from: "__PRODUCTION_BUILDDATE__",
                                  to: "ts="+now,
                          }

                        ]
                  },

                  html: {
                        src: [loader_php, index_html],             // dest_dir+'*.php',
                        overwrite: true,
                        replacements: [
                          {
                                  from: /<!-- footer replace from here -->[\s\S]+<!-- footer replace to here -->/,                   // string replacement
                                  to: footer_script_line,
                          },

                          {
                                  from: 'href="',
                                  to: 'href="/',
                          },
                          {
                                  from: 'src="',
                                  to: 'src="/',
                          },
                          {
                                  from: '.js"',
                                  to: '.js?ts='+now+'"',
                          },
                          {
                                  from: '.css"',
                                  to: '.css?ts='+now+'"',
                          },
                        ]
                  },


                },

  }

  config.cssmin.target.files[dest_dir+css_dir_name+'/mc.min.css'] = [css_dir_src+'style.css'], // this is added here because we cant use the constants in the key constructor


  grunt.initConfig(config);


  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-text-replace');

  // grunt.loadNpmTasks("grunt-remove-logging"); // ,'removelogging'
  grunt.loadNpmTasks('grunt-remove-logging-calls');

//
  grunt.registerTask('default', ['clean','cssmin','copy:js_to_tmp','replace:jsproduction' ,'removeLoggingCalls'  ,'uglify','concat','copy:rest','replace:html']);//, 'clean:tmp' // we dont clean the temp directory else we wont be able to identify the original line of error

};
